﻿using API_JEEJOB.DBEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_JEEJOB.Models
{
    public class ThanhPhoDTO
    {
        public ThanhPhoDTO()
        {
            Diadiemlamviec = new HashSet<Diadiemlamviec>();
            Noilamviecmongmuon = new HashSet<Noilamviecmongmuon>();
            Ungvien = new HashSet<Ungvien>();
        }

        public int IdThanhpho { get; set; }
        public string Tenthanhpho { get; set; }
        public int Soluongtin { get; set; }

        public virtual ICollection<Diadiemlamviec> Diadiemlamviec { get; set; }
        public virtual ICollection<Noilamviecmongmuon> Noilamviecmongmuon { get; set; }
        public virtual ICollection<Ungvien> Ungvien { get; set; }
    }

    public class NganhDTO
    {
        public NganhDTO()
        {
            Diadiemlamviec = new HashSet<Diadiemlamviec>();
            Noilamviecmongmuon = new HashSet<Noilamviecmongmuon>();
            Ungvien = new HashSet<Ungvien>();
        }

        public int IdNganh { get; set; }
        public string Tennganh{ get; set; }
        public int Soluongtin { get; set; }

        public virtual ICollection<Diadiemlamviec> Diadiemlamviec { get; set; }
        public virtual ICollection<Noilamviecmongmuon> Noilamviecmongmuon { get; set; }
        public virtual ICollection<Ungvien> Ungvien { get; set; }
    }
}
