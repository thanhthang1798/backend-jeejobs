﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_JEEJOB.Models
{
    public class UserSocial
    {
        public string name { get; set; }
        public string email { get; set; }
        public string provider { get; set; }
        public string provideid { get; set; }
        public string photoUrl { get; set; }
        public string token { get; set; }
        public string idToken { get; set; }
    }
}
