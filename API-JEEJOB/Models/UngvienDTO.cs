﻿using API_JEEJOB.DBEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_JEEJOB.Models
{
    public class UngvienDTO
    {
        public UngvienDTO()
        {
            Bangcap = new HashSet<Bangcap>();
            Congviecmongmuon = new HashSet<Congviecmongmuon>();
            Kinhnghiemlamviec = new HashSet<Kinhnghiemlamviec>();
            Loaicongviecungvien = new HashSet<Loaicongviecungvien>();
            Luucongviec = new HashSet<Luucongviec>();
            Luuungvien = new HashSet<Luuungvien>();
            Ngoainguungvien = new HashSet<Ngoainguungvien>();
            Thongbaocongviec = new HashSet<Thongbaocongviec>();
            Ungtuyen = new HashSet<Ungtuyen>();
        }

        public int IdUngvien { get; set; }
        public string Email { get; set; }
        public string Matkhau { get; set; }
        public string Hinhanh { get; set; }
        public string Ho { get; set; }
        public string Ten { get; set; }
        public string Diachi { get; set; }
        public int? Gioitinh { get; set; }
        public bool? Docthan { get; set; }
        public DateTime? Ngaysinh { get; set; }
        public string Sdt { get; set; }
        public DateTime? Ngaytao { get; set; }
        public int? IdThanhpho { get; set; }
        public string UrlCv { get; set; }
        public decimal? Luong { get; set; }
        public int? Kinhnghiem { get; set; }
        public string Mota { get; set; }
        public bool? Isdelete { get; set; }
        public string Contentdelete { get; set; }
        public string Trangthai { get; set; }
        public int? Luotxem { get; set; }
        public int? Nganhmongmuon { get; set; }
        public string Capbacmongmuon { get; set; }
        public string Tenthanhpho { get; set; }
        public int? DataSort { get; set; }
        public bool? daluu { get; set; }
        public string Thoigianlamviecmongmuon { get; set; }
        public string Muctieunghenghiep { get; set; }
        public string Vitriungtuyen { get; set; }
        public bool? Sociallogin { get; set; }

        public virtual Thanhpho IdThanhphoNavigation { get; set; }
        public virtual ICollection<Bangcap> Bangcap { get; set; }
        public virtual ICollection<Congviecmongmuon> Congviecmongmuon { get; set; }
        public virtual ICollection<Kinhnghiemlamviec> Kinhnghiemlamviec { get; set; }
        public virtual ICollection<Loaicongviecungvien> Loaicongviecungvien { get; set; }
        public virtual ICollection<Luucongviec> Luucongviec { get; set; }
        public virtual ICollection<Luuungvien> Luuungvien { get; set; }
        public virtual ICollection<Ngoainguungvien> Ngoainguungvien { get; set; }
        public virtual ICollection<Thongbaocongviec> Thongbaocongviec { get; set; }
        public virtual ICollection<Ungtuyen> Ungtuyen { get; set; }
    }
}
