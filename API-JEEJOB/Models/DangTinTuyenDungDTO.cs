﻿using API_JEEJOB.DBEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_JEEJOB.Models
{
    public class DangTinTuyenDungDTO
    {
        public DangTinTuyenDungDTO()
        {
            Luucongviec = new HashSet<Luucongviec>();
            Nganhtuyendung = new HashSet<Nganhtuyendung>();
            Phucloituyendung = new HashSet<Phucloituyendung>();
            Trinhdotuyendung = new HashSet<Trinhdotuyendung>();
            Ungtuyen = new HashSet<Ungtuyen>();
        }
        public int IdTintuyendung { get; set; }
        public string Tieude { get; set; }
        public string Chucdanh { get; set; }
        public string Capbac { get; set; }
        public int? Soluong { get; set; }
        public string Motacongviec { get; set; }
        public string Yeucaucongviec { get; set; }
        public string Yeucaukynang { get; set; }
        public decimal? Minluong { get; set; }
        public decimal? Maxluong { get; set; }
        public bool? Thuongluong { get; set; }
        public DateTime? Ngaydang { get; set; }
        public DateTime? Ngayhethan { get; set; }
        public int? Gioitinh { get; set; }
        public int? IdDiadiemlamviec { get; set; }
        public int? IdNhatuyendung { get; set; }
        public int? IdLoaicongviec { get; set; }
        public int? Kinhnghiem { get; set; }
        public string Emaillienhe { get; set; }
        public string Sdtlienhe { get; set; }
        public string Nguoilienhe { get; set; }
        public bool? Isdelete { get; set; }
        public string Contentdelete { get; set; }
        public string Trangthai { get; set; }
        public int? Douutien { get; set; }
        public int? Luotxem { get; set; }
        public int? Datasort { get; set; }

        public string Logo { get; set; }
        public string Tencongty { get; set; }
        public string Tenthanhpho { get; set; }
        public int IdThanhpho { get; set; }

        public virtual Diadiemlamviec IdDiadiemlamviecNavigation { get; set; }
        public virtual Loaicongviec IdLoaicongviecNavigation { get; set; }
        public virtual Nhatuyendung IdNhatuyendungNavigation { get; set; }
        public virtual ICollection<Luucongviec> Luucongviec { get; set; }
        public virtual ICollection<Nganhtuyendung> Nganhtuyendung { get; set; }
        public virtual ICollection<Phucloituyendung> Phucloituyendung { get; set; }
        public virtual ICollection<Trinhdotuyendung> Trinhdotuyendung { get; set; }
        public virtual ICollection<Ungtuyen> Ungtuyen { get; set; }

    }
}
