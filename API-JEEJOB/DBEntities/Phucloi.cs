﻿using System;
using System.Collections.Generic;

namespace API_JEEJOB.DBEntities
{
    public partial class Phucloi
    {
        public Phucloi()
        {
            Phucloicongviecmongmuon = new HashSet<Phucloicongviecmongmuon>();
            Phucloituyendung = new HashSet<Phucloituyendung>();
        }

        public int IdPhucloi { get; set; }
        public string Tenphucloi { get; set; }

        public virtual ICollection<Phucloicongviecmongmuon> Phucloicongviecmongmuon { get; set; }
        public virtual ICollection<Phucloituyendung> Phucloituyendung { get; set; }
    }
}
