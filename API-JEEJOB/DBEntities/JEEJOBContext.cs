﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace API_JEEJOB.DBEntities
{
    public partial class JEEJOBContext : DbContext
    {
        public JEEJOBContext(DbContextOptions<JEEJOBContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Bangcap> Bangcap { get; set; }
        public virtual DbSet<Congviecmongmuon> Congviecmongmuon { get; set; }
        public virtual DbSet<Dangtintuyendung> Dangtintuyendung { get; set; }
        public virtual DbSet<Diadiemlamviec> Diadiemlamviec { get; set; }
        public virtual DbSet<Kinhnghiemlamviec> Kinhnghiemlamviec { get; set; }
        public virtual DbSet<Kynangungvien> Kynangungvien { get; set; }
        public virtual DbSet<Loaicongviec> Loaicongviec { get; set; }
        public virtual DbSet<Loaicongviecungvien> Loaicongviecungvien { get; set; }
        public virtual DbSet<Luucongviec> Luucongviec { get; set; }
        public virtual DbSet<Luuungvien> Luuungvien { get; set; }
        public virtual DbSet<Nganh> Nganh { get; set; }
        public virtual DbSet<Nganhnghemongmuon> Nganhnghemongmuon { get; set; }
        public virtual DbSet<Nganhtuyendung> Nganhtuyendung { get; set; }
        public virtual DbSet<Nganhungvien> Nganhungvien { get; set; }
        public virtual DbSet<Ngoaingu> Ngoaingu { get; set; }
        public virtual DbSet<Ngoainguungvien> Ngoainguungvien { get; set; }
        public virtual DbSet<Nhatuyendung> Nhatuyendung { get; set; }
        public virtual DbSet<Noilamviecmongmuon> Noilamviecmongmuon { get; set; }
        public virtual DbSet<Phucloi> Phucloi { get; set; }
        public virtual DbSet<Phucloicongviecmongmuon> Phucloicongviecmongmuon { get; set; }
        public virtual DbSet<Phucloituyendung> Phucloituyendung { get; set; }
        public virtual DbSet<Quantrivien> Quantrivien { get; set; }
        public virtual DbSet<Thanhpho> Thanhpho { get; set; }
        public virtual DbSet<Thanhphoungvien> Thanhphoungvien { get; set; }
        public virtual DbSet<Trinhdo> Trinhdo { get; set; }
        public virtual DbSet<Trinhdotuyendung> Trinhdotuyendung { get; set; }
        public virtual DbSet<Ungtuyen> Ungtuyen { get; set; }
        public virtual DbSet<Ungvien> Ungvien { get; set; }
        public virtual DbSet<VwRand> VwRand { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server=localhost, 1433;Database=JEEJOB;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Bangcap>(entity =>
            {
                entity.HasKey(e => e.IdBangcap);

                entity.ToTable("BANGCAP");

                entity.Property(e => e.IdBangcap).HasColumnName("ID_BANGCAP");

                entity.Property(e => e.Chuyennganh)
                    .HasColumnName("CHUYENNGANH")
                    .HasMaxLength(50);

                entity.Property(e => e.Ghichu)
                    .HasColumnName("GHICHU")
                    .HasColumnType("ntext");

                entity.Property(e => e.IdTrinhdo).HasColumnName("ID_TRINHDO");

                entity.Property(e => e.IdUngvien).HasColumnName("ID_UNGVIEN");

                entity.Property(e => e.Ngaybatdau)
                    .HasColumnName("NGAYBATDAU")
                    .HasColumnType("date");

                entity.Property(e => e.Ngayketthuc)
                    .HasColumnName("NGAYKETTHUC")
                    .HasColumnType("date");

                entity.Property(e => e.Tenbangcap)
                    .HasColumnName("TENBANGCAP")
                    .HasMaxLength(50);

                entity.Property(e => e.Tentruong)
                    .HasColumnName("TENTRUONG")
                    .HasMaxLength(200);

                entity.HasOne(d => d.IdTrinhdoNavigation)
                    .WithMany(p => p.Bangcap)
                    .HasForeignKey(d => d.IdTrinhdo)
                    .HasConstraintName("FK_BANGCAP_TRINHDO");

                entity.HasOne(d => d.IdUngvienNavigation)
                    .WithMany(p => p.Bangcap)
                    .HasForeignKey(d => d.IdUngvien)
                    .HasConstraintName("FK_BANGCAP_UNGVIEN");
            });

            modelBuilder.Entity<Congviecmongmuon>(entity =>
            {
                entity.HasKey(e => e.IdCongviecmongmuon);

                entity.ToTable("CONGVIECMONGMUON");

                entity.Property(e => e.IdCongviecmongmuon).HasColumnName("ID_CONGVIECMONGMUON");

                entity.Property(e => e.Capbac)
                    .HasColumnName("CAPBAC")
                    .HasMaxLength(50);

                entity.Property(e => e.IdLoaicongviec).HasColumnName("ID_LOAICONGVIEC");

                entity.Property(e => e.IdNganh).HasColumnName("ID_NGANH");

                entity.Property(e => e.IdThanhpho).HasColumnName("ID_THANHPHO");

                entity.Property(e => e.IdUngvien).HasColumnName("ID_UNGVIEN");

                entity.Property(e => e.Luong)
                    .HasColumnName("LUONG")
                    .HasColumnType("money");

                entity.Property(e => e.Ngaytao)
                    .HasColumnName("NGAYTAO")
                    .HasColumnType("date");

                entity.Property(e => e.Noikhac)
                    .HasColumnName("NOIKHAC")
                    .HasComment("Nơi làm việc khác ngoài những nơi đã chọn");

                entity.Property(e => e.Tenvieclam)
                    .HasColumnName("TENVIECLAM")
                    .HasMaxLength(500);

                entity.Property(e => e.Thongbao).HasColumnName("THONGBAO");

                entity.Property(e => e.Thuongluong).HasColumnName("THUONGLUONG");

                entity.HasOne(d => d.IdLoaicongviecNavigation)
                    .WithMany(p => p.Congviecmongmuon)
                    .HasForeignKey(d => d.IdLoaicongviec)
                    .HasConstraintName("FK_CONGVIECMONGMUON_LOAICONGVIEC");

                entity.HasOne(d => d.IdUngvienNavigation)
                    .WithMany(p => p.Congviecmongmuon)
                    .HasForeignKey(d => d.IdUngvien)
                    .HasConstraintName("FK_CONGVIECMONGMUON_UNGVIEN");
            });

            modelBuilder.Entity<Dangtintuyendung>(entity =>
            {
                entity.HasKey(e => e.IdTintuyendung);

                entity.ToTable("DANGTINTUYENDUNG");

                entity.Property(e => e.IdTintuyendung).HasColumnName("ID_TINTUYENDUNG");

                entity.Property(e => e.Capbac)
                    .HasColumnName("CAPBAC")
                    .HasMaxLength(200);

                entity.Property(e => e.Chucdanh)
                    .HasColumnName("CHUCDANH")
                    .HasMaxLength(200);

                entity.Property(e => e.Contentdelete).HasColumnName("CONTENTDELETE");

                entity.Property(e => e.Datasort).HasColumnName("DATASORT");

                entity.Property(e => e.Douutien).HasColumnName("DOUUTIEN");

                entity.Property(e => e.Emaillienhe)
                    .HasColumnName("EMAILLIENHE")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Gioitinh).HasColumnName("GIOITINH");

                entity.Property(e => e.IdDiadiemlamviec).HasColumnName("ID_DIADIEMLAMVIEC");

                entity.Property(e => e.IdLoaicongviec).HasColumnName("ID_LOAICONGVIEC");

                entity.Property(e => e.IdNhatuyendung).HasColumnName("ID_NHATUYENDUNG");

                entity.Property(e => e.Isdelete).HasColumnName("ISDELETE");

                entity.Property(e => e.Kinhnghiem).HasColumnName("KINHNGHIEM");

                entity.Property(e => e.Luotxem).HasColumnName("LUOTXEM");

                entity.Property(e => e.Maxluong)
                    .HasColumnName("MAXLUONG")
                    .HasColumnType("money");

                entity.Property(e => e.Minluong)
                    .HasColumnName("MINLUONG")
                    .HasColumnType("money");

                entity.Property(e => e.Motacongviec).HasColumnName("MOTACONGVIEC");

                entity.Property(e => e.Ngaydang)
                    .HasColumnName("NGAYDANG")
                    .HasColumnType("date");

                entity.Property(e => e.Ngayhethan)
                    .HasColumnName("NGAYHETHAN")
                    .HasColumnType("date");

                entity.Property(e => e.Nguoilienhe)
                    .HasColumnName("NGUOILIENHE")
                    .HasMaxLength(200);

                entity.Property(e => e.Sdtlienhe)
                    .HasColumnName("SDTLIENHE")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Soluong).HasColumnName("SOLUONG");

                entity.Property(e => e.Thuongluong).HasColumnName("THUONGLUONG");

                entity.Property(e => e.Tieude)
                    .HasColumnName("TIEUDE")
                    .HasMaxLength(200);

                entity.Property(e => e.Trangthai)
                    .HasColumnName("TRANGTHAI")
                    .HasMaxLength(50);

                entity.Property(e => e.Yeucaucongviec).HasColumnName("YEUCAUCONGVIEC");

                entity.Property(e => e.Yeucaukynang).HasColumnName("YEUCAUKYNANG");

                entity.HasOne(d => d.IdDiadiemlamviecNavigation)
                    .WithMany(p => p.Dangtintuyendung)
                    .HasForeignKey(d => d.IdDiadiemlamviec)
                    .HasConstraintName("FK_DANGTINTUYENDUNG_DIADIEMLAMVIEC");

                entity.HasOne(d => d.IdLoaicongviecNavigation)
                    .WithMany(p => p.Dangtintuyendung)
                    .HasForeignKey(d => d.IdLoaicongviec)
                    .HasConstraintName("FK_DANGTINTUYENDUNG_LOAICONGVIEC");

                entity.HasOne(d => d.IdNhatuyendungNavigation)
                    .WithMany(p => p.Dangtintuyendung)
                    .HasForeignKey(d => d.IdNhatuyendung)
                    .HasConstraintName("FK_DANGTINTUYENDUNG_NHATUYENDUNG");
            });

            modelBuilder.Entity<Diadiemlamviec>(entity =>
            {
                entity.HasKey(e => e.IdDiadiemlamviec);

                entity.ToTable("DIADIEMLAMVIEC");

                entity.Property(e => e.IdDiadiemlamviec).HasColumnName("ID_DIADIEMLAMVIEC");

                entity.Property(e => e.Diachi)
                    .HasColumnName("DIACHI")
                    .HasMaxLength(200);

                entity.Property(e => e.IdNhatuyendung).HasColumnName("ID_NHATUYENDUNG");

                entity.Property(e => e.IdThanhpho).HasColumnName("ID_THANHPHO");

                entity.HasOne(d => d.IdNhatuyendungNavigation)
                    .WithMany(p => p.Diadiemlamviec)
                    .HasForeignKey(d => d.IdNhatuyendung)
                    .HasConstraintName("FK_DIADIEMLAMVIEC_NHATUYENDUNG");

                entity.HasOne(d => d.IdThanhphoNavigation)
                    .WithMany(p => p.Diadiemlamviec)
                    .HasForeignKey(d => d.IdThanhpho)
                    .HasConstraintName("FK_DIADIEMLAMVIEC_THANHPHO");
            });

            modelBuilder.Entity<Kinhnghiemlamviec>(entity =>
            {
                entity.HasKey(e => e.IdKinhnghiemlamviec);

                entity.ToTable("KINHNGHIEMLAMVIEC");

                entity.Property(e => e.IdKinhnghiemlamviec).HasColumnName("ID_KINHNGHIEMLAMVIEC");

                entity.Property(e => e.Chucdanh)
                    .HasColumnName("CHUCDANH")
                    .HasMaxLength(50);

                entity.Property(e => e.Chucvu)
                    .HasColumnName("CHUCVU")
                    .HasMaxLength(50);

                entity.Property(e => e.Congviechientai).HasColumnName("CONGVIECHIENTAI");

                entity.Property(e => e.Ghichu)
                    .HasColumnName("GHICHU")
                    .HasColumnType("ntext");

                entity.Property(e => e.IdUngvien).HasColumnName("ID_UNGVIEN");

                entity.Property(e => e.Tencongty)
                    .HasColumnName("TENCONGTY")
                    .HasMaxLength(200);

                entity.Property(e => e.Thoigianbatdau)
                    .HasColumnName("THOIGIANBATDAU")
                    .HasColumnType("date");

                entity.Property(e => e.Thoigianketthuc)
                    .HasColumnName("THOIGIANKETTHUC")
                    .HasColumnType("date");

                entity.HasOne(d => d.IdUngvienNavigation)
                    .WithMany(p => p.Kinhnghiemlamviec)
                    .HasForeignKey(d => d.IdUngvien)
                    .HasConstraintName("FK_KINHNGHIEMLAMVIEC_UNGVIEN");
            });

            modelBuilder.Entity<Kynangungvien>(entity =>
            {
                entity.HasKey(e => e.IdKynangcanhan);

                entity.ToTable("KYNANGUNGVIEN");

                entity.Property(e => e.IdKynangcanhan).HasColumnName("ID_KYNANGCANHAN");

                entity.Property(e => e.IdUngvien).HasColumnName("ID_UNGVIEN");

                entity.Property(e => e.Motakynang).HasColumnName("MOTAKYNANG");

                entity.Property(e => e.Tenkynang)
                    .HasColumnName("TENKYNANG")
                    .HasMaxLength(200);

                entity.HasOne(d => d.IdUngvienNavigation)
                    .WithMany(p => p.Kynangungvien)
                    .HasForeignKey(d => d.IdUngvien)
                    .HasConstraintName("FK_KYNANGUNGVIEN_UNGVIEN");
            });

            modelBuilder.Entity<Loaicongviec>(entity =>
            {
                entity.HasKey(e => e.IdLoaicongviec);

                entity.ToTable("LOAICONGVIEC");

                entity.Property(e => e.IdLoaicongviec).HasColumnName("ID_LOAICONGVIEC");

                entity.Property(e => e.Tenloaicongviec)
                    .HasColumnName("TENLOAICONGVIEC")
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<Loaicongviecungvien>(entity =>
            {
                entity.HasKey(e => new { e.IdUngvien, e.IdLoaicongviec });

                entity.ToTable("LOAICONGVIECUNGVIEN");

                entity.Property(e => e.IdUngvien).HasColumnName("ID_UNGVIEN");

                entity.Property(e => e.IdLoaicongviec).HasColumnName("ID_LOAICONGVIEC");

                entity.HasOne(d => d.IdLoaicongviecNavigation)
                    .WithMany(p => p.Loaicongviecungvien)
                    .HasForeignKey(d => d.IdLoaicongviec)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_LOAICONGVIECUNGVIEN_LOAICONGVIEC");

                entity.HasOne(d => d.IdUngvienNavigation)
                    .WithMany(p => p.Loaicongviecungvien)
                    .HasForeignKey(d => d.IdUngvien)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_LOAICONGVIECUNGVIEN_UNGVIEN");
            });

            modelBuilder.Entity<Luucongviec>(entity =>
            {
                entity.HasKey(e => new { e.IdUngvien, e.IdTintuyendung })
                    .HasName("PK_LUUCONGVIEC_1");

                entity.ToTable("LUUCONGVIEC");

                entity.Property(e => e.IdUngvien).HasColumnName("ID_UNGVIEN");

                entity.Property(e => e.IdTintuyendung).HasColumnName("ID_TINTUYENDUNG");

                entity.Property(e => e.Ngayluu)
                    .HasColumnName("NGAYLUU")
                    .HasColumnType("date");

                entity.HasOne(d => d.IdTintuyendungNavigation)
                    .WithMany(p => p.Luucongviec)
                    .HasForeignKey(d => d.IdTintuyendung)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_LUUCONGVIEC_DANGTINTUYENDUNG");

                entity.HasOne(d => d.IdUngvienNavigation)
                    .WithMany(p => p.Luucongviec)
                    .HasForeignKey(d => d.IdUngvien)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_LUUCONGVIEC_UNGVIEN");
            });

            modelBuilder.Entity<Luuungvien>(entity =>
            {
                entity.HasKey(e => new { e.IdNhatuyendung, e.IdUngvien });

                entity.ToTable("LUUUNGVIEN");

                entity.Property(e => e.IdNhatuyendung).HasColumnName("ID_NHATUYENDUNG");

                entity.Property(e => e.IdUngvien).HasColumnName("ID_UNGVIEN");

                entity.Property(e => e.Ngayluu)
                    .HasColumnName("NGAYLUU")
                    .HasColumnType("date");

                entity.HasOne(d => d.IdNhatuyendungNavigation)
                    .WithMany(p => p.Luuungvien)
                    .HasForeignKey(d => d.IdNhatuyendung)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_LUUUNGVIEN_NHATUYENDUNG");

                entity.HasOne(d => d.IdUngvienNavigation)
                    .WithMany(p => p.Luuungvien)
                    .HasForeignKey(d => d.IdUngvien)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_LUUUNGVIEN_UNGVIEN");
            });

            modelBuilder.Entity<Nganh>(entity =>
            {
                entity.HasKey(e => e.IdNganh);

                entity.ToTable("NGANH");

                entity.Property(e => e.IdNganh).HasColumnName("ID_NGANH");

                entity.Property(e => e.Tennganh)
                    .HasColumnName("TENNGANH")
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<Nganhnghemongmuon>(entity =>
            {
                entity.HasKey(e => new { e.IdNganh, e.IdCongviecmongmuon });

                entity.ToTable("NGANHNGHEMONGMUON");

                entity.Property(e => e.IdNganh).HasColumnName("ID_NGANH");

                entity.Property(e => e.IdCongviecmongmuon).HasColumnName("ID_CONGVIECMONGMUON");

                entity.HasOne(d => d.IdCongviecmongmuonNavigation)
                    .WithMany(p => p.Nganhnghemongmuon)
                    .HasForeignKey(d => d.IdCongviecmongmuon)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_NGANHNGHEMONGMUON_CONGVIECMONGMUON");

                entity.HasOne(d => d.IdNganhNavigation)
                    .WithMany(p => p.Nganhnghemongmuon)
                    .HasForeignKey(d => d.IdNganh)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_NGANHNGHEMONGMUON_NGANH");
            });

            modelBuilder.Entity<Nganhtuyendung>(entity =>
            {
                entity.HasKey(e => new { e.IdTintuyendung, e.IdNganh });

                entity.ToTable("NGANHTUYENDUNG");

                entity.Property(e => e.IdTintuyendung).HasColumnName("ID_TINTUYENDUNG");

                entity.Property(e => e.IdNganh).HasColumnName("ID_NGANH");

                entity.HasOne(d => d.IdNganhNavigation)
                    .WithMany(p => p.Nganhtuyendung)
                    .HasForeignKey(d => d.IdNganh)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_NGANHTUYENDUNG_NGANH");

                entity.HasOne(d => d.IdTintuyendungNavigation)
                    .WithMany(p => p.Nganhtuyendung)
                    .HasForeignKey(d => d.IdTintuyendung)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_NGANHTUYENDUNG_DANGTINTUYENDUNG");
            });

            modelBuilder.Entity<Nganhungvien>(entity =>
            {
                entity.HasKey(e => new { e.IdNganh, e.IdUngvien });

                entity.ToTable("NGANHUNGVIEN");

                entity.Property(e => e.IdNganh).HasColumnName("ID_NGANH");

                entity.Property(e => e.IdUngvien).HasColumnName("ID_UNGVIEN");

                entity.HasOne(d => d.IdUngvienNavigation)
                    .WithMany(p => p.Nganhungvien)
                    .HasForeignKey(d => d.IdUngvien)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_NGANHUNGVIEN_UNGVIEN");
            });

            modelBuilder.Entity<Ngoaingu>(entity =>
            {
                entity.HasKey(e => e.IdNgoaingu);

                entity.ToTable("NGOAINGU");

                entity.Property(e => e.IdNgoaingu).HasColumnName("ID_NGOAINGU");

                entity.Property(e => e.Tenngoaingu)
                    .HasColumnName("TENNGOAINGU")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Ngoainguungvien>(entity =>
            {
                entity.HasKey(e => new { e.IdNgoaingu, e.IdUngvien })
                    .HasName("PK_NGOAINGUUNGVIEN_1");

                entity.ToTable("NGOAINGUUNGVIEN");

                entity.Property(e => e.IdNgoaingu).HasColumnName("ID_NGOAINGU");

                entity.Property(e => e.IdUngvien).HasColumnName("ID_UNGVIEN");

                entity.Property(e => e.Coban).HasColumnName("COBAN");

                entity.Property(e => e.Doc).HasColumnName("DOC");

                entity.Property(e => e.Ghichu)
                    .HasColumnName("GHICHU")
                    .HasColumnType("ntext");

                entity.Property(e => e.Nghe).HasColumnName("NGHE");

                entity.Property(e => e.Noi).HasColumnName("NOI");

                entity.Property(e => e.Viet).HasColumnName("VIET");

                entity.HasOne(d => d.IdNgoainguNavigation)
                    .WithMany(p => p.Ngoainguungvien)
                    .HasForeignKey(d => d.IdNgoaingu)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_NGOAINGUUNGVIEN_NGOAINGU");

                entity.HasOne(d => d.IdUngvienNavigation)
                    .WithMany(p => p.Ngoainguungvien)
                    .HasForeignKey(d => d.IdUngvien)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_NGOAINGUUNGVIEN_UNGVIEN");
            });

            modelBuilder.Entity<Nhatuyendung>(entity =>
            {
                entity.HasKey(e => e.IdNhatuyendung);

                entity.ToTable("NHATUYENDUNG");

                entity.HasIndex(e => e.Email)
                    .HasName("UQ__NHATUYEN__161CF7245C40A6BC")
                    .IsUnique();

                entity.Property(e => e.IdNhatuyendung).HasColumnName("ID_NHATUYENDUNG");

                entity.Property(e => e.Bannercongty).HasColumnName("BANNERCONGTY");

                entity.Property(e => e.Contentdelete).HasColumnName("CONTENTDELETE");

                entity.Property(e => e.Diachi).HasColumnName("DIACHI");

                entity.Property(e => e.Douutien).HasColumnName("DOUUTIEN");

                entity.Property(e => e.Email)
                    .HasColumnName("EMAIL")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Gioithieucongty).HasColumnName("GIOITHIEUCONGTY");

                entity.Property(e => e.Hinhanh).HasColumnName("HINHANH");

                entity.Property(e => e.Isdelete).HasColumnName("ISDELETE");

                entity.Property(e => e.Logo).HasColumnName("LOGO");

                entity.Property(e => e.Luotxem).HasColumnName("LUOTXEM");

                entity.Property(e => e.Matkhau)
                    .HasColumnName("MATKHAU")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Nganhnghehoatdong).HasColumnName("NGANHNGHEHOATDONG");

                entity.Property(e => e.Ngaydangky)
                    .HasColumnName("NGAYDANGKY")
                    .HasColumnType("date");

                entity.Property(e => e.Quymocongty).HasColumnName("QUYMOCONGTY");

                entity.Property(e => e.Sdt)
                    .HasColumnName("SDT")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Showlogo).HasColumnName("SHOWLOGO");

                entity.Property(e => e.Sociallogin).HasColumnName("SOCIALLOGIN");

                entity.Property(e => e.Tencongty)
                    .HasColumnName("TENCONGTY")
                    .HasMaxLength(200);

                entity.Property(e => e.Tenviettat)
                    .HasColumnName("TENVIETTAT")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Trangthai).HasColumnName("TRANGTHAI");

                entity.Property(e => e.Videogioithieu).HasColumnName("VIDEOGIOITHIEU");

                entity.Property(e => e.Website).HasColumnName("WEBSITE");

                entity.HasOne(d => d.NganhnghehoatdongNavigation)
                    .WithMany(p => p.Nhatuyendung)
                    .HasForeignKey(d => d.Nganhnghehoatdong)
                    .HasConstraintName("FK_NHATUYENDUNG_NGANH");
            });

            modelBuilder.Entity<Noilamviecmongmuon>(entity =>
            {
                entity.HasKey(e => new { e.IdCongviecmongmuon, e.IdThanhpho });

                entity.ToTable("NOILAMVIECMONGMUON");

                entity.Property(e => e.IdCongviecmongmuon).HasColumnName("ID_CONGVIECMONGMUON");

                entity.Property(e => e.IdThanhpho).HasColumnName("ID_THANHPHO");

                entity.HasOne(d => d.IdCongviecmongmuonNavigation)
                    .WithMany(p => p.Noilamviecmongmuon)
                    .HasForeignKey(d => d.IdCongviecmongmuon)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_NOILAMVIECMONGMUON_CONGVIECMONGMUON");

                entity.HasOne(d => d.IdThanhphoNavigation)
                    .WithMany(p => p.Noilamviecmongmuon)
                    .HasForeignKey(d => d.IdThanhpho)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_NOILAMVIECMONGMUON_THANHPHO");
            });

            modelBuilder.Entity<Phucloi>(entity =>
            {
                entity.HasKey(e => e.IdPhucloi);

                entity.ToTable("PHUCLOI");

                entity.Property(e => e.IdPhucloi).HasColumnName("ID_PHUCLOI");

                entity.Property(e => e.Tenphucloi)
                    .HasColumnName("TENPHUCLOI")
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<Phucloicongviecmongmuon>(entity =>
            {
                entity.HasKey(e => new { e.IdPhucloi, e.IdCongviecmongmuon })
                    .HasName("PK_PHUCLOICONGVIECMONGMUON_1");

                entity.ToTable("PHUCLOICONGVIECMONGMUON");

                entity.Property(e => e.IdPhucloi).HasColumnName("ID_PHUCLOI");

                entity.Property(e => e.IdCongviecmongmuon).HasColumnName("ID_CONGVIECMONGMUON");

                entity.Property(e => e.Phucloimongmuon)
                    .HasColumnName("PHUCLOIMONGMUON")
                    .HasMaxLength(225);

                entity.HasOne(d => d.IdCongviecmongmuonNavigation)
                    .WithMany(p => p.Phucloicongviecmongmuon)
                    .HasForeignKey(d => d.IdCongviecmongmuon)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PHUCLOICONGVIECMONGMUON_CONGVIECMONGMUON1");

                entity.HasOne(d => d.IdPhucloiNavigation)
                    .WithMany(p => p.Phucloicongviecmongmuon)
                    .HasForeignKey(d => d.IdPhucloi)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PHUCLOICONGVIECMONGMUON_PHUCLOI");
            });

            modelBuilder.Entity<Phucloituyendung>(entity =>
            {
                entity.HasKey(e => new { e.IdPhucloi, e.IdTintuyendung });

                entity.ToTable("PHUCLOITUYENDUNG");

                entity.Property(e => e.IdPhucloi).HasColumnName("ID_PHUCLOI");

                entity.Property(e => e.IdTintuyendung).HasColumnName("ID_TINTUYENDUNG");

                entity.Property(e => e.Motaphucloi).HasColumnName("MOTAPHUCLOI");

                entity.HasOne(d => d.IdPhucloiNavigation)
                    .WithMany(p => p.Phucloituyendung)
                    .HasForeignKey(d => d.IdPhucloi)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PHUCLOITUYENDUNG_PHUCLOI");

                entity.HasOne(d => d.IdTintuyendungNavigation)
                    .WithMany(p => p.Phucloituyendung)
                    .HasForeignKey(d => d.IdTintuyendung)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PHUCLOITUYENDUNG_DANGTINTUYENDUNG");
            });

            modelBuilder.Entity<Quantrivien>(entity =>
            {
                entity.HasKey(e => e.IdQtv)
                    .HasName("PK_NGUOIDUNG");

                entity.ToTable("QUANTRIVIEN");

                entity.Property(e => e.IdQtv).HasColumnName("ID_QTV");

                entity.Property(e => e.Email)
                    .HasColumnName("EMAIL")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Matkhau)
                    .HasColumnName("MATKHAU")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Sdt)
                    .HasColumnName("SDT")
                    .HasMaxLength(15)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Thanhpho>(entity =>
            {
                entity.HasKey(e => e.IdThanhpho);

                entity.ToTable("THANHPHO");

                entity.Property(e => e.IdThanhpho).HasColumnName("ID_THANHPHO");

                entity.Property(e => e.Tenthanhpho)
                    .HasColumnName("TENTHANHPHO")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Thanhphoungvien>(entity =>
            {
                entity.HasKey(e => new { e.IdUngvien, e.IdThanhpho });

                entity.ToTable("THANHPHOUNGVIEN");

                entity.Property(e => e.IdUngvien).HasColumnName("ID_UNGVIEN");

                entity.Property(e => e.IdThanhpho).HasColumnName("ID_THANHPHO");

                entity.HasOne(d => d.IdUngvienNavigation)
                    .WithMany(p => p.Thanhphoungvien)
                    .HasForeignKey(d => d.IdUngvien)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_THANHPHOUNGVIEN_UNGVIEN");
            });

            modelBuilder.Entity<Trinhdo>(entity =>
            {
                entity.HasKey(e => e.IdTrinhdo);

                entity.ToTable("TRINHDO");

                entity.Property(e => e.IdTrinhdo).HasColumnName("ID_TRINHDO");

                entity.Property(e => e.Giatri).HasColumnName("GIATRI");

                entity.Property(e => e.Tentrinhdo)
                    .HasColumnName("TENTRINHDO")
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<Trinhdotuyendung>(entity =>
            {
                entity.HasKey(e => new { e.IdTrinhdo, e.IdTuyendung });

                entity.ToTable("TRINHDOTUYENDUNG");

                entity.Property(e => e.IdTrinhdo).HasColumnName("ID_TRINHDO");

                entity.Property(e => e.IdTuyendung).HasColumnName("ID_TUYENDUNG");

                entity.HasOne(d => d.IdTrinhdoNavigation)
                    .WithMany(p => p.Trinhdotuyendung)
                    .HasForeignKey(d => d.IdTrinhdo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TRINHDOTUYENDUNG_TRINHDO");

                entity.HasOne(d => d.IdTuyendungNavigation)
                    .WithMany(p => p.Trinhdotuyendung)
                    .HasForeignKey(d => d.IdTuyendung)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TRINHDOTUYENDUNG_DANGTINTUYENDUNG");
            });

            modelBuilder.Entity<Ungtuyen>(entity =>
            {
                entity.HasKey(e => new { e.IdUngvien, e.IdCongviec })
                    .HasName("PK_UNGTUYEN_1");

                entity.ToTable("UNGTUYEN");

                entity.Property(e => e.IdUngvien).HasColumnName("ID_UNGVIEN");

                entity.Property(e => e.IdCongviec).HasColumnName("ID_CONGVIEC");

                entity.Property(e => e.Daxem).HasColumnName("DAXEM");

                entity.Property(e => e.Ngayungtuyen)
                    .HasColumnName("NGAYUNGTUYEN")
                    .HasColumnType("date");

                entity.HasOne(d => d.IdCongviecNavigation)
                    .WithMany(p => p.Ungtuyen)
                    .HasForeignKey(d => d.IdCongviec)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UNGTUYEN_DANGTINTUYENDUNG");

                entity.HasOne(d => d.IdUngvienNavigation)
                    .WithMany(p => p.Ungtuyen)
                    .HasForeignKey(d => d.IdUngvien)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UNGTUYEN_UNGVIEN");
            });

            modelBuilder.Entity<Ungvien>(entity =>
            {
                entity.HasKey(e => e.IdUngvien);

                entity.ToTable("UNGVIEN");

                entity.HasIndex(e => e.Email)
                    .HasName("UQ__UNGVIEN__161CF724BA49BA05")
                    .IsUnique();

                entity.Property(e => e.IdUngvien).HasColumnName("ID_UNGVIEN");

                entity.Property(e => e.Capbacmongmuon)
                    .HasColumnName("CAPBACMONGMUON")
                    .HasMaxLength(200);

                entity.Property(e => e.Contentdelete).HasColumnName("CONTENTDELETE");

                entity.Property(e => e.Diachi)
                    .HasColumnName("DIACHI")
                    .HasMaxLength(200);

                entity.Property(e => e.Docthan).HasColumnName("DOCTHAN");

                entity.Property(e => e.Email)
                    .HasColumnName("EMAIL")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Gioitinh).HasColumnName("GIOITINH");

                entity.Property(e => e.Hinhanh).HasColumnName("HINHANH");

                entity.Property(e => e.Ho)
                    .HasColumnName("HO")
                    .HasMaxLength(50);

                entity.Property(e => e.IdThanhpho).HasColumnName("ID_THANHPHO");

                entity.Property(e => e.Isdelete).HasColumnName("ISDELETE");

                entity.Property(e => e.Kinhnghiem).HasColumnName("KINHNGHIEM");

                entity.Property(e => e.Luong)
                    .HasColumnName("LUONG")
                    .HasColumnType("money");

                entity.Property(e => e.Luotxem).HasColumnName("LUOTXEM");

                entity.Property(e => e.Matkhau)
                    .HasColumnName("MATKHAU")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Mota).HasColumnName("MOTA");

                entity.Property(e => e.Muctieunghenghiep).HasColumnName("MUCTIEUNGHENGHIEP");

                entity.Property(e => e.Nganhmongmuon).HasColumnName("NGANHMONGMUON");

                entity.Property(e => e.Ngaysinh)
                    .HasColumnName("NGAYSINH")
                    .HasColumnType("date");

                entity.Property(e => e.Ngaytao)
                    .HasColumnName("NGAYTAO")
                    .HasColumnType("date");

                entity.Property(e => e.Sdt)
                    .HasColumnName("SDT")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Sociallogin).HasColumnName("SOCIALLOGIN");

                entity.Property(e => e.Ten)
                    .HasColumnName("TEN")
                    .HasMaxLength(50);

                entity.Property(e => e.Thoigianlamviecmongmuon)
                    .HasColumnName("THOIGIANLAMVIECMONGMUON")
                    .HasMaxLength(500);

                entity.Property(e => e.Trangthai)
                    .HasColumnName("TRANGTHAI")
                    .HasMaxLength(50);

                entity.Property(e => e.UrlCv).HasColumnName("URL_CV");

                entity.Property(e => e.Vitriungtuyen).HasColumnName("VITRIUNGTUYEN");

                entity.HasOne(d => d.IdThanhphoNavigation)
                    .WithMany(p => p.Ungvien)
                    .HasForeignKey(d => d.IdThanhpho)
                    .HasConstraintName("FK_UNGVIEN_THANHPHO");
            });

            modelBuilder.Entity<VwRand>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vwRand");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
