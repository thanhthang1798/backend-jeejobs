﻿using System;
using System.Collections.Generic;

namespace API_JEEJOB.DBEntities
{
    public partial class Thanhpho
    {
        public Thanhpho()
        {
            Diadiemlamviec = new HashSet<Diadiemlamviec>();
            Noilamviecmongmuon = new HashSet<Noilamviecmongmuon>();
            Ungvien = new HashSet<Ungvien>();
        }

        public int IdThanhpho { get; set; }
        public string Tenthanhpho { get; set; }

        public virtual ICollection<Diadiemlamviec> Diadiemlamviec { get; set; }
        public virtual ICollection<Noilamviecmongmuon> Noilamviecmongmuon { get; set; }
        public virtual ICollection<Ungvien> Ungvien { get; set; }
    }
}
