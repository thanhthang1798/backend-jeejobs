﻿using System;
using System.Collections.Generic;

namespace API_JEEJOB.DBEntities
{
    public partial class Quan
    {
        public Quan()
        {
            Diadiemlamviec = new HashSet<Diadiemlamviec>();
            Ungvien = new HashSet<Ungvien>();
        }

        public int IdQuan { get; set; }
        public string Tenquan { get; set; }
        public int? IdThanhpho { get; set; }

        public virtual Thanhpho IdThanhphoNavigation { get; set; }
        public virtual ICollection<Diadiemlamviec> Diadiemlamviec { get; set; }
        public virtual ICollection<Ungvien> Ungvien { get; set; }
    }
}
