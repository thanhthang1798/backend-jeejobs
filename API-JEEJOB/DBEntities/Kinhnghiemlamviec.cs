﻿using System;
using System.Collections.Generic;

namespace API_JEEJOB.DBEntities
{
    public partial class Kinhnghiemlamviec
    {
        public int IdKinhnghiemlamviec { get; set; }
        public int? IdUngvien { get; set; }
        public string Chucdanh { get; set; }
        public string Tencongty { get; set; }
        public string Chucvu { get; set; }
        public DateTime? Thoigianbatdau { get; set; }
        public DateTime? Thoigianketthuc { get; set; }
        public bool? Congviechientai { get; set; }
        public string Ghichu { get; set; }

        public virtual Ungvien IdUngvienNavigation { get; set; }
    }
}
