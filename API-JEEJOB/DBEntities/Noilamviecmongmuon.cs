﻿using System;
using System.Collections.Generic;

namespace API_JEEJOB.DBEntities
{
    public partial class Noilamviecmongmuon
    {
        public int IdCongviecmongmuon { get; set; }
        public int IdThanhpho { get; set; }

        public virtual Congviecmongmuon IdCongviecmongmuonNavigation { get; set; }
        public virtual Thanhpho IdThanhphoNavigation { get; set; }
    }
}
