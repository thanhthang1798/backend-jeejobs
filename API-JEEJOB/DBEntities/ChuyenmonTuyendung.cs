﻿using System;
using System.Collections.Generic;

namespace API_JEEJOB.DBEntities
{
    public partial class ChuyenmonTuyendung
    {
        public int IdChuyenmon { get; set; }
        public int IdTintuyendung { get; set; }

        public virtual Chuyenmon IdChuyenmonNavigation { get; set; }
        public virtual Dangtintuyendung IdTintuyendungNavigation { get; set; }
    }
}
