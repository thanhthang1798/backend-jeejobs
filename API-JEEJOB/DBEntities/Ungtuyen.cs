﻿using System;
using System.Collections.Generic;

namespace API_JEEJOB.DBEntities
{
    public partial class Ungtuyen
    {
        public int IdUngvien { get; set; }
        public int IdCongviec { get; set; }
        public DateTime? Ngayungtuyen { get; set; }
        public bool? Daxem { get; set; }

        public virtual Dangtintuyendung IdCongviecNavigation { get; set; }
        public virtual Ungvien IdUngvienNavigation { get; set; }
    }
}
