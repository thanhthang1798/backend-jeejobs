﻿using System;
using System.Collections.Generic;

namespace API_JEEJOB.DBEntities
{
    public partial class Ngoaingu
    {
        public Ngoaingu()
        {
            Ngoainguungvien = new HashSet<Ngoainguungvien>();
        }

        public int IdNgoaingu { get; set; }
        public string Tenngoaingu { get; set; }

        public virtual ICollection<Ngoainguungvien> Ngoainguungvien { get; set; }
    }
}
