﻿using System;
using System.Collections.Generic;

namespace API_JEEJOB.DBEntities
{
    public partial class Kynangungvien
    {
        public int IdKynangcanhan { get; set; }
        public string Tenkynang { get; set; }
        public string Motakynang { get; set; }
        public int? IdUngvien { get; set; }

        public virtual Ungvien IdUngvienNavigation { get; set; }
    }
}
