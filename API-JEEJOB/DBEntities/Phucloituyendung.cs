﻿using System;
using System.Collections.Generic;

namespace API_JEEJOB.DBEntities
{
    public partial class Phucloituyendung
    {
        public int IdPhucloi { get; set; }
        public int IdTintuyendung { get; set; }
        public string Motaphucloi { get; set; }

        public virtual Phucloi IdPhucloiNavigation { get; set; }
        public virtual Dangtintuyendung IdTintuyendungNavigation { get; set; }
    }
}
