﻿using System;
using System.Collections.Generic;

namespace API_JEEJOB.DBEntities
{
    public partial class Nganhnghemongmuon
    {
        public int IdNganh { get; set; }
        public int IdCongviecmongmuon { get; set; }

        public virtual Congviecmongmuon IdCongviecmongmuonNavigation { get; set; }
        public virtual Nganh IdNganhNavigation { get; set; }
    }
}
