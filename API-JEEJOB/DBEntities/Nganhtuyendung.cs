﻿using System;
using System.Collections.Generic;

namespace API_JEEJOB.DBEntities
{
    public partial class Nganhtuyendung
    {
        public int IdTintuyendung { get; set; }
        public int IdNganh { get; set; }

        public virtual Nganh IdNganhNavigation { get; set; }
        public virtual Dangtintuyendung IdTintuyendungNavigation { get; set; }
    }
}
