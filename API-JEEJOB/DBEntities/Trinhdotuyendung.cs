﻿using System;
using System.Collections.Generic;

namespace API_JEEJOB.DBEntities
{
    public partial class Trinhdotuyendung
    {
        public int IdTuyendung { get; set; }
        public int IdTrinhdo { get; set; }

        public virtual Trinhdo IdTrinhdoNavigation { get; set; }
        public virtual Dangtintuyendung IdTuyendungNavigation { get; set; }
    }
}
