﻿using System;
using System.Collections.Generic;

namespace API_JEEJOB.DBEntities
{
    public partial class Nganhungvien
    {
        public int IdNganh { get; set; }
        public int IdUngvien { get; set; }

        public virtual Ungvien IdUngvienNavigation { get; set; }
    }
}
