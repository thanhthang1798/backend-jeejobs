﻿using System;
using System.Collections.Generic;

namespace API_JEEJOB.DBEntities
{
    public partial class Loaicongviecungvien
    {
        public int IdUngvien { get; set; }
        public int IdLoaicongviec { get; set; }

        public virtual Loaicongviec IdLoaicongviecNavigation { get; set; }
        public virtual Ungvien IdUngvienNavigation { get; set; }
    }
}
