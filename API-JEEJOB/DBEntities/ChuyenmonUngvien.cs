﻿using System;
using System.Collections.Generic;

namespace API_JEEJOB.DBEntities
{
    public partial class ChuyenmonUngvien
    {
        public int IdUngvien { get; set; }
        public int IdChuyenmon { get; set; }

        public virtual Chuyenmon IdChuyenmonNavigation { get; set; }
        public virtual Ungvien IdUngvienNavigation { get; set; }
    }
}
