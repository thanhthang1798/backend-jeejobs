﻿using System;
using System.Collections.Generic;

namespace API_JEEJOB.DBEntities
{
    public partial class Luucongviec
    {
        public int IdUngvien { get; set; }
        public int IdTintuyendung { get; set; }
        public DateTime? Ngayluu { get; set; }

        public virtual Dangtintuyendung IdTintuyendungNavigation { get; set; }
        public virtual Ungvien IdUngvienNavigation { get; set; }
    }
}
