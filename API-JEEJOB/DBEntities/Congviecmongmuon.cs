﻿using System;
using System.Collections.Generic;

namespace API_JEEJOB.DBEntities
{
    public partial class Congviecmongmuon
    {
        public Congviecmongmuon()
        {
            Nganhnghemongmuon = new HashSet<Nganhnghemongmuon>();
            Noilamviecmongmuon = new HashSet<Noilamviecmongmuon>();
            Phucloicongviecmongmuon = new HashSet<Phucloicongviecmongmuon>();
        }

        public int IdCongviecmongmuon { get; set; }
        public int? IdUngvien { get; set; }
        public string Capbac { get; set; }
        public bool? Noikhac { get; set; }
        public int? Thongbao { get; set; }
        public int? IdThanhpho { get; set; }
        public int? IdLoaicongviec { get; set; }
        public int? IdNganh { get; set; }
        public decimal? Luong { get; set; }
        public bool? Thuongluong { get; set; }
        public DateTime? Ngaytao { get; set; }
        public string Tenvieclam { get; set; }

        public virtual Loaicongviec IdLoaicongviecNavigation { get; set; }
        public virtual Ungvien IdUngvienNavigation { get; set; }
        public virtual ICollection<Nganhnghemongmuon> Nganhnghemongmuon { get; set; }
        public virtual ICollection<Noilamviecmongmuon> Noilamviecmongmuon { get; set; }
        public virtual ICollection<Phucloicongviecmongmuon> Phucloicongviecmongmuon { get; set; }
    }
}
