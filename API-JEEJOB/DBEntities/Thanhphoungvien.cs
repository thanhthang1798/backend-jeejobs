﻿using System;
using System.Collections.Generic;

namespace API_JEEJOB.DBEntities
{
    public partial class Thanhphoungvien
    {
        public int IdUngvien { get; set; }
        public int IdThanhpho { get; set; }

        public virtual Ungvien IdUngvienNavigation { get; set; }
    }
}
