﻿using System;
using System.Collections.Generic;

namespace API_JEEJOB.DBEntities
{
    public partial class Thongbaocongviec
    {
        public int IdThongbao { get; set; }
        public int? IdUngvien { get; set; }
        public int? Capbac { get; set; }
        public decimal? Luong { get; set; }
        public int? IdLoaicongviec { get; set; }
        public bool? Noikhac { get; set; }
        public string Thongbao { get; set; }
        public bool? Thuongluong { get; set; }
        public DateTime? Ngaytao { get; set; }
        public int? IdNganh { get; set; }
        public int? IdThanhpho { get; set; }

        public virtual Loaicongviec IdLoaicongviecNavigation { get; set; }
        public virtual Nganh IdNganhNavigation { get; set; }
        public virtual Thanhpho IdThanhphoNavigation { get; set; }
        public virtual Ungvien IdUngvienNavigation { get; set; }
    }
}
