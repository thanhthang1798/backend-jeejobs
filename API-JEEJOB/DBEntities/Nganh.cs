﻿using System;
using System.Collections.Generic;

namespace API_JEEJOB.DBEntities
{
    public partial class Nganh
    {
        public Nganh()
        {
            Nganhnghemongmuon = new HashSet<Nganhnghemongmuon>();
            Nganhtuyendung = new HashSet<Nganhtuyendung>();
            Nhatuyendung = new HashSet<Nhatuyendung>();
        }

        public int IdNganh { get; set; }
        public string Tennganh { get; set; }

        public virtual ICollection<Nganhnghemongmuon> Nganhnghemongmuon { get; set; }
        public virtual ICollection<Nganhtuyendung> Nganhtuyendung { get; set; }
        public virtual ICollection<Nhatuyendung> Nhatuyendung { get; set; }
    }
}
