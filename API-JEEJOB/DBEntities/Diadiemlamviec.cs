﻿using System;
using System.Collections.Generic;

namespace API_JEEJOB.DBEntities
{
    public partial class Diadiemlamviec
    {
        public Diadiemlamviec()
        {
            Dangtintuyendung = new HashSet<Dangtintuyendung>();
        }

        public int IdDiadiemlamviec { get; set; }
        public int? IdNhatuyendung { get; set; }
        public int? IdThanhpho { get; set; }
        public string Diachi { get; set; }

        public virtual Nhatuyendung IdNhatuyendungNavigation { get; set; }
        public virtual Thanhpho IdThanhphoNavigation { get; set; }
        public virtual ICollection<Dangtintuyendung> Dangtintuyendung { get; set; }
    }
}
