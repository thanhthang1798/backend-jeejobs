﻿using System;
using System.Collections.Generic;

namespace API_JEEJOB.DBEntities
{
    public partial class Chuyenmon
    {
        public Chuyenmon()
        {
            ChuyenmonTuyendung = new HashSet<ChuyenmonTuyendung>();
            ChuyenmonUngvien = new HashSet<ChuyenmonUngvien>();
        }

        public int IdChuyenmon { get; set; }
        public int? IdNganh { get; set; }
        public string Tenchuyenmon { get; set; }

        public virtual Nganh IdNganhNavigation { get; set; }
        public virtual ICollection<ChuyenmonTuyendung> ChuyenmonTuyendung { get; set; }
        public virtual ICollection<ChuyenmonUngvien> ChuyenmonUngvien { get; set; }
    }
}
