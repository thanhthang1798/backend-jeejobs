﻿using System;
using System.Collections.Generic;

namespace API_JEEJOB.DBEntities
{
    public partial class Loaicongviec
    {
        public Loaicongviec()
        {
            Congviecmongmuon = new HashSet<Congviecmongmuon>();
            Dangtintuyendung = new HashSet<Dangtintuyendung>();
            Loaicongviecungvien = new HashSet<Loaicongviecungvien>();
        }

        public int IdLoaicongviec { get; set; }
        public string Tenloaicongviec { get; set; }

        public virtual ICollection<Congviecmongmuon> Congviecmongmuon { get; set; }
        public virtual ICollection<Dangtintuyendung> Dangtintuyendung { get; set; }
        public virtual ICollection<Loaicongviecungvien> Loaicongviecungvien { get; set; }
    }
}
