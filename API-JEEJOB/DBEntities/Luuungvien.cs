﻿using System;
using System.Collections.Generic;

namespace API_JEEJOB.DBEntities
{
    public partial class Luuungvien
    {
        public int IdNhatuyendung { get; set; }
        public int IdUngvien { get; set; }
        public DateTime? Ngayluu { get; set; }

        public virtual Nhatuyendung IdNhatuyendungNavigation { get; set; }
        public virtual Ungvien IdUngvienNavigation { get; set; }
    }
}
