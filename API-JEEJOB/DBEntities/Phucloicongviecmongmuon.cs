﻿using System;
using System.Collections.Generic;

namespace API_JEEJOB.DBEntities
{
    public partial class Phucloicongviecmongmuon
    {
        public int IdPhucloi { get; set; }
        public int IdCongviecmongmuon { get; set; }
        public string Phucloimongmuon { get; set; }

        public virtual Congviecmongmuon IdCongviecmongmuonNavigation { get; set; }
        public virtual Phucloi IdPhucloiNavigation { get; set; }
    }
}
