﻿using System;
using System.Collections.Generic;

namespace API_JEEJOB.DBEntities
{
    public partial class Trinhdo
    {
        public Trinhdo()
        {
            Bangcap = new HashSet<Bangcap>();
            Trinhdotuyendung = new HashSet<Trinhdotuyendung>();
        }

        public int IdTrinhdo { get; set; }
        public string Tentrinhdo { get; set; }
        public int? Giatri { get; set; }

        public virtual ICollection<Bangcap> Bangcap { get; set; }
        public virtual ICollection<Trinhdotuyendung> Trinhdotuyendung { get; set; }
    }
}
