﻿using J2.Businesses.Interfaces;
using J2.DATA.UnitOfWork;
using Microsoft.AspNetCore.Mvc;

namespace API_JEEJOB.V2Controllers
{
    [ApiController]
    [Route("apiv2/[controller]")]
    public class uowController : ControllerBase
    {
        private readonly ICitiesServices _citiesServices;
        public uowController(ICitiesServices citiesServices)
        {
            _citiesServices = citiesServices;
        }

        [HttpGet]
        public IActionResult Get()
        {
            return Ok(_citiesServices.GetDSTP());
        }
    }
}
