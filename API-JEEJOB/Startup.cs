﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API_JEEJOB.DBEntities;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Cors.Infrastructure;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using API_JEEJOB.Utils;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.Extensions.FileProviders;
using System.IO;
using J2.DATA.UnitOfWork;
using J2.Businesses.Businesses;
using J2.Businesses.Interfaces;
using Swashbuckle.AspNetCore.SwaggerGen;
using Microsoft.OpenApi.Models;
using System.Reflection;

namespace API_JEEJOB
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            #region Cấu hình CORS
            var corsBuilder = new CorsPolicyBuilder();
            corsBuilder.AllowCredentials();
            corsBuilder.AllowAnyHeader();
            corsBuilder.AllowAnyMethod();
            corsBuilder.WithOrigins("http://localhost:4200", "http://localhost:4205");
            corsBuilder.AllowCredentials();

            services.AddCors(options =>
            {
                options.AddPolicy("SiteCorsPolicy", corsBuilder.Build());
            });
            #endregion

            var connection = Configuration.GetConnectionString("JEEJOBDatabase");
            services.AddDbContextPool<JEEJOBContext>(option => option.UseSqlServer(connection));
            services.AddControllers();

            services.AddDistributedMemoryCache(); // Adds a default in-memory implementation of IDistributedCache

            #region đăng kí dependency injection cho service
            services.AddScoped<J2.DATA.DBEntities.DBEntities>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<ICitiesServices, CitiesServices>();
            services.AddDbContextPool<J2.DATA.DBEntities.DBEntities>(option => option.UseSqlServer(connection)); 
            #endregion
            //Configure session
            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromSeconds(Convert.ToInt32(Constant.TOKENEXPIRED));
                options.Cookie.IsEssential = true;
                options.Cookie.SecurePolicy = CookieSecurePolicy.SameAsRequest;
                options.Cookie.SameSite = SameSiteMode.None;
            });


          //Configure JWT Token Authentication
          services.AddAuthentication(auth =>
            {
                auth.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                auth.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                auth.DefaultSignInScheme = CookieAuthenticationDefaults.AuthenticationScheme;
            }).AddCookie(options =>
            {
                options.Cookie.SameSite = SameSiteMode.None;
                options.Cookie.SecurePolicy = CookieSecurePolicy.SameAsRequest;

            }).AddJwtBearer(options =>
            {
                options.RequireHttpsMetadata = false;
                options.SaveToken = true;
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,

                    ValidIssuer = Constant.ISUSER,
                    ValidAudience = Constant.AUDIENCE,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Constant.JWTENCRYPTKEY)),
                    ClockSkew = TimeSpan.Zero
                };
            });

            services.Configure<FormOptions>(o => {
                o.ValueLengthLimit = int.MaxValue;
                o.MultipartBodyLengthLimit = int.MaxValue;
                o.MemoryBufferThreshold = int.MaxValue;
            });

            #region swagger
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Your API", Version = "v1" }); 
                //c.SwaggerDoc("v2", new OpenApiInfo { Title = "V2 API", Version = "v2" });

                // Đường dẫn tới thư mục chứa các controller V2
                var controllerAssembly = typeof(API_JEEJOB.V2Controllers.uowController).Assembly;
                var apiV2ControllerName = controllerAssembly.GetName().Name;
                c.SwaggerDoc("v2", new OpenApiInfo { Title = apiV2ControllerName, Version = "v2" });

                // Bao gồm chú thích từ các tệp XML được tạo ra từ mã nguồn
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);

            });
            #endregion

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(options =>
                {
                    options.SwaggerEndpoint("/swagger/v1/swagger.json", "v1");
                    options.SwaggerEndpoint("/swagger/v2/swagger.json", "V2");
                    //options.RoutePrefix = string.Empty;
                });
            }

            

            app.UseSwagger(options =>
            {
                options.SerializeAsV2 = true;
            });


            #region Cấu hình CORS
            app.UseCors("SiteCorsPolicy");
            #endregion

            app.UseHttpsRedirection();

            app.UseRouting();

            //// IMPORTANT: This session call MUST go before UseMvc()
            app.UseSession();

            //Add JWToken to all incoming HTTP Request Header
            app.Use(async (context, next) =>
            {
                var JWToken = context.Session.GetString(Utils.Constant.ACCESSTOKEN);
                if (!string.IsNullOrEmpty(JWToken))
                {
                    context.Request.Headers.Add("Authorization", "Bearer " + JWToken);
                }
                await next();
            });
            // Upload file
            app.UseStaticFiles();
            app.UseStaticFiles(new StaticFileOptions()
            {
                FileProvider = new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(), @"Resources")),
                RequestPath = new PathString("/Resources")
            });


            //Add JWToken Authentication service
            app.UseAuthentication();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();

                //endpoints.MapControllerRoute(
                //    name: "V2Controllers",
                //    pattern: "apiv2/{controller=Home}/{action=Index}/{id?}",
                //    defaults: new { area = "V2Controllers" },
                //    constraints: new { controller = ".*" }
                //    );


            });

        }
    }
}
