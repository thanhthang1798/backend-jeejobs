﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using API_JEEJOB.DBEntities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using System.Collections.Generic;
using API_JEEJOB.Utils;

namespace API_JEEJOB.Controllers
{
    
    [Route("api/[controller]")]
    [ApiController]
    public class TokenController : ControllerBase
    {
        public IConfiguration _configuration;
        private readonly JEEJOBContext _context;

        public TokenController(IConfiguration config, JEEJOBContext context)
        {
            _configuration = config;
            _context = context;
        }

        [HttpPost]
        public async Task<IActionResult> Post(Quantrivien _qtv)
        {

            if (_qtv != null && _qtv.Email != null && _qtv.Matkhau != null)
            {
                var user = await GetUser(_qtv.Email, _qtv.Matkhau);

                if (user != null)
                {
                    // //create claims details based on the user information
                    // var claims = new[] {
                    // new Claim(JwtRegisteredClaimNames.Sub, _configuration["Jwt:Subject"]),
                    // new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                    // new Claim(JwtRegisteredClaimNames.Iat, DateTime.UtcNow.ToString()),
                    // new Claim("IDQTV", user.IdQtv.ToString()),
                    // new Claim("SDT", user.Sdt),
                    // new Claim("UserName", user.Matkhau),
                    // new Claim("Email", user.Email),

                    //};

                    // var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]));

                    // var signIn = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

                    // var token = new JwtSecurityToken(_configuration["Jwt:Issuer"], _configuration["Jwt:Audience"], claims, expires: DateTime.UtcNow.AddDays(1), signingCredentials: signIn);

                    // return Ok(new JwtSecurityTokenHandler().WriteToken(token));

                    // Basic claims
                    Claim claimUserData = new Claim(ClaimTypes.UserData, JsonConvert.SerializeObject(_qtv));
                    Claim claimUserName = new Claim(ClaimTypes.Name, _qtv.Email);
                    List<Claim> listClaims = new List<Claim>();
                    listClaims.Add(claimUserData);
                    listClaims.Add(claimUserName);

                    // Get token expired info from Constant
                    string TokenExpireConfig = Constant.TOKENEXPIRED;
                    if (string.IsNullOrEmpty(TokenExpireConfig))
                        TokenExpireConfig = "86400";

                    var secretKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Constant.JWTENCRYPTKEY));
                    var signinCredentials = new SigningCredentials(secretKey, SecurityAlgorithms.HmacSha256);
                    var jwtSecurityToken = new JwtSecurityToken(
                        issuer: Constant.ISUSER,
                        audience: Constant.AUDIENCE,
                        claims: listClaims,
                        expires: DateTime.Now.AddSeconds(Convert.ToInt32(TokenExpireConfig)),
                        signingCredentials: signinCredentials
                    );

                    var tokenString = new JwtSecurityTokenHandler().WriteToken(jwtSecurityToken);

                    return Ok(tokenString);
                }
                else
                {
                    return BadRequest("Invalid credentials");
                }
            }
            else
            {
                return BadRequest();
            }
        }

        private async Task<Quantrivien> GetUser(string email, string password)
        {
            return await _context.Quantrivien.FirstOrDefaultAsync(u => u.Email == email && u.Matkhau == password);
        }
    }
}