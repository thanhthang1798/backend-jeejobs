﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using API_JEEJOB.DBEntities;

namespace API_JEEJOB.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NganhnghemongmuonsController : ControllerBase
    {
        private readonly JEEJOBContext _context;

        public NganhnghemongmuonsController(JEEJOBContext context)
        {
            _context = context;
        }

        // GET: api/Nganhnghemongmuons
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Nganhnghemongmuon>>> GetNganhnghemongmuon()
        {
            return await _context.Nganhnghemongmuon.ToListAsync();
        }

        // GET: api/Nganhnghemongmuons/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Nganhnghemongmuon>> GetNganhnghemongmuon(int id)
        {
             var nganhnghemongmuon = await (from s in _context.Nganhnghemongmuon
                                            where s.IdCongviecmongmuon.Equals(id)
                               select new
                               {
                                   s.IdNganh,
                                   s.IdNganhNavigation.Tennganh
                               }).Distinct().ToListAsync();
            if (nganhnghemongmuon == null)
            {
                return NotFound();
            }

            return Ok(nganhnghemongmuon);
        }

        // PUT: api/Nganhnghemongmuons/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}&{idCongviecmongmuon}")]
        public async Task<IActionResult> PutNganhnghemongmuon(int id,int idCongviecmongmuon, Nganhnghemongmuon nganhnghemongmuon)
        {
            if (id != nganhnghemongmuon.IdNganh && idCongviecmongmuon != nganhnghemongmuon.IdCongviecmongmuon)
            {
                return BadRequest();
            }

            _context.Entry(nganhnghemongmuon).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!NganhnghemongmuonExists(id, idCongviecmongmuon))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Nganhnghemongmuons
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Nganhnghemongmuon>> PostNganhnghemongmuon(Nganhnghemongmuon nganhnghemongmuon)
        {
            _context.Nganhnghemongmuon.Add(nganhnghemongmuon);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (NganhnghemongmuonExists(nganhnghemongmuon.IdNganh, nganhnghemongmuon.IdCongviecmongmuon))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetNganhnghemongmuon", new { id = nganhnghemongmuon.IdNganh }, nganhnghemongmuon);
        }

        // DELETE: api/Nganhnghemongmuons/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Nganhnghemongmuon>> DeleteNganhnghemongmuon(int id)
        {
            var list = await (from s in _context.Nganhnghemongmuon
                              where s.IdCongviecmongmuon.Equals(id)
                              select s).ToListAsync();
            int count = 0;

            if (list != null)
            {
                foreach (Nganhnghemongmuon item in list) // _list is an instance of List<string>
                {
                    count++;
                    _context.Nganhnghemongmuon.Remove(item);
                    await _context.SaveChangesAsync();
                }
            }
            else
            {
                return NotFound();
            }

            return Ok(list);
        }

        private bool NganhnghemongmuonExists(int id,int idCongviecmongmuon)
        {
            return _context.Nganhnghemongmuon.Any(e => e.IdNganh == id && e.IdCongviecmongmuon == idCongviecmongmuon);
        }
    }
}
