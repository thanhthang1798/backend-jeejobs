﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using API_JEEJOB.DBEntities;

namespace API_JEEJOB.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DiadiemlamviecsController : ControllerBase
    {
        private readonly JEEJOBContext _context;

        public DiadiemlamviecsController(JEEJOBContext context)
        {
            _context = context;
        }

        // GET: api/Diadiemlamviecs
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Diadiemlamviec>>> GetDiadiemlamviec()
        {
            var diadiemlamviec = await (from s in _context.Diadiemlamviec

                                     select new
                                     {
                                         s.IdDiadiemlamviec,
                                         s.IdNhatuyendung,
                                         s.IdThanhpho,
                                         s.IdThanhphoNavigation.Tenthanhpho,
                                         s.Diachi
                                     }).ToListAsync();
            return Ok(diadiemlamviec);
        }

        // GET: api/Diadiemlamviecs/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Diadiemlamviec>> GetDiadiemlamviecTheoNTD(int id)
        {
            var diadiemlamviec = await (from s in _context.Diadiemlamviec
                                        where s.IdNhatuyendung.Equals(id)
                                        select new
                                        {
                                            s.IdDiadiemlamviec,
                                            s.IdNhatuyendung,
                                            s.IdThanhpho,
                                            s.IdThanhphoNavigation.Tenthanhpho,
                                            s.Diachi
                                        }).ToListAsync();
            if (diadiemlamviec == null)
            {
                return NotFound();
            }

            return Ok(diadiemlamviec);
        }

        // PUT: api/Diadiemlamviecs/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutDiadiemlamviec(int id, Diadiemlamviec diadiemlamviec)
        {
            if (id != diadiemlamviec.IdDiadiemlamviec)
            {
                return BadRequest();
            }

            _context.Entry(diadiemlamviec).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DiadiemlamviecExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Diadiemlamviecs
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Diadiemlamviec>> PostDiadiemlamviec(Diadiemlamviec diadiemlamviec)
        {
            _context.Diadiemlamviec.Add(diadiemlamviec);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetDiadiemlamviec", new { id = diadiemlamviec.IdDiadiemlamviec }, diadiemlamviec);
        }

        // DELETE: api/Diadiemlamviecs/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Diadiemlamviec>> DeleteDiadiemlamviec(int id)
        {
            var diadiemlamviec = await _context.Diadiemlamviec.FindAsync(id);
            if (diadiemlamviec == null)
            {
                return NotFound();
            }

            _context.Diadiemlamviec.Remove(diadiemlamviec);
            await _context.SaveChangesAsync();

            return diadiemlamviec;
        }

        private bool DiadiemlamviecExists(int id)
        {
            return _context.Diadiemlamviec.Any(e => e.IdDiadiemlamviec == id);
        }
    }
}
