﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using API_JEEJOB.DBEntities;
using System.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore.Internal;
using API_JEEJOB.Models;

namespace API_JEEJOB.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UngviensController : ControllerBase
    {
        private readonly JEEJOBContext _context;

        public UngviensController(JEEJOBContext context)
        {
            _context = context;
        }

        // GET: api/Ungviens
        //[Authorize]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Ungvien>>> GetUngvien()
        {
            var ungvien = await (from s in _context.Ungvien
                                 where s.Isdelete.Equals(false)
                                 select new
                                 {
                                     s.IdUngvien,
                                     s.Diachi,
                                     s.Email,
                                     s.Gioitinh,
                                     s.Hinhanh,
                                     s.Kinhnghiem,
                                     s.Luong,
                                     s.Ngaysinh,
                                     s.Ngaytao,
                                     s.Sdt,
                                     s.Docthan,
                                     s.Bangcap,
                                     s.UrlCv,
                                     s.Kinhnghiemlamviec,
                                     s.Loaicongviecungvien,
                                     s.Mota,
                                     s.Ngoainguungvien,
                                     s.Ten,
                                     s.Ho,
                                     s.Matkhau,
                                     s.IdThanhpho,
                                     s.IdThanhphoNavigation.Tenthanhpho,
                                     s.Congviecmongmuon,
                                     s.Isdelete,
                                     s.Luotxem,
                                     s.Nganhmongmuon,
                                     s.Capbacmongmuon,
                                     s.Muctieunghenghiep,
                                     s.Trangthai,
                                     s.Thoigianlamviecmongmuon,
                                     s.Vitriungtuyen,
                                     s.Sociallogin
                                 }).ToListAsync();
            return Ok(ungvien);
        }



        // GET: api/Ungviens/5
        //[Authorize]
        [HttpGet("{id}")]
        public async Task<ActionResult<Ungvien>> GetUngvien(int id)
        {
            var ungvien = await (from s in _context.Ungvien
                                 where s.IdUngvien.Equals(id) && s.Isdelete.Equals(false)
                                 select new
                                 {
                                     s.IdUngvien,
                                     s.Diachi,
                                     s.Email,
                                     s.Gioitinh,
                                     s.Hinhanh,
                                     s.Kinhnghiem,
                                     s.Luong,
                                     s.Ngaysinh,
                                     s.Ngaytao,
                                     s.Sdt,
                                     s.Docthan,
                                     s.Bangcap,
                                     s.UrlCv,
                                     s.Kinhnghiemlamviec,
                                     s.Loaicongviecungvien,
                                     s.Mota,
                                     s.Ngoainguungvien,
                                     s.Ten,
                                     s.Ho,
                                     s.Matkhau,
                                     s.Congviecmongmuon,
                                     s.Isdelete,
                                     s.IdThanhpho,
                                     s.IdThanhphoNavigation.Tenthanhpho,
                                     s.Luotxem,
                                     s.Nganhmongmuon,
                                     s.Capbacmongmuon,
                                     s.Thoigianlamviecmongmuon,
                                     s.Muctieunghenghiep,
                                     s.Trangthai,
                                     s.Vitriungtuyen,
                                     s.Sociallogin

                                 }).ToListAsync();
            if (ungvien == null)
            {
                return NotFound();
            }

            return Ok(ungvien);
        }
        //search Ứng viên
        [HttpGet("searchUngvien")]
        public List<UngvienDTO> searchUngvien(int idNhatuyendung,int ?idThanhpho,int? idNganh,int?kinhnghiem)
        {
            var ungvien = (from s in _context.Ungvien
                           where s.Isdelete.Equals(false)
                           select new UngvienDTO
                           {
                               IdUngvien = s.IdUngvien,
                               Diachi = s.Diachi,
                               Email = s.Email,
                               Gioitinh = s.Gioitinh,
                               Hinhanh = s.Hinhanh,
                               Kinhnghiem = s.Kinhnghiem,
                               Luong = s.Luong,
                               Ngaysinh = s.Ngaysinh,
                               Ngaytao = s.Ngaytao,
                               Sdt = s.Sdt,
                               Docthan = s.Docthan,
                               Bangcap = s.Bangcap,
                               UrlCv = s.UrlCv,
                               Kinhnghiemlamviec = s.Kinhnghiemlamviec,
                               Loaicongviecungvien = s.Loaicongviecungvien,
                               Mota = s.Mota,
                               Ngoainguungvien = s.Ngoainguungvien,
                               Ten = s.Ten,
                               Ho = s.Ho,
                               Matkhau = s.Matkhau,
                               Congviecmongmuon = s.Congviecmongmuon,
                               Isdelete = s.Isdelete,
                               IdThanhpho = s.IdThanhpho,
                               Tenthanhpho = s.IdThanhphoNavigation.Tenthanhpho,
                               Luotxem = s.Luotxem,
                               Nganhmongmuon = s.Nganhmongmuon,
                               Capbacmongmuon = s.Capbacmongmuon,
                               Trangthai = s.Trangthai,
                               Thoigianlamviecmongmuon = s.Thoigianlamviecmongmuon,
                               Muctieunghenghiep = s.Muctieunghenghiep,
                               Vitriungtuyen = s.Vitriungtuyen,
                               Sociallogin = s.Sociallogin,
                               DataSort = 0,
                               daluu = false
                           });


            if (idThanhpho > 0)
            {
                ungvien = from s in ungvien
                          where s.IdThanhpho.Equals(idThanhpho)
                          select s;
            }

            if (idNganh > 0)
            {
                ungvien = from s in ungvien
                          where s.Nganhmongmuon.Equals(idNganh)
                          select s;
            }
            if(kinhnghiem > 0)
            {
                ungvien = from s in ungvien
                          where s.Kinhnghiem >= kinhnghiem
                          select s;
            }

            var data = ungvien.Distinct().ToList();

            if (idNhatuyendung > 0)
            {
                var ntd = _context.Nhatuyendung.FirstOrDefault(x => x.IdNhatuyendung == idNhatuyendung);
                var luuungvien = from xxx in _context.Luuungvien
                                 where xxx.IdNhatuyendung.Equals(idNhatuyendung)
                                 select xxx;


                foreach (var item in data)
                {
                    foreach(var luu in luuungvien)
                    {
                        if(item.IdUngvien == luu.IdUngvien)
                        {
                            item.daluu = true;
                        }
                    }
                    

                    if (item.Nganhmongmuon == ntd.Nganhnghehoatdong)
                    {
                        item.DataSort++;
                    }

                    foreach (var rs in ntd.Diadiemlamviec)
                    {

                    }
                }
            }
            



            return data.ToList();
        }



        // GET: api/Ungviens/5
        //[Authorize]
        [HttpGet("search")]
        public async Task<ActionResult<Ungvien>> GetUngvienLoc(int? idThanhpho,int? idnganh, int? maxluong, int? minluong,string?capbac)
        {
            var ungvien = (from s in _context.Ungvien
                                 where s.Isdelete.Equals(false)
                                 select new
                                 {
                                     s.IdUngvien,
                                     s.Diachi,
                                     s.Email,
                                     s.Gioitinh,
                                     s.Hinhanh,
                                     s.Kinhnghiem,
                                     s.Luong,
                                     s.Ngaysinh,
                                     s.Ngaytao,
                                     s.Sdt,
                                     s.Docthan,
                                     s.Bangcap,
                                     s.UrlCv,
                                     s.Kinhnghiemlamviec,
                                     s.Loaicongviecungvien,
                                     s.Mota,
                                     s.Ngoainguungvien,
                                     s.Ten,
                                     s.Ho,
                                     s.Matkhau,
                                     s.IdThanhpho,
                                     s.IdThanhphoNavigation.Tenthanhpho,
                                     s.Congviecmongmuon,
                                     s.Isdelete,
                                     s.Luotxem,
                                     s.Nganhmongmuon,
                                     s.Capbacmongmuon,
                                     s.Thoigianlamviecmongmuon,
                                     s.Trangthai,
                                     s.Muctieunghenghiep,
                                     s.Vitriungtuyen,
                                     s.Sociallogin
                                 });
            if (idThanhpho != null && idThanhpho > 0)
            {
                ungvien = from uv in ungvien
                          join nganh in _context.Congviecmongmuon on uv.IdUngvien equals nganh.IdUngvien
                          join nnmm in _context.Noilamviecmongmuon on nganh.IdCongviecmongmuon equals nnmm.IdCongviecmongmuon
                          where nnmm.IdThanhpho.Equals(idThanhpho)
                          select uv;
            }
            if (idnganh != null && idnganh > 0)
            {
                ungvien = from uv in ungvien
                          join nganh in _context.Congviecmongmuon on uv.IdUngvien equals nganh.IdUngvien
                          join nnmm in _context.Nganhnghemongmuon on nganh.IdCongviecmongmuon equals nnmm.IdCongviecmongmuon
                          where nnmm.IdNganh.Equals(idnganh) select uv;
            }
            if (capbac != null && capbac != "Tất cả")
            {
                ungvien = from uv in ungvien
                          join nganh in _context.Congviecmongmuon on uv.IdUngvien equals nganh.IdUngvien
                          where nganh.Capbac.Equals(capbac) || nganh.Capbac.Equals("Tất cả")
                          select uv;
                //ungvien = from uv in ungvien
                //          join nganh in _context.Loaicongviecungvien on uv.IdUngvien equals nganh.IdUngvien
                //          where nganh.IdLoaicongviec.Equals(loaicongviec)
                //          select uv;
            }

            var data = ungvien.ToList();


            return Ok(data);
        }

        // PUT: api/Ungviens/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        //[Authorize]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutUngvien(int id, Ungvien ungvien)
        {
            if (id != ungvien.IdUngvien)
            {
                return BadRequest();
            }

            _context.Entry(ungvien).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UngvienExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Ungviens
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        //[Authorize]
        [HttpPost]
        public async Task<ActionResult<Ungvien>> PostUngvien(Ungvien ungvien)
        {
            _context.Ungvien.Add(ungvien);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (UngvienExists(ungvien.IdUngvien))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetUngvien", new { id = ungvien.IdUngvien }, ungvien);
        }

        // DELETE: api/Ungviens/5
        //[Authorize]
        [HttpDelete("{id}")]
        public async Task<ActionResult<Ungvien>> DeleteUngvien(int id)
        {
            var ungvien = await _context.Ungvien.FindAsync(id);
            if (ungvien == null)
            {
                return NotFound();
            }

            _context.Ungvien.Remove(ungvien);
            await _context.SaveChangesAsync();

            return ungvien;
        }
        //[Authorize]
        [HttpGet("ungtuyen/{id}")]
        public async Task<ActionResult<IEnumerable<Dangtintuyendung>>> GetUngtuyen(int id)
        {
            var dangtintuyendung = await (from s in _context.Dangtintuyendung
                                          join ungtuyen in _context.Ungtuyen on s.IdTintuyendung equals ungtuyen.IdCongviec
                                          where ungtuyen.IdUngvien.Equals(id)
                                          select new
                                          {
                                              s.IdTintuyendung,
                                              s.Tieude,
                                              s.Chucdanh,
                                              s.Minluong,
                                              s.Maxluong,
                                              s.Thuongluong,
                                              s.Trinhdotuyendung,
                                              ungtuyen.Ngayungtuyen,
                                          }).Distinct().ToListAsync();
            return Ok(dangtintuyendung);
        }

        // get danh sách công việc mà ứng viên đã lưu
        [HttpGet("luucongviec/{id}")]
        public async Task<ActionResult<IEnumerable<Dangtintuyendung>>> GetLuuCongviec(int id)
        {
            var dangtintuyendung = await (from s in _context.Dangtintuyendung
                                          join luu in _context.Luucongviec on s.IdTintuyendung equals luu.IdTintuyendung
                                          where luu.IdUngvien.Equals(id)
                                          select new
                                          {
                                              s.IdTintuyendung,
                                              s.Tieude,
                                              s.Chucdanh,
                                              s.Minluong,
                                              s.Maxluong,
                                              s.Thuongluong,
                                              s.Trinhdotuyendung,
                                              s.Ngayhethan,
                                              luu.Ngayluu,
                                          }).Distinct().ToListAsync();
            return Ok(dangtintuyendung);
        }


        private bool UngvienExists(int id)
        {
            return _context.Ungvien.Any(e => e.IdUngvien == id);
        }
    }
}
