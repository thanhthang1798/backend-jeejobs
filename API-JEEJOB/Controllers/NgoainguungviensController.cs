﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using API_JEEJOB.DBEntities;

namespace API_JEEJOB.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NgoainguungviensController : ControllerBase
    {
        private readonly JEEJOBContext _context;

        public NgoainguungviensController(JEEJOBContext context)
        {
            _context = context;
        }

        // GET: api/Ngoainguungviens
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Ngoainguungvien>>> GetNgoainguungvien()
        {
            var ngoaingu = await (from s in _context.Ngoainguungvien
                                  join q in _context.Ngoaingu on s.IdNgoaingu equals q.IdNgoaingu
                                  select new
                                  {
                                      s.IdUngvien,
                                      s.Coban,
                                      s.Doc,
                                      s.Nghe,
                                      s.Noi,
                                      s.Viet,
                                      q.Tenngoaingu,
                                      s.Ghichu,
                                      s.IdNgoaingu

                                  }).ToListAsync();
            if (ngoaingu == null)
            {
                return NotFound();
            }

            return Ok(ngoaingu);
        }

        // GET: api/Ngoainguungviens/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Ngoainguungvien>> GetNgoainguungvien(int id)
        {
            var ngoaingu = await (from s in _context.Ngoainguungvien
                                  join q in _context.Ngoaingu on s.IdNgoaingu equals q.IdNgoaingu
                                  where s.IdNgoaingu.Equals(id)
                                  select new
                                  {
                                      s.IdUngvien,
                                      s.Coban,
                                      s.Doc,
                                      s.Nghe,
                                      s.Noi,
                                      s.Viet,
                                      q.Tenngoaingu,
                                      s.Ghichu,
                                      s.IdNgoaingu

                                  }).ToListAsync();
            if (ngoaingu == null)
            {
                return NotFound();
            }

            return Ok(ngoaingu);
        }

        //getngoainguungvien theo id ung vien
        [HttpGet("ungvien/{id}")]
        public async Task<ActionResult<Ngoainguungvien>> GetNgoainguTheoUV(int id)
        {
            var ngoaingu = await (from s in _context.Ngoainguungvien
                                  join q in _context.Ngoaingu on s.IdNgoaingu equals q.IdNgoaingu
                                  where s.IdUngvien.Equals(id)
                                  select new
                                  {
                                      s.IdUngvien,
                                      s.Coban,
                                      s.Doc,
                                      s.Nghe,
                                      s.Noi,
                                      s.Viet,
                                      q.Tenngoaingu,
                                      s.Ghichu,
                                      s.IdNgoaingu

                                  }).ToListAsync();
            if (ngoaingu == null)
            {
                return NotFound();
            }

            return Ok(ngoaingu);
        }
        // PUT: api/Ngoainguungviens/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}&&{key}")]
        public async Task<IActionResult> PutNgoainguungvien(int id,int key, Ngoainguungvien ngoainguungvien)
        {
            if (id != ngoainguungvien.IdNgoaingu && key != ngoainguungvien.IdUngvien)
            {
                return BadRequest();
            }

            _context.Entry(ngoainguungvien).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!NgoainguungvienExists(id,key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetNgoainguungvien", new { id = ngoainguungvien.IdNgoaingu }, ngoainguungvien);
        }

        // POST: api/Ngoainguungviens
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Ngoainguungvien>> PostNgoainguungvien(Ngoainguungvien ngoainguungvien)
        {
            _context.Ngoainguungvien.Add(ngoainguungvien);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (NgoainguungvienExists(ngoainguungvien.IdNgoaingu,ngoainguungvien.IdUngvien))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetNgoainguungvien", new { id = ngoainguungvien.IdNgoaingu }, ngoainguungvien);
        }

        // DELETE: api/Ngoainguungviens/5
        [HttpDelete("{id}&&{key}")]
        public async Task<ActionResult<Ngoainguungvien>> DeleteNgoainguungvien(int id,int key)
        {
            var ngoainguungvien = await _context.Ngoainguungvien.FirstOrDefaultAsync(x => (x.IdNgoaingu == id && x.IdUngvien == key));
            if (ngoainguungvien == null)
            {
                return NotFound();
            }

            _context.Ngoainguungvien.Remove(ngoainguungvien);
            await _context.SaveChangesAsync();

            return ngoainguungvien;
        }

        private bool NgoainguungvienExists(int id,int key)
        {
            return _context.Ngoainguungvien.Any(e => e.IdNgoaingu == id && e.IdUngvien == key);
        }
    }
}
