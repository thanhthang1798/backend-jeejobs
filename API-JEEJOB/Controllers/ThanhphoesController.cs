﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using API_JEEJOB.DBEntities;
using API_JEEJOB.Models;

namespace API_JEEJOB.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ThanhphoesController : ControllerBase
    {
        private readonly JEEJOBContext _context;

        public ThanhphoesController(JEEJOBContext context)
        {
            _context = context;
        }

        // GET: api/Thanhphoes
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Thanhpho>>> GetThanhpho()
        {
            var thanhpho = _context.Thanhpho.FromSqlRaw("SELECT ID_THANHPHO, TENTHANHPHO FROM THANHPHO ").ToList();
            return thanhpho;

        }

        // GET: api/Thanhphoes/5-----get quận từ thành phố
        [HttpGet("{id}")]
        public async Task<ActionResult<Thanhpho>> GetThanhpho(int id)
        {
            var quan = await (from s in _context.Thanhpho
                              where s.IdThanhpho.Equals(id)
                              select new
                              {
                                  s.IdThanhpho,
                                  s.Tenthanhpho
                              }).ToListAsync();
            if (quan == null)
            {
                return NotFound();
            }

            return Ok(quan);
        }

        [HttpGet("soluongtin")]
        public List<ThanhPhoDTO> Getsoluong()
        {
            var soluong = (from ct in _context.Thanhpho
                           select new ThanhPhoDTO
                           {
                               IdThanhpho = ct.IdThanhpho,
                               Tenthanhpho = ct.Tenthanhpho,
                               Soluongtin = 0
                           }).ToList();
            var x = (from td in _context.Dangtintuyendung
                     where td.Ngayhethan > (DateTime.Now).AddDays(-1) && td.Isdelete.Equals(false)
                     select new { 
                        td.IdTintuyendung,
                        td.IdDiadiemlamviecNavigation.IdThanhpho
                     });

            foreach(var item in x){

                foreach(var tp in soluong)
                {
                    if(item.IdThanhpho == tp.IdThanhpho)
                    {
                        tp.Soluongtin++;
                    }
                }
            }

            var data = from s in soluong orderby s.Soluongtin descending select s;
            return data.ToList();
        }
        // GET: api/Thanhphoes/chitiet/5
        [HttpGet("chitiet/{id}")]
        public async Task<ActionResult<Thanhpho>> GetChitietThanhpho(int id)
        {
            var quan = await (from s in _context.Thanhpho
                              where s.IdThanhpho.Equals(id)
                              select new
                              {
                                  s.IdThanhpho,
                                  s.Tenthanhpho
                              }).ToListAsync();
            if (quan == null)
            {
                return NotFound();
            }

            return Ok(quan);
        }

        // PUT: api/Thanhphoes/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutThanhpho(int id, Thanhpho thanhpho)
        {
            if (id != thanhpho.IdThanhpho)
            {
                return BadRequest();
            }

            _context.Entry(thanhpho).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ThanhphoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Thanhphoes
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Thanhpho>> PostThanhpho(Thanhpho thanhpho)
        {
            _context.Thanhpho.Add(thanhpho);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetThanhpho", new { id = thanhpho.IdThanhpho }, thanhpho);
        }

        // DELETE: api/Thanhphoes/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Thanhpho>> DeleteThanhpho(int id)
        {
            var thanhpho = await _context.Thanhpho.FindAsync(id);
            if (thanhpho == null)
            {
                return NotFound();
            }

            _context.Thanhpho.Remove(thanhpho);
            await _context.SaveChangesAsync();

            return thanhpho;
        }

        private bool ThanhphoExists(int id)
        {
            return _context.Thanhpho.Any(e => e.IdThanhpho == id);
        }
    }
}
