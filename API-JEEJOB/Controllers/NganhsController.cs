﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using API_JEEJOB.DBEntities;
using API_JEEJOB.Models;

namespace API_JEEJOB.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NganhsController : ControllerBase
    {
        private readonly JEEJOBContext _context;

        public NganhsController(JEEJOBContext context)
        {
            _context = context;
        }

        // GET: api/Nganhs
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Nganh>>> GetNganh()
        {
            var nganh = await (from s in _context.Nganh

                                     select new
                                     {
                                         s.IdNganh,
                                         s.Tennganh
                                     }).ToListAsync();
            return Ok(nganh);
        }

        // GET: api/Nganhs/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Nganh>> GetNganh(int id)
        {
            var nganh = await (from s in _context.Nganh
                               where s.IdNganh.Equals(id)
                               select new
                               {
                                   s.IdNganh,
                                   s.Tennganh
                               }).ToListAsync();
            if (nganh == null)
            {
                return NotFound();
            }

            return Ok(nganh);
        }
        // GET: api/Nganhs/5
        [HttpGet("getSoluong")]
        public List<NganhDTO> GetNganhCoSoluong(int id)
        {
            var nganh = (from s in _context.Nganh
                               select new NganhDTO
                               {
                                   IdNganh = s.IdNganh,
                                   Tennganh = s.Tennganh,
                                   Soluongtin = 0
                               }).ToList();

            var x = (from td in _context.Dangtintuyendung
                     where td.Ngayhethan > (DateTime.Now).AddDays(-1) && td.Isdelete.Equals(false)
                     select new
                     {
                         td.Nganhtuyendung,
                         td.IdTintuyendung,
                         td.IdDiadiemlamviecNavigation.IdThanhpho
                     });

            foreach (var item in x)
            {
                foreach (var tp in nganh)
                {
                    foreach(var y in item.Nganhtuyendung)
                    {
                        if (y.IdNganh == tp.IdNganh)
                        {
                            tp.Soluongtin++;
                        }
                    }
                }
            }

            var data = from s in nganh orderby s.Soluongtin descending select s;
            return data.ToList();
        }
        // GET: api/Nganhs/baidang/1
        [HttpGet("baidang/{id}")]
        public async Task<ActionResult<Nganh>> GetNganhCongty(int id)
        {
            var nganh = await (from s in _context.Nganh
                               select new
                               {
                                   s.IdNganh,
                                   s.Tennganh
                               }).Distinct().ToListAsync();
            if (nganh == null)
            {
                return NotFound();
            }

            return Ok(nganh);
        }
        // GET: api/Nganhs/ungvien/1
        [HttpGet("ungvien/{id}")]
        public async Task<ActionResult<Nganh>> GetNganhungvien(int id)
        {
            var nganh = await (from s in _context.Nganh
                               select new
                               {
                                   s.IdNganh,
                                   s.Tennganh
                               }).Distinct().ToListAsync();
            if (nganh == null)
            {
                return NotFound();
            }

            return Ok(nganh);
        }

        // PUT: api/Nganhs/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutNganh(int id, Nganh nganh)
        {
            if (id != nganh.IdNganh)
            {
                return BadRequest();
            }

            _context.Entry(nganh).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!NganhExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Nganhs
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Nganh>> PostNganh(Nganh nganh)
        {
            _context.Nganh.Add(nganh);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetNganh", new { id = nganh.IdNganh }, nganh);
        }

        // DELETE: api/Nganhs/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Nganh>> DeleteNganh(int id)
        {
            var nganh = await _context.Nganh.FindAsync(id);
            if (nganh == null)
            {
                return NotFound();
            }

            _context.Nganh.Remove(nganh);
            await _context.SaveChangesAsync();

            return nganh;
        }

        private bool NganhExists(int id)
        {
            return _context.Nganh.Any(e => e.IdNganh == id);
        }
    }
}
