﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using API_JEEJOB.DBEntities;

namespace API_JEEJOB.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class KynangungviensController : ControllerBase
    {
        private readonly JEEJOBContext _context;

        public KynangungviensController(JEEJOBContext context)
        {
            _context = context;
        }

        // GET: api/Kynangungviens
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Kynangungvien>>> GetKynangungvien()
        {
            return await _context.Kynangungvien.ToListAsync();
        }

        // GET: api/Kynangungviens/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Kynangungvien>> GetKynangungvien(int id)
        {
            var kynangungvien = await _context.Kynangungvien.FindAsync(id);

            if (kynangungvien == null)
            {
                return NotFound();
            }

            return kynangungvien;
        }
        
        // GET: api/Kynangungviens/5
        [HttpGet("ungvien/{id}")]
        public async Task<ActionResult<Kynangungvien>> Chitiet(int id)
        {
            var kynangungvien = _context.Kynangungvien.Where(x=>x.IdUngvien==id).ToList();

            if (kynangungvien == null)
            {
                return NotFound();
            }

            return Ok(kynangungvien);
        }

        // PUT: api/Kynangungviens/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutKynangungvien(int id, Kynangungvien kynangungvien)
        {
            if (id != kynangungvien.IdKynangcanhan)
            {
                return BadRequest();
            }

            _context.Entry(kynangungvien).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!KynangungvienExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Kynangungviens
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Kynangungvien>> PostKynangungvien(Kynangungvien kynangungvien)
        {
            _context.Kynangungvien.Add(kynangungvien);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetKynangungvien", new { id = kynangungvien.IdKynangcanhan }, kynangungvien);
        }

        // DELETE: api/Kynangungviens/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Kynangungvien>> DeleteKynangungvien(int id)
        {
            var kynangungvien = await _context.Kynangungvien.FindAsync(id);
            if (kynangungvien == null)
            {
                return NotFound();
            }

            _context.Kynangungvien.Remove(kynangungvien);
            await _context.SaveChangesAsync();

            return kynangungvien;
        }

        private bool KynangungvienExists(int id)
        {
            return _context.Kynangungvien.Any(e => e.IdKynangcanhan == id);
        }
    }
}
