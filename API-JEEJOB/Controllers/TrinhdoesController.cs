﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using API_JEEJOB.DBEntities;

namespace API_JEEJOB.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TrinhdoesController : ControllerBase
    {
        private readonly JEEJOBContext _context;

        public TrinhdoesController(JEEJOBContext context)
        {
            _context = context;
        }

        // GET: api/Trinhdoes
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Trinhdo>>> GetTrinhdo()
        {
            var trinhdo = await (from s in _context.Trinhdo

                                     select new
                                     {
                                         s.IdTrinhdo,
                                         s.Tentrinhdo,
                                         s.Giatri
                                     }).ToListAsync();
            return Ok(trinhdo);
        }

        // GET: api/Trinhdoes/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Trinhdo>> GetTrinhdo(int id)
        {
            var trinhdo = await (from s in _context.Trinhdo

                                 select new
                                 {
                                     s.IdTrinhdo,
                                     s.Tentrinhdo,
                                     s.Giatri
                                 }).ToListAsync();
            if (trinhdo == null)
            {
                return NotFound();
            }

            return Ok(trinhdo);
        }

        // PUT: api/Trinhdoes/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTrinhdo(int id, Trinhdo trinhdo)
        {
            if (id != trinhdo.IdTrinhdo)
            {
                return BadRequest();
            }

            _context.Entry(trinhdo).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TrinhdoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Trinhdoes
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Trinhdo>> PostTrinhdo(Trinhdo trinhdo)
        {
            _context.Trinhdo.Add(trinhdo);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetTrinhdo", new { id = trinhdo.IdTrinhdo }, trinhdo);
        }

        // DELETE: api/Trinhdoes/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Trinhdo>> DeleteTrinhdo(int id)
        {
            var trinhdo = await _context.Trinhdo.FindAsync(id);
            if (trinhdo == null)
            {
                return NotFound();
            }

            _context.Trinhdo.Remove(trinhdo);
            await _context.SaveChangesAsync();

            return trinhdo;
        }

        private bool TrinhdoExists(int id)
        {
            return _context.Trinhdo.Any(e => e.IdTrinhdo == id);
        }
    }
}
