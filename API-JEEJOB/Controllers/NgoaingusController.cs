﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using API_JEEJOB.DBEntities;

namespace API_JEEJOB.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NgoaingusController : ControllerBase
    {
        private readonly JEEJOBContext _context;

        public NgoaingusController(JEEJOBContext context)
        {
            _context = context;
        }

        // GET: api/Ngoaingus
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Ngoaingu>>> GetNgoaingu()
        {
            return await _context.Ngoaingu.ToListAsync();
        }

        // GET: api/Ngoaingus/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Ngoaingu>> GetNgoaingu(int id)
        {
            var ngoaingu = await _context.Ngoaingu.FindAsync(id);

            if (ngoaingu == null)
            {
                return NotFound();
            }

            return ngoaingu;
        }

        // PUT: api/Ngoaingus/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutNgoaingu(int id, Ngoaingu ngoaingu)
        {
            if (id != ngoaingu.IdNgoaingu)
            {
                return BadRequest();
            }

            _context.Entry(ngoaingu).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!NgoainguExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Ngoaingus
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Ngoaingu>> PostNgoaingu(Ngoaingu ngoaingu)
        {
            _context.Ngoaingu.Add(ngoaingu);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetNgoaingu", new { id = ngoaingu.IdNgoaingu }, ngoaingu);
        }

        // DELETE: api/Ngoaingus/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Ngoaingu>> DeleteNgoaingu(int id)
        {
            var ngoaingu = await _context.Ngoaingu.FindAsync(id);
            if (ngoaingu == null)
            {
                return NotFound();
            }

            _context.Ngoaingu.Remove(ngoaingu);
            await _context.SaveChangesAsync();

            return ngoaingu;
        }

        private bool NgoainguExists(int id)
        {
            return _context.Ngoaingu.Any(e => e.IdNgoaingu == id);
        }
    }
}
