﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using API_JEEJOB.DBEntities;

namespace API_JEEJOB.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PhucloisController : ControllerBase
    {
        private readonly JEEJOBContext _context;

        public PhucloisController(JEEJOBContext context)
        {
            _context = context;
        }

        // GET: api/Phuclois
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Phucloi>>> GetPhucloi()
        {
            return await _context.Phucloi.ToListAsync();
        }

        // GET: api/Phuclois/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Phucloi>> GetPhucloi(int id)
        {
            var phucloi = await _context.Phucloi.FindAsync(id);

            if (phucloi == null)
            {
                return NotFound();
            }

            return phucloi;
        }

        // PUT: api/Phuclois/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPhucloi(int id, Phucloi phucloi)
        {
            if (id != phucloi.IdPhucloi)
            {
                return BadRequest();
            }

            _context.Entry(phucloi).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PhucloiExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Phuclois
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Phucloi>> PostPhucloi(Phucloi phucloi)
        {
            _context.Phucloi.Add(phucloi);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetPhucloi", new { id = phucloi.IdPhucloi }, phucloi);
        }

        // DELETE: api/Phuclois/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Phucloi>> DeletePhucloi(int id)
        {
            var phucloi = await _context.Phucloi.FindAsync(id);
            if (phucloi == null)
            {
                return NotFound();
            }

            _context.Phucloi.Remove(phucloi);
            await _context.SaveChangesAsync();

            return phucloi;
        }

        private bool PhucloiExists(int id)
        {
            return _context.Phucloi.Any(e => e.IdPhucloi == id);
        }
    }
}
