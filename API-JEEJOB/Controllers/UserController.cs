﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using API_JEEJOB.DBEntities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using System.Collections.Generic;
using API_JEEJOB.Utils;
using System.Linq;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Http;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using API_JEEJOB.Models;

namespace API_JEEJOB.Controllers
{

    public class UserController : ControllerBase
    {
        public IConfiguration _configuration;
        private readonly JEEJOBContext _context;
        private ResponeResult responeResult = new ResponeResult();
        public UserController(IConfiguration config, JEEJOBContext context)
        {
            _configuration = config;
            _context = context;
        }

        #region API Methods login QUẢN TRỊ VIÊN

        [AllowAnonymous]
        [Route("api/qtvlogin")]
        [HttpPost]
        public ResponeResult Login([FromBody] Quantrivien loginModel)
        {

            try
            {
                if (loginModel == null)
                {
                    AddResponeError(ref responeResult, "", "Thông tin không hợp lệ! Vui lòng kiểm tra lại!");
                    return responeResult;
                }
                var dataresult = this.GetUser(loginModel.Email, loginModel.Matkhau);

                if (dataresult != null)
                {
                    // Prepare info for create Access Token
                    var tokeOptions = this.GenerateTokenOptionQTV(dataresult);

                    Session _session = this.GetSessionQTV(dataresult.IdQtv);
                    HttpContext.Session.Set(Utils.Constant.SESSION_NAME, ToByteArray<Utils.Session>(_session));

                    // Create Access Token
                    var tokenString = new JwtSecurityTokenHandler().WriteToken(tokeOptions);

                    // Save a token into session object
                    HttpContext.Session.SetString(Utils.Constant.ACCESSTOKEN, tokenString);

                    // Return a brief user info for client
                    responeResult.RepData = new object[] { dataresult };
                }
                else
                {
                    responeResult = new ResponeResult
                    { IsOk = false, MessageText = "Tên đăng nhập hoặc mật khẩu không chính xác!" };
                }

                return responeResult;
            }
            catch (Exception ex)
            {
                AddResponeError(ref responeResult, "", ex.ToString());
                return responeResult;
            }
        }



        private Quantrivien GetUser(string email, string password)
        {
            return _context.Quantrivien.FirstOrDefault(x => (x.Email == email && x.Matkhau == password));
        }

        private JwtSecurityToken GenerateTokenOptionQTV(Quantrivien _qtv)
        {
            // Basic claims
            Claim claimUserData = new Claim(ClaimTypes.UserData, JsonConvert.SerializeObject(_qtv));
            Claim claimUserName = new Claim(ClaimTypes.Name, _qtv.Email);
            List<Claim> listClaims = new List<Claim>();
            listClaims.Add(claimUserData);
            listClaims.Add(claimUserName);

            // Get token expired info from Constant
            string TokenExpireConfig = Constant.TOKENEXPIRED;
            if (string.IsNullOrEmpty(TokenExpireConfig))
                TokenExpireConfig = "86400";

            var secretKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Constant.JWTENCRYPTKEY));
            var signinCredentials = new SigningCredentials(secretKey, SecurityAlgorithms.HmacSha256);
            var jwtSecurityToken = new JwtSecurityToken(
                issuer: Constant.ISUSER,
                audience: Constant.AUDIENCE,
                claims: listClaims,
                expires: DateTime.Now.AddSeconds(Convert.ToInt32(TokenExpireConfig)),
                signingCredentials: signinCredentials
            );


            return jwtSecurityToken;
        }


        private Utils.Session GetSessionQTV(int userID)
        {
            try
            {
                Utils.Session session = new Utils.Session();
                var user = _context.Quantrivien.FirstOrDefault(x => (x.IdQtv == userID));
                if (user == null)
                    return null;

                //session.User = user;
                session.UserID = user.IdQtv;
                session.IsAdmin = true;
                session.IsLogin = true;
                return session;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        #endregion

        #region API Methods login ỨNG VIÊN
        [AllowAnonymous]
        [Route("api/ungvienlogin")]
        [HttpPost]
        public ResponeResult UVLogin([FromBody] Ungvien loginModel)
        {
            try
            {
                if (loginModel == null)
                {
                    AddResponeError(ref responeResult, "", "Thông tin không hợp lệ! Vui lòng kiểm tra lại!");
                    return responeResult;
                }
                var dataresult = this.Getungvien(loginModel.Email, loginModel.Matkhau);

                if (dataresult != null)
                {
                    // Prepare info for create Access Token
                    var tokeOptions = this.GenerateTokenOptionUV(dataresult);

                    Session _session = this.GetSessionUV(dataresult.IdUngvien);
                    HttpContext.Session.Set(Utils.Constant.SESSION_NAME, ToByteArray<Utils.Session>(_session));

                    // Create Access Token
                    var tokenString = new JwtSecurityTokenHandler().WriteToken(tokeOptions);

                    // Save a token into session object
                    HttpContext.Session.SetString(Utils.Constant.ACCESSTOKEN, tokenString);

                    // Return a brief user info for client
                    responeResult.RepData = new object[] { dataresult };
                }
                else
                {
                    responeResult = new ResponeResult
                    { IsOk = false, MessageText = "Tên đăng nhập hoặc mật khẩu không chính xác!" };
                }

                return responeResult;
            }
            catch
            {
                AddResponeError(ref responeResult, "", "Thông tin không hợp lệ! Vui lòng kiểm tra lại!");
                return responeResult;
            }
        }



        private Ungvien Getungvien(string email, string password)
        {
            return _context.Ungvien.FirstOrDefault(x => (x.Email == email && x.Matkhau == password));
        }

        private JwtSecurityToken GenerateTokenOptionUV(Ungvien _ungvien)
        {
            // Basic claims
            Claim claimUserData = new Claim(ClaimTypes.UserData, JsonConvert.SerializeObject(_ungvien));
            Claim claimUserName = new Claim(ClaimTypes.Name, _ungvien.Email);
            List<Claim> listClaims = new List<Claim>();
            listClaims.Add(claimUserData);
            listClaims.Add(claimUserName);

            // Get token expired info from Constant
            string TokenExpireConfig = Constant.TOKENEXPIRED;
            if (string.IsNullOrEmpty(TokenExpireConfig))
                TokenExpireConfig = "86400";

            var secretKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Constant.JWTENCRYPTKEY));
            var signinCredentials = new SigningCredentials(secretKey, SecurityAlgorithms.HmacSha256);
            var jwtSecurityToken = new JwtSecurityToken(
                issuer: Constant.ISUSER,
                audience: Constant.AUDIENCE,
                claims: listClaims,
                expires: DateTime.Now.AddSeconds(Convert.ToInt32(TokenExpireConfig)),
                signingCredentials: signinCredentials
            );


            return jwtSecurityToken;
        }


        private Utils.Session GetSessionUV(int userID)
        {
            try
            {
                Utils.Session session = new Utils.Session();
                var user = _context.Ungvien.FirstOrDefault(x => (x.IdUngvien == userID));
                if (user == null)
                    return null;

                //session.User = user;
                session.UserID = user.IdUngvien;
                session.IsAdmin = false;
                session.IsLogin = true;
                return session;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        #endregion

        #region API Methods login NHÀ TUYỂN DỤNG

        [AllowAnonymous]
        [Route("api/nhatuyendunglogin")]
        [HttpPost]
        public ResponeResult NTDLogin([FromBody] Nhatuyendung loginModel)
        {
            try
            {
                if (loginModel == null)
                {
                    AddResponeError(ref responeResult, "", "Thông tin không hợp lệ! Vui lòng kiểm tra lại!");
                    return responeResult;
                }
                var dataresult = this.GetNTD(loginModel.Email, loginModel.Matkhau);

                if (dataresult != null)
                {
                    // Prepare info for create Access Token
                    var tokeOptions = this.GenerateTokenOptionNTD(dataresult);

                    Session _session = this.GetSessionNTD(dataresult.IdNhatuyendung);
                    HttpContext.Session.Set(Utils.Constant.SESSION_NAME, ToByteArray<Utils.Session>(_session));

                    // Create Access Token
                    var tokenString = new JwtSecurityTokenHandler().WriteToken(tokeOptions);

                    // Save a token into session object
                    HttpContext.Session.SetString(Utils.Constant.ACCESSTOKEN, tokenString);

                    // Return a brief user info for client
                    responeResult.RepData = new object[] { dataresult };
                }
                else
                {
                    responeResult = new ResponeResult
                    { IsOk = false, MessageText = "Tên đăng nhập hoặc mật khẩu không chính xác!" };
                }

                return responeResult;
            }
            catch
            {
                AddResponeError(ref responeResult, "", "Thông tin không hợp lệ! Vui lòng kiểm tra lại!");
                return responeResult;
            }
        }



        private Nhatuyendung GetNTD(string email, string password)
        {
            return _context.Nhatuyendung.FirstOrDefault(x => (x.Email == email && x.Matkhau == password));
        }

        private JwtSecurityToken GenerateTokenOptionNTD(Nhatuyendung _nhatuyendung)
        {
            // Basic claims
            Claim claimUserData = new Claim(ClaimTypes.UserData, JsonConvert.SerializeObject(_nhatuyendung));
            Claim claimUserName = new Claim(ClaimTypes.Name, _nhatuyendung.Email);
            List<Claim> listClaims = new List<Claim>();
            listClaims.Add(claimUserData);
            listClaims.Add(claimUserName);

            // Get token expired info from Constant
            string TokenExpireConfig = Constant.TOKENEXPIRED;
            if (string.IsNullOrEmpty(TokenExpireConfig))
                TokenExpireConfig = "86400";

            var secretKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Constant.JWTENCRYPTKEY));
            var signinCredentials = new SigningCredentials(secretKey, SecurityAlgorithms.HmacSha256);
            var jwtSecurityToken = new JwtSecurityToken(
                issuer: Constant.ISUSER,
                audience: Constant.AUDIENCE,
                claims: listClaims,
                expires: DateTime.Now.AddSeconds(Convert.ToInt32(TokenExpireConfig)),
                signingCredentials: signinCredentials
            );


            return jwtSecurityToken;
        }


        private Utils.Session GetSessionNTD(int userID)
        {
            try
            {
                Utils.Session session = new Utils.Session();
                var user = _context.Nhatuyendung.FirstOrDefault(x => (x.IdNhatuyendung == userID));
                if (user == null)
                    return null;

                //session.User = user;
                session.UserID = user.IdNhatuyendung;
                session.IsAdmin = false;
                session.IsLogin = true;
                return session;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        #endregion

        #region Logout
        [AllowAnonymous]
        [Route("api/logout")]
        [HttpPost]
        public ResponeResult Logout()
        {
            try
            {
                ResponeResult repData = new ResponeResult() { IsOk = true };
                HttpContext.Session.Clear();
                return repData;
            }
            catch (Exception ex)
            {
                return responeResult;
            }
        }
        #endregion

        #region Base setting

        [NonAction]
        public void AddResponeError(ref ResponeResult repData, string messageCode = "", string messageText = "", string messageError = "")
        {
            repData.IsOk = false;
            repData.MessageCode = messageCode;
            repData.MessageText = messageText;
            repData.MessageError = messageError;
        }

        [NonAction]
        public byte[] ToByteArray<T>(T obj)
        {
            if (obj == null)
                return null;
            BinaryFormatter bf = new BinaryFormatter();
            using (MemoryStream ms = new MemoryStream())
            {
                bf.Serialize(ms, obj);
                return ms.ToArray();
            }
        }


        #endregion

        #region
        [Route("api/kiemtraemail/{email}")]
        [HttpGet]
        public bool checkMail(string email)
        {
            var user = _context.Ungvien.Any(x => x.Email == email);
            if (user)
            {
                return true;
            }
            else
            {
                return _context.Nhatuyendung.Any(x => x.Email == email);
            }
        }




        #endregion


        #region Login social
        [AllowAnonymous]
        [Route("api/Loginsocial")]
        [HttpPost]
        public ResponeResult PostNganh([FromBody]UserSocial user)
        {
            var respon = new ResponeResult();
            var users = _context.Ungvien.FirstOrDefault(x => x.Email == user.email);
            bool isntd = _context.Nhatuyendung.Any(x => x.Email == user.email);
            if (isntd)
            {
                return respon = new ResponeResult
                { IsOk = false, MessageText = "Tài khoản đã đăng ký ở phần nhà tuyển dụng" };
            }
            if (users == null & user.email != null)
            {
                var ungvien = new Ungvien();
                ungvien.Email = user.email;
                ungvien.Matkhau = "123456";
                ungvien.Hinhanh = user.photoUrl;
                ungvien.Isdelete = false;
                ungvien.Ngaytao = DateTime.Now;
                ungvien.Luotxem = 0;

                string[] words = user.name.Split(' ');
                //ungvien.Ho = words.;
                for (int i = 0; i < words.Length - 1; i++)
                {
                    ungvien.Ten += words[i];
                }
                ungvien.Ho = words.Last().ToString();
                ungvien.Sociallogin = true;


                _context.Ungvien.Add(ungvien);
                _context.SaveChanges();
                return UVLogin(ungvien);
                //return CreatedAtAction("GetUngvien", new { id = ungvien.IdUngvien }, ungvien);
            }

            if (user == null)
            {
                return respon = new ResponeResult
                { IsOk = false, MessageText = "Đăng nhập thất bại" };
            }


            return UVLogin(users);
        }


        #endregion

        #region Login social NTD
        [AllowAnonymous]
        [Route("api/LoginsocialNTD")]
        [HttpPost]
        public ResponeResult NTDLogiinSocial([FromBody]UserSocial user)
        {
            var respon = new ResponeResult();
            var users = _context.Nhatuyendung.FirstOrDefault(x => x.Email == user.email);
            bool isuv = _context.Ungvien.Any(x => x.Email == user.email);
            if (isuv)
            {
                return respon = new ResponeResult
                { IsOk = false, MessageText = "Tài khoản đã đăng ký ở phần ứng viên" };
            }

            if (users == null & user.email != null)
            {
                var nhatuyendung = new Nhatuyendung();
                nhatuyendung.Email = user.email;
                nhatuyendung.Matkhau = "123456";
                nhatuyendung.Logo = user.photoUrl;
                nhatuyendung.Isdelete = false;
                nhatuyendung.Ngaydangky = DateTime.Now;
                nhatuyendung.Luotxem = 0;
                nhatuyendung.Sociallogin = true;
                _context.Nhatuyendung.Add(nhatuyendung);
                _context.SaveChanges();
                return NTDLogin(nhatuyendung);

                //return CreatedAtAction("GetUngvien", new { id = ungvien.IdUngvien }, ungvien);
            }

            return NTDLogin(users);
        }


        #endregion


    }
}