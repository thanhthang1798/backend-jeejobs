﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using API_JEEJOB.DBEntities;

namespace API_JEEJOB.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class KinhnghiemlamviecsController : ControllerBase
    {
        private readonly JEEJOBContext _context;

        public KinhnghiemlamviecsController(JEEJOBContext context)
        {
            _context = context;
        }

        // GET: api/Kinhnghiemlamviecs
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Kinhnghiemlamviec>>> GetKinhnghiemlamviec()
        {
            return await _context.Kinhnghiemlamviec.ToListAsync();
        }

        // GET: api/Kinhnghiemlamviecs/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Kinhnghiemlamviec>> GetKinhnghiemlamviec(int id)
        {
            var kinhnghiemlamviec = await _context.Kinhnghiemlamviec.FindAsync(id);

            if (kinhnghiemlamviec == null)
            {
                return NotFound();
            }

            return kinhnghiemlamviec;
        }
        
        // GET: api/Kinhnghiemlamviecs/ungvien/5
        [HttpGet("ungvien/{id}")]
        public async Task<ActionResult<Kinhnghiemlamviec>> GetKinhnghiemlamviecUV(int id)
        {
            var kinhnghiemlamviec = await (from s in _context.Kinhnghiemlamviec
                                           where s.IdUngvien.Equals(id)
                                          select new
                                          {
                                           s.Chucdanh,
                                           s.Chucvu,
                                           s.Congviechientai,
                                           s.Ghichu,
                                           s.IdKinhnghiemlamviec,
                                           s.IdUngvien,
                                           s.Tencongty,
                                           s.Thoigianbatdau,
                                           s.Thoigianketthuc,
                                          }).ToListAsync();

            if (kinhnghiemlamviec == null)
            {
                return NotFound();
            }

            return Ok(kinhnghiemlamviec);
        }

        // PUT: api/Kinhnghiemlamviecs/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}&{key}")]
        public async Task<IActionResult> PutKinhnghiemlamviec(int id,int key, Kinhnghiemlamviec kinhnghiemlamviec)
        {
            if (id != kinhnghiemlamviec.IdKinhnghiemlamviec && key!=kinhnghiemlamviec.IdUngvien)
            {
                return BadRequest();
            }

            _context.Entry(kinhnghiemlamviec).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!KinhnghiemlamviecExists(id,key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Kinhnghiemlamviecs
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Kinhnghiemlamviec>> PostKinhnghiemlamviec(Kinhnghiemlamviec kinhnghiemlamviec)
        {
            _context.Kinhnghiemlamviec.Add(kinhnghiemlamviec);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetKinhnghiemlamviec", new { id = kinhnghiemlamviec.IdKinhnghiemlamviec }, kinhnghiemlamviec);
        }

        // DELETE: api/Kinhnghiemlamviecs/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Kinhnghiemlamviec>> DeleteKinhnghiemlamviec(int id)
        {
            var kinhnghiemlamviec = await _context.Kinhnghiemlamviec.FindAsync(id);
            if (kinhnghiemlamviec == null)
            {
                return NotFound();
            }

            _context.Kinhnghiemlamviec.Remove(kinhnghiemlamviec);
            await _context.SaveChangesAsync();

            return kinhnghiemlamviec;
        }

        private bool KinhnghiemlamviecExists(int id,int key)
        {
            return _context.Kinhnghiemlamviec.Any(e => e.IdKinhnghiemlamviec == id && e.IdUngvien==key);
        }
    }
}
