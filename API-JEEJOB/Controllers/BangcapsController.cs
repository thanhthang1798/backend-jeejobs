﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using API_JEEJOB.DBEntities;

namespace API_JEEJOB.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BangcapsController : ControllerBase
    {
        private readonly JEEJOBContext _context;

        public BangcapsController(JEEJOBContext context)
        {
            _context = context;
        }

        // GET: api/Bangcaps
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Bangcap>>> GetBangcap()
        {
            var bangcap = await (from s in _context.Bangcap
                                 join td in _context.Trinhdo on s.IdTrinhdo equals td.IdTrinhdo
                                 select new
                                     {
                                         s.IdBangcap,
                                         s.IdUngvien,
                                         s.IdTrinhdo,
                                         s.Chuyennganh,
                                         s.Tentruong,
                                         s.Tenbangcap,
                                         s.Ngaybatdau,
                                         s.Ngayketthuc,
                                         td.Tentrinhdo,
                                         s.Ghichu
                                     }).ToListAsync();
            return Ok(bangcap);
        }

        // GET: api/Bangcaps/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Bangcap>> GetBangcap(int id)
        {
            var bangcap = await (from s in _context.Bangcap
                                 join td in _context.Trinhdo on s.IdTrinhdo equals td.IdTrinhdo
                                 where s.IdBangcap.Equals(id)
                                 select new
                                 {
                                     s.IdBangcap,
                                     s.IdUngvien,
                                     s.IdTrinhdo,
                                     s.Chuyennganh,
                                     s.Tentruong,
                                     s.Tenbangcap,
                                     s.Ngaybatdau,
                                     s.Ngayketthuc,
                                     td.Tentrinhdo,
                                     s.Ghichu
                                 }).ToListAsync();

            if (bangcap == null)
            {
                return NotFound();
            }

            return Ok(bangcap);
        }
        // GET: api/Bangcaps/ungvien/1
        [HttpGet("ungvien/{id}")]
        public async Task<ActionResult<Bangcap>> GetBangcapUV(int id)
        {
            var bangcap = await (from s in _context.Bangcap
                                 join td in _context.Trinhdo on s.IdTrinhdo equals td.IdTrinhdo
                                 where s.IdUngvien.Equals(id)
                                 select new
                                 {
                                     s.IdBangcap,
                                     s.IdUngvien,
                                     s.IdTrinhdo,
                                     s.Chuyennganh,
                                     s.Tentruong,
                                     s.Tenbangcap,
                                     s.Ngaybatdau,
                                     s.Ngayketthuc,
                                     td.Tentrinhdo,
                                     s.IdTrinhdoNavigation.Giatri,
                                     s.Ghichu
                                 }).ToListAsync();

            if (bangcap == null)
            {
                return NotFound();
            }

            return Ok(bangcap);
        }

        // PUT: api/Bangcaps/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutBangcap(int id, Bangcap bangcap)
        {
            if (id != bangcap.IdBangcap)
            {
                return BadRequest();
            }

            _context.Entry(bangcap).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BangcapExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Bangcaps
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Bangcap>> PostBangcap(Bangcap bangcap)
        {
            _context.Bangcap.Add(bangcap);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetBangcap", new { id = bangcap.IdBangcap }, bangcap);
        }

        // DELETE: api/Bangcaps/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Bangcap>> DeleteBangcap(int id)
        {
            var bangcap = await _context.Bangcap.FindAsync(id);
            if (bangcap == null)
            {
                return NotFound();
            }

            _context.Bangcap.Remove(bangcap);
            await _context.SaveChangesAsync();

            return bangcap;
        }

        private bool BangcapExists(int id)
        {
            return _context.Bangcap.Any(e => e.IdBangcap == id);
        }
    }
}
