﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using API_JEEJOB.DBEntities;

namespace API_JEEJOB.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoaicongviecsController : ControllerBase
    {
        private readonly JEEJOBContext _context;

        public LoaicongviecsController(JEEJOBContext context)
        {
            _context = context;
        }

        // GET: api/Loaicongviecs
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Loaicongviec>>> GetLoaicongviec()
        {
            var loaicongviec = await (from s in _context.Loaicongviec
                                     select new
                                     {
                                         s.IdLoaicongviec,
                                         s.Tenloaicongviec
                                     }).ToListAsync();
            return Ok(loaicongviec);
        }

        // GET: api/Loaicongviecs/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Loaicongviec>> GetLoaicongviec(int id)
        {
            var loaicongviec = await (from s in _context.Loaicongviec
                                      where s.IdLoaicongviec.Equals(id)
                                      select new
                                      {
                                          s.IdLoaicongviec,
                                          s.Tenloaicongviec
                                      }).ToListAsync();
            if (loaicongviec == null)
            {
                return NotFound();
            }

            return Ok(loaicongviec);
        }

        // PUT: api/Loaicongviecs/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutLoaicongviec(int id, Loaicongviec loaicongviec)
        {
            if (id != loaicongviec.IdLoaicongviec)
            {
                return BadRequest();
            }

            _context.Entry(loaicongviec).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LoaicongviecExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Loaicongviecs
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Loaicongviec>> PostLoaicongviec(Loaicongviec loaicongviec)
        {
            _context.Loaicongviec.Add(loaicongviec);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetLoaicongviec", new { id = loaicongviec.IdLoaicongviec }, loaicongviec);
        }

        // DELETE: api/Loaicongviecs/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Loaicongviec>> DeleteLoaicongviec(int id)
        {
            var loaicongviec = await _context.Loaicongviec.FindAsync(id);
            if (loaicongviec == null)
            {
                return NotFound();
            }

            _context.Loaicongviec.Remove(loaicongviec);
            await _context.SaveChangesAsync();

            return loaicongviec;
        }

        private bool LoaicongviecExists(int id)
        {
            return _context.Loaicongviec.Any(e => e.IdLoaicongviec == id);
        }
    }
}
