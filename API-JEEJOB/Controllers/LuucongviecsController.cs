﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using API_JEEJOB.DBEntities;

namespace API_JEEJOB.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LuucongviecsController : ControllerBase
    {
        private readonly JEEJOBContext _context;

        public LuucongviecsController(JEEJOBContext context)
        {
            _context = context;
        }

        // GET: api/Luucongviecs
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Luucongviec>>> GetLuucongviec()
        {
            return await _context.Luucongviec.ToListAsync();
        }

        [HttpGet("{id}&{idUngvien}")]
        public bool Check(int id, int idUngvien)
        {
            return _context.Luucongviec.Any(e => e.IdTintuyendung == id && e.IdUngvien == idUngvien);
        }



        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Luucongviec>> PostLuucongviec(Luucongviec luucongviec)
        {
            _context.Luucongviec.Add(luucongviec);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (LuucongviecExists(luucongviec.IdUngvien,luucongviec.IdTintuyendung))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return Ok(true);
        }

        // DELETE: api/Luucongviecs/5
        [HttpDelete("{id}&{idUngvien}")]
        public async Task<ActionResult<Luucongviec>> DeleteLuucongviec(int id,int idUngvien)
        {
            var luucongviec = await _context.Luucongviec.FirstOrDefaultAsync(x => x.IdTintuyendung == id && x.IdUngvien == idUngvien);
            if (luucongviec == null)
            {
                return NotFound();
            }

            _context.Luucongviec.Remove(luucongviec);
            await _context.SaveChangesAsync();

            return luucongviec;
        }

        private bool LuucongviecExists(int id,int idUngvien)
        {
            return _context.Luucongviec.Any(e => e.IdUngvien == id && e.IdTintuyendung == idUngvien);
        }
    }
}
