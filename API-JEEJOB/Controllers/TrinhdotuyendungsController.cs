﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using API_JEEJOB.DBEntities;

namespace API_JEEJOB.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TrinhdotuyendungsController : ControllerBase
    {
        private readonly JEEJOBContext _context;

        public TrinhdotuyendungsController(JEEJOBContext context)
        {
            _context = context;
        }

        // GET: api/Trinhdotuyendungs
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Trinhdotuyendung>>> GetTrinhdotuyendung()
        {
            return await _context.Trinhdotuyendung.ToListAsync();
        }

        // GET: api/Trinhdotuyendungs/5 get trinhf độ tuyển dụng theo id bài đăng
        [HttpGet("{id}")]
        public async Task<ActionResult<Trinhdotuyendung>> GetTrinhdotuyendung(int id)
        {
            var trinhdotuyendung = await (from s in _context.Trinhdotuyendung
                                          join td in _context.Trinhdo on s.IdTrinhdo equals td.IdTrinhdo
                                          where s.IdTuyendungNavigation.IdTintuyendung.Equals(id)
                                          select new
                                          {
                                             s.IdTrinhdo,
                                             td.Tentrinhdo
                                          }).ToListAsync();

            if (trinhdotuyendung == null)
            {
                return NotFound();
            }

            return Ok(trinhdotuyendung);
        }

        // PUT: api/Trinhdotuyendungs/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}&&{key}")]
        public async Task<IActionResult> PutTrinhdotuyendung(int id,int key, Trinhdotuyendung trinhdotuyendung)
        {
            if (id != trinhdotuyendung.IdTrinhdo && key != trinhdotuyendung.IdTuyendung)
            {
                return BadRequest();
            }

            _context.Entry(trinhdotuyendung).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TrinhdotuyendungExists(id,key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Trinhdotuyendungs
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Trinhdotuyendung>> PostTrinhdotuyendung(Trinhdotuyendung trinhdotuyendung)
        {
            _context.Trinhdotuyendung.Add(trinhdotuyendung);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (TrinhdotuyendungExists(trinhdotuyendung.IdTrinhdo,trinhdotuyendung.IdTuyendung))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetTrinhdotuyendung", new { id = trinhdotuyendung.IdTrinhdo }, trinhdotuyendung);
        }

        // DELETE: api/Trinhdotuyendungs/5
        [HttpDelete("{id}&&{key}")]
        public async Task<ActionResult<Trinhdotuyendung>> DeleteTrinhdotuyendung(int id,int key)
        {
            var trinhdotuyendung = await _context.Trinhdotuyendung.FirstOrDefaultAsync(x => (x.IdTrinhdo == id && x.IdTuyendung == key));
            if (trinhdotuyendung == null)
            {
                return NotFound();
            }

            _context.Trinhdotuyendung.Remove(trinhdotuyendung);
            await _context.SaveChangesAsync();

            return trinhdotuyendung;
        }

        private bool TrinhdotuyendungExists(int id,int key)
        {
            return _context.Trinhdotuyendung.Any(e => e.IdTrinhdo == id && e.IdTuyendung ==key);
        }
    }
}
