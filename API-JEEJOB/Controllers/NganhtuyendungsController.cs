﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using API_JEEJOB.DBEntities;

namespace API_JEEJOB.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NganhtuyendungsController : ControllerBase
    {
        private readonly JEEJOBContext _context;

        public NganhtuyendungsController(JEEJOBContext context)
        {
            _context = context;
        }

        // GET: api/Nganhtuyendungs
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Nganhtuyendung>>> GetNganhtuyendung()
        {
            return await _context.Nganhtuyendung.ToListAsync();
        }

        // GET: api/Nganhtuyendungs/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Nganhtuyendung>> GetNganhtuyendung(int id)
        {
            var nganhtuyendung = await (from s in _context.Nganhtuyendung
                                        where s.IdTintuyendung.Equals(id)
                               select new
                               {
                                   s.IdNganhNavigation.Tennganh,
                                   s.IdNganh,
                                   s.IdTintuyendung
                               }).Distinct().ToListAsync();
            if (nganhtuyendung == null)
            {
                return NotFound();
            }

            return Ok(nganhtuyendung);
        }

        // PUT: api/Nganhtuyendungs/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}&{idNganh}")]
        public async Task<IActionResult> PutNganhtuyendung(int id,int idNganh, Nganhtuyendung nganhtuyendung)
        {
            if (id != nganhtuyendung.IdTintuyendung && idNganh != nganhtuyendung.IdNganh)
            {
                return BadRequest();
            }

            _context.Entry(nganhtuyendung).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!NganhtuyendungExists(id, idNganh))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Nganhtuyendungs
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Nganhtuyendung>> PostNganhtuyendung(Nganhtuyendung nganhtuyendung)
        {
            _context.Nganhtuyendung.Add(nganhtuyendung);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (NganhtuyendungExists(nganhtuyendung.IdTintuyendung,nganhtuyendung.IdNganh))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetNganhtuyendung", new { id = nganhtuyendung.IdTintuyendung }, nganhtuyendung);
        }

        // DELETE: api/Nganhtuyendungs/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Nganhtuyendung>> DeleteNganhtuyendung(int id)
        {
            //var nganhtuyendung = await _context.Nganhtuyendung.FirstOrDefaultAsync(s => s.IdTintuyendung == id);
            var list = await (from s in _context.Nganhtuyendung
                                        where s.IdTintuyendung.Equals(id)
                                        select s).ToListAsync();
            int count = 0;
            //if (nganhtuyendung == null)
            //{
            //    return NotFound();
            //}

            if(list != null)
            {
                foreach (Nganhtuyendung item in list) // _list is an instance of List<string>
                {
                    count++;
                    _context.Nganhtuyendung.Remove(item);
                    await _context.SaveChangesAsync();
                }
            }
            else
            {
                return NotFound();
            }

            //_context.Nganhtuyendung.Remove(nganhtuyendung);
            //await _context.SaveChangesAsync();

            return Ok(list);
        }

        private bool NganhtuyendungExists(int id,int idNganh)
        {
            return _context.Nganhtuyendung.Any(e => e.IdTintuyendung == id && e.IdNganh== idNganh);
        }
    }
}
