﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using API_JEEJOB.DBEntities;

namespace API_JEEJOB.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NoilamviecmongmuonsController : ControllerBase
    {
        private readonly JEEJOBContext _context;

        public NoilamviecmongmuonsController(JEEJOBContext context)
        {
            _context = context;
        }

        // GET: api/Noilamviecmongmuons
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Noilamviecmongmuon>>> GetNoilamviecmongmuon()
        {
            return await _context.Noilamviecmongmuon.ToListAsync();
        }

        // GET: api/Noilamviecmongmuons/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Noilamviecmongmuon>> GetNoilamviecmongmuon(int id)
        {
            var noilamviecmongmuon = await (from s in _context.Noilamviecmongmuon
                                            where s.IdCongviecmongmuon.Equals(id)
                                            select new
                                            {
                                                s.IdThanhpho,
                                                s.IdThanhphoNavigation.Tenthanhpho
                                            }).Distinct().ToListAsync();

            if (noilamviecmongmuon == null)
            {
                return NotFound();
            }

            return Ok(noilamviecmongmuon);
        }

        // PUT: api/Noilamviecmongmuons/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}&{idThanhpho}")]
        public async Task<IActionResult> PutNoilamviecmongmuon(int id,int idThanhpho, Noilamviecmongmuon noilamviecmongmuon)
        {
            if (id != noilamviecmongmuon.IdCongviecmongmuon)
            {
                return BadRequest();
            }

            _context.Entry(noilamviecmongmuon).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!NoilamviecmongmuonExists(id, idThanhpho))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Noilamviecmongmuons
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Noilamviecmongmuon>> PostNoilamviecmongmuon(Noilamviecmongmuon noilamviecmongmuon)
        {
            _context.Noilamviecmongmuon.Add(noilamviecmongmuon);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (NoilamviecmongmuonExists(noilamviecmongmuon.IdCongviecmongmuon,noilamviecmongmuon.IdThanhpho))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetNoilamviecmongmuon", new { id = noilamviecmongmuon.IdCongviecmongmuon }, noilamviecmongmuon);
        }

        // DELETE: api/Noilamviecmongmuons/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Noilamviecmongmuon>> DeleteNoilamviecmongmuon(int id)
        {
            var list = await (from s in _context.Noilamviecmongmuon
                              where s.IdCongviecmongmuon.Equals(id)
                              select s).ToListAsync();
            int count = 0;

            if (list != null)
            {
                foreach (Noilamviecmongmuon item in list) // _list is an instance of List<string>
                {
                    count++;
                    _context.Noilamviecmongmuon.Remove(item);
                    await _context.SaveChangesAsync();
                }
            }
            else
            {
                return NotFound();
            }

            //_context.Nganhtuyendung.Remove(nganhtuyendung);
            //await _context.SaveChangesAsync();

            return Ok(list);
        }

        private bool NoilamviecmongmuonExists(int id,int idThanhpho)
        {
            return _context.Noilamviecmongmuon.Any(e => e.IdCongviecmongmuon == id && e.IdThanhpho == idThanhpho);
        }
    }
}
