﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using API_JEEJOB.DBEntities;
using System.Data;
using API_JEEJOB;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore.Internal;
using API_JEEJOB.Models;

namespace API_JEEJOB.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class DangtintuyendungsController : ControllerBase
    {
        private readonly JEEJOBContext _context;

        public DangtintuyendungsController(JEEJOBContext context)
        {
            _context = context;
        }

        // GET: api/Dangtintuyendungs
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Dangtintuyendung>>> GetDangtintuyendung(int? idUngvien)
        {
            var x = (DateTime.Now).AddDays(-1);
            var dangtintuyendung = (from s in _context.Dangtintuyendung
                                    join ntd in _context.Nhatuyendung on s.IdNhatuyendung equals ntd.IdNhatuyendung
                                    join q in _context.Thanhpho on s.IdDiadiemlamviecNavigation.IdThanhpho equals q.IdThanhpho
                                    where s.Isdelete.Equals(false) && s.Ngayhethan >= (DateTime.Now).AddDays(-1)
                                    orderby s.Ngaydang descending
                                    select new DangTinTuyenDungDTO
                                    {
                                        Logo = s.IdNhatuyendungNavigation.Logo,
                                        IdTintuyendung = s.IdTintuyendung,
                                        Tieude = s.Tieude,
                                        Chucdanh = s.Chucdanh,
                                        Capbac = s.Capbac,
                                        Soluong = s.Soluong,
                                        Motacongviec = s.Motacongviec,
                                        Yeucaucongviec = s.Yeucaucongviec,
                                        Yeucaukynang = s.Yeucaukynang,
                                        Minluong = s.Minluong,
                                        Maxluong = s.Maxluong,
                                        Thuongluong = s.Thuongluong,
                                        Ngaydang = s.Ngaydang,
                                        Ngayhethan = s.Ngayhethan,
                                        Gioitinh = s.Gioitinh,
                                        IdDiadiemlamviec = s.IdDiadiemlamviec,
                                        IdNhatuyendung = s.IdNhatuyendung,
                                        IdLoaicongviec = s.IdLoaicongviec,
                                        Kinhnghiem = s.Kinhnghiem,
                                        Emaillienhe = s.Emaillienhe,
                                        Sdtlienhe = s.Sdtlienhe,
                                        Nguoilienhe = s.Nguoilienhe,
                                        Tencongty = ntd.Tencongty,
                                        IdDiadiemlamviecNavigation = s.IdDiadiemlamviecNavigation,
                                        Trinhdotuyendung = s.Trinhdotuyendung,
                                        Phucloituyendung = s.Phucloituyendung,
                                        Tenthanhpho = s.IdDiadiemlamviecNavigation.IdThanhphoNavigation.Tenthanhpho,
                                        IdThanhpho = q.IdThanhpho,
                                        Nganhtuyendung = s.Nganhtuyendung,
                                        Isdelete = s.Isdelete,
                                        Douutien = s.Douutien,
                                        Luotxem = s.Luotxem,
                                        Datasort = 0,
                                        Trangthai = s.Trangthai
                                    });
            var data = dangtintuyendung.ToList();


            if (idUngvien > 0)
            {
                var xxx = _context.Congviecmongmuon.Any(x => x.IdUngvien == idUngvien);
                if (xxx)
                {
                    var tintt = (from user in _context.Congviecmongmuon
                                 join khuvuc in _context.Noilamviecmongmuon on user.IdCongviecmongmuon equals khuvuc.IdCongviecmongmuon
                                 where user.IdUngvien.Equals(idUngvien)
                                 select new
                                 {
                                     user.Noilamviecmongmuon,
                                     user.Nganhnghemongmuon
                                 }).ToList().Last();

                    foreach (var tin in data)
                    {
                        foreach (var tp in tintt.Noilamviecmongmuon)
                        {
                            if (tp.IdThanhpho == tin.IdDiadiemlamviecNavigation.IdThanhpho)
                            {
                                tin.Datasort++;

                            }
                        }

                        foreach (var tp in tintt.Nganhnghemongmuon)
                        {
                            foreach (var nn in tin.Nganhtuyendung)
                            {
                                if (tp.IdNganh == nn.IdNganh)
                                {
                                    tin.Datasort++;

                                }
                            }

                        }
                    }
                }
            }

            var DT = (from da in data
                      orderby da.Datasort descending
                      select da);


            return Ok(DT);

        }

        // GET: api/Dangtintuyendungs
        [HttpGet("admin")]
        public List<DangTinTuyenDungDTO> AdminGetTin()
        {
            var dangtintuyendung = (from s in _context.Dangtintuyendung
                                    join ntd in _context.Nhatuyendung on s.IdNhatuyendung equals ntd.IdNhatuyendung
                                    join q in _context.Thanhpho on s.IdDiadiemlamviecNavigation.IdThanhpho equals q.IdThanhpho
                                    where s.Isdelete.Equals(false)
                                    orderby s.Ngaydang descending 
                                    select new DangTinTuyenDungDTO
                                    {
                                        Logo = s.IdNhatuyendungNavigation.Logo,
                                        IdTintuyendung = s.IdTintuyendung,
                                        Tieude = s.Tieude,
                                        Chucdanh = s.Chucdanh,
                                        Capbac = s.Capbac,
                                        Soluong = s.Soluong,
                                        Motacongviec = s.Motacongviec,
                                        Yeucaucongviec = s.Yeucaucongviec,
                                        Yeucaukynang = s.Yeucaukynang,
                                        Minluong = s.Minluong,
                                        Maxluong = s.Maxluong,
                                        Thuongluong = s.Thuongluong,
                                        Ngaydang = s.Ngaydang,
                                        Ngayhethan = s.Ngayhethan,
                                        Gioitinh = s.Gioitinh,
                                        IdDiadiemlamviec = s.IdDiadiemlamviec,
                                        IdNhatuyendung = s.IdNhatuyendung,
                                        IdLoaicongviec = s.IdLoaicongviec,
                                        Kinhnghiem = s.Kinhnghiem,
                                        Emaillienhe = s.Emaillienhe,
                                        Sdtlienhe = s.Sdtlienhe,
                                        Nguoilienhe = s.Nguoilienhe,
                                        Tencongty = ntd.Tencongty,
                                        IdDiadiemlamviecNavigation = s.IdDiadiemlamviecNavigation,
                                        Trinhdotuyendung = s.Trinhdotuyendung,
                                        Phucloituyendung = s.Phucloituyendung,
                                        Tenthanhpho = s.IdDiadiemlamviecNavigation.IdThanhphoNavigation.Tenthanhpho,
                                        IdThanhpho = q.IdThanhpho,
                                        Nganhtuyendung = s.Nganhtuyendung,
                                        Isdelete = s.Isdelete,
                                        Douutien = s.Douutien,
                                        Luotxem = s.Luotxem,
                                        Datasort = 0,
                                        Trangthai = s.Trangthai
                                    });
            var data = dangtintuyendung.ToList();

            return data;

        }

        [HttpGet("soluong")]
        public int Soluong(int? idNganh, int? idThanhpho, string? chucvu, int? idUngvien, int pageSize, int pageNum, bool luong, decimal? minluong, decimal? maxluong, string? stringSearch)
        {

            var dangtintuyendung = (from s in _context.Dangtintuyendung
                                    join ntd in _context.Nhatuyendung on s.IdNhatuyendung equals ntd.IdNhatuyendung
                                    join q in _context.Thanhpho on s.IdDiadiemlamviecNavigation.IdThanhpho equals q.IdThanhpho
                                    where s.Isdelete.Equals(false) && s.Ngayhethan >= (DateTime.Now).AddDays(-1) && s.Trangthai.Equals("Đã duyệt")
                                    orderby s.IdTintuyendung descending
                                    select new DangTinTuyenDungDTO
                                    {
                                        Logo = s.IdNhatuyendungNavigation.Logo,
                                        IdTintuyendung = s.IdTintuyendung,
                                        Tieude = s.Tieude,
                                        Chucdanh = s.Chucdanh,
                                        Capbac = s.Capbac,
                                        Soluong = s.Soluong,
                                        Motacongviec = s.Motacongviec,
                                        Yeucaucongviec = s.Yeucaucongviec,
                                        Yeucaukynang = s.Yeucaukynang,
                                        Minluong = s.Minluong,
                                        Maxluong = s.Maxluong,
                                        Thuongluong = s.Thuongluong,
                                        Ngaydang = s.Ngaydang,
                                        Ngayhethan = s.Ngayhethan,
                                        Gioitinh = s.Gioitinh,
                                        IdDiadiemlamviec = s.IdDiadiemlamviec,
                                        IdNhatuyendung = s.IdNhatuyendung,
                                        IdLoaicongviec = s.IdLoaicongviec,
                                        Kinhnghiem = s.Kinhnghiem,
                                        Emaillienhe = s.Emaillienhe,
                                        Sdtlienhe = s.Sdtlienhe,
                                        Nguoilienhe = s.Nguoilienhe,
                                        Tencongty = ntd.Tencongty,
                                        IdDiadiemlamviecNavigation = s.IdDiadiemlamviecNavigation,
                                        Trinhdotuyendung = s.Trinhdotuyendung,
                                        Phucloituyendung = s.Phucloituyendung,
                                        Tenthanhpho = s.IdDiadiemlamviecNavigation.IdThanhphoNavigation.Tenthanhpho,
                                        IdThanhpho = q.IdThanhpho,
                                        IdLoaicongviecNavigation = s.IdLoaicongviecNavigation,
                                        Nganhtuyendung = s.Nganhtuyendung,
                                        Isdelete = s.Isdelete,
                                        Douutien = s.Douutien,
                                        Luotxem = s.Luotxem,
                                        Datasort = 0
                                    });

            if (idThanhpho != null && idThanhpho > 0)
            {
                dangtintuyendung = dangtintuyendung.Where(x => x.IdThanhpho == idThanhpho);

            }
            if (idNganh != null && idNganh > 0)
            {
                dangtintuyendung = from tin in dangtintuyendung
                                   join ng in _context.Nganhtuyendung on tin.IdTintuyendung equals ng.IdTintuyendung
                                   where ng.IdNganh.Equals(idNganh)
                                   select tin;
                //.Where(x => x.IdThanhpho == idThanhpho);
            }
            if (chucvu != null && (chucvu == "Quản lý" || chucvu == "Nhân viên"))
            {
                dangtintuyendung = from tin in dangtintuyendung
                                   where tin.Capbac.Equals(chucvu)
                                   select tin;
            }

            if (minluong > 0)
            {
                dangtintuyendung = from tin in dangtintuyendung
                                   where tin.Maxluong > minluong
                                   select tin;
            }

            if (maxluong > 0)
            {
                dangtintuyendung = from tin in dangtintuyendung
                                   where tin.Minluong < maxluong || tin.Minluong == null
                                   select tin;
            }

            if (stringSearch != null)
            {
                dangtintuyendung = from tin in dangtintuyendung
                                   where tin.Tieude.Contains(stringSearch)
                                   select tin;
            }


            return dangtintuyendung.Count();
        }


        [HttpGet("search")]
        public async Task<ActionResult<IEnumerable<Dangtintuyendung>>> SearchTTD(int? idNganh, int? idThanhpho, string? chucvu, int? idUngvien, int pageSize, int pageNum, bool luong, decimal? minluong, decimal? maxluong, string? stringSearch)
        {

            var dangtintuyendung = (from s in _context.Dangtintuyendung
                                    join ntd in _context.Nhatuyendung on s.IdNhatuyendung equals ntd.IdNhatuyendung
                                    join q in _context.Thanhpho on s.IdDiadiemlamviecNavigation.IdThanhpho equals q.IdThanhpho
                                    where s.Isdelete.Equals(false) && s.Ngayhethan >= (DateTime.Now).AddDays(-1) && s.Trangthai.Equals("Đã duyệt")
                                    orderby s.IdTintuyendung descending
                                    select new DangTinTuyenDungDTO
                                    {
                                        Logo = s.IdNhatuyendungNavigation.Logo,
                                        IdTintuyendung = s.IdTintuyendung,
                                        Tieude = s.Tieude,
                                        Chucdanh = s.Chucdanh,
                                        Capbac = s.Capbac,
                                        Soluong = s.Soluong,
                                        Motacongviec = s.Motacongviec,
                                        Yeucaucongviec = s.Yeucaucongviec,
                                        Yeucaukynang = s.Yeucaukynang,
                                        Minluong = s.Minluong,
                                        Maxluong = s.Maxluong,
                                        Thuongluong = s.Thuongluong,
                                        Ngaydang = s.Ngaydang,
                                        Ngayhethan = s.Ngayhethan,
                                        Gioitinh = s.Gioitinh,
                                        IdDiadiemlamviec = s.IdDiadiemlamviec,
                                        IdNhatuyendung = s.IdNhatuyendung,
                                        IdLoaicongviec = s.IdLoaicongviec,
                                        Kinhnghiem = s.Kinhnghiem,
                                        Emaillienhe = s.Emaillienhe,
                                        Sdtlienhe = s.Sdtlienhe,
                                        Nguoilienhe = s.Nguoilienhe,
                                        Tencongty = ntd.Tencongty,
                                        IdDiadiemlamviecNavigation = s.IdDiadiemlamviecNavigation,
                                        Trinhdotuyendung = s.Trinhdotuyendung,
                                        Phucloituyendung = s.Phucloituyendung,
                                        Tenthanhpho = s.IdDiadiemlamviecNavigation.IdThanhphoNavigation.Tenthanhpho,
                                        IdThanhpho = q.IdThanhpho,
                                        IdLoaicongviecNavigation = s.IdLoaicongviecNavigation,
                                        Nganhtuyendung = s.Nganhtuyendung,
                                        Isdelete = s.Isdelete,
                                        Douutien = s.Douutien,
                                        Luotxem = s.Luotxem,
                                        Datasort = 0
                                    });

            if (idThanhpho != null && idThanhpho > 0)
            {
                dangtintuyendung = dangtintuyendung.Where(x => x.IdThanhpho == idThanhpho);

            }
            if (idNganh != null && idNganh > 0)
            {
                dangtintuyendung = from tin in dangtintuyendung
                                   join ng in _context.Nganhtuyendung on tin.IdTintuyendung equals ng.IdTintuyendung
                                   where ng.IdNganh.Equals(idNganh)
                                   select tin;
                //.Where(x => x.IdThanhpho == idThanhpho);
            }
            if (chucvu == "Quản lý")
            {
                dangtintuyendung = from tin in dangtintuyendung
                                   where tin.Capbac.Equals(chucvu)
                                   select tin;
            }

            if (minluong > 0)
            {
                dangtintuyendung = from tin in dangtintuyendung
                                   where tin.Maxluong > minluong
                                   select tin;
            }

            if (maxluong > 0)
            {
                dangtintuyendung = from tin in dangtintuyendung
                                   where tin.Minluong < maxluong || tin.Minluong == null
                                   select tin;
            }

            if (stringSearch != null)
            {
                dangtintuyendung = from tin in dangtintuyendung
                                   where tin.Tieude.Contains(stringSearch)
                                   select tin;
            }



            var data = dangtintuyendung.ToList();



            if (idUngvien > 0)
            {
                var xxx = _context.Congviecmongmuon.Any(x => x.IdUngvien == idUngvien);
                if (xxx)
                {
                    var tintt = (from user in _context.Congviecmongmuon
                                 join khuvuc in _context.Noilamviecmongmuon on user.IdCongviecmongmuon equals khuvuc.IdCongviecmongmuon
                                 where user.IdUngvien.Equals(idUngvien)
                                 select new
                                 {
                                     user.Noilamviecmongmuon,
                                     user.Nganhnghemongmuon
                                 }).ToList().Last();

                    foreach (var tin in data)
                    {
                        foreach (var tp in tintt.Noilamviecmongmuon)
                        {
                            if (tp.IdThanhpho == tin.IdDiadiemlamviecNavigation.IdThanhpho)
                            {
                                tin.Datasort++;

                            }
                        }

                        foreach (var tp in tintt.Nganhnghemongmuon)
                        {
                            foreach (var nn in tin.Nganhtuyendung)
                            {
                                if (tp.IdNganh == nn.IdNganh)
                                {
                                    tin.Datasort++;

                                }
                            }

                        }
                    }
                }
            }


            var xxxx = (from da in data
                        orderby da.Ngaydang descending
                        select da);

            var DT = (from da in xxxx
                      orderby da.Datasort descending
                      select da);

            if (luong)
            {
                DT = from s in DT orderby s.Minluong descending select s;
                DT = from s in DT orderby s.Maxluong descending select s;
            }

            var list = DT.Skip(pageSize * (pageNum - 1)).Take(pageSize).ToList();
            return Ok(list);
        }



        // GET: api/Dangtintuyendungs/congty/id
        [HttpGet("congty/{id}")]
        public async Task<ActionResult<IEnumerable<Dangtintuyendung>>> GetDangtintuyendungCongty(int id)
        {
            var dangtintuyendung = await (from s in _context.Dangtintuyendung
                                          join ntd in _context.Nhatuyendung on s.IdNhatuyendung equals ntd.IdNhatuyendung
                                          join q in _context.Thanhpho on s.IdDiadiemlamviecNavigation.IdThanhpho equals q.IdThanhpho
                                          where s.IdNhatuyendung.Equals(id) && s.Isdelete.Equals(false)
                                          orderby s.IdTintuyendung descending
                                          select new
                                          {
                                              s.IdNhatuyendungNavigation.Logo,
                                              s.IdTintuyendung,
                                              s.Tieude,
                                              s.Chucdanh,
                                              s.Capbac,
                                              s.Soluong,
                                              s.Motacongviec,
                                              s.Yeucaucongviec,
                                              s.Yeucaukynang,
                                              s.Minluong,
                                              s.Maxluong,
                                              s.Thuongluong,
                                              s.Ngaydang,
                                              s.Ngayhethan,
                                              s.Gioitinh,
                                              s.IdDiadiemlamviec,
                                              s.IdNhatuyendung,
                                              s.IdLoaicongviec,
                                              s.Kinhnghiem,
                                              s.Emaillienhe,
                                              s.Sdtlienhe,
                                              s.Nguoilienhe,
                                              ntd.Tencongty,
                                              s.IdDiadiemlamviecNavigation,
                                              s.Trinhdotuyendung,
                                              s.Phucloituyendung,
                                              q.Tenthanhpho,
                                              q.IdThanhpho,
                                              s.IdLoaicongviecNavigation,
                                              s.Isdelete,
                                              s.Contentdelete,
                                              s.Douutien,
                                              s.Luotxem,
                                              s.Nganhtuyendung,
                                              s.Trangthai
                                          }).ToListAsync();
            return Ok(dangtintuyendung);
        }



        [HttpGet("ungtuyen/{id}")]
        public async Task<ActionResult<IEnumerable<Dangtintuyendung>>> Getungvienungtuyen(int id)
        {
            var ungvien = await (from s in _context.Ungvien
                                 join ut in _context.Ungtuyen on s.IdUngvien equals ut.IdUngvien
                                 where ut.IdCongviec.Equals(id)
                                 select new
                                 {
                                     s.Ho,
                                     s.Ten,
                                     s.IdUngvien,
                                     s.Gioitinh,
                                     s.Kinhnghiem,
                                     s.Luong,
                                     s.IdThanhphoNavigation,
                                     s.Loaicongviecungvien,
                                     s.Ngoainguungvien,
                                     s.Congviecmongmuon,
                                     ut.Ngayungtuyen,
                                     ut.Daxem

                                 }).ToListAsync();
            return Ok(ungvien);
        }


        // GET: api/Dangtintuyendungs/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Dangtintuyendung>> GetDangtintuyendung(int id)
        {
            var dangtintuyendung = await (from s in _context.Dangtintuyendung
                                          join ntd in _context.Nhatuyendung on s.IdNhatuyendung equals ntd.IdNhatuyendung
                                          join q in _context.Thanhpho on s.IdDiadiemlamviecNavigation.IdThanhpho equals q.IdThanhpho
                                          where s.IdTintuyendung.Equals(id)
                                          orderby s.IdTintuyendung descending
                                          select new
                                          {
                                              s.IdTintuyendung,
                                              s.Tieude,
                                              s.Chucdanh,
                                              s.Capbac,
                                              s.Soluong,
                                              s.Motacongviec,
                                              s.Yeucaucongviec,
                                              s.Yeucaukynang,
                                              s.Minluong,
                                              s.Maxluong,
                                              s.Thuongluong,
                                              s.Ngaydang,
                                              s.Ngayhethan,
                                              s.Gioitinh,
                                              s.IdDiadiemlamviec,
                                              s.IdNhatuyendung,
                                              s.IdLoaicongviec,
                                              s.Kinhnghiem,
                                              s.Emaillienhe,
                                              s.Sdtlienhe,
                                              ntd.Tencongty,
                                              s.Nguoilienhe,
                                              q.Tenthanhpho,
                                              q.IdThanhpho,
                                              s.Trinhdotuyendung,
                                              s.Phucloituyendung,
                                              s.IdLoaicongviecNavigation,
                                              s.IdDiadiemlamviecNavigation,
                                              s.Isdelete,
                                              s.Douutien,
                                              s.Luotxem,
                                              s.Nganhtuyendung,
                                              s.Trangthai,
                                          }).ToListAsync();
            if (dangtintuyendung == null)
            {
                return NotFound();
            }

            return Ok(dangtintuyendung);
        }

        // PUT: api/Dangtintuyendungs/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.

        [HttpPut("{id}")]
        public async Task<IActionResult> PutDangtintuyendung(int id, Dangtintuyendung dangtintuyendung)
        {
            if (id != dangtintuyendung.IdTintuyendung)
            {
                return BadRequest();
            }

            _context.Entry(dangtintuyendung).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DangtintuyendungExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }


        // POST: api/Dangtintuyendungs
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.

        [HttpPost]
        public async Task<ActionResult<Dangtintuyendung>> PostDangtintuyendung(Dangtintuyendung dangtintuyendung)
        {
            _context.Dangtintuyendung.Add(dangtintuyendung);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (DangtintuyendungExists(dangtintuyendung.IdTintuyendung))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetDangtintuyendung", new { id = dangtintuyendung.IdTintuyendung }, dangtintuyendung);
        }

        // DELETE: api/Dangtintuyendungs/5

        [HttpDelete("{id}")]
        public async Task<ActionResult<Dangtintuyendung>> DeleteDangtintuyendung(int id)
        {
            var dangtintuyendung = await _context.Dangtintuyendung.FindAsync(id);
            if (dangtintuyendung == null)
            {
                return NotFound();
            }

            _context.Dangtintuyendung.Remove(dangtintuyendung);
            await _context.SaveChangesAsync();

            return dangtintuyendung;
        }

        private bool DangtintuyendungExists(int id)
        {
            return _context.Dangtintuyendung.Any(e => e.IdTintuyendung == id);
        }
    }
}
