﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using API_JEEJOB.DBEntities;

namespace API_JEEJOB.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LuuungviensController : ControllerBase
    {
        private readonly JEEJOBContext _context;

        public LuuungviensController(JEEJOBContext context)
        {
            _context = context;
        }

        // GET: api/Luuungviens
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Luuungvien>>> GetLuuungvien()
        {
            return await _context.Luuungvien.ToListAsync();
        }

        [HttpGet("{id}&{idUngvien}")]
        public bool Check(int id, int idUngvien)
        {
            return _context.Luuungvien.Any(e => e.IdNhatuyendung == id && e.IdUngvien == idUngvien);
        }



        // POST: api/Luuungviens
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Luuungvien>> PostLuuungvien(Luuungvien luuungvien)
        {
            _context.Luuungvien.Add(luuungvien);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (LuuungvienExists(luuungvien.IdNhatuyendung,luuungvien.IdUngvien))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return Ok(true);
        }

        // DELETE: api/Luuungviens/5
        [HttpDelete("{id}&{idUngvien}")]
        public async Task<ActionResult<Luuungvien>> DeleteLuuungvien(int id,int idUngvien)
        {
            var luuungvien = await _context.Luuungvien.FirstOrDefaultAsync(x => x.IdNhatuyendung == id && x.IdUngvien == idUngvien);
            if (luuungvien == null)
            {
                return NotFound();
            }

            _context.Luuungvien.Remove(luuungvien);
            await _context.SaveChangesAsync();

            return luuungvien;
        }

        private bool LuuungvienExists(int id,int idUngvien)
        {
            return _context.Luuungvien.Any(e => e.IdNhatuyendung == id && e.IdUngvien == idUngvien);
        }
    }
}
