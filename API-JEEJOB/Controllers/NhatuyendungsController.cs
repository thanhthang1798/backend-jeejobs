﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using API_JEEJOB.DBEntities;
using Dapper;

namespace API_JEEJOB.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NhatuyendungsController : ControllerBase
    {
        private readonly JEEJOBContext _context;


        public NhatuyendungsController(JEEJOBContext context)
        {
            _context = context;
        }

        // GET: api/Nhatuyendungs
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Nhatuyendung>>> GetNhatuyendung()
        {
            var nhatuyendung = await (from s in _context.Nhatuyendung
                                      where s.Isdelete.Equals(false)
                                      orderby s.IdNhatuyendung descending
                                      select new
                                      {
                                          s.IdNhatuyendung,
                                          s.Email,
                                          s.Matkhau,
                                          s.Logo,
                                          s.Showlogo,
                                          s.Dangtintuyendung.Count,
                                          s.Tencongty,
                                          s.Tenviettat,
                                          s.Quymocongty,
                                          s.Nganhnghehoatdong,
                                          s.Diachi,
                                          s.Sdt,
                                          s.Website,
                                          s.Bannercongty,
                                          s.Videogioithieu,
                                          s.Hinhanh,
                                          s.Gioithieucongty,
                                          s.Ngaydangky,
                                          s.Diadiemlamviec,
                                          s.Isdelete,
                                          s.Douutien,
                                          s.Luotxem,
                                          s.Sociallogin
                                      }).ToListAsync();
            return Ok(nhatuyendung);
        }
        // phân trang nha tuyển dụng
        [HttpGet("{page}&{row}")]
        public async Task<ActionResult<IEnumerable<Nhatuyendung>>> GetpageNTD(int page, int row)
        {

            if (row == null)
            {
                row = 10;
            }
            string query = @"select *,COUNT(*) OVER () as TotalRecords from NHATUYENDUNG
                            order by ID_NHATUYENDUNG
                            OFFSET (" + page + @" - 1)*" + row + @"ROWS FETCH NEXT " + row + @" ROWS ONLY";


            return _context.Nhatuyendung.FromSqlRaw(query).ToList();
        }

        // GET: api/Nhatuyendungs/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Nhatuyendung>> GetNhatuyendung(int id)
        {
            var nhatuyendung = await (from s in _context.Nhatuyendung
                                      where s.IdNhatuyendung.Equals(id) && s.Isdelete.Equals(false)
                                      select new
                                      {
                                          s.IdNhatuyendung,
                                          s.Email,
                                          s.Matkhau,
                                          s.Logo,
                                          s.Showlogo,
                                          s.Tencongty,
                                          s.Tenviettat,
                                          s.Nganhnghehoatdong,
                                          s.Quymocongty,
                                          s.Diachi,
                                          s.Sdt,
                                          s.Website,
                                          s.Bannercongty,
                                          s.Videogioithieu,
                                          s.Hinhanh,
                                          s.Gioithieucongty,
                                          s.Diadiemlamviec,
                                          s.Isdelete,
                                          s.Ngaydangky,
                                          s.Douutien,
                                          s.Luotxem,
                                          s.Sociallogin
                                      }).ToListAsync();

            if (nhatuyendung == null)
            {
                return NotFound();
            }

            return Ok(nhatuyendung);
        }
        // GET: api/Nhatuyendungs/luuungvien/id
        [HttpGet("luuungvien/{id}")]
        public async Task<ActionResult<Ungtuyen>> GetNhatuyendungFrombaidang(int id)
        {
            var nhatuyendung = await (from s in _context.Ungvien
                                      join ut in _context.Luuungvien on s.IdUngvien equals ut.IdUngvien
                                      where ut.IdNhatuyendung.Equals(id)
                                      select new
                                      {
                                          s.IdUngvien,
                                          s.Diachi,
                                          s.Email,
                                          s.Gioitinh,
                                          s.Hinhanh,
                                          s.Kinhnghiem,
                                          s.Luong,
                                          s.Ngaysinh,
                                          s.Ngaytao,
                                          s.Sdt,
                                          s.Docthan,
                                          s.Bangcap,
                                          s.IdThanhpho,
                                          s.IdThanhphoNavigation.Tenthanhpho,
                                          s.Ten,
                                          s.Ho,
                                          s.Matkhau,
                                          ut.Ngayluu,
                                          s.Isdelete,
                                          s.Luotxem,
                                          s.Sociallogin
                                      }).ToListAsync();

            if (nhatuyendung == null)
            {
                return NotFound();
            }

            return Ok(nhatuyendung);
        }

        // GET: api/Nhatuyendungs/diadiemlamviec?idntd=1
        [HttpGet("diadiemlamviec")]
        public async Task<ActionResult<Diadiemlamviec>> Getdiadiemlamviec(int idntd)
        {
            var nhatuyendung = await (from s in _context.Diadiemlamviec
                                      join q in _context.Thanhpho on s.IdThanhpho equals q.IdThanhpho
                                      where s.IdNhatuyendung.Equals(idntd)
                                      select new
                                      {
                                          s.Diachi,
                                          s.IdDiadiemlamviec,
                                          q.Tenthanhpho,
                                          q.IdThanhpho
                                      }).ToListAsync();

            if (nhatuyendung == null)
            {
                return NotFound();
            }

            return Ok(nhatuyendung);
        }

        // PUT: api/Nhatuyendungs/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutNhatuyendung(int id, Nhatuyendung nhatuyendung)
        {
            if (id != nhatuyendung.IdNhatuyendung)
            {
                return BadRequest();
            }

            _context.Entry(nhatuyendung).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!NhatuyendungExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Nhatuyendungs
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Nhatuyendung>> PostNhatuyendung(Nhatuyendung nhatuyendung)
        {
            _context.Nhatuyendung.Add(nhatuyendung);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (NhatuyendungExists(nhatuyendung.IdNhatuyendung))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetNhatuyendung", new { id = nhatuyendung.IdNhatuyendung }, nhatuyendung);
        }

        // DELETE: api/Nhatuyendungs/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Nhatuyendung>> DeleteNhatuyendung(int id)
        {
            var nhatuyendung = await _context.Nhatuyendung.FindAsync(id);
            if (nhatuyendung == null)
            {
                return NotFound();
            }

            _context.Nhatuyendung.Remove(nhatuyendung);
            await _context.SaveChangesAsync();

            return nhatuyendung;
        }


        private bool NhatuyendungExists(int id)
        {
            return _context.Nhatuyendung.Any(e => e.IdNhatuyendung == id);
        }
    }
}
