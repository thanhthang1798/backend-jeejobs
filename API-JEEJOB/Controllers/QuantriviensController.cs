﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using API_JEEJOB.DBEntities;

namespace API_JEEJOB.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class QuantriviensController : ControllerBase
    {
        private readonly JEEJOBContext _context;

        public QuantriviensController(JEEJOBContext context)
        {
            _context = context;
        }

        // GET: api/Quantriviens
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Quantrivien>>> GetQuantrivien()
        {
            var quantrivien = await (from s in _context.Quantrivien

                                  select new
                                  {
                                      s.IdQtv,
                                      s.Matkhau,
                                      s.Email,
                                      s.Sdt
                                  }).ToListAsync();
            return Ok(quantrivien);
        }

        // GET: api/Quantriviens/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Quantrivien>> GetQuantrivien(int id)
        {
            var quantrivien = await (from s in _context.Quantrivien
                                     where s.IdQtv.Equals(id)
                                     select new
                                     {
                                         s.IdQtv,
                                         s.Matkhau,
                                         s.Email,
                                         s.Sdt
                                     }).ToListAsync();

            if (quantrivien == null)
            {
                return NotFound();
            }

            return Ok(quantrivien);
        }

        // PUT: api/Quantriviens/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutQuantrivien(string id, Quantrivien quantrivien)
        {
            if (id != quantrivien.Email)
            {
                return BadRequest();
            }

            _context.Entry(quantrivien).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!QuantrivienExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Quantriviens
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Quantrivien>> PostQuantrivien(Quantrivien quantrivien)
        {
            _context.Quantrivien.Add(quantrivien);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (QuantrivienExists(quantrivien.Email))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetQuantrivien", new { id = quantrivien.Email }, quantrivien);
        }

        // DELETE: api/Quantriviens/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Quantrivien>> DeleteQuantrivien(string id)
        {
            var quantrivien = await _context.Quantrivien.FindAsync(id);
            if (quantrivien == null)
            {
                return NotFound();
            }

            _context.Quantrivien.Remove(quantrivien);
            await _context.SaveChangesAsync();

            return quantrivien;
        }
        


        private bool QuantrivienExists(string id)
        {
            return _context.Quantrivien.Any(e => e.Email == id);
        }
    }
}
