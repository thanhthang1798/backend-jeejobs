﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using API_JEEJOB.DBEntities;

namespace API_JEEJOB.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CongviecmongmuonsController : ControllerBase
    {
        private readonly JEEJOBContext _context;

        public CongviecmongmuonsController(JEEJOBContext context)
        {
            _context = context;
        }

        // GET: api/Congviecmongmuons
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Congviecmongmuon>>> GetCongviecmongmuon()
        {
            return await _context.Congviecmongmuon.ToListAsync();
        }

        // GET: api/Congviecmongmuons/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Congviecmongmuon>> GetCongviecmongmuon(int id)
        {
            var congviecmongmuon = await _context.Congviecmongmuon.FindAsync(id);

            if (congviecmongmuon == null)
            {
                return NotFound();
            }

            return congviecmongmuon;
        }
        
        // GET: api/Congviecmongmuons/5
        [HttpGet("ungvien/{id}")]
        public async Task<ActionResult<Congviecmongmuon>> GetCongviecmongmuonUV(int id)
        {
            var congviecmongmuon = await (from s in _context.Congviecmongmuon
                                          where s.IdUngvien.Equals(id)
                                          select new
                                          {
                                              s.Tenvieclam,
                                              s.IdThanhpho,
                                              s.IdNganh,
                                              s.IdLoaicongviecNavigation.Tenloaicongviec,
                                              s.IdLoaicongviec,
                                              s.Luong,
                                              s.Thuongluong,
                                              s.Thongbao,
                                              s.Noikhac,
                                              s.Capbac,
                                              s.Ngaytao,
                                              s.IdCongviecmongmuon,
                                              s.Noilamviecmongmuon,
                                              s.Nganhnghemongmuon
                                          }).Distinct().ToListAsync();
            return Ok(congviecmongmuon);
        }

        // PUT: api/Congviecmongmuons/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCongviecmongmuon(int id, Congviecmongmuon congviecmongmuon)
        {
            if (id != congviecmongmuon.IdCongviecmongmuon)
            {
                return BadRequest();
            }

            _context.Entry(congviecmongmuon).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CongviecmongmuonExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Congviecmongmuons
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Congviecmongmuon>> PostCongviecmongmuon(Congviecmongmuon congviecmongmuon)
        {
            _context.Congviecmongmuon.Add(congviecmongmuon);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCongviecmongmuon", new { id = congviecmongmuon.IdCongviecmongmuon }, congviecmongmuon);
        }

        // DELETE: api/Congviecmongmuons/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Congviecmongmuon>> DeleteCongviecmongmuon(int id)
        {
            var congviecmongmuon = await _context.Congviecmongmuon.FindAsync(id);
            if (congviecmongmuon == null)
            {
                return NotFound();
            }

            _context.Congviecmongmuon.Remove(congviecmongmuon);
            await _context.SaveChangesAsync();

            return congviecmongmuon;
        }

        private bool CongviecmongmuonExists(int id)
        {
            return _context.Congviecmongmuon.Any(e => e.IdCongviecmongmuon == id);
        }
    }
}
