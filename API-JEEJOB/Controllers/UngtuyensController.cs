﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using API_JEEJOB.DBEntities;

namespace API_JEEJOB.Controllers
{
    
    [Route("api/[controller]")]
    [ApiController]
    public class UngtuyensController : ControllerBase
    {
        private readonly JEEJOBContext _context;

        public UngtuyensController(JEEJOBContext context)
        {
            _context = context;
        }

        // GET: api/Ungtuyens
        [HttpGet("{id}&{idTintuyendung}")]
        public bool GetUngtuyen(int id, int idTintuyendung)
        {
            return _context.Ungtuyen.Any(e => e.IdUngvien == id && e.IdCongviec == idTintuyendung);
        }
        
        // GET: api/Ungtuyens
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Ungtuyen>>> check()
        {
            var ungtuyen = await (from s in _context.Ungtuyen
                                     select new
                                     {
                                         s.IdUngvien,
                                         s.IdCongviec,
                                         s.Ngayungtuyen
                                     }).ToListAsync();
            return Ok(ungtuyen);
        }
        // GET: api/Ungtuyens
        [HttpGet("thongbaoungtuyen/{idNTD}")]
        public async Task<ActionResult<IEnumerable<Ungtuyen>>> checkTB(int idNTD)
        {
            var ungtuyen = await (from s in _context.Dangtintuyendung
                                  where s.IdNhatuyendung.Equals(idNTD)
                                  select new
                                  {
                                      s.IdTintuyendung,
                                      s.Tieude,
                                     dsungtuyen = s.Ungtuyen.Where(x => x.Daxem != true)
                                  }).ToListAsync();
            return Ok(ungtuyen);
        }


        // POST: api/Ungtuyens
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Ungtuyen>> PostUngtuyen(Ungtuyen ungtuyen)
        {
            _context.Ungtuyen.Add(ungtuyen);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (UngtuyenExists(ungtuyen.IdUngvien,ungtuyen.IdCongviec))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return Ok(true);
        }

        // DELETE: api/Ungtuyens/5
        [HttpPut]
        public async Task<ActionResult<Ungtuyen>> UpdateUngtuyen(Ungtuyen ut)
        {

            _context.Entry(ut).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
                return ut;
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UngtuyenExists(ut.IdCongviec,ut.IdUngvien))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }
        // DELETE: api/Ungtuyens/5
        [HttpDelete("{id}&{idTintuyendung}")]
        public async Task<ActionResult<Ungtuyen>> DeleteUngtuyen(int id,int idTintuyendung)
        {
            var ungtuyen = await _context.Ungtuyen.FirstOrDefaultAsync(e => e.IdUngvien == id && e.IdCongviec == idTintuyendung);
            if (ungtuyen == null)
            {
                return NotFound();
            }

            _context.Ungtuyen.Remove(ungtuyen);
            await _context.SaveChangesAsync();

            return ungtuyen;
        }

        private bool UngtuyenExists(int idTintuyendung,int id)
        {
            return _context.Ungtuyen.Any(e => e.IdUngvien == id && e.IdCongviec == idTintuyendung);
        }
    }
}
