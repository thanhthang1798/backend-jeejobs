﻿using J2.Businesses.Interfaces;
using J2.DATA.DBEntities;
using J2.DATA.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J2.Businesses.Businesses
{
    public class CitiesServices : ICitiesServices
    {
        private readonly IUnitOfWork _unitOfWork;
        public CitiesServices(IUnitOfWork unitOfWork) { 
            _unitOfWork = unitOfWork;
        }
        public List<Thanhpho> GetDSTP()
        {
            return _unitOfWork.CitiesRepositories.GetAll().ToList();
        }
    }
}
