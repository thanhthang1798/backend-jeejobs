﻿using J2.DATA.DBEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J2.Businesses.Interfaces
{
    public interface ICitiesServices
    {
        List<Thanhpho> GetDSTP();
    }
}
