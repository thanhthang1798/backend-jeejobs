﻿
USE [master]
GO

/****** Object:  Database [JEEJOB]    Script Date: 1/17/2024 4:27:28 PM ******/
/*
CREATE DATABASE [JEEJOB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'JEEJOB', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL16.THANGNT\MSSQL\DATA\JEEJOB.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'JEEJOB_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL16.THANGNT\MSSQL\DATA\JEEJOB_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT, LEDGER = OFF
GO

ALTER DATABASE [JEEJOB] SET COMPATIBILITY_LEVEL = 120
GO

IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [JEEJOB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
*/

ALTER DATABASE [JEEJOB] SET ANSI_NULL_DEFAULT OFF
GO

ALTER DATABASE [JEEJOB] SET ANSI_NULLS OFF
GO

ALTER DATABASE [JEEJOB] SET ANSI_PADDING OFF
GO

ALTER DATABASE [JEEJOB] SET ANSI_WARNINGS OFF
GO

ALTER DATABASE [JEEJOB] SET ARITHABORT OFF
GO

ALTER DATABASE [JEEJOB] SET AUTO_CLOSE OFF
GO

ALTER DATABASE [JEEJOB] SET AUTO_SHRINK OFF
GO

ALTER DATABASE [JEEJOB] SET AUTO_UPDATE_STATISTICS ON
GO

ALTER DATABASE [JEEJOB] SET CURSOR_CLOSE_ON_COMMIT OFF
GO

ALTER DATABASE [JEEJOB] SET CURSOR_DEFAULT  GLOBAL
GO

ALTER DATABASE [JEEJOB] SET CONCAT_NULL_YIELDS_NULL OFF
GO

ALTER DATABASE [JEEJOB] SET NUMERIC_ROUNDABORT OFF
GO

ALTER DATABASE [JEEJOB] SET QUOTED_IDENTIFIER OFF
GO

ALTER DATABASE [JEEJOB] SET RECURSIVE_TRIGGERS OFF
GO

ALTER DATABASE [JEEJOB] SET  DISABLE_BROKER
GO

ALTER DATABASE [JEEJOB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO

ALTER DATABASE [JEEJOB] SET DATE_CORRELATION_OPTIMIZATION OFF
GO

ALTER DATABASE [JEEJOB] SET TRUSTWORTHY OFF
GO

ALTER DATABASE [JEEJOB] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO

ALTER DATABASE [JEEJOB] SET PARAMETERIZATION SIMPLE
GO

ALTER DATABASE [JEEJOB] SET READ_COMMITTED_SNAPSHOT OFF
GO

ALTER DATABASE [JEEJOB] SET HONOR_BROKER_PRIORITY OFF
GO

ALTER DATABASE [JEEJOB] SET RECOVERY SIMPLE
GO

ALTER DATABASE [JEEJOB] SET  MULTI_USER
GO

ALTER DATABASE [JEEJOB] SET PAGE_VERIFY CHECKSUM
GO

ALTER DATABASE [JEEJOB] SET DB_CHAINING OFF
GO

ALTER DATABASE [JEEJOB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF )
GO

ALTER DATABASE [JEEJOB] SET TARGET_RECOVERY_TIME = 0 SECONDS
GO

ALTER DATABASE [JEEJOB] SET DELAYED_DURABILITY = DISABLED
GO

ALTER DATABASE [JEEJOB] SET ACCELERATED_DATABASE_RECOVERY = OFF
GO

EXEC sys.sp_db_vardecimal_storage_format N'JEEJOB', N'ON'
GO

ALTER DATABASE [JEEJOB] SET QUERY_STORE = ON
GO

USE [JEEJOB]
GO

/****** Object:  View [dbo].[vwRand]    Script Date: 1/17/2024 4:27:28 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[BANGCAP]    Script Date: 1/17/2024 4:27:28 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[CONGVIECMONGMUON]    Script Date: 1/17/2024 4:27:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[DANGTINTUYENDUNG]    Script Date: 1/17/2024 4:27:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[DIADIEMLAMVIEC]    Script Date: 1/17/2024 4:27:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[KINHNGHIEMLAMVIEC]    Script Date: 1/17/2024 4:27:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[KYNANGUNGVIEN]    Script Date: 1/17/2024 4:27:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[LOAICONGVIEC]    Script Date: 1/17/2024 4:27:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[LOAICONGVIECUNGVIEN]    Script Date: 1/17/2024 4:27:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[LUUCONGVIEC]    Script Date: 1/17/2024 4:27:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[LUUUNGVIEN]    Script Date: 1/17/2024 4:27:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[NGANH]    Script Date: 1/17/2024 4:27:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[NGANHNGHEMONGMUON]    Script Date: 1/17/2024 4:27:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[NGANHTUYENDUNG]    Script Date: 1/17/2024 4:27:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[NGANHUNGVIEN]    Script Date: 1/17/2024 4:27:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[NGOAINGU]    Script Date: 1/17/2024 4:27:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[NGOAINGUUNGVIEN]    Script Date: 1/17/2024 4:27:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[NHATUYENDUNG]    Script Date: 1/17/2024 4:27:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[NOILAMVIECMONGMUON]    Script Date: 1/17/2024 4:27:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[PHUCLOI]    Script Date: 1/17/2024 4:27:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[PHUCLOICONGVIECMONGMUON]    Script Date: 1/17/2024 4:27:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[PHUCLOITUYENDUNG]    Script Date: 1/17/2024 4:27:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[QUANTRIVIEN]    Script Date: 1/17/2024 4:27:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[THANHPHO]    Script Date: 1/17/2024 4:27:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[THANHPHOUNGVIEN]    Script Date: 1/17/2024 4:27:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[TRINHDO]    Script Date: 1/17/2024 4:27:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[TRINHDOTUYENDUNG]    Script Date: 1/17/2024 4:27:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[UNGTUYEN]    Script Date: 1/17/2024 4:27:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[UNGVIEN]    Script Date: 1/17/2024 4:27:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET IDENTITY_INSERT [dbo].[BANGCAP] ON
GO

INSERT [dbo].[BANGCAP] ([ID_BANGCAP], [ID_UNGVIEN], [ID_TRINHDO], [CHUYENNGANH], [TENTRUONG], [TENBANGCAP], [NGAYBATDAU], [NGAYKETTHUC], [GHICHU]) VALUES (1002, 10, 3, N'Cắt may', N'HUFI', N'cử nhân', CAST(N'2016-01-01' AS Date), CAST(N'2020-09-01' AS Date), N'hoàn thành tốt')
GO

INSERT [dbo].[BANGCAP] ([ID_BANGCAP], [ID_UNGVIEN], [ID_TRINHDO], [CHUYENNGANH], [TENTRUONG], [TENBANGCAP], [NGAYBATDAU], [NGAYKETTHUC], [GHICHU]) VALUES (2003, 8, 2, N'dấdaqeqwe', N'eeeeeeeeeeeeeeeeee', N'eeeeeeeeeeee', CAST(N'2020-06-01' AS Date), CAST(N'2020-11-01' AS Date), N'eeeeeeeeeeeeeeeeeeeeeeeeeeeee')
GO

INSERT [dbo].[BANGCAP] ([ID_BANGCAP], [ID_UNGVIEN], [ID_TRINHDO], [CHUYENNGANH], [TENTRUONG], [TENBANGCAP], [NGAYBATDAU], [NGAYKETTHUC], [GHICHU]) VALUES (2004, 1, 3, N'CNTT - Ứng dụng phần mềm', N'Cao Đẳng Thực Hành FPT Polytechnic ', N'Tốt nghiệp loại khá', CAST(N'2018-08-01' AS Date), CAST(N'2021-01-01' AS Date), N'Tốt nghiệp loại khá')
GO

INSERT [dbo].[BANGCAP] ([ID_BANGCAP], [ID_UNGVIEN], [ID_TRINHDO], [CHUYENNGANH], [TENTRUONG], [TENBANGCAP], [NGAYBATDAU], [NGAYKETTHUC], [GHICHU]) VALUES (2005, 1, 2, N'Chuyên ngành: Phát triển phần mềm', N'TRƯỜNG ĐẠI HỌC LẠC HỒNG', NULL, CAST(N'2018-01-01' AS Date), CAST(N'2020-06-01' AS Date), N' Sử dụng thành thạo các công cụ Word, Excel, Power Point')
GO

INSERT [dbo].[BANGCAP] ([ID_BANGCAP], [ID_UNGVIEN], [ID_TRINHDO], [CHUYENNGANH], [TENTRUONG], [TENBANGCAP], [NGAYBATDAU], [NGAYKETTHUC], [GHICHU]) VALUES (2006, 1018, 3, N'Công nghệ thông tin', N'Đại học công nghiệp thực Phẩm TP.HCM', NULL, CAST(N'2020-12-01' AS Date), CAST(N'2020-11-01' AS Date), N'Tích cực tham gia các hoạt động đoàn')
GO

SET IDENTITY_INSERT [dbo].[BANGCAP] OFF
GO

SET IDENTITY_INSERT [dbo].[CONGVIECMONGMUON] ON
GO

INSERT [dbo].[CONGVIECMONGMUON] ([ID_CONGVIECMONGMUON], [ID_UNGVIEN], [CAPBAC], [NOIKHAC], [THONGBAO], [ID_THANHPHO], [ID_LOAICONGVIEC], [ID_NGANH], [LUONG], [THUONGLUONG], [NGAYTAO], [TENVIECLAM]) VALUES (31, 7, N'Nhân viên', 0, NULL, 0, 2, 0, NULL, 1, CAST(N'2020-07-06' AS Date), N'Thông Báo')
GO

INSERT [dbo].[CONGVIECMONGMUON] ([ID_CONGVIECMONGMUON], [ID_UNGVIEN], [CAPBAC], [NOIKHAC], [THONGBAO], [ID_THANHPHO], [ID_LOAICONGVIEC], [ID_NGANH], [LUONG], [THUONGLUONG], [NGAYTAO], [TENVIECLAM]) VALUES (33, 9, N'Nhân viên', 1, NULL, 0, 1, 0, NULL, 0, CAST(N'2020-07-06' AS Date), N'Thông Báo')
GO

INSERT [dbo].[CONGVIECMONGMUON] ([ID_CONGVIECMONGMUON], [ID_UNGVIEN], [CAPBAC], [NOIKHAC], [THONGBAO], [ID_THANHPHO], [ID_LOAICONGVIEC], [ID_NGANH], [LUONG], [THUONGLUONG], [NGAYTAO], [TENVIECLAM]) VALUES (1028, 3, N'Nhân viên', 0, NULL, 0, 2, 0, NULL, 1, CAST(N'2020-07-08' AS Date), N'Thông Báo')
GO

INSERT [dbo].[CONGVIECMONGMUON] ([ID_CONGVIECMONGMUON], [ID_UNGVIEN], [CAPBAC], [NOIKHAC], [THONGBAO], [ID_THANHPHO], [ID_LOAICONGVIEC], [ID_NGANH], [LUONG], [THUONGLUONG], [NGAYTAO], [TENVIECLAM]) VALUES (2031, 1015, N'Nhân viên', 1, NULL, 0, 1, 0, NULL, 0, CAST(N'2020-07-09' AS Date), N'Thông Báo')
GO

INSERT [dbo].[CONGVIECMONGMUON] ([ID_CONGVIECMONGMUON], [ID_UNGVIEN], [CAPBAC], [NOIKHAC], [THONGBAO], [ID_THANHPHO], [ID_LOAICONGVIEC], [ID_NGANH], [LUONG], [THUONGLUONG], [NGAYTAO], [TENVIECLAM]) VALUES (2044, 1017, N'Nhân viên', 0, NULL, 0, 1, 0, NULL, 1, CAST(N'2020-07-10' AS Date), N'Thông Báo')
GO

INSERT [dbo].[CONGVIECMONGMUON] ([ID_CONGVIECMONGMUON], [ID_UNGVIEN], [CAPBAC], [NOIKHAC], [THONGBAO], [ID_THANHPHO], [ID_LOAICONGVIEC], [ID_NGANH], [LUONG], [THUONGLUONG], [NGAYTAO], [TENVIECLAM]) VALUES (2084, 1, N'Nhân viên', 0, NULL, 0, 1, 0, 1000000.0000, 0, CAST(N'2020-07-18' AS Date), N'NHÂN VIÊN IT')
GO

INSERT [dbo].[CONGVIECMONGMUON] ([ID_CONGVIECMONGMUON], [ID_UNGVIEN], [CAPBAC], [NOIKHAC], [THONGBAO], [ID_THANHPHO], [ID_LOAICONGVIEC], [ID_NGANH], [LUONG], [THUONGLUONG], [NGAYTAO], [TENVIECLAM]) VALUES (4087, 1039, N'Quản lý', 0, NULL, 0, 2, 0, 10000000.0000, 0, CAST(N'2020-07-28' AS Date), NULL)
GO

INSERT [dbo].[CONGVIECMONGMUON] ([ID_CONGVIECMONGMUON], [ID_UNGVIEN], [CAPBAC], [NOIKHAC], [THONGBAO], [ID_THANHPHO], [ID_LOAICONGVIEC], [ID_NGANH], [LUONG], [THUONGLUONG], [NGAYTAO], [TENVIECLAM]) VALUES (4088, 1018, N'Tất cả', 0, NULL, 0, 2, 0, 10000000.0000, 0, CAST(N'2020-08-10' AS Date), N'Nhân Viên IT')
GO

SET IDENTITY_INSERT [dbo].[CONGVIECMONGMUON] OFF
GO

SET IDENTITY_INSERT [dbo].[DANGTINTUYENDUNG] ON
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (1, N'Tuyển lập trình web ', N'Lập trình Website', N'Nhân viên', 10, N'Viết website cho công ty', N'Biết HTML - CSS - JQUERY', N'- Có khả năng thiết kế tốt và có kỹ năng giám sát công việc tốt.
- Có tư duy và kỹ năng giao tiếp tốt.
- Có khả năng làm việc trong môi trường áp lực cao, Đi công tác xa nhà.
- Chăm chỉ, có trách nhiệm trong công việc.', NULL, NULL, 1, CAST(N'2020-07-22' AS Date), CAST(N'2026-10-06' AS Date), 0, 1002, 1, 1, 1, N'user111@gmail.com', N'0346006260', N'Anh Tú', 0, NULL, N'Đã duyệt', 1, 576, 0)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2, N'Tuyển Kế toán', N'Kế toán viên', N'Nhân viên', 10, N'Làm sổ sách', N'có bằng đh', N'- Có khả năng thiết kế tốt và có kỹ năng giám sát công việc tốt.
- Có tư duy và kỹ năng giao tiếp tốt.
- Có khả năng làm việc trong môi trường áp lực cao, Đi công tác xa nhà.
- Chăm chỉ, có trách nhiệm trong công việc.', 3000000.0000, 6000000.0000, 0, CAST(N'2020-06-01' AS Date), CAST(N'2026-10-06' AS Date), 0, 1002, 1, 2, 0, N'user111@gmail.com', N'0346006260', N'Anh Hùng', 1, NULL, N'Đã duyệt', 1, 897, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (3, N'Tuyển thiết kế giao diện', N'UI/UX designer', N'Nhân viên', 10, N'thiết kế font-end', N'biết làm việc', N'- Có khả năng thiết kế tốt và có kỹ năng giám sát công việc tốt.
- Có tư duy và kỹ năng giao tiếp tốt.
- Có khả năng làm việc trong môi trường áp lực cao, Đi công tác xa nhà.
- Chăm chỉ, có trách nhiệm trong công việc.', 5500000.0000, 6000000.0000, 0, CAST(N'2020-06-01' AS Date), CAST(N'2026-10-06' AS Date), 0, 3, 2, 3, 3, N'grear@gmail.com', N'0346006260', N'Anh Hưng', 0, NULL, N'Đã duyệt', 1, 700, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (1005, N'Tuyển dụng mới', N'Coder', N'Nhân viên', 10, N'Biết ngôn ngữ lập trình', N'Biết C# và Typescripts', N'- Có khả năng thiết kế tốt và có kỹ năng giám sát công việc tốt.
- Có tư duy và kỹ năng giao tiếp tốt.
- Có khả năng làm việc trong môi trường áp lực cao, Đi công tác xa nhà.
- Chăm chỉ, có trách nhiệm trong công việc.', 4500000.0000, 6000000.0000, 0, CAST(N'2020-07-21' AS Date), CAST(N'2026-10-06' AS Date), 0, 1002, 1, 1, 1, N'user111@gmail.com', N'0346006260', N'Anh thắng', 0, NULL, N'Đã duyệt', 1, 237, 0)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2014, N'Kỹ Sư Cơ Điện Thu Nhập: Từ 6,5 Triệu Đến 40 Triệu, Tùy Thuộc Năng Lực)', N'Nhân viên', N'Quản lý', 10, N'Kỹ sư cơ: Thiết kế, báo giá, quản lý và giám sát tiến độ lắp đặt, kiểm tra chạy thử cho các hệ thống cơ: Hệ thống thông gió và điều hòa không khí (HVAC), Hệ thống cấp thoát nước (P & D), Hệ thống chữa cháy (FF) và các hệ thống phụ trợ khác cho các dự án phòng sạch và nhà công nghiệp

- Kỹ sử điện: Thiết kế, báo giá, quản lý và giám sát tiến độ lắp đặt kiểm tra chạy thử cho hệ thống điện và điều khiển: Hệ thống trạm biến áp, trạm trung thế, hạ thế cấp nguồn chính, BMS, hệ thống báo cháy, hệ thống an ninh, hệ thống điện thoại và dữ liệu, hệ thống chiếu sáng, hệ thống điều khiển, ...
Địa điểm làm việc: Hà Nội, Hồ Chí Minh và các dự án trên lãnh thổ Việt Nam', N'Kinh Nghiệm: Có kinh nghiệm làm việc từ 0 đến 10 năm cho các lĩnh vực thiết kế, giám sát, thi công cơ điện
- Công dân Việt Nam
- Tốt nghiệp đại học trong lĩnh vực:
+ Điều hòa thông gió.
+ Cấp thoát nước.
+ Cơ khí.
+ Tự động hóa, điều khiển tự động.
+ Hệ thống điện, thiết bị điện.
- Thành thạo tiếng Anh (cả hai kỹ năng nói và viết). Biết tiếng Nhật là một lợi thế.
- Biết sử dụng phần mềm Auto CAD và Phần mềm văn phòng như MS Project, MS office. Biết sử dụng phần mềm CADEWA là một lợi thế', N'- Có khả năng thiết kế tốt và có kỹ năng giám sát công việc tốt.
- Có tư duy và kỹ năng giao tiếp tốt.
- Có khả năng làm việc trong môi trường áp lực cao, Đi công tác xa nhà.
- Chăm chỉ, có trách nhiệm trong công việc.', 3000000.0000, 10000000.0000, 0, CAST(N'2020-06-29' AS Date), CAST(N'2026-10-06' AS Date), 0, 5032, 29, 2, 2, N'ntd15@gmail.com', N'0283973824', N'Phòng nhân sự', 0, NULL, N'Đã duyệt', 1, 401, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2015, N'
Chuyên Viên Phân Tích Tài Chính Doanh Nghiệp', N'Nhân viên', N'Nhân viên', 10, N'- Làm việc trực tiếp với các ngân hàng về các công việc liên quan lĩnh vực ngân hàng, tài chính của Doanh nghiệp. Chủ động đàm phán các điều khoản và điều kiện đem lại hiệu quả cao nhất cho công ty.
- Theo dõi diễn biến tỷ giá hàng ngày, cân đối kế hoạch thanh toán dựa trên nhu cầu thông quan hàng hóa nhập khẩu.
- Theo dõi các khoản nợ vay ngắn và trung hạn.
- Thu thập, tổng hợp thông tin từ các bộ phận liên quan,phân tích các chỉ số tài chính, làm công tác dự báo (cân đối dòng tiền và kế hoạch mua hàng) dựa trên các diễn biến định kỳ và đột xuất.
- Định kỳ phân tích, đánh giá hiệu quả kinh Doanh. Đưa ra ý kiến tham vấn cho cấp trên, các bộ phận liên quan để có kế hoạch tài chính hiệu quả nhất trong công tác nhập khẩu, cân đối hàng tồn kho, sử dụng phương thức thanh toán hiệu quả nhất đối với từng ngân hàng giao dịch.
- Đưa ra kiến nghị nhanh nhạy, kịp thời khi có những biến động bất thường về thị trường tài chính, ngân hàng; về chỉ số phản ánh tình hình kết quả hoạt động kinh Doanh.
- Thực hiện các công việc, báo cáo khác theo chỉ đạo của cấp trên/Ban Giám Đốc.', N'Tốt nghiệp Đại học chuyên ngành tài chính, ngân hàng, kế toán, kiểm toán hoặc các chuyên ngành liên quan.
Ưu tiên ứng viên có chứng chỉ CPA, ACCA hoặc CFA; có kiến thức về xuất nhập khẩu- lĩnh vực thanh toán quốc tế.
Có khả năng lập kế hoạch, báo cáo.
Có kỹ năng trình bày, làm việc đội nhóm, làm việc dưới áp lực cao.
Có khả năng phân tích và tổng hợp vấn đề. Nhạy bén với các biến động, xu hướng tài chính.
Có tính kiên nhẫn, cẩn thận, chính xác.
Đọc, hiểu bằng tiếng Anh tốt. "', N'Giao tiếp bằng tiếng anh thành thạo', 4000000.0000, 10000000.0000, 0, CAST(N'2020-07-29' AS Date), CAST(N'2026-10-06' AS Date), 0, 1002, 1, 1, 3, N'user111@gmail.com', N'0283973844', N'Phòng HCNS - Ms.Nhi', 0, NULL, N'Đã duyệt', 1, 961, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2016, N'Biên – Phiên Dịch IT Làm Việc Tại Nhật Bản', N'Phiên dịch viên', N'Nhân viên', 10, N'Phiên dịch và dịch thuật giữa kỹ sư hệ thống Nhật Bản và Lập trình viên người Việt
Hỗ trợ cho các lập trình viên người Việt vừa đến Nhật trong công việc và đời sống như hỗ trợ thuê nhà ở, v.v.', N'・Người Việt Nam
・Thông thạo tiếng nhật
・Có chứng chỉ N1
・Tốt nghiệp đại học Việt Nam hay Nhật Bản
・Sống ở Tokyo
・Có kinh nghiệm phiên dịch
・Có khả năng truyền đạt với các kỹ sư lập trình
・Quan tâm đến lĩnh vực phần mềm máy tính', N'- Có khả năng thiết kế tốt và có kỹ năng giám sát công việc tốt.
- Có tư duy và kỹ năng giao tiếp tốt.
- Có khả năng làm việc trong môi trường áp lực cao, Đi công tác xa nhà.
- Chăm chỉ, có trách nhiệm trong công việc.', 3000000.0000, 3000000.0000, 0, CAST(N'2020-06-29' AS Date), CAST(N'2026-10-06' AS Date), 0, 5033, 30, 2, 3, N'ntd16@gmail.com', N'0283973423', N'Phòng nhân sự', 0, NULL, N'Đã duyệt', 1, 942, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2017, N'Nhân Viên Kinh Doanh Dự Án SmartHome', N'Nhân viên', N'Nhân viên', 10, N'Kỹ sư cơ: Thiết kế, báo giá, quản lý và giám sát tiến độ lắp đặt, kiểm tra chạy thử cho các hệ thống cơ: Hệ thống thông gió và điều hòa không khí (HVAC), Hệ thống cấp thoát nước (P & D), Hệ thống chữa cháy (FF) và các hệ thống phụ trợ khác cho các dự án phòng sạch và nhà công nghiệp

- Kỹ sử điện: Thiết kế, báo giá, quản lý và giám sát tiến độ lắp đặt kiểm tra chạy thử cho hệ thống điện và điều khiển: Hệ thống trạm biến áp, trạm trung thế, hạ thế cấp nguồn chính, BMS, hệ thống báo cháy, hệ thống an ninh, hệ thống điện thoại và dữ liệu, hệ thống chiếu sáng, hệ thống điều khiển, ...
Địa điểm làm việc: Hà Nội, Hồ Chí Minh và các dự án trên lãnh thổ Việt Nam', N'Tốt nghiệp Đại học chuyên ngành tài chính, ngân hàng, kế toán, kiểm toán hoặc các chuyên ngành liên quan.
Ưu tiên ứng viên có chứng chỉ CPA, ACCA hoặc CFA; có kiến thức về xuất nhập khẩu- lĩnh vực thanh toán quốc tế.
Có khả năng lập kế hoạch, báo cáo.
Có kỹ năng trình bày, làm việc đội nhóm, làm việc dưới áp lực cao.
Có khả năng phân tích và tổng hợp vấn đề. Nhạy bén với các biến động, xu hướng tài chính.
Có tính kiên nhẫn, cẩn thận, chính xác.
Đọc, hiểu bằng tiếng Anh tốt. "', N'Giao tiếp bằng tiếng anh thành thạo', 4000000.0000, 10000000.0000, 0, CAST(N'2020-07-25' AS Date), CAST(N'2026-10-06' AS Date), 0, 1002, 1, 2, 2, N'user111@gmail.com', N'0987123133', N'Phòng nhân sự', 0, NULL, N'Đã xóa', 1, 520, 0)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2733, N'Nhân Viên Kinh Doanh Thị Trường (Có Sẵn Khách Hàng Tiềm Năng)', N'Nhân viên', N'Nhân viên', 2, N'- Tiếp nhận thông tin khách hàng tiềm năng và có sẵn nhu cầu từ Bộ phận tư vấn qua điện thoại (Telemarketing).- Sắp xếp đến gặp trực tiếp khách hàng mà Bộ phận tư vấn qua điện thoại (Telemarketing) đã hẹn khách hàng theo lịch hẹn có sẵn.- Không cần tự tìm kiếm Khách hàng, chỉ tập trung khai thác đúng đối tượng khách hàng có nhu cầu cần được tư vấn dịch vụ, tỉ lệ chốt khách hàng cao.- Hỗ trợ tư vấn, giải đáp thắc mắc cho khách hàng về các giải pháp liên quan đến tài chính, bảo hiểm.- Hỗ trợ Khách hàng ký Hợp đồng với công ty- Công việc chi tiết sẽ được trao đổi khi phỏng vấn THỜI GIAN VÀ ĐỊA ĐIỂM LÀM VIỆC- Thời gian làm việc: Giờ hành chính Thứ 2 – Sáng Thứ 7 (Chủ nhật nghỉ)- Địa điểm làm việc: Văn phòng công ty hoặc di chuyển theo lịch hẹn của Bộ phận tư vấn qua điện thoại', N'- Nam tốt nghiệp THPT trở lên- Có phương tiện đi lại, yêu thích công việc đi thị trường- Sức khỏe tốt, có thể gắn bó lâu dài- Yêu thích công việc tư vấn, giao tiếp và bán hàng trực tiếp- Khả năng nói chuyện lưu loát, thái độ tốt', NULL, 5000000.0000, 25000000.0000, 0, CAST(N'2020-07-26' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5005, 6, 2, 0, N'ntd2@gmail.com', N'0749117811', N'Ms. Nguyệt', 0, NULL, N'Đã duyệt', 1, 688, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2734, N'hot Chuyên Viên Kinh Doanh Sản Phẩm Hộp Cao Cấp', N'Nhân viên', N'Nhân viên', 5, N'- Chuyên viên kinh doanh, phát triển mở rộng thị trường theo chiến lược của công ty- Tư vấn và chăm sóc khách hàng- Giải đáp thắc mắc của khách hàng- Tiến hành giao dịch, ký hết hợp đồng- Chi tiết trao đổi khi phỏng vấn', N'- TN Đại Học chuyên ngành kinh tế, quản trị kinh doanh, marketing.. - Biết nhận định và đánh giá thị trường.- Có kỹ năng giao tiếp, đàm phán tốt-Tích cực, cầu tiến và chuyên nghiệp trong công việc là những phẩm chất không thể thiếu.', NULL, 6000000.0000, 12000000.0000, 0, CAST(N'2020-07-29' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5005, 6, 2, 1, N'ntd2@gmail.com', N'0539999880', N'Ms Chinh', 0, NULL, N'Đã duyệt', 1, 589, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2735, N'Chủ Quản CMM', N'Nhân viên', N'Nhân viên', 1, N'- Sử dụng, hiệu chuẩn và lập trình máy đo ba chiều CMM, kiểm tra đo kiểm các chi tiết gia công, các sản phẩm cơ khí chính xác- Sử dụng các dụng cụ đo: thước cặp, panm, đo ren - lỗ... để kiểm tra sản phẩm- Kiểm soát chất lượng theo đúng yêu cầu bản vẽ kỹ thuật', N'Biết lập trình đo CMMBiết đánh giá hiệu chuẩnBiết đọc và hiểu bản vẽ kỹ thuậtBiết vẽ autocar và làm Excel', NULL, 10000000.0000, 15000000.0000, 0, CAST(N'2020-06-21' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5005, 6, 2, 3, N'ntd2@gmail.com', N'0211682473', N'Ms. Lan', 0, NULL, N'Đã duyệt', 1, 763, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2736, N'Chuyên Viên Kinh Doanh Thương Mại Điện Tử', N'Nhân viên', N'Quản lý', 5, N'- Nghiên cứu thị trường, lập kế hoạch và triển khai bán sản phẩm của công ty- Trực tiếp triển khai các chiến dịch quảng cáo trên các kênh Facebook, Google, Zalo... theo ngân sách được duyệt.- Thực hiện các công việc khác theo sự phân công của Lãnh đạo.', N'+ Tốt nghiệp Trung cấp, Cao đẳng trở lên;+  Có kinh nghiệm về thương mại điện tử, website, Fanpage+  Có tinh thần ham học hỏi, có khả năng tự học cao;', NULL, 0.0000, 0.0000, 1, CAST(N'2020-06-30' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5005, 6, 2, 0, N'ntd2@gmail.com', N'0214540864', N'Chị Vân', 0, NULL, N'Đã duyệt', 1, 505, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2737, N'Nhân Viên Tư Vấn Khách Hàng Tại Văn Phòng', N'Nhân viên', N'Quản lý', 8, N'- Làm việc giờ hành chính tại văn phòng.- Tìm hiểu nhu cầu và mong muốn của khách hàng.- Tư vấn thông tin sản phẩm đầy đủ cho khách hàng.- Chăm sóc khách hàng, đánh giá khách hàng tiềm năng của dự án.- Hỗ trợ thủ tục hồ sơ và cách thức kinh doanh, phương thức thanh toán dịch vụ, sản phẩm cho khách hàng.- Báo cáo công việc thường xuyên cho Quản lý trực tiếp.CÔNG VIỆC KHÔNG ÁP ĐẶT DOANH THU CÁ NHÂN.', N'- Nam/ Nữ dưới 28 tuổi, ngoại hình ưa nhìn.- Chịu khó học hỏi và đam mê kinh doanh.- Khả năng giao tiếp và chốt giao dịch.- Chưa có kinh nghiệm được đào tạo nghề.', NULL, 7000000.0000, 15000000.0000, 0, CAST(N'2020-07-01' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5005, 6, 2, 0, N'ntd2@gmail.com', N'0150397917', N'Chị My', 0, NULL, N'Đã duyệt', 1, 523, 0)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2738, N'Nhân Viên Hành Chính Văn Phòng', N'Nhân viên', N'Nhân viên', 10, N'- Gọi điện thoại tư vấn và chăm sóc khách hàng có nhu cầu theo data có sẵn do công ty cung cấp.- Xây dựng hệ thống khách hàng tiềm năng, khách hàng mục tiêu- Đánh giá khách hàng tiềm năng, thông tin đến bộ phận kinh doanh - Công việc cụ thể sẽ được hướng dẫn chi tiết khi vào làm việc', N'- Không yêu cầu kinh nghiệm, sẽ được đào tạo khi vào làm việc tại công ty.- Nhanh nhẹn, nhiệt tình.- Kỹ năng giao tiếp, thương lượng, đàm phán tốt.- Kiên trì, trung thực, thẳng thắn, chịu được áp lực công việc.- Có trách nhiệm, ham học hỏi, nhiệt huyết trong công việc.', NULL, 7000000.0000, 10000000.0000, 0, CAST(N'2020-06-20' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5008, 7, 2, 0, N'ntd3@gmail.com', N'0260240345', N'Ms Thoa', 0, NULL, N'Đã duyệt', 1, 938, 0)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2739, N'Nhân Viên Dán Nhãn Bao Bì', N'Nhân viên', N'Nhân viên', 20, N'- Kiểm tra độ dày mỏng của sản phẩm- Phân loại nhãn thành phẩm - Ghi chép số liệu sản xuất- Chưa biết việc sẽ được đào tạo miễn phí', N'- Siêng năng, cẩn thận, có tinh thần cầu tiến.- Có tinh thần ý thức trách nhiệm cao, trung thực, nhiệt tình, năng động.', NULL, 8000000.0000, 11000000.0000, 0, CAST(N'2020-07-25' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5008, 7, 2, 0, N'ntd3@gmail.com', N'0850615851', N'Mr Phát', 0, NULL, N'Đã duyệt', 1, 582, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2740, N'Nhân Viên Tư Vấn Tại Văn Phòng (Thu Nhập Lên Tới 30 Triệu / Tháng)', N'Nhân viên', N'Nhân viên', 5, N'- Tìm kiếm, khai thác danh sách khách hàng tiềm năng và lưu trữ thông tin khách hàng;- Giới thiệu, tư vấn sản phẩm cho khách hàng qua điện thoại;- Hổ trợ Sales thiết lập lịch hẹn với khách hàng tiềm năng;- Chăm sóc và duy trì mối quan hệ với khách hàng tiềm năng cũng như khách hàng cũ nếu có.', N'- Giao tiếp tốt, nhanh nhẹn, hoạt bát.- Tốt nghiệp các trường Trung cấp trở lên.', NULL, 10000000.0000, 30000000.0000, 0, CAST(N'2020-06-22' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5008, 7, 2, 0, N'ntd3@gmail.com', N'0279026875', N'Ms. Nụ', 0, NULL, N'Đã duyệt', 1, 1003, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2741, N'Nhân Viên Kinh Doanh Thiết Bị Đèn Chiếu Sáng', N'Nhân viên', N'Nhân viên', 10, N'- Tìm hiểu và mở rộng thị trường kinh doanh của công ty- Tư vấn, giới thiệu sản phẩm thiết bị đèn chiếu sáng - Phối hợp với các phòng ban trong công ty thực hiện các yêu cầu cung cấp dịch vụ cho khách hàng;- Đề xuất các chính sách bán hàng, chương trình hậu mãi- Chăm sóc hệ thống khách hàng được giao', N'- Yêu cầu tốt nghiệp trung cấp trở lên - Ưu tiên các ứng viên có kinh nghiệm làm trong lĩnh vực vật liệu xây dưng, nội thất, thiết bị điện,...- Nhiệt tình, trung thực, nhanh nhẹn, xử lý tình huống linh hoạt, chủ động; Khả năng truyền đạt, giao tiếp, xây dựng mối quan hệ tốt.- Có phương tiện đi lại- Website : https://denledsvlight.com/', NULL, 7000000.0000, 15000000.0000, 0, CAST(N'2020-07-09' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5008, 7, 2, 0, N'ntd3@gmail.com', N'0374292622', N'Mr Quý', 0, NULL, N'Đã duyệt', 1, 928, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2742, N'Nhân Viên Bán Hàng Showroom (Đèn Trang Trí Cao Cấp)', N'Nhân viên', N'Quản lý', 5, N'- Tìm hiểu và mở rộng thị trường kinh doanh của công ty- Tư vấn và bán các sản phẩm công ty kinh doanh và được phân công phụ trách- Phối hợp với các phòng ban trong công ty thực hiện các yêu cầu cung cấp dịch vụ cho khách hàng;- Đề xuất các chính sách bán hàng, chương trình hậu mã- Sản phẩm là nội thất nhập khẩu Châu Âu- Trao đổi chi tiết công việc trong quá trình phỏng vấn.', N'- Yêu cầu trình độ Trung cấp trở lên.- Ưu tiên ứng viên có kinh nghiệm bán và tư vấn nội thất ít nhất 1 năm.- Có khả năng nắm bắt công việc nhanh, làm việc với áp lực cao- Nhiệt tình, trung thực, nhanh nhẹn, xử lý tình huống linh hoạt, chủ động; Khả năng truyền đạt, giao tiếp, xây dựng mối quan hệ tốt.', NULL, 8000000.0000, 20000000.0000, 0, CAST(N'2020-07-07' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5008, 7, 2, 0, N'ntd3@gmail.com', N'0122266374', N'Anh Hưng', 0, NULL, N'Đã duyệt', 1, 131, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2743, N'Nhân Viên Kinh Doanh [Bắc Giang, Hải Phòng, Vĩnh Phúc]', N'Nhân viên', N'Quản lý', 10, N'- Tuyển nhân viên kinh doanh tại tất cả các tỉnh thành phía bắc.- Giới thiệu, tư vấn sản phẩm cho khách hàng .- Mở rộng, tìm kiếm mở đại lý mới ở tất cả các tỉnh- Viếng thăm, chăm sóc khách hàng thường xuyên, tư vấn cho khách hàng về sản phẩm của công ty. Đảm bảo cập nhật thông tin về nhà phân phối, nhằm nắm bắt thị trường, xu hướng và những dự báo tiêu dùng. - Thực hiện các nghiệp vụ bán hàng để hỗ trợ nhà phân phối bán hàng tốt hơn.- Tìm hiểu các nhu cầu của nhà phân phối, kiến nghị những thay đổi cần thiết với cấp quản lý để phục vụ nhà phân phối được hoàn thiện hơn.- Chủ động đưa ra các đề nghị phù hợp để đảm bảo công việc được thực hiện tốt.- Tìm hiểu thị trường và đối thủ cạnh tranh.- Kiểm tra và theo dõi tình hình thanh toán của khách hàng.', N'- Nam từ 25 tuổi trở lên- Có Kinh nghiệm từ 1 năm trở lên- Có sức khỏe tốt, nhiệt tình, tâm huyết với công việc .- Trung thực , thẳng thắn', NULL, 25000000.0000, 30000000.0000, 0, CAST(N'2020-06-22' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5011, 8, 2, 1, N'ntd4@gmail.com', N'0301944467', N'Ms Thảo', 0, NULL, N'Đã duyệt', 1, 468, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2744, N'Chuyên Viên Đối Ngoại Và Dự Án (Không Yêu Cầu Kinh Nghiệm)', N'Nhân viên', N'Quản lý', 2, N'– Lên đơn hàng, đặt hàng, soạn thảo hợp đồng,và làm việc với các đối tác nước ngoài (Nhà cung cấp thiết bị y tế của Singapore, Mỹ, Pháp, Đức, Hàn Quốc).– Quản trị sự hợp tác củɑ các nhà cung cấp nước ngoài (Lâu dài và thân thiết).– Tìm kiếm các đối tác nước ngoài mới (Nếu cần).– Phụ trách các chương trình đào tạo của các đối tác khi có chương trình đào tạo tại Việt Nam và nước ngoài– Đón tiếp đối tác khi họ sang Việt Nam làm việc– Dịch cɑtɑlogue sản phẩm.– Dịch trong hội thảo, hội nghị.', N'–Tốt nghiệp Đại học chính quy các trường khối kinh tế, đối ngoại, ngoại ngữ (có kiến thức về kinh tế);– Ngoại ngữ: có khả năng đọc hiểu và soạn văn bản bằng tiếng Anh (TOEIC 700 /IELTS 6.5…), giao tiếp tốt với người nước ngoài;– Có kỹ năng giao tiếp, thuyết trình, thuyết phục tốt, không nói ngọng, không nói giọng địa phương;– Có khả năng xử lý tình huống linh hoạt, làm việc độc lập, chủ động, quản lý thời gian hiệu quả;– Có trách nhiệm trong công việc và biết sắp xếp làm việc ngoài giờ khi có yêu cầu.– Sử dụng thành thạo vi tính văn phòng, các phần mềm liên quɑn đến công việc, làm việc độc lập', NULL, 10000000.0000, 15000000.0000, 0, CAST(N'2020-06-28' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5011, 8, 2, 0, N'ntd4@gmail.com', N'0363927596', N'Ms. Thuận', 0, NULL, N'Đã duyệt', 1, 965, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2745, N'Digital Marketing Có Kinh Nghiệm', N'Nhân viên', N'Nhân viên', 5, N'- Có kiến thức và kinh nghiệm về Digital Marketing, am hiểu và thành thạo các thuật ngữ như SEO, SEM, Google Adwords, Google Analytic, Facebook Ads, Email, SMS …- Biết cách phát triển thương hiệu và quảng bá sản phẩm dịch vụ trên các kênh online như: Wordpress Website, Facebook, Google và các trang mạng xã hội khác.- Quản lý chi phí, kiểm tra, giám sát các hoạt động marketing online.- Định hướng và ra quyết định sử dụng nội dung, hình ảnh marketing - Lên kế hoạch và triển khai chương trình khuyến mãi, Event cho công ty.- Đề xuất các giải pháp về lượt truy cập, giải quyết các tình huống phát sinh.- Đề xuất, thực hiện các chương trình marketing đạt mục tiêu doanh thu của công ty.- Trực tiếp làm việc để hoàn thành các dự án cần thiết.', N'- Độ tuổi: từ 30 tuổi - 40 tuổi. - Giới tính: Không yêu cầu- Đã có kinh nghiệm quản lý đội, nhóm từ 5 người trở lên.- Có kinh nghiệm về Marketing Online- Thành thạo internet và sử dụng các công cụ online như Facebook, Ads, SEO, SMS Marketing….- Am hiểu về Cpanel và hosting… là một lợi thế.- Ưu tiên ứng viên có khả năng giao tiếp tiếng Anh.- Có khả năng sử dụng ngôn ngữ, viết bài tốt.- Có trách nhiệm trong công việc', NULL, 10000000.0000, 12000000.0000, 0, CAST(N'2020-07-17' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5011, 8, 2, 3, N'ntd4@gmail.com', N'0980259519', N'Ms Hạnh', 0, NULL, N'Đã duyệt', 1, 296, 0)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2746, N'hot Kế Toán Tổng Hợp', N'Nhân viên', N'Quản lý', 1, N'Kiểm tra Kế toán tiền lương - bảo hiểm - công đoàn cơ sở, Thanh toán tạm ứng,- Theo dõi công nợ khách hàng, Kế toán thuế, Hành chính - nhân sự của công ty xây dựng.Cụ thể:- Tập hợp công của công nhân làm tại các công trình, tính lương chính, lương thêm giờ; tính lương của các đội công nhân thuê ngoài.- Bảo hiểm: Quản lý hồ sơ bảo hiểm của toàn bộ hơn 50 cán bộ CNV trong công ty; Hoàn thiện hồ sơ, thủ tục hưởng chế độ Bảo hiểm của cán bộ CNV.- Công đoàn: Làm việc trực tiếp với Liên đoàn lao động Thành phố để thành lập Công đoàn cơ sở của Công ty. - Thanh toán các bộ giải trình tạm ứng của nhân viên trong quá trình công tác, tiếp khách; theo dõi công nợ tạm ứng của cán bộ CNV;- Xuất hóa đơn đầu ra, hạch toán Doanh thu, chi phí hàng tháng; - Theo dõi công nợ đầu ra, đầu vào; Thu hồi công nợ đầu vào và thanh toán công nợ đầu ra khi đến hạn; Lập biên bản đối chiếu công nợ định kì với khách hàng, Soạn thảo các công văn gia hạn nợ, đòi nợ, các biên bản trừ công nợ và các giấy tờ khác liên quan; Công nợ với nhân công thuê ngoài; Hàng tuần lập bảng tổng hợp công nợ báo cáo cấp trên.- Tập hợp hóa đơn đầu vào, đầu ra; Theo dõi, lập tờ khai thuế Giá trị gia tăng hàng tháng; Nộp thuế thu nhập cá nhân tạm tính hàng tháng, Thực hiện các thủ tục đăng ký mã số thuế thu nhập cá nhân cho các nhân viên mới; - Lập quyết toán thuế thu nhập cá nhân hàng năm; - Hạch toán vào sổ sách, phần mềm kế toán các tài khoản tiền gửi ngân hàng, tạm ứng, phân bổ chi phí lương.- Hành chính - Nhân sự: Theo dõi, cập nhật, báo cáo hàng tháng về sự thay đổi nhân sự; Tiếp nhận hồ sơ nhân sự; Soạn thảo các quyết định, công văn, thông báo tới toàn thể cán bộ CNV và tới các cơ sở ban ngành quản lý như Thuế, Bảo hiểm, Liên đoàn lao động...; Lập biên bản họp, các giấy đề nghị gửi ban lãnh đạo; Tổ chức công tác lưu trữ giấy tờ, tài liệu.Tham gia tổ chức các sự kiện như: Kỉ niệm ngày thành lập Công ty, Giao lưu văn hóa văn nghệ, Tổ chức từ thiện, Du lịch quy mô toàn công ty...- Tính toán  sản xuất : Tính giá vốn cho công trình .Tính toán, điều chỉnh hợp lý số dư cuối kỳ chuyển sang số dư đầu kỳ năm hiện hành.- Theo dõi và thực hiện trích khấu hao tài sản cố định, công cụ dụng cụ. Phân bổ theo từng khoản mục chi phí. - Kiểm tra, giám sát việc luân chuyển hàng tồn kho, xác định trị giá nguyên vật liệu xuất kho (bình quân gia quyền liên hoàn)._ Kiểm tra , tính toán  , và khai báo xuất nhập khẩu ( nếu có ) hiện công ty làm dịch vụ .- Trao đổi cụ thể khi phỏng vấn', N'-    Tốt nghiệp chuyên ngành : KÊ TOÁN TÀI CHÍNH – có bằng kê toán trưởng là một lợi thế-    Sử dụng thành thạo các phần mềm kế toán, đã có kinh nghiệm làm các công ty hàn quốc .-    Đam mê, Nhiệt tình, Trung thực trong công việc-    Khả năng sáng tạo cao, khả năng làm việc độc lập, làm việc nhóm tốt.-    Chịu được áp lực cao trong công việc, xử lí và hoàn thành công việc đúng thời hạn được giao.-     Năng động, sáng tạo có tư duy logic, có khả năng làm việc độc lập', NULL, 8000000.0000, 14000000.0000, 0, CAST(N'2020-07-13' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5011, 8, 2, 2, N'ntd4@gmail.com', N'0216101483', N'Ms. Thiết', 0, NULL, N'Đã duyệt', 1, 445, 0)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2747, N'Trưởng Phòng Y Tế', N'Quản lý', N'Nhân viên', 1, N'- Đề xuất và định hướng công việc liên quan đến hồ sơ pháp lý theo yêu cầu của Ban lãnh đạo về quản lý trang thiết bị y tế- Lập báo cáo và phân tích các hoạt động pháp lý trong lĩnh vực y tế của Tập đoàn- Tư vấn và tham gia giải quyết các phát sinh về mặt luật pháp trong quản lý, sử dụng trang thiết bị y tế- Kiểm tra và cập nhật bổ sung các yêu cầu luật pháp từ cơ quan nhà nước để chuẩn bị các thủ tục pháp lý cho hoạt động quản lý trang thiết bị y tế- Tham gia các công việc khác theo nội quy, quy chế của công ty', N'- Tốt nghiệp đại học chuyên ngành Dược trở lên- Có kinh nghiệm làm việc trong lĩnh vực y tế phòng khám- Am hiểu về trang thiết bị y tế và biết cách sử dụng trang thiết bị y tế trong phòng khám- Hiểu biết về các tiêu chuẩn áp dụng hoặc cấp giấy chứng nhận đăng ký lưu hành đối với trang thiết bị y tế, hồ sơ đề nghị cấp mới, cấp lại, gia hạn lưu hành trang thiết bị y tế- Nắm rõ các quy định của pháp luật có liên quan về việc quản lý, sử dụng trang thiết bị y tế tại các phòng khám của các dự án Tập đoàn và các công ty thành viên- Có đầu óc nhạy bén, làm việc nhóm và làm việc độc lập', NULL, 20000000.0000, 25000000.0000, 0, CAST(N'2020-07-03' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5011, 8, 2, 5, N'ntd4@gmail.com', N'0718639826', N'Phòng Nhân Sự', 0, NULL, N'Đã duyệt', 1, 380, 0)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2748, N'Nhân Sự (Thu Nhập 10 - 12 Triệu)', N'Nhân viên', N'Quản lý', 1, N'1. Tính lương công nhân viên.2. Làm bảo hiểm do nhà nước quy định (BHXH, BHYT, BHTN..).3. Tuyển dụng và đào tạo.', N'1. Có kinh nghiệm công việc nhân sư (tính lương, bảo hiểm, tuyển dụng và đào tạo).2. Thành thạo máy vi tính.3. Trình độ tiếng Hoa hoặc tiếng Anh khá.4. Chăm chỉ, nhiệt tình và tỉ mỉ.', NULL, 10000000.0000, 12000000.0000, 0, CAST(N'2020-06-27' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5014, 9, 2, 0, N'ntd5@gmail.com', N'0997866868', N'Lý Phúc Nhuận', 0, NULL, N'Đã duyệt', 1, 663, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2749, N'Tổ Trưởng Sản Xuất Biết Tiếng Trung', N'Nhân viên', N'Nhân viên', 6, N'- Quản lý đi làm, kỷ luật, hiệu quả sản xuất và tác nhân gây lỗi-  Đào tạo nhân viên mới trạm mới và nhân viên cũ trạm mới-  Hoàn thành các nhiệm vụ mà kỹ sư sản xuất giao phó', N'- Tốt nghiệp Cấp 3 trở lên- Có 1 năm Kinh nghiệm ở các vị trí tương đương- Biết tiếng Trung', NULL, 15000000.0000, 20000000.0000, 0, CAST(N'2020-07-16' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5014, 9, 2, 1, N'ntd5@gmail.com', N'0306266133', N'Mr. Tú', 0, NULL, N'Đã duyệt', 1, 795, 0)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2750, N'Kỹ Sư Cơ Khí / Chế Tạo Máy', N'Nhân viên', N'Nhân viên', 1, N'- Tham gia lập kế hoạch, bố trí chuẩn bị sản xuất và theo dõi thực hiện;- Tham gia lập kế hoạch bảo dưỡng máy, thiết bị định kỳ và phối hợp thực hiện,- Xây dựng, ban hành và phổ biến các tài liệu kỹ thuật công nghệ (bản vẽ kỹ thuật, lưu đồ sản xuất, hướng dẫn công nghệ sản xuất, hướng dẫn kiểm tra, chỉ tiêu chất lượng, quy cách bao gói) các sản phẩm của xưởng sản xuất (giấy, vở, đồ dùng học sinh);- Kiểm soát việc thực hiện theo các quy trình, tiêu chuẩn quy định đã ban hành tại xưởng sản xuất;- Kiểm soát chất lượng vật tư đầu vào, hướng dẫn kỹ thuật, giám sát quá trình sản xuất, kiểm tra chất lượng thành phẩm trước khi nhập kho; Lưu giữ mẫu và hồ sơ đúng quy định;- Tham gia nghiên cứu phát triển công nghệ, đánh giá, chế thử và hoàn thiện sản phẩm mới;- Thực hiện lập kế hoạch và báo cáo định kỳ.- Các công việc khác thuộc chức năng, nhiệm vụ của đơn vị quản lý trực tiếp. Làm việc tại 672 Ngô Gia Tự, Đức Giang, Long Biên, Hà Nội.', N'- Trình độ: Cao đẳng trở lên.- Chuyên ngành: Kỹ thuật cơ khí/chế tạo máy - Kỹ năng/khả năng: + Thành thạo Word, Excel, Autocad, Corel và các trang thiết bị văn phòng; + Kỹ năng phân tích, giải quyết vấn đề và tư duy độc lập;+ Kỹ năng làm việc nhóm; + Kỹ năng tổng hợp, lập báo cáo;- Ưu tiên ứng viên có kinh nghiệm về kỹ thuật tại các đơn vị sản xuất giấy vở, nhựa và văn phòng phẩm', NULL, 8000000.0000, 12000000.0000, 0, CAST(N'2020-07-08' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5014, 9, 2, 1, N'ntd5@gmail.com', N'0951644634', N'Ms Thủy', 0, NULL, N'Đã duyệt', 1, 931, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2751, N'Nhân Viên Thiết Kế Nội Thất', N'Nhân viên', N'Quản lý', 3, N'- Thiết kế ý tưởng nội thất các công trình dân dụng: biệt thự, nhà phố, văn phòng, khu đô thị, căn hộ, khách sạn, … và các sản phẩm nội thất và các hạng mục nội thất, ngoại thất công trình.- Dựng phối cảnh và render nội thất- Bố cục, sắp xếp khoa học các hình ảnh và bản vẽ của phương án để trình bày với khách hàng.', N'- Thành thạo các chương trình thiết kế: Autocad, 3D, Photoshop,  sketchup, Corel Draw, Illustrator, - Biết vi tính văn phòng- Độ tuổi: Không giới hạn tuổi', NULL, 10000000.0000, 15000000.0000, 0, CAST(N'2020-07-12' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5014, 9, 2, 1, N'ntd5@gmail.com', N'0861764685', N'Mrs Biểu', 0, NULL, N'Đã duyệt', 1, 953, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2752, N'Nhân Viên Kinh Doanh ( &gt;10 Triệu)', N'Nhân viên', N'Quản lý', 5, N'- Trực tiếp tìm kiếm và phát triển thị trường kênh bán- Liên hệ với khách hàng (kênh bán, đại lý, cửa hàng…) để chào sản phẩm, ký HĐ với KH- Kiểm tra hoạt động của kênh bán và chăm sóc kênh bán, thu hồi công nợ- Chịu trách nhiệm về chỉ tiêu được giao và các KPIs liên quan đến công việc- Thực hiện các nhiệm vụ khác do Giám đốc bán hàng phân công (nếu có)- Báo cáo trực tiếp cho Giám đốc bán hàng', N'- Tốt nghiệp Cao đẳng trở lên- Giới tính nam- Có từ 1 đến 2 năm kinh nghiệm bán hàng kinh doanh- Kỹ năng giao tiếp tốt, đam mê yêu thích kinh doanh.- Giao tiếp tốt, xử lý tình huống nhanh nhạy- Chịu được áp lực công việc, Có kỹ năng làm việc nhóm & làm việc độc lập- Có khả năng viết bài marketing là 1 lợi thế', NULL, 7000000.0000, 15000000.0000, 0, CAST(N'2020-07-24' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5014, 9, 2, 1, N'ntd5@gmail.com', N'0731937800', N'Mrs Hằng', 0, NULL, N'Đã duyệt', 1, 253, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2753, N'Trưởng Nhóm Content Marketing', N'Nhân viên', N'Quản lý', 2, N'- Quản lý nội dung trên các kênh.- Tham gia lên ý tưởng và thực hiện các chất liệu phục vụ nội dung về: hình ảnh / clip / Hình GIF / livestream...- Tham gia lên ý tưởng cho các chiến dịch marketing- Lên ý tưởng và chịu trách nhiệm biên soạn nội dung truyền thông cho các sản phẩm tour và branding, từ đó định hướng sản xuất chất liệu truyền thông cho các team khác (team sản xuất, thiết kế hình ảnh, video) trên các kênh: Facebook / Website / Zalo / Email / Youtube)- Quản lý team Cộng tác viên để thực thi đảm bảo chất lượng các nội dung.- Các CV khác theo yêu cầu quản lý trực tiếp.', N'- Có kinh nghiệm làm việc content marketing ít nhất 2 năm trên các platform như Facebook, Instagram, Website, Youtube..- Có kinh nghiệm xây dựng content strategy / content plan dài hạn, ngắn hạn, theo campaign & quản lý thực thi theo kế hoạch.- Có khả năng làm việc độc lập, làm việc nhóm, chịu được áp lực công việc.- Tinh thần làm việc trách nhiệm.', NULL, 12000000.0000, 15000000.0000, 0, CAST(N'2020-06-30' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5014, 9, 2, 2, N'ntd5@gmail.com', N'0112206952', N'Ms Châu', 0, NULL, N'Đã duyệt', 1, 983, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2754, N'hot Nhân Viên Thị Trường (Có Xe Di Chuyển)', N'Nhân viên', N'Nhân viên', 5, N'- Duy trì và mở mới các khách hàng trong khu vực mình quản lý .- Báo cáo trực tiếp cho trưởng phòng ,ban giám đốc tình hình kinh doanh khu vực mình quản lý.- Lập lịch công tác tuần ,tháng và đề xuất phương án bán hàng .', N'- Tốt nghiệp trung cấp trở lên ,- Có 2 năm kinh nghiệm làm thị trường vùng ,- Biết kỹ năng chăm sóc khách hàng ,và báo cáo bán hàng,kỹ năng thuyết phục khách hàng.- Trung thực nhiệt tình .', NULL, 10000000.0000, 30000000.0000, 0, CAST(N'2020-07-10' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5014, 9, 2, 2, N'ntd5@gmail.com', N'0752641355', N'Nguyễn Đăng Tuấn', 0, NULL, N'Đã duyệt', 1, 289, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2755, N'Thiết Kế Thời Trang - Mẫu In (Bình Dương)', N'Nhân viên', N'Nhân viên', 2, N'- Lên ý tưởng, thiết kế các mẫu.- Nghiên cứu, tìm kiếm các xu hướng thời trang quốc tế mới nhất hiện nay, thường xuyên tìm hiểu và nắm bắt xu hướng, nhu cầu khách hàng.- Các công việc khác theo yêu cầu của cấp trên.- Công việc sẽ được trao đỏi cụ thể khi phỏng vấn.', N'- Sử dụng thành thạo các phần mềm thiết kế Corel, Photoshop, AI…- Gout thẩm mỹ tinh tế, sang trọng.- Có kinh nghiệm, Am hiểu thời trang, ưu tiên có kinh nghiệm về mặt hàng thời trang thể thao- Sáng tạo, nhiệt huyết, trách nhiệm cao trong công việc.', NULL, 8000000.0000, 10000000.0000, 0, CAST(N'2020-07-01' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5014, 9, 2, 0, N'ntd5@gmail.com', N'0489671910', N'Mr Thìn', 0, NULL, N'Chưa duyệt', 1, 688, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2756, N'[ Hà Nội, Hồ Chí Minh ] - Nhân Viên Bán Hàng Tại Showrom - Nữ Giới', N'Nhân viên', N'Nhân viên', 4, N'-Tư vấn, giới thiệu và bán sản phẩm-Phát triển và duy trì mối quan hệ tốt với khách hàng-Thực hiện đạt và vượt các chỉ tiêu doanh số và doanh thu đặt ra-Trưng bày và quản lý, theo dõi hàng hóa tại quầy được giao-Thể hiện hình ảnh, thương hiệu và bán hàng hiệu quả phong cách bán hàng chuyên nghiệpTại Hồ Chí Minh: AEON MALL TÂN PHÚ, SÀI GÒN CENTRETại Hà Nội: Royal City và Vicom Center Phạm Ngọc Thạch', N'-Tốt nghiệp từ PTTH trở lên-Cao từ 1m55 trở lên, Ngoại hình khá, khuôn mặt ưa nhìn.-Nhanh nhẹn, trung thực, trách nhiệm, năng động, sáng tạo.-Có khả năng giao tiếp thuyết phục tốt, yêu thích công việc kinh doanh, bán hàng.-Có kinh nghiệm bán hàng, biết ngoại ngữ là một lợi thế -Kĩ năng phát triển thương hiệu hàng hóa, thương hiệu Công ty.', NULL, 8000000.0000, 10000000.0000, 0, CAST(N'2020-07-02' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5014, 9, 2, 0, N'ntd5@gmail.com', N'0359490490', N'Ms Hoa', 0, NULL, N'Đã duyệt', 1, 191, 0)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2757, N'hot Kỹ Thuật Viên - Field Application Engineer', N'Nhân viên', N'Nhân viên', 1, N'- Đảm trách việc hỗ trợ bác sĩ tim mạch cấy máy điều trị nhịp tim- Kiểm tra và theo dõi máy đã được cấy trong người bệnh nhân- Tư vấn bác sĩ và bệnh nhân- Hỗ trợ ban giám đốc soạn các hồ sơ dự thầu bệnh viện,- Hỗ trợ chương trình nghiên cứu có tầm vóc quốc tế của công ty,- Soạn và trình các bài giới thiệu thiết bị cho ban giám đốc vác các bác sĩ bệnh viện (không thường xuyên)', N' Thách thức chính cho ứng viên là: - Do kỹ thuật này không có trường lớp nào đào tạo tại VN, nên phải học từ kinh nghiệm các đồng nghiệp hiện đang làm việc trong công ty, từ giám đốc kỹ thuật của Cty ở Mỹ, và tự học qua mạng về điện sinh lý tim.- Sẽ phải thấu hiểu các chức năng các máy điều trị nhịp tim được công ty cung cấp. Nhiều thiết bị có cả mấy trăm thông số có thể điều chỉnh được!- Cần phải hợp tác với các đồng nghiệp trong công ty- Từ phân tích các số liệu ghi lại trong máy đã được cấy trong người bệnh nhân, sẽ chẩn đoán các cơn loạn nhip của bệnh nhân, rồi sẽ làm việc với bác sĩ để giúp bệnh nhân trở lại cuộc sống bình thường.- Cần phải hợp tác tốt với các đồng nghiệp trong công ty. Điều kiện dự tuyển - Tốt nghiệp đại học chính quy chuyên ngành: y sinh, điện tử, điện- Có tinh thần luôn luôn học hỏi và khả năng tiếp nhận kiến thức mới nhanh qua sách và qua mạng.- Năng động, giao tiếp tốt, có tinh thần trách nhiệm với công việc, có khả năng làm việc độc lập, theo nhóm- Khả năng đọc, dịch tài liệu tiếng Anh, giao tiếp tiếng Anh tốt: nếu đạt sẽ được điểm ưu tiên.- Sử dụng thành thạo Microsoft Office: Word, Excel, Access; và một trong các chương trình đồ họa như Corel hay Photoshop.- Có sức khỏe tốt, sẵn sàng đi công tác (có thể qua đêm) ở tỉnh, theo sự phân công của công ty', NULL, 8000000.0000, 12000000.0000, 0, CAST(N'2020-07-12' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5014, 9, 2, 0, N'ntd5@gmail.com', N'0721983381', N'Nguyễn Thị Phương Dung', 0, NULL, N'Đã duyệt', 1, 178, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2758, N'Nhân Viên Kinh Doanh Lương Trên 10 Triệu', N'Nhân viên', N'Nhân viên', 6, N'- Bán hàng chất phụ gia , dầu nhớt, má phanh cho xe ô tô, xe máy, các phương tiện vận tải chuyên dụng - Giới thiệu sản phẩm, hướng dẫn, tư vấn cho khách hàng các thông tin về sản phẩm .- Chăm sóc những khách hàng cũ được công ty giao, phát triển khách hàng mới, thiết lập mối quan hệ với các gara ô tô, showroom ô tô- Thương lượng, đàm phán với đối tác để ký kết đơn hàng.- Quản lý và thu hồi công nợ đúng hạn- Lập báo cáo tình hình thị trường, báo cáo kế hoạch công tác, kinh doanh theo tuần, tháng, năm', N'- Không yêu cầu kinh nghiệm. Ưu tiên những ứng viên đã làm Sales / sản xuất tại các công ty kinh doanh vận tải- Hiểu biết và có kiến thức trong lĩnh vực ô tô, công nghệ- Tốt nghiệp Cao đẳng, Đại học .- Có khả năng làm việc độc lập và theo nhóm- Ăn nói lưu loát, giao tiếp tốt.- Tính tình trung thực, chịu khó, cẩn thận, có khả năng thuyết phục.- Nhiệt huyết, ham học hỏi, có tinh thần cầu tiến, yêu thích công việc kinh doanh.- Thành thạo máy tính văn phòng.- Có phương tiện đi lại', NULL, 10000000.0000, 13000000.0000, 0, CAST(N'2020-06-19' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5014, 9, 2, 0, N'ntd5@gmail.com', N'0473372364', N'Mr. Tuấn', 0, NULL, N'Đã duyệt', 1, 424, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2759, N'Nhân Viên Nữ Trả Lời Facebook Và Các Kênh TMđt Lương 10 Triệu Trở Lên HCM', N'Nhân viên', N'Nhân viên', 3, N'- Trả lời tin nhắn khách hàng trên Facebook, Shopee, soạn hàng phụ kế toán- Thời gian làm việc :- Ca Full : Từ 8h - 21h tối : Lương cơ bản 10 triệu + doanh số- Ca chiều : 15h30p - 21h30p : Lương 7tr + thưởng doanh số (Không Xoay Ca, Làm Cố Định) - Địa điểm làm việc: Gần Khu công nghiệp Tân Bình', N'- Ưu tiên làm việc lâu dài.- Mức lương khởi điểm chỉ là con số ban đầu, bạn sẽ nhận được phần thưởng xứng đáng với sự nỗ lực của mình.- Đánh máy nhanh.- Có kinh nghiệm trả lời khách hàng qua Fanpage và các kênh thương mại Shopee...', NULL, 10000000.0000, 12000000.0000, 0, CAST(N'2020-07-15' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5017, 10, 2, 0, N'ntd6@gmail.com', N'0718320566', N'Ms.Trang', 0, NULL, N'Đã duyệt', 1, 761, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2760, N'Nhân Viên Hỗ Trợ Người Tìm Việc', N'Nhân viên', N'Nhân viên', 3, N'- Gọi điện hỗ trợ người tìm việc tạo hồ sơ trực tuyến- Kiểm duyệt hồ sơ của người tìm việc- Tư vấn, giải đáp thắc mắc cho người tìm việc qua hotline và hotmail- Kiểm soát chất lượng hồ sơ qua hệ thống quản lý- Thực hiện công việc theo chỉ đạo của cấp trên- Chi tiết trao đổi khi phỏng vấn', N'- Nữ độ tuổi từ 22 đến 26.- Có kỹ năng giao tiếp qua điện thoại tốt- Có tính chịu khó, cẩn thận là một lợi thế- Tư duy logic- Thao tác tin học văn phòng tốt, đánh máy nhanh và thành thạo- Điều kiện bắt buộc: Có máy tính xách tay (win 10) để phục vụ cho công việc.', NULL, 5000000.0000, 7000000.0000, 0, CAST(N'2020-06-21' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5017, 10, 2, 0, N'ntd6@gmail.com', N'0160939367', N'Ms. Như', 0, NULL, N'Đã duyệt', 1, 561, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2761, N'Nhân Viên Văn Phòng - Hồ Chí Minh', N'Nhân viên', N'Nhân viên', 5, N'- Làm việc tại văn phòng công ty Số lượng: 5 người - Gọi điện và tư vấn khách hàng - Đàm phán và kí kết hợp đồng. - Chăm sóc khách hàng sau bán hàng. - Hoàn thành chỉ tiêu đề ra.- Chi tiết công việc sẽ trao đổi cụ thể hơn khi phỏng vấn.http://hienphanco.com.vn/', N'- Nam, nữ từ 22 tuổi trở lên- Tốt nghiệp : Đại học - cao đẳng - trung cấp các ngành liên quan đến nông nghiệp và hoá học.- Kinh nghiệm: Đã từng làm kinh doanh bán hàng qua điện thoại .- Có khả năng độc lập trong công việc, khả năng tự giác cao.- Năng động, nhiệt tình,chịu được áp lực trong công việc.', NULL, 7000000.0000, 10000000.0000, 0, CAST(N'2020-06-19' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5017, 10, 2, 0, N'ntd6@gmail.com', N'0194925538', N'Ms.Quy', 0, NULL, N'Đã duyệt', 1, 928, 0)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2762, N'Nhân Viên Triển Khai Phần Mềm Kế Toán', N'Nhân viên', N'Nhân viên', 5, N'• Giới thiệu, tư vấn cho khách hàng về phần mềm kế toán SimSoft.• Cài đặt, hướng dẫn sử dụng phần mềm kế toán SimSoft cho khách hàng.• Tư vấn, hỗ trợ khách hàng trong quá trình sử dụng phần mềm.', N'• Tốt nghiệp Đại học/Cao đẳng chuyên ngành Tài chính - Kế toán hoặc CNTT.• Có kinh nghiệm làm việc tối thiểu là 02 năm.• Hiểu biết sâu về nghiệp vụ kế toán và lĩnh vực phần mềm.• Là người cầu tiến, có tinh thần trách nhiệm cao.• UV gửi kèm theo Phiếu thông tin ứng viên (tải từ website của DAMIhttp://damivn.com/upload/DaMi_Thong_Tin_Ung_Vien.doc', NULL, 7000000.0000, 12000000.0000, 0, CAST(N'2020-07-02' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5017, 10, 2, 2, N'ntd6@gmail.com', N'0615034566', N'Ms. Phương Thanh', 0, NULL, N'Đã duyệt', 1, 318, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2763, N'hot [Thanh Xuân] Nữ Nhân Viên Tư Vấn Tuyển Dụng Không Yêu Cầu Kinh Nghiệm', N'Nhân viên', N'Quản lý', 3, N'- Tìm kiếm và khai thác thông tin Khách hàng- Thiết lập cuộc gọi, tư vấn và giới thiệu cho khách hàng các gói dịch vụ đăng tin/search CV của My Work(Telesales)- Đàm phán, thương lượng và thực hiện các thủ tục ký kết hợp đồng với khách hàng.- Xây dựng và duy trì mối quan hệ tốt với khách hàng.- Bảo đảm hoàn thành chỉ tiêu cá nhân và kế hoạch kinh doanh theo lộ trình phát triển của Công ty.', N'- Nghiêm túc, trách nhiệm với công việc và chỉ tiêu kinh doanh của cá nhân- Kỹ năng giao tiếp, thuyết trình và đàm phán tốt.- Không nói ngọng, nói lắp, ưu tiên những người có kinh nghiệm trong lĩnh vực dịch vụ khách hàng, tuyển dụng và chăm sóc khách hàng.- Không yêu cầu phải có kinh nghiệm, sẽ được đào tạo hoàn toàn miễn phí- Thành thạo Vi tính văn phòng. - Có laptop cá nhân.', NULL, 5000000.0000, 7000000.0000, 0, CAST(N'2020-06-24' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5017, 10, 2, 0, N'ntd6@gmail.com', N'0146069118', N'Ms Quỳnh Anh', 0, NULL, N'Đã duyệt', 1, 275, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2764, N'Chuyên Viên Kinh Doanh Bất Động Sản', N'Nhân viên', N'Nhân viên', 50, N'- Tìm kiếm, khai thác, tư vấn khách hàng và bán các sản phẩm và dịch vụ BĐS củɑ công ty- Chủ động khai thác nguồn khách hàng tiềm năng- Ϲhăm sóc khách hàng hiện tại và khách hàng tiềm năng- Tiếp thu, nâng cɑo kiến thức về sản phẩm và thị trường để tư vấn chốt hợp đồng khách hàng- Phối hợp với đội nhóm để thực hiện chỉ tiêu chung.', N'- Không yêu cầu kinh nghiệm- Chăm chỉ nhiệt tình, ham học hỏi và có tinh thần đạo đức cầu tiến- Tác phong chuyên nghiệp, lịch sự- Giao tiếp tốt', NULL, 15000000.0000, 30000000.0000, 0, CAST(N'2020-07-03' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5020, 11, 2, 0, N'ntd7@gmail.com', N'0134113404', N'Ms Thảo Nguyên', 0, NULL, N'Đã duyệt', 1, 533, 0)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2765, N'Cán Bộ Pháp Lý, Quản Lý Công Nợ Bên Công Ty Tài Chính / Ngân Hàng', N'Nhân viên', N'Quản lý', 10, N'Công ty cổ phần thu hồi nợ HP là đơn vị quản lý, xử lý nợ pháp lý, xử lý nợ tố tụng (khởi kiện khách nợ theo các quy định của pháp luật)  cho các công ty tài chính / ngân hàng thuộc hệ thống ngân hàng nhà nước. Hiện công ty CP thu hồi nợ HP có nhu cầu tuyển dụng nhân sự cụ thể như sau:1. Vị trí tuyển dụng:- Cán bộ pháp lý địa bàn:- Mô tả công việc: Xác minh thông tin địa chỉ / thu nhập khách nợ theo hồ sơ vụ việc phía ngân hàng cung cấp. Xác minh đối chiếu khoản nợ theo luật định. Giúp bộ phận pháp chế lập hồ sơ khởi kiện khách nợ theo yêu cầu. Đại diện cho ngân hàng / công ty tài chính nộp hồ sơ pháp lý khởi kiện khách nợ đến các cơ quan nhà nước có thẩm quyền theo ủy quyền.2. Địa bàn làm việc :- Nha Trang: 02 NV- Diên Khánh: 02 NV- Cam Lâm, Cam Ranh: 02 NV- Ninh Hòa, Vạn Ninh: 02 NV- Ninh Thuận: 02 NV- Bình Thuận: 02 NV- Đồng Nai: 02 NV- Bình Dương: 02 NV- Lâm Đồng: 02 NV- Kon Tum: 02 NV- Đắc Lắc: 02 NV- Gia Lai: 02 NV- Đắc Nông: 02 NV- Quảng Bình:02 NV- Quảng Trị:02 NV  - Thừa Thiên Huế :02 NV  - Quảng Nam: 02 NV- Quảng Ngãi: 02 NV- Bình Định: 02 NV- Phú Yên: 02 NV- Đà Nẵng: 02 NV (Có hỗ trợ 1.000.000đ/tháng)- Hải Phòng: 02 NV (Có hỗ trợ 1.000.000đ/tháng)- Hà Nội : 02 NV (Có hỗ trợ 1.000.000đ/tháng)- Hải Dương : 02 NV  (Có hỗ trợ 1.000.000đ/tháng)- Quảng Ninh : 02 NV  (Có hỗ trợ 1.000.000đ/tháng)- Hưng Yên : 02 NV (Có hỗ trợ 1.000.000đ/tháng)', N'- Tốt nghiệp trung cấp trở lên- Trung thực, nhanh nhẹn', NULL, 12000000.0000, 15000000.0000, 0, CAST(N'2020-06-25' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5020, 11, 2, 0, N'ntd7@gmail.com', N'0879870896', N'Ms Hà', 0, NULL, N'Đã duyệt', 1, 289, 0)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2766, N'hot [Stda- Cenland] Chuyên Viên Tư Vấn Bất Động Sản Lương Cb Tới 12Triệu - Thu Nhập Từ 20-100 Triệu / Tháng', N'Nhân viên', N'Nhân viên', 50, N'VỚI CHẾ ĐỘ LƯƠNG + MÔI TRƯỜNG + ĐÀO TẠO + GIỎ HÀNG + PHÍ HOA HỒNG HOT NHẤT THỊ TRƯỜNG BĐS MIỀN BẮCCENLAND đang không ngừng phát triển, mở rộng quy mô trở thành hệ thống phân phối bất động sản chuyên nghiệp, lớn mạnh bậc nhất quốc gia.CHÚNG TÔI CẦN BẠN:- Nắm vững các thông tin về dự án.- Tìm kiếm & chăm sóc khách hàng tiềm năng- Báo giá, thương thảo hợp đồng mua bán, thỏa thuận thời hạn thanh toán và giao dịch theo quy định của Công ty.-Tham gia tất cả các khóa đào tạo cho do công ty tổ chức, thường xuyên nâng cao năng lực cá nhân thông qua bổ sung/cập nhật các kiến thức, kỹ năng về bất động sản.', N'- Tự tin, năng động- Có kỹ năng làm việc độc lập và làm việc nhóm- Có khả năng giao tiếpTuy nhiên chúng tôi không coi trọng bằng cấp, chúng tôi quan trọng hiệu năng làm việc, quyết tâm không bỏ lỡ nhân tài nên:- Chỉ cần bạn tốt nghiệp trung cấp, cao đẳng, đại học, sinh viên mới ra trường, ứng viên làm việc các ngành nghề khá chưa có kinh nghiệm bạn cũng đừng quá e ngại, chỉ cần bạn có niềm đam mê, yêu thích BDS và thực sự muốn làm giàu CHÚNG TÔI SẼ ĐÀO TẠO BẠNChúng tôi chào đón những ứng viên có kinh nghiệm bán hàng trong lĩnh vực Bất động sản, ngân hàng, chứng khoán, ôtô, bảo hiểm, hoặc cử nhân tốt nghiệp chuyên ngành bất động sản, quản trị kinh doanh, marketing và sẵn sàng dành cơ hội cho những ứng viên dám khẳng định mình ở vị trí TRƯỞNG NHÓM, TRƯỞNG PHÒNG KINH DOANH DÁM THỬ - DÁM ĐÁNH ĐỔI – DÁM LÀM – DÁM SAI – DÁM HỌC CÁI MỚI.', NULL, 25000000.0000, 30000000.0000, 0, CAST(N'2020-07-06' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5020, 11, 2, 0, N'ntd7@gmail.com', N'0142135765', N'Ms Trang', 0, NULL, N'Đã duyệt', 1, 45, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2767, N'Kỹ Sư Dự Toán - Lương + Phụ Cấp + Thưởng', N'Nhân viên', N'Nhân viên', 10, N' Công việc dự toán sẽ sử dụng các phần mềm chuyên dụng để thực hiện bốc khối lượng của các thiết bị điều hòa, thiết bị thông gió, thiết bị điện, thiết bị vệ sinh, thiết bị cấp nước, thiết bị thải nước, thiết bị nước nóng, thiết bị chữa cháy, thiết bị gas,… Từ khối lượng đã bốc sẽ tính toán ra tổng chi phí để thi công từng loại thiết bị (chi phí vật tư, chi phí nhân công và một số chi phí khác). Tất cả nhân viên mới đều có thể tự tin làm dự toán sau 1 tháng đào tạo từ các kỹ sư có chuyên môn cao trong công ty.Môi trường làm việc: Là môi trường để tất cả các bạn trẻ chưa có kinh nghiệm có thể phát huy khả năng, trau dồi kiến thức để trở thành những người dẫn đầu về lĩnh vực dự toán trong tương lai. Tất cả nhân viên trong công ty đều bắt đầu từ không có kinh nghiệm đối với lĩnh vực dự toán hoàn toàn mới ở Việt Nam và hiện tại đã trở thành kỹ sư dự toán, người hướng dẫn và đào tạo cho nhân viên mới tại công ty. Đây chính là cơ hội tiềm năng nếu các bạn muốn định hướng tương lai ổn định, trở thành cán sự trụ cột của công ty.', N' Trình độ: Đại học, cao đẳng, trung cấp (Không yêu cầu chuyên môn, tất cả các ngành học về kỹ thuật, kinh tế, văn hóa,… đều có thể ứng tuyển nếu các bạn có mong muốn tìm hiểu và học hỏi về lĩnh vực này.) Đặc thù công việc đòi hỏi sự tập trung, tỉ mỉ, chăm chỉ, sẵn sàng tiếp thu và học hỏi nên công ty luôn chào đón những bạn có đức tính này, hoan nghênh những bạn nữ mong muốn có công việc ổn định, thử sức với cơ hội nghề nghiệp mới. Có tinh thần trách nhiệm, trung thực và nhiệt tình trong công việc. Có khả năng làm việc theo nhóm và có thể chịu được áp lực cao trong công việc. Yêu thích tiếng Nhật, văn hóa, con người và phong cách làm việc của người Nhật.', NULL, 7000000.0000, 10000000.0000, 0, CAST(N'2020-07-10' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5020, 11, 2, 0, N'ntd7@gmail.com', N'0595183055', N'P.Nhân Sự', 0, NULL, N'Đã duyệt', 1, 477, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2768, N'Nhân Viên Chăm Sóc Khách Hàng (Hà Nội)', N'Nhân viên', N'Nhân viên', 20, N'- Chăm sóc khách hàng trước và sau khi sử dụng dịch vụ.- Đầu mối nhận và giải quyết mọi thông tin về khiếu nại của khách hàng.- Triển khai các chương trình telemarketing trực tiếp đến khách hàng, tư vấn, chăm sóc khách hàng thân thiết.', N'- Tự tin giao tiếp.- Nhanh nhẹn, trung thực.Nhiệt tình, có tinh thần trách nhiệm cao.', NULL, 7000000.0000, 10000000.0000, 0, CAST(N'2020-07-25' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5020, 11, 2, 0, N'ntd7@gmail.com', N'0567579352', N'Hoàng Yến', 0, NULL, N'Đã duyệt', 1, 438, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2769, N'Kỹ Thuật Viên Lập Trình CNC (Vĩnh Phương, Nha Trang)', N'Nhân viên', N'Nhân viên', 5, N'- Lập chương trình gia công cho các máy CNC chế biến gỗ dựa theo tài liệu bản vẽ đã được thẩm duyệt.- Làm việc theo sự phân công của trưởng phòng kỹ thuật- Các công việc cụ thể sẽ được trao đổi trong buổi phỏng vấn', N'- Trình độ cao đẳng trở lên - chuyên ngành cơ khí chế tạo máy- Biết sử dụng Pro - Creo, Mastercam, Autocad...- Năng động, sức khỏe tốt, chịu khó, nhiệt tình trong công việc- Trung thực, nhanh nhẹn, có trách nhiệm - Chịu được áp lực cao trong công việc', NULL, 10000000.0000, 15000000.0000, 0, CAST(N'2020-08-01' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5020, 11, 2, 0, N'ntd7@gmail.com', N'0230117967', N'Mr Giáp', 0, NULL, N'Đã duyệt', 1, 594, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2770, N'Chuyên Viên Quản Lý Chứng Từ Xe Máy Thiết Bị', N'Nhân viên', N'Quản lý', 1, N'- Làm hợp đồng và theo dõi tiến độ giao hàng với nhà cung cấp, đảm bảo thiết bị ung cấp kịp thời, chất lượng;- Theo dõi báo hiểm, quản lý kiểm hóa hồ sơ bán hàng, báo cáo doanh thu hàng tháng;- Quản lý lưu trữ hồ sơ MMTB như cavet, đăng kiểm, hồ sơ trước bạ, các hồ sơ liên quan khác;- Cung cấp đầy đủ chứng từ xuất xứ và chứng nhận chất lượng hàng hóa, báo cáo thử nghiệm mẫu cho các bộ phận khi cần thiết;- Đề xuất phương án kinh doanh MMTB, phương án tài chính thuê mua, thuê tài, vay đầu tư tài sản;- Chịu trách nhiệm và giải quyết sự cố;- Theo dõi công nợ, thương thuyết thời hạn thanh toán;- Báo cáo trực tiếp Giám đốc hàng tháng.', N'- Có 02 năm kinh nghiệm trở lên tại các công tác liên quan;- Am hiểu mức độ khá về máy móc thiết bị: Các loại ô tô con, máy móc công trình như Xe ben, Xe xúc đào, ủi, lu;- Hiểu biết về tính năng Cơ giới, Thiết bị, Máy móc xây dựng.', NULL, 15000000.0000, 20000000.0000, 0, CAST(N'2020-07-31' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5020, 11, 2, 2, N'ntd7@gmail.com', N'0187108247', N'Công Ty CP Đầu Tư Xây Dựng Trung Nam', 0, NULL, N'Đã duyệt', 1, 268, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2771, N'Mechanical Engineer (Kỹ Sư Cơ Khí)', N'Nhân viên', N'Quản lý', 8, N'- Work description: Implementing all work scopes of mechanical field and other relevance work scopes according with manager’s requirements (discussing more details when interview).', N'- Requirement: English communication; graduated or more; technical major is preferable (mechanical.ETC.)- Male, > 21 years old- Honest, hard- work, can stand high pressure- Respect professional ethics and security principles', NULL, 10000000.0000, 12000000.0000, 0, CAST(N'2020-07-03' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5023, 12, 2, 0, N'ntd8@gmail.com', N'0142599806', N'Ms Việt Kiều', 0, NULL, N'Đã duyệt', 1, 570, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2772, N'Nhân Viên Tiếp Thị Trực Tiếp – Direct Sales Agent', N'Nhân viên', N'Nhân viên', 5, N'Vai trò direct marketing-Chịu trách nhiệm về chỉ tiêu số lượng khách hàng đến từ kênh tiếp thị trực tiếp.-Tìm kiếm khách hàng mới từ kênh tiếp thị trực tiếp ngoài thị trường.-Trình bày/ giới thiệu tư vấn các chương trình Anh ngữ đến khách hàng tiềm năng.-Nhập dữ liệu khách hàng vào hệ thống và theo sát những khách hàng tiềm năng.-Xây dựng hình ảnh và thương hiệu của VUS ở những nơi tiếp xúc với khách hàng thông qua phong cách, trang phục, sales booth.-Liên hệ công ty, trường học, tổ chức, câu lạc bộ để tìm kiếm khách hàng tiềm năng.-Tổ chức, tham gia vào những sự kiện có liên quan đến tư vấn & bán các chương trình đào tạo Anh ngữ.-Tham gia các buổi huấn luyện về sản phẩm, kỹ năng tư vấn, hệ thống, phần mềm.-Đề xuất các vị trí tiếp thị trực tiếp, hoạt động và các chương trình khuyến mãi phù hợp với nhu cầu của khách hàng.-Phối hợp với bộ phận Marketing trung tâm để yêu cầu và nhận tài liệu cho hoạt động tiếp thị trực tiếp.-Thường xuyên cập nhật các hoạt động của công ty để có thông tin tư vấn khách hàng.-Thực hiện các báo cáo tiếp thị trực tiếp cho cấp trên.', N'•Tuổi: 18-30•Giới tính: Nam/Nữ•Bằng cấp: Tốt nghiệp THPT, Trung cấp trở lên.•Ngoại hình: Dễ nhìn, năng động, tự tin, chịu áp lực cao.•Yêu cầu khác:-Có kinh nghiệm trong lĩnh vực bán hàng, marketing ít nhất 1 năm.-Yêu thích công việc Tiếp thị qua điện thoại/ Bán hang trực tiếp.-Giọng nói chuẩn, rõ ràng.-Kỹ năng giao tiếp, đàm phán, thuyết phục tốt-Khả năng làm việc độc lập và làm việc nhóm.-Kiên trì, năng động, sáng tạo, tinh thần trách nhiệm cao và có khả năng làm việc dưới áp lực cao.-Kiến thức về lĩnh vực giáo dục là một lợi thế-Ưu tiên ứng viên mới ra trường có đam mê bán hàng thích kiếm tiền.- Ứng viên đã có kinh nghiệm làm Nhân viên tiếp thị qua điện thoại, bán hàng, đã làm việc tại Call Center là một lợi thế.', NULL, 10000000.0000, 12000000.0000, 0, CAST(N'2020-07-11' AS Date), CAST(N'2026-10-06' AS Date), NULL, 1002, 1, 2, 0, N'user111@gmail.com', N'0271164883', N'Phòng Nhân Sự', 0, NULL, N'Đã duyệt', 1, 511, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2773, N'Nhân Viên Bán Hàng Kênh Nhà Hàng, Khách Sạn Lương Từ 10 Triệu Trở Lên', N'Nhân viên', N'Nhân viên', 8, N'- Phụ trách tìm kiếm và phát triển khách hàng trong khu vực đc chỉ định- Thu thập thông tin khách hàng- Chịu trách nhiệm về tính chính xác thông tin khách hàng đã cung cấp- Làm báo cáo hàng ngày, báo cáo cho GSMV', N'- Ưu tiên có kinh nghiệm bán hàng kênh Nhà Hàng, Khách sạn (Horeca)- Siêng năng, chịu khó, giao tiếp tốt, cầu tiến, trung thực- Ngoại hình dễ nhìn, thân thiện (ưu tiên phụ nữ có gia đình)- Chịu được áp lực trong công việc, tinh thần cầu tiến', NULL, 10000000.0000, 12000000.0000, 0, CAST(N'2020-07-05' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5023, 12, 2, 2, N'ntd8@gmail.com', N'0731411261', N'Anh Trường', 0, NULL, N'Đã duyệt', 1, 85, 0)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2774, N'Du Học Sinh Làm Việc Tại Nhật (Toàn Quốc)', N'Nhân viên', N'Nhân viên', 50, N'- Học tiếng nhật 6 tháng tại Việt Nam- Học thêm 1 năm 6 tháng tại trường tiếng tại Nhật- Sẽ được học lên trường Senmon nếu thành tích tại trường tiếng tốt ( free học phí tại trường Senmon) - Sau khi tốt nghiệp trường tiếng sẽ được làm việc cho các cơ sở dưỡng lão của tập đoàn ( với mức lương trên 50 tr/ tháng , 8 tiếng / ngày, chưa tính thưởng )- Hỗ trợ người lớn tuổi ở viện trong cuộc sống hàng ngày , vui chơi, trò chuyện cùng những người cao niên.', N'- Đã tốt nghiệp cấp 3, từ 18~24 tuổi- Ưu tiên cho những bạn sinh viên ngành y, dược, điều dưỡng,...- Mong muốn du học Nhật, có thái độ nghiêm túc với công việc điều dưỡng.', NULL, 0.0000, 0.0000, 1, CAST(N'2020-07-08' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5023, 12, 2, 0, N'ntd8@gmail.com', N'0356987110', N'Chị Thảo', 0, NULL, N'Đã duyệt', 1, 202, 0)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2775, N'Nhân Viên Kinh Doanh BĐS -Tân Bình - Lương 10 Triệu Chính Thức', N'Nhân viên', N'Nhân viên', 20, N'- Gọi điện thoại tư vấn và chăm sóc Khách hàng theo data có sẵn do công ty cung cấp.- Tìm kiếm, mở rộng mối quan hệ khách hàng;- Xây dựng hệ thống khách hàng tiềm năng, khách hàng mục tiêu, lên kế hoạch thường xuyên chăm sóc định kỳ khách hàng- Triển khai kế hoạch bán hàng và các chương trình bán hàng thuộc dự án do Công ty đề ra.- Công việc cụ thể sẽ được hướng dẫn chi tiết khi vào làm việc- Công cụ Marketing (tìm kiếm khách hàng) chính như: Marketing online (đăng tin, quảng cáo Facebook, Google); sale phone, tìm kiếm khách hàng trực tiếp.', N'Nam, nữ tuổi từ 18- 30- Không yêu cầu kinh nghiệm, sẽ được đào tạo khi vào làm việc tại công ty.- Nhanh nhẹn, nhiệt tình.- Kỹ năng giao tiếp, thương lượng, đàm phán tốt.- Kiên trì, trung thực, thẳng thắn, chịu được áp lực công việc.-Có trách nhiệm, ham học hỏi, nhiệt huyết trong công việc.', NULL, 7000000.0000, 10000000.0000, 0, CAST(N'2020-07-03' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5023, 12, 2, 0, N'ntd8@gmail.com', N'0659102091', N'Mr Hiếu', 0, NULL, N'Đã duyệt', 1, 872, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2776, N'Nhân Viên Kinh Doanh (Lương 15 -20 Triệu)', N'Nhân viên', N'Nhân viên', 2, N'- Duy trì và phát triển quan hệ với khách hàng một cách hiệu quả và quảng bá sản phẩm vật tư, hóa chất vs thiết bị nha khoa- Phụ trách giải quyết những yêu cầu của khách hàng liên quan đến thông tin sản phẩm và những yêu cầu khác.- Duy trì các buổi gặp gỡ khách hàng ở khu vực phụ trách định kỳ hàng tuần.- Thực hiện các nhiệm vụ khác theo sự chỉ đạo của Giám Sát Kinh Doanh.', N'- Siêng năng, chăm chỉ- Sử dụng thành thạo ngoại ngữ (được ưu tiên)-Tốt nghiệp trung cấp trở lên', NULL, 15000000.0000, 20000000.0000, 0, CAST(N'2020-07-15' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5023, 12, 2, 0, N'ntd8@gmail.com', N'0474219272', N'Ms Ly', 0, NULL, N'Đã duyệt', 1, 212, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2777, N'Trưởng Phòng Kinh Doanh Bất Động Sản', N'Trưởng phòng', N'Quản lý', 2, N'-Nghiên cứu thị trường bất động sản, tìm kiếm và xây dựng nguồn khách hàng mới tiềm năng có nhu cầu về đất nền, nhà phố, …-Quản lý, điều hành nhóm kinh doanh, lên kế hoạch và định hướng cho nhân viên đạt mục tiêu doanh số bán hàng.-Phối hợp, triển khai các chương trình marketing của Công ty đề ra.-Tuyển dụng, Đào tạo, huấn luyện nhân viên kinh doanh.-Hỗ trợ nhân viên kinh doanh trong việc đàm phán, giao dịch với khách hàng-Các công việc khác theo yêu cầu của cấp trên.', N'- Nam từ 30 – 40 tuổi.- Có kinh nghiệm ở vị trí quản lý tối thiểu 6 năm trong ngành bất động sản. Ưu tiên ứng viên có kinh nghiệm tối thiểu 3 năm về mảng bất động sản đất nền, nhà phố.- Có kiến thức sâu chuyên ngành BĐS; Am hiểu về ngành, doanh nghiệp hoạt động trong lĩnh vực xây dựng và bất động sản, về các sản phẩm, dịch vụ bất động sản.- Muốn phát triển bản thân, đột phá thu nhập.- Giao tiếp tốt. Có năng lực quản lý, truyền đạt, đàm phán và chốt sale,…', NULL, 0.0000, 0.0000, 1, CAST(N'2020-06-21' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5023, 12, 2, 0, N'ntd8@gmail.com', N'0833715846', N'Chị Mai', 0, NULL, N'Đã duyệt', 1, 332, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2778, N'Nhân Viên QA - HCM', N'Nhân viên', N'Nhân viên', 3, N'- soạn qui trình theo hệ thống quản lý chất lượng HACCP, BRC, ISO…  - Làm báo cáo kiểm hàng, thẩm tra nhà máy, vùng nuôi thủy sản… -  Gởi báo cáo cho khách hàng quốc tế. Lưu trữ hệ thống hồ sơ, tài liệu cty,...- Có thể định kỳ đi công tác làm việc với nhà máy, vùng nuôi thủy sản…', N'- Tốt  nghiệp Đại học  chuyên ngành Hóa Thực phẩm,Thủy hải sản, … - Có kiến thức về các tiêu chuẩn chất lương thực phẩm (HACCP, BRC, ISO 22000…) Tiếng Anh :  yêu cầu tốt 4 kỹ năng : nói nghe đọc viết. Máy tính: thành thạo Office, E mail, và các phần mềm VP khác.- Kinh nghiệm: Không cần kinh nghiệm nhiều, sẽ được huấn luyện đào tạo nghiệp vụ chuyên môn. Đặc biệt yêu cầu cao về các kỹ năng tiếng Anh, giao tiếp tự tin, đọc viết tốt.- Phẩm chất: Thông minh, nắm bắt tiếp thu nhanh, cẩn thận, siêng năng, làm việc có trách nhiệm cao.', NULL, 8000000.0000, 10000000.0000, 0, CAST(N'2020-07-17' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5023, 12, 2, 0, N'ntd8@gmail.com', N'0595563899', N'Phòng Nhân Sự', 0, NULL, N'Đã duyệt', 1, 78, 0)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2779, N'Chuyên Viên Tư Vấn Bảo Hiểm', N'Mới tốt nghiệp / Thực tập sinh', N'1002', 10, N'- Chuyên viên tư vấn bảo hiểm- Chăm sóc khách hàng', N'• Nam/ Nữ tuổi từ 22-55, hiện đang không làm tại bất kỳ công ty bảo hiểm nhân thọ nào trên toàn quốc.• Trình độ: Tốt nghiệp THPT trở lên• Sử dụng thành thạo Smartphone, máy tính bảng, PC/Laptop.• Có tinh thần cầu tiến, ham học hỏi, trung thực.• Dành được thời gian tối thiểu 2 tiếng/ ngày để tham gia các khóa đào tạo cơ bản – chuyên sâu về bảo hiểm trong vòng 2 tháng đầu tiên.', NULL, 7000000.0000, 15000000.0000, 0, CAST(N'2020-06-23' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5023, 12, 3, 0, N'ntd8@gmail.com', N'0659112100', N'Mrs. Thảo', 0, NULL, N'Đã duyệt', 1, 822, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2780, N'Nhân Viên Kinh Doanh (Hà Nội Và Tp. Hồ Chí Minh)', N'Nhân viên', N'Nhân viên', 3, N'- Triển khai kinh doanh các sản phẩm trong lĩnh vực thẩm mỹ và mỹ phẩm cho các bệnh viện thẩm mỹ và trung tâm phẫu thuật thẩm mỹ- Liên hệ, hỗ trợ để các bộ phận có thể cung cấp sản phẩm tại nhiều thẩm mỹ viện trên địa bàn Hà Nội và thành phố HCM- Năng động mở rộng thị trường, duy trì và phát triển quan hệ với khách hàng- Tham gia tổ chức các hội thảo khoa học trong các bệnh viện với sự tham gia của các bác sỹ và Giáo sư nước ngoài.Văn phòng làm việc:- Hà Nội: The Pearls Office, số 35-N03 Khu đô thị Dịch Vọng mới, phường Dịch Vọng, quận Cầu Giấy, Hà Nội.- Hồ Chí Minh: 31C Lý Tự Trọng, Bến Nghé, Quận 1, Hồ Chí Minh', N'- Trình độ: Trung cấp, Cao đẳng, Đại học- Nhanh nhẹn, nhiệt tình trong công việc.- Kỹ năng giao tiếp, đàm phán tốt.- Trung thực, chăm chỉ, chịu được áp lực công việc- Năng động nhiệt tình, có trách nhiệm trong công việc.- Xây dựng nhóm làm việc hiệu quả và hòa đồng với đồng nghiệp.- Ưu tiên các bạn có 1-2 năm kinh nghiệm làm việc liên quan đến kinh doanh', NULL, 10000000.0000, 20000000.0000, 0, CAST(N'2020-07-06' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5023, 12, 2, 0, N'ntd8@gmail.com', N'0759876970', N'Chị Hà', 0, NULL, N'Đã duyệt', 1, 877, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2781, N'Nhân Viên Content - Thu Nhập Trên 10 Triệu / Tháng', N'Nhân viên', N'Nhân viên', 1, N'• Viết bài PR thị trường, PR dự án, thương hiệu, nội dung các chiến dịch quảng cáo: kịch bản phim, kịch bản phóng sự, bài viết facebook, bài viết trên website công ty bằng tiếng Anh.• Lên ý tưởng và lập kế hoạch, thực hiện các chiến dịch marketing cho các dự án của bộ phận kinh doanh nước ngoài triển khai trong năm.• Đưa ra được những tiêu đề hấp dẫn và phù hợp.• Biên tập nội dung theo yêu cầu.• Update các tin tức, status trên fanpage, website hàng ngày.', N'• Nam/nữ, tốt nghiệp báo chí/marketing và ngành liên quan.• Có khả năng viết tốt.• Có trách nhiệm trong công việc, chịu được áp lực công việc.• Có tư duy tốt và hiểu biết về thương hiệu, marketing và có đam mê với lĩnh vực này.• Có khả năng độc lập lên kế hoạch và triển khai công việc theo định hướng đề ra.• Thành thạo tin học văn phòng (Word, Exel, Poiwerpoint...)', NULL, 8000000.0000, 12000000.0000, 0, CAST(N'2020-07-17' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5027, 24, 2, 0, N'ntd10@gmail.com', N'0762951606', N'Ms.Linh', 0, NULL, N'Đã xóa', 1, 370, 0)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2782, N'[Bình Dương] -Nhân Viên Kho', N'Nhân viên', N'Nhân viên', 4, N'- Hỗ trợ công việc trong kho thành phẩm- Theo dõi đơn hàng xuất nhập kho- Cụ thể trao đổi khi phỏng vấn.', N'-Nam 20-35 tuổi , trung học trở lên, sử dụng thành thạo vi tính văn phòng,- ưu tiên biết lái xe nâng và có kinh nghiệm công việc liên quan .', NULL, 8000000.0000, 10000000.0000, 0, CAST(N'2020-07-13' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5027, 24, 2, 1, N'ntd10@gmail.com', N'0848330796', N'Recruitment Team', 0, NULL, N'Đã duyệt', 1, 871, 0)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2783, N'Chuyên Viên Kinh Doanh Bất Động Sản Không Yêu Cầu Kinh Nghiệm', N'Nhân viên', N'Nhân viên', 30, N'- Tìm kiếm khách hàng và dẫn Khách hàng đi xem nhà thổ cư - Thời gian làm việc linh hoạt không cần đến công ty nhiều, có chấp nhận sinh viên mới ra trường, chờ bằng tốt nghiệp - BĐS Thiên Khôi số 1 về thổ cư có 5 Trụ sở tại HN cho mọi người tiện làm việc+ Trụ sở chính : 18 Tam Trinh+ Trụ sở Đống Đa ; 174 Láng,+ Trụ sở Hà Đông : 50 Nguyễn Văn Lộc,+ Trụ sở Cầu Giấy : 37 Nguyễn Văn Huyên+ Trụ sở Long Biên : 139 Hồng Tiến', N'- Đam mê kinh doanh, ham học hỏi.-  Có phương tiện đi lại, laptop, smartphone.- Không cần kinh nghiệm, vì sẽ được các chuyên gia dày dặn kinh nghiệm đào tạo và hỗ trợ', NULL, 0.0000, 0.0000, 1, CAST(N'2020-07-10' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5027, 24, 2, 0, N'ntd10@gmail.com', N'0866179564', N'Ms Hải Đường', 0, NULL, N'Chưa duyệt', 1, 580, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2784, N'hot Nhân Viên Kinh Doanh Thu Nhập 15 - 25 Triệu (Bắc Ninh)', N'Nhân viên', N'Nhân viên', 6, N'- Thu thập phân tích thông tin về thị trường, tìm kiếm thông tin khách hàng.- Tiếp cận khách hàng, xử lý thông tin, giao dịch, xây dựng mối quan hệ, xúc tiến hợp đồng- Lên kế hoạch thực hiện, xúc tiến làm việc với các đối tác, nhà cung cấp- Cung cấp thông tin khách hàng, đối tác, kiểm tra thông tin kỹ thuật hàng hóa và điều kiện thương mại, điều kiện chào thầu phạm vi phụ trách, phục vụ xây dựng báo giá và chuẩn bị hồ sơ dự thầu.- Xây dựng mối quan hệ khách hàng, tư vấn, định hướng sản phẩm cho khách hàng, chốt hợp đồng- Hiểu rõ nhu cầu, nguồn lực của khách hàng. Đảm bảo khách hàng hiểu rõ sản phẩm, dịch vụ bán hàng từ đó thúc đẩy việc mua hàng, sử dụng sản phẩm, chia sẻ trải nghiệm, giới thiệu sản phẩm cho bên thứ 3 và phản hồi những yêu cầu cải tiến cho công ty', N'- Tốt nghiệp Đại học trở lên (chuyên ngành: Quản trị kinh doanh, Đối ngoại, Marketing)Hệ đào tạo: Chính Quy- Sử dụng máy tính thành thạo (Word, Excel & Power point, internet)-  Kỹ năng giao tiếp tốt - Kỹ năng chăm sóc Khách hàng- Tối thiểu có 2 năm kinh nghiệm trong lĩnh vực Kinh Doanh-  Có khả năng tổ chức, triển khai công việc một cách độc lập và quản lý thời gian tốt.-  Khả năng xử lý tình huống tốt, khéo léo, hiểu biết.- Nắm rõ về các sản phẩm, ngành nghề của công ty.- Trung thực, chịu khó, ham học hỏi- Trung thực, có tinh thần trách nhiệm và cầu tiến.-  Nhiệt tình, thần trách nhiệm cao trong công việc-  Nhanh nhẹn hoạt bát, giao tiếp khéo, năng động, trong công việc', NULL, 15000000.0000, 25000000.0000, 0, CAST(N'2020-07-02' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5027, 24, 2, 0, N'ntd10@gmail.com', N'0743596986', N'Ms Phương Anh', 0, NULL, N'Đã duyệt', 1, 62, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2785, N'Nhân Viên Khảo Sát Địa Chất Công Trình (Đà Nẵng)', N'Nhân viên', N'Nhân viên', 2, N'- Hiểu biết về công tác khảo sát ĐCCT.- Lập phương án khảo sát và Báo cáo khảo sát ĐCCT.- Triển khai công tác khảo sát Địa chất công trình tại hiện trường', N'- Ứng viên nam. Tốt nghiệp Đại học chính quy ngành ĐCCT.- Kinh nghiệm tối thiểu 5 năm.- Thành thạo tin học văn phòng Word, Excel, CAD...- Hiểu biết về công tác khảo sát. Biết lập báo cáo khảo sát ĐCCT.- Chăm chỉ, nhiệt tình, sẵn sàng đi công tác xa.Mức lương: thoả thuận. Được hưởng các chế độ chính sách: BHXH, thưởng, chính sách phúc lợi khác...,', NULL, 15000000.0000, 20000000.0000, 0, CAST(N'2020-06-22' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5027, 24, 2, 5, N'ntd10@gmail.com', N'0947447539', N'Lê Thị Thúy Vy', 0, NULL, N'Đã duyệt', 1, 108, 0)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2786, N'Content Marketing / Content Leader', N'Nhân viên', N'Nhân viên', 2, N'– Xây dựng nội dung trên các kênh truyền thông của doanh nghiệp. Chủ yếu nội dung 70% trên kênh Facebook- Phối hợp với team Media ( Design, Quay/Dựng Video ) để sản xuất các dữ liệu truyền thông - Biên tập, xây dựng các nội dung kịch bản video khách hàng, video giới thiệu sản phẩm, TVC quảng cáo,…– Viết content bán hàng , Content Ads trên Facebook – Báo cáo hiệu quả công việc định kỳ theo yêu cầu.- Thực hiện các công việc khác theo sự phân công của quản lý', N'- Tốt nghiệp Đại học, Cao đẳng chuyên ngành PR, báo chí, marketing hoặc tương đương- Kinh nghiệm ít nhất 1 năm trong lĩnh vực content marketing. (Ưu tiên ngành: Beauty, SPA, Thẩm Mỹ Viện, Nha khoa thẩm mỹ,... )- Khả năng viết tốt Content Ads, Content Bán hàng, Content Quảng Cáo.- Làm việc, Phối hợp tốt với team Media ( bao gồm Design, Quay, Dựng Video )  để sản xuất các dữ liệu truyền thông- Có đam mê và có kỹ năng viết lách- Sáng tạo và cập nhật xu hướng thị trường- Có tinh thần trách nhiệm và cam kết hoàn thành công việc- Có sự chủ động trong công việc', NULL, 8000000.0000, 12000000.0000, 0, CAST(N'2020-07-28' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5027, 24, 2, 1, N'ntd10@gmail.com', N'0824780034', N'Mr Cường', 0, NULL, N'Đã duyệt', 1, 723, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2787, N'Thiết Kế Nội Thất', N'Quản lý', N'Quản lý', 1, N'- Thiết kế ý tưởng nội thất các công trình dân dụng: biệt thự, nhà phố, văn phòng, khu đô thị, căn hộ, khách sạn, … và các sản phẩm nội thất và các hạng mục nội thất, ngoại thất công trình.- Dựng phối cảnh và render nội thất- Bố cục, sắp xếp khoa học các hình ảnh và bản vẽ của phương án để trình bày với khách hàng.- Đảm bảo chất lượng và thời gian giao hồ sơ theo yêu cầu của khách hàng.- Thực hiện các công việc khác do Trưởng phòng phân công.', N'- Sử dụng thành thạo phần mềm: Autocad, Sketchup, Vray sketchup, Photoshop, 3Dmax- Nhanh nhẹn, ham học hỏi, chịu khó trong công việc', NULL, 8000000.0000, 12000000.0000, 0, CAST(N'2020-08-07' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5027, 24, 2, 0, N'ntd10@gmail.com', N'0273401009', N'Ms Thủy', 0, NULL, N'Đã duyệt', 1, 682, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2788, N'Nhân Viên Thiết Kế 2D- Quận Phú Nhuận', N'Nhân viên', N'Nhân viên', 3, N'- Người có thể áp dụng khả năng thiết kế của mình trên nhiều thể loại sản phẩm nhưsocial content, POSM, KV…- Định hướng, tự sáng tạo ý tưởng từ các bài viết content- Xử lý hình ảnh theo yêu cầu của khách hàng và các bộ phận liên quan', N'- Tốt nghiệp các trường/khóa học thiết kế, các ngành đồ họa ứng dụng- Sử dụng thành thạo các công cụ thiết kế: AI, AE, Photoshop, dựng clip cơ bản,…- Có kinh nghiệm trong việc thiết kế hình ảnh social content.- Khả năng làm việc độc lập cũng như trong một tập thể/nhóm: tiếp nhận feedback,thảo luận vấn đề, đưa giải pháp và triển khai thực hiện.- Có thêm khả năng làm các clip keyframe, … là một lợi thế mạnh!', NULL, 9000000.0000, 15000000.0000, 0, CAST(N'2020-07-07' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5027, 24, 2, 1, N'ntd10@gmail.com', N'0363080842', N'Ms Thanh', 0, NULL, N'Đã duyệt', 1, 802, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2789, N'Leader Sàn Thương Mại Điện Tử', N'Trưởng nhóm', N'Quản lý', 2, N'- Xây dựng chương trình, kế hoạch thúc đẩy doanh số và hiệu quả bán hàng trên các kênh online (web, face, forum, sàn TMĐT,...)- Nghiên cứu thị trường, đối thủ cạnh tranh; khảo sát khách hàng- Tổ chức triển khai, giám sát thực hiện và báo cáo công việc của team; chịu trách nhiệm về hiệu quả hoạt động chung của team và của từng nhân viên cấp dưới', N'- Nam / nữ Tuổi 24 trở lên, tốt nghiệp đại học các ngành nghề, am hiểu về marketing thương mại điện tử, quản trị kinh doanh. Ứng viên có hiểu biết và kinh nghiệm trong lĩnh vực kinh doanh online hoặc đã từng sale sàn thương mại điện tử là một lợi thế.- Có ít nhất 01 năm kinh nghiệm ở vị trí tương đương; ưu tiên ứng viên đến từ các sàn thương mại điện tử (Lazada, Tiki, Shopee, adayroi…)- Khả năng phân tích, nhạy bén với cơ hội thị trường, với các xu hướng mới.- Kỹ năng phân tích, tổng hợp, làm báo cáo.- Nhiệt tình và trách nhiệm với công việc.- Mong muốn gắn bó lâu dài', NULL, 10000000.0000, 15000000.0000, 0, CAST(N'2020-07-12' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5027, 24, 2, 1, N'ntd10@gmail.com', N'0898191544', N'Mr Khánh', 0, NULL, N'Đã duyệt', 1, 793, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2790, N'hot Kỹ Sư - Kiến Trúc Sư Ban Quản Lý Dự Án [ Tại Hải Dương]', N'Nhân viên', N'Nhân viên', 5, N'- Thực hiện triển khai, phối hợp với đơn vị tư vấn trong công tác lập hồ sơ Quyhoạch, thiết kế cơ sở, thiết kế Bản vẽ thi công trình các Sở, ngành tham gia ý kiến, thẩm định hồ sơ.- Thực hiện công tác trình thẩm định hồ sơ Quy hoạch, Thiết kế cơ sở, Thiết kế BVTC.- Lập kế hoạch công việc hàng tuần, tháng, quý, năm dưới sự chỉ đạo của Ban Giám Đốc cty và BQLDA.- Chuẩn bị tài liệu phục vụ báo cáo đồ án quy hoạch, thành phần hồ sơ nộp phục vụ công tác thẩm định hồ sơ.- Kiểm tra hồ sơ thiết kế, bóc tách khối lượng thiết kế, kiểm tra dự toán thiết kế.- Lập các văn bản phê duyệt dự án, phê duyệt thiết kế - dự toán, phê duyệt kế hoạch lựa chọn nhà thầu.- Phối hợp với đơn vị Tư vấn lập các văn bản liên quan trong quá trình lựa chọn nhà thầu theo luật đấu thầu.- Kiểm soát nhà thầu triển khai thi công các gói thầu về tiến độ, chất lượng, kỹ mỹ thuật.- Phối hợp với đơn vị Tư vấn giám sát kiểm soát hồ sơ quản lý chất lượng các gói thầu, hồ sơ thanh quyết toán công trình.- Lập hồ sơ trình nghiệm thu, bàn giao, quyết toán dự án hoàn thành.- Thực hiện các công việc khác liên quan đến quá trình thực hiện dự án theo sự chỉ đạo của Lãnh đạo Công ty và Ban Quản lý dự án', N'- Tốt nghiệp đại học chính quy chuyên ngành kiến trúc, quy hoạch, hạ tầng kỹ thuật, xây dựng cầu đường- Có kinh nghiệm từ 3-5 năm trong nghành xây dựng, công việc tương đương- (Ưu tiên đã làm vị trí tương đương tại các tập đoàn khác)- Có tinh thần trách nhiệm cao trong công việc, chịu được áp lực công việc- Trung thực, nhanh nhẹn trong quá trình triển khai công việc, có tư duy giải quyết công việc nhạy bén- Nắm vững được các quy định liên quan của pháp luật trong lĩnh vực hoạt động đầu tư xây dựng- Sử dụng thành thạo Word, Excel, Cad, phần mềm thiết kế đồ họa 3Dmax; Sketup…', NULL, 15000000.0000, 20000000.0000, 0, CAST(N'2020-07-30' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5027, 24, 2, 3, N'ntd10@gmail.com', N'0226173605', N'Chị Mai', 0, NULL, N'Đã duyệt', 1, 529, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2791, N'Web Developer', N'Nhân viên', N'Nhân viên', 5, N'- Tham gia Xây dựng mới các dự án khách hàng từ Châu Âu , Mỹ, Singapore..- Chuyển từ psd file sang html, wordpress.- Bảo trì, sửa lỗi các dự án website, ứng dụng trên nền web.- Xây dựng API.- Phát triển theme/plugin của Wordpress và Magento', N'- Có kinh nghiệm về: CSS3, Jquery, HTML5 và PHP.- Đã làm việc với wordpress và đã xây dựng được ít nhất 1 website.LỢI THẾ KHI ĐẠT THÊM NHỮNG YÊU CẦU SAU:- Ham học hỏi.- Thành thạo tiếng Anh.- Từng làm việc với Magento.- Lập trình ứng dụng trên điện thoại.', NULL, 7000000.0000, 15000000.0000, 0, CAST(N'2020-07-02' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5028, 25, 2, 0, N'ntd11@gmail.com', N'0734358047', N'Mr Oánh', 0, NULL, N'Đã duyệt', 1, 134, 0)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2792, N'Nhân Viên Thiết Kế 3D Tại Bình Thạnh', N'Nhân viên', N'Nhân viên', 1, N'- Thiết kế nội thất, shop, showroom, quầy bar, tủ trưng bày, kệ trưng bày SP ở siêu thị, gian hàng hội chợ triển lãm, không gian chức năng khác- Tư vấn thiết kế và đưa ra ý tưởng sáng tạo, tư vấn cho khách hàng.- Triển khai các kế hoạch Thiết kế.', N'- Tốt nghiệp chuyên ngành với 1 năm kinh nghiệm trở lên.- Sử dụng tốt các phần mềm đồ họa 3D max và Photoshop. Ưu tiên các ứng viên biết Corel, Sketchup,...- Sáng tạo, có kiến thức tốt về mỹ thuật, khả năng trình bày ý tưởng- Nhiệt tình, năng động, ham học hỏi', NULL, 7000000.0000, 10000000.0000, 0, CAST(N'2020-07-13' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5028, 25, 2, 0, N'ntd11@gmail.com', N'0177492039', N'Ms. Nhi', 0, NULL, N'Chưa duyệt', 1, 490, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2793, N'Nhân Viên Tiktok', N'Nhân viên', N'Nhân viên', 1, N'•Sáng tạo nội dung, ý tưởng và cùng team Content sản xuất, quay, dựng Video trên Tiktok•Tìm kiếm, kết nối với KOLs để tạo ra các Influencer Marketing Campaigns trên TikTok•Nghiên cứu trends, bắt trends, tìm hiểu cách thức hoạt động, thuật toán theo thời gian của TikTok•Lập kế hoạch phát triển hệ thống kênh Tik Tok bao gồm phát triển nội dung và tối ưu lượt tương tác trên hệ thống kênh.•Tìm kiếm và đào tạo KOLs Tik Tok mới cho hệ thống kênh.•Chi tiết công việc sẽ trao đổi thêm trong buổi phỏng vấn.        Địa điểm làm việc : Đường Lê Thị Riêng , Quận 12 , TP HCM', N'•Đã từng có kinh nghiệm ở vị trí tương tự hoặc đã từng hoạt động trên TikTok•Sáng tạo, năng nổ, tự tin vào khả năng bắt trends và tạo trends•Tư duy thẩm mỹ và hình ảnh tốt•Đã từng có kinh nghiệm làm Video Production là một lợi thế', NULL, 8000000.0000, 15000000.0000, 0, CAST(N'2020-07-12' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5028, 25, 2, 0, N'ntd11@gmail.com', N'0522336914', N'Nhân Sự', 0, NULL, N'Đã duyệt', 1, 738, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2794, N'Nhân Viên Kinh Doanh Dự Án Tại Quận 7', N'Nhân viên', N'Nhân viên', 4, N'- Tìm kiếm thông tin dự án mới - Tiếp cận dự án, khách hàng (chủ đầu tư, tổng thầu, các thầu nhôm kính hoặc mặt dựng.., đại lý..) để đưa sản phẩm vào cung cấp- Mở rộng thị trường cung cấp- Phát triển thương hiệu và sản phẩm- Báo cáo thông tin dự án và trao đổi trực tiếp với sếp Tổng- Làm việc tại văn phòng kinh doanh: Số 16 đường số 4, KDC Him Lam, phường Tân Hưng, Q7, TPHCM - Thời gian làm việc: Thứ hai – thứ 6: 8h – 17h                                   Thứ bảy làm sáng từ 8h – 12h', N'- Có quan hệ với các nhà thầu ngành xây dựng - Am hiểu về xây dựng (đặc biệt về hạng mục nhôm kính, mặt dựng, cladding, nội ngoại thất…là một lợi thế) - Đọc hiểu và vẽ được CAD (nếu có thể) - Tiếng Anh giao tiếp (hoặc tiếng Hàn)', NULL, 7000000.0000, 25000000.0000, 0, CAST(N'2020-07-07' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5028, 25, 2, 1, N'ntd11@gmail.com', N'0954011329', N'Ms Mi', 0, NULL, N'Đã duyệt', 1, 459, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2832, N'Quản Lý Tiềm Năng', N'Quản lý', N'Quản lý', 2, N'- Chịu trách nhiệm lên kế hoạch tuyển dụng, xây dựng nhóm kinh doanh theo định hướng của công ty- Quản lý đội nhóm, hoàn thành chỉ tiêu và công việc cấp trên yêu cầu- Lập kế hoạch hoạt động kinh doanh cho nhóm và từng nhân viên theo năm, quý, tháng- Lên kế hoạch hỗ trợ cho đội nhóm- Đào tạo kỹ năng, kiến thức cho nhân viên bên dưới', N'- Tốt nghiệp đại học trở lên- Kỷ luật, trung thực, nhanh nhẹn, chủ động, trách nhiệm và tư duy tích cực- Có kinh nghiệm quản lý là một lợi thế- 27 tuổi trở lên- Kinh nghiệm làm việc từ 3 năm trở lên( ở tất cả ngành nghề, không yêu cầy có kinh nghiệm ở vị trí quản lý)- Nhiệt tình có trách nhiệm trong công việc', NULL, 15000000.0000, 20000000.0000, 0, CAST(N'2020-06-19' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5029, 26, 2, 0, N'ntd12@gmail.com', N'0968645444', N'Chị Trang', 0, NULL, N'Đã duyệt', 1, 214, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2833, N'[Thẩm Mỹ Viện Xavia ] - Nhân Viên Telesale (Mức Lương 10-15 Triệu / Tháng)', N'Nhân viên', N'Nhân viên', 10, N'HỆ THỐNG THẨM MỸ VIỆN XAVIA CẦN TUYỂN 10 NHÂN VIÊN TELESALES-Gọi điện, tư vấn cho khách hàng theo data có sẵn;-Tư vấn các dịch vụ của hệ thống thẩm mỹ viện;-Chăm sóc, hướng dẫn và giải đáp các thắc mắc của khách hàng;-Được cung cấp data, cung cấp máy tính và điện thoại;-Được đào tạo kỹ năng telesales.', N'-Giọng nói dễ nghe;-Khả năng giao tiếp, chăm sóc khách hàng tốt;-Khả năng chốt sales-Ưu tiên ứng viên có kinh nghiệm telesales, CSKH hoặc từng làm tại hệ thống thẩm mỹ viện;-Khả năng làm việc nhóm, nhiệt tình, chăm chỉ, chịu được áp lực cao.', NULL, 10000000.0000, 15000000.0000, 0, CAST(N'2020-07-17' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5029, 26, 2, 0, N'ntd12@gmail.com', N'0321286455', N'Ms Hiền', 0, NULL, N'Đã duyệt', 1, 229, 0)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2834, N'Nhân Viên Kinh Doanh Thiết Bị Cơ Khí', N'Nhân viên', N'Nhân viên', 3, N'- Giới thiệu,  tư vấn và cung cấp thông tin sản phẩm  thiết bị cơ khí, Dụng cụ cắt và đá mài.- Xây dựng, phát triển khách hàng tiềm năng dựa trên danh sách khách hàng có sẵn và tìm kiếm khách hàng mới- Phối hợp với bộ phận  kỹ thuật và báo giá để tư vấn, báo giá cho khách hàng.- Đảm bảo hoàn thành chỉ tiêu bán hàng.- Chăm sóc tốt khách hàng, luôn lắng nghe và đáp ứng nhu cầu của khách hàng về sản phẩm,  dịch vụ của Công ty.', N'- Tốt nghiệp ĐH, CĐ các ngành liên quan kinh tế, cơ khí - Kinh nghiệm ít nhất 1 năm làm kinh doanh, Marketing, sale suppost,… về hàng cơ khí- Ứng viên mới ra trường ngành cơ khí, cơ điện tử, tự động hoá vẫn sẽ được tạo cơ hội,công ty sẽ đào tạo thêm về sản phẩm.- Tính cách trung thực, hòa đồng, vui vẻ, thân thiện và thích nghi tốt, chịu được áp lực công việc cao, chăm chỉ & cầu tiến…- Có trách nhiệm & tính kỉ luật cao.- Chi tiết sẽ trao đổi thêm trong quá trình phỏng vấn.', NULL, 7000000.0000, 15000000.0000, 0, CAST(N'2020-07-11' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5029, 26, 2, 1, N'ntd12@gmail.com', N'0729924375', N'Ms Hiền', 0, NULL, N'Đã duyệt', 1, 136, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2835, N'Nhân Viên Tư Vấn Bất Động Sản (Dự Án Gem Sky World Của Chủ Đầu Tư Đất Xanh)', N'Nhân viên', N'Nhân viên', 5, N'+ Tư vấn các khách hàng có nhu cầu về bất động sản do công ty cung cấp.+ Triển khai kế hoạch bán hàng và các chương trình bán hàng thuộc dự án của Công ty.+ Tư vấn qua điện thoại hoặc trực tiếp tại công ty.+ Làm việc tại văn phòng, được đào tạo bài bản.+ Cung cấp, tư vấn đầy đủ thông tin sản phẩm. Đàm phán, thương lượng và chốt hợp đồng với khách hàng.+ Chi tiết sẽ được trao đổi khi phỏng vấn.+ Tất cả đều được hỗ trợ đào tạo bởi Quản lý, đồng nghiệp, bộ phận hỗ trợ kinh doanh.', N'+ Không cần kinh nghiệm trong lĩnh vực bất động sản.+ Năng động, chịu khó học hỏi, có tinh thần cầu tiến.+ Có laptop và xe di chuyển+ Kỹ năng giao tiếp, giọng nói rõ ràng dễ nghe.', NULL, 0.0000, 0.0000, 1, CAST(N'2020-07-03' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5029, 26, 2, 0, N'ntd12@gmail.com', N'0697025618', N'Mr Văn', 0, NULL, N'Đã duyệt', 1, 529, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2836, N'Nhân Viên Tư Vấn Ngành Thẩm Mỹ  Lương Cứng 10 Triệu', N'Nhân viên', N'Nhân viên', 10, N'- Gọi điện và tư vấn cho khách hàng về các dịch vụ điều trị sẹo rỗ từ số điện thoại của Admin.- Quản lý thông tin khách hàng và cập nhật trên hệ thống cơ sở dữ liệu- Xây dựng mối quan hệ với khách hàng thông qua sự tập trung vào nhu cầu mong muốn của khách hàng và chốt sales- Phối hợp với các NVKD và nhân viên các phòng ban khác để đạt mục tiêu kinh doanh- Báo cáo với cấp trên về tiến độ và kết quả kinh doanh- Đặt lịch khách hàng với Bác Sĩ', N'- Có kinh nghiệm tư vấn từ 02 năm trở lên- Ưu tiên có kinh nghiệm về tư vấn trực tiếp bên ngành thẩm mỹ- Ngoại hình ưa nhìn, chăm chỉ, nhiệt tình, giao tiếp tốt- Quản lý, sắp xếp công việc khoa học', NULL, 10000000.0000, 30000000.0000, 0, CAST(N'2020-06-22' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5029, 26, 2, 2, N'ntd12@gmail.com', N'0211425882', N'Ms Hiếu', 0, NULL, N'Đã duyệt', 1, 121, 0)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2837, N'[Thanh Trì] Kế Toán Tổng Hợp', N'Nhân viên', N'Nhân viên', 1, N'- Tập hợp và hạch toán quản lý tất cả các chứng từ bán hàng, công nợ,thu chi, xuất nhập kho,…- Làm báo cáo thuế, báo cáo tài chính năm và các công việc liên quan đến thuế- Làm lương và bảo hiểm chế độ cho các nhân viên trong công ty  - Kiểm đếm hàng nhập kho, xuất kho; Kiểm kê kho định kỳ đối chiếu với sổ kho, thẻ kho-Các công việc khác do phụ trách phân công', N'- Tốt nghiệp Đại học chuyên ngành kế toán. -Trung thực, nhanh nhẹn, có sức khỏe.- Ưu tiên biết sử dụng phần mềm kế toán Misa và excel, worl - Kỹ năng: kỹ năng giao tiếp tốt, sử dụng máy tính tốt thành thạo, phần mềm Misa, office, các phần mềm ứng dụng cơ bản khác.- Khả năng làm việc độc lập và phối hợp nhóm;', NULL, 8000000.0000, 10000000.0000, 0, CAST(N'2020-06-25' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5029, 26, 2, 1, N'ntd12@gmail.com', N'0149805980', N'P.Hcns', 0, NULL, N'Đã duyệt', 1, 743, 0)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2838, N'Nhân Viên Bán Hàng Online - Thu Nhập Lên Đến 20 Triệu', N'Nhân viên', N'Nhân viên', 4, N'-Đăng bài giới thiệu sản phẩm, dịch vụ lên các nền tảng bán hàng trực tuyến như Shopee, Tiki, Sendo,... hoặc các trang mạng xã hội như Facebook, Zalo.-Trả lời phản hồi và thắc mắc của khách hàng về những sản phẩm, dịch vụ trên.-Trực tiếp trả lời, tư vấn, báo giá cho khách hàng về giá cả, tiêu chuẩn kỹ thuật, chất lượng sản phẩm do khách hàng công ty đã làm SEO Google hoặc khách hàng tự tìm kiếm trên Website và gọi tới đường dây nóng của công ty…-Tư vấn cho khách hàng sản phẩm, dịch vụ nào là phù hợp nhất với nhu cầu sử dụng của họ và thuyết phục họ mua hàng.-Xác nhận và chốt đơn hàng.-Tạo đơn hàng trên hệ thống của công ty, doanh nghiệp.-Tìm kiếm khách hàng tiềm năng mới.-Tìm hiểu các chiến lược bán hàng mới, hiệu quả hơn.-Tham gia vào các cuộc họp, hội thảo dành cho nhân viên bán hàng online do công ty tổ chức.-Báo cáo kết quả làm việc với cấp trên.', N'- Tốt nghiệp trung cấp, cao đẳng, Đại học trở lên chuyên nghành Quản trị kinh doanh hoặc các nghành có liên quan - Kinh nghiệm tối thiểu 06 tháng làm vị trí bán hàng Online hoặc Marketing online, ưu tiên ứng viên làm trong lĩnh vực xây lắp hoặc cung ứng vật tư xây lắp- Kỹ năng đàm phán, thuyết trình, tự tin.- Chịu được áp lực công việc- Không yêu cầu về giới tính.- Am hiểu về marketing, truyền thông, thương mại điện tử,…', NULL, 7000000.0000, 20000000.0000, 0, CAST(N'2020-07-01' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5029, 26, 2, 0, N'ntd12@gmail.com', N'0675476297', N'Mr Diệp', 0, NULL, N'Đã duyệt', 1, 115, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2839, N'Kế Toán Trưởng (Nhà Hàng Nét Huế)', N'Trưởng nhóm', N'Quản lý', 1, N'- Thực hiện việc lập các mẫu tài liệu, giấy tờ theo đúng quy định của pháp luật;- Lên các kế hoạch công việc chi tiết và tổ chức thực hiện các nhiệm vụ của bộ phận kế toán;- Thường xuyên tổ chức hoạt động kiểm kê các nguồn tài sản, dòng tiền liên quan đến hoạt động kinh doanh hoặc quyền lợi của Hệ thống;- Điều phối công việc cho các kế toán viên phù hợp;- Giám sát, kiểm tra, đánh giá hiệu quả công việc của nhân viên;- Đào tạo, hướng dẫn, nâng cao nghiệp vụ chuyên môn cho kế toán định kỳ;- Quản lý hệ thống kế toán, sổ sách kế toán, hóa đơn - chứng từ Hệ thống;- Quản lý, giám sát chung các hoạt động sản xuất kinh doanhCông việc trao đổi cụ thể trong phỏng vấn', N'- Giới tính: Nữ, độ tuổi: dưới 45 tuổi- Tốt nghiệp đại học- Đã có nhiều năm kinh nghiệm làm kế toán và ít nhất 3 năm kinh nghiệm làm kế toán trưởng- Có kinh nghiệm trong việc xây dựng quy trình kế toán, quy trình kiểm soát- Tư duy tốt, nhanh nhẹn, trung thực- Yêu cầu có kinh nghiệm 1 năm trở lên ở lĩnh vực nhà hàng', NULL, 15000000.0000, 20000000.0000, 0, CAST(N'2020-07-12' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5029, 26, 2, 0, N'ntd12@gmail.com', N'0478969691', N'Ms Huyền', 0, NULL, N'Đã duyệt', 1, 405, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2840, N'Kỹ Thuật Viên Triển Khai Công Trình Mạng Tại Nghệ An, Bắc Ninh', N'Nhân viên', N'Nhân viên', 3, N'- Thi Công Cài đặt tất cả phần cứng mới, hệ thống, và phần mềm cho các mạng.- Cài đặt, cấu hình, duy trì mạng lưới dịch vụ, thiết bị và các thiết bị.- Quản lý tất cả các hệ thống sao lưu và khôi phục giao thức.- Lập kế hoạch và hỗ trợ mạng và cơ sở hạ tầng thông tin.- Thực hiện phân tích xử lý sự cố máy chủ, máy trạm và hệ thống liên quan.- Lắp rắp, cài đặt bảo dưỡng mạng lan, camera- Thi công, bảo trì Camera- Giám sát phần mềm và bảo mật mạng. Công việc sẽ được trao đổi cụ thể hơn khi phỏng vấn- Vị trí làm việc :  Khu Công Nghiệp VSIP,  Hưng Nguyên,Tỉnh Nghệ An', N'- Có kinh nghiệm Thi công làm công trình lắp đặt hệ thống mạng camera- Có chứng chỉ: CCNA, MCSA hoặc cao hơn là 1 lợi thế- Am hiều về hệ thống công trình - không cần bằng cắp chỉ cần kinh nghiệm', NULL, 10000000.0000, 15000000.0000, 0, CAST(N'2020-07-09' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5029, 26, 2, 1, N'ntd12@gmail.com', N'0460780873', N'Ms.Linh', 0, NULL, N'Đã duyệt', 1, 99, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2841, N'Kiểm Toán Viên (Cpa)(Upto $1000)', N'Nhân viên', N'Nhân viên', 2, N'- Thực hiện các công việc liên quan đến các hợp đồng giao dịch liên kết và chuyển giá- Kiểm toán BCTC của khách hàng của công ty trong mùa kiểm toán và làm các cuộc review, soát xét giữa kỳ….- Phân công công việc cho các thành viên trong nhóm- Quản lý nhóm kiểm toán, lên kế hoạch và sắp sếp lịch kiểm toán với khách hàng- Review và ký phát hành BCTC…- Liên hệ, quản lý khách hàng - Thực hiện những nhiệm vụ khác do Ban Giám đốc trực tiếp giao.', N'- Ưu tiên ứng viên có kinh nghiệm về chuyển giá.- Có chứng chỉ hành nghề kiểm toán viên (CPA) ít nhất 2 năm trở lên.- Có kinh nghiệm kế toán, kiểm toán. Trong đó ít nhất 1 năm làm trưởng nhóm kiểm toán hoặc trưởng bộ phận.- Có kinh nghiệm trong lĩnh vực giao dịch liên kết và chuyển giá.- Tiếng Anh giao tiếp.', NULL, 17000000.0000, 25000000.0000, 0, CAST(N'2020-07-12' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5030, 27, 2, 0, N'ntd13@gmail.com', N'0179187423', N'Ms.Nguyệt', 0, NULL, N'Đã duyệt', 1, 871, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2842, N'Kế Toán Tổng Hợp', N'Nhân viên', N'Nhân viên', 2, N'- Quản lý quỹ tiền mặt của công ty, lập các báo cáo khi cần.- Quản lý hàng tồn kho để nhập xuất hàng hóa.- Làm báo giá, biên bản bàn giao, phiếu thu tiền, phiếu xuất kho, xuất hóa đơn, làm hợp đồng- Chốt và đối chiếu công nợ phải thu đối với khách hàng, phải trả với nhà cung cấp- Quản lý và lưu trữ hợp đồng đầu vào và đầu ra.- Giao dịch ngân hàng, cơ quan bảo hiểm, cơ quan thuế.- Các công việc khác theo sự phân công của công ty.', N'-Không yêu cầu kinh nghiệm - Tốt nghiệp trung cấp trở lên.- Trung thực- tận tụy - gắn bó.- Cẩn thận - nhiệt tình - chăm chỉ.', NULL, 6000000.0000, 10000000.0000, 0, CAST(N'2020-07-08' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5030, 27, 2, 0, N'ntd13@gmail.com', N'0801180271', N'Anh Đỗ Như Toản', 0, NULL, N'Đã xóa', 1, 245, 0)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2843, N'Nhân Viên Bán Hàng', N'Nhân viên', N'Nhân viên', 3, N'- Bán Hóa chất tẩy rửa REVCLEAN  dùng trong vệ sinh công nghiệp, vệ sinh khách sạn, vệ sinh nhà máy, vệ sinh nhà hàng ... - Nắm bắt các sản phẩm mà công ty đang phát triển. Tìm kiếm khách hàng, liên hệ và tìm hiểu nhu cầu khách hàng, tư vấn những vấn đề liên quan đến kỹ thuật cho khách hàng- Lập kế hoạch bán hàng, phân tích khách hàng tiềm năng, duy trì khách hàng hiện có- Chăm sóc khách hàng sau khi ký hợp đồng- Kết hợp với Nhân viên  Admin theo dõi thu hồi công nợ khách hàng do mình phụ trách.', N'- Yêu thích và đam mê làm kinh doanh bán hàng- Yêu cầu bằng cấp: Cao đẳng trở lên (ưu tiên các chuyên nhành: Quản trị kinh doanh, Marketing,...)- Yêu cầu độ tuổi: 22 - 35 tuổi- Có ít nhất 1 năm kinh nghiệm làm kinh doanh (tất cả các ngành nghề)- Kỹ năng làm việc theo nhóm- Kỹ năng thuyết trình, thuyết phục cao- Thành thạo các phần mềm vẽ kỹ thuật: Tin học văn phòng, thành thạo việc tìm kiếm trên internet,…- Có tư duy tốt, nắm bắt được ý tưởng khách hàng và triển khai tốt công việc- Nhanh nhẹn, có ý tưởng mới- Có tinh thần trách nhiệm cao và chịu áp lực trong công việc. - Sẵn sàng đi công tác tỉnh khi cần cho công việc', NULL, 7000000.0000, 10000000.0000, 0, CAST(N'2020-06-19' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5030, 27, 2, 0, N'ntd13@gmail.com', N'0757926653', N'Ms Tâm', 0, NULL, N'Đã duyệt', 1, 596, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2844, N'Nhân Viên Thiết Kế  - Graphic Designer Lương Trên 10 Triệu', N'Quản lý', N'Quản lý', 2, N'- Quản lý, đảm bảo các yêu cầu thiết kế của khách hang.- Thiết kế, xử lý file chế bản.- Chịu trách nhiệm thiết kế toàn bộ các ấn phẩm quảng cáo Online và Offline khi Công ty có yêu cầu.- Thực hiện mọi công việc: xóa, sửa, tách nền, cắt, ghép… các hình ảnh chất lượng (Tự sản xuất hay khai thác được) để sử dụng, phục vụ cho mục đích Kinh doanh và Marketing của Công ty.', N'+ Tốt nghiệp Cao đẳng, Đại học chuyên ngành thiết kế / Mỹ thuật công nghiệp…+ Sáng tạo và có khả năng nghiên cứu, nắm bắt trend nhanh cùng các xu hướng thiết kế mới.+ Am hiểu và sử dụng thành thạo công nghệ thiết kế bằng HTML5.- Kỹ năng:+ Sử dụng thành thạo các phần mềm thiết kế và chỉnh sửa hình ảnh chuyên nghiệp gồm Photoshop, AI, AE, Illustrator / Corel, Premiere+ Ưu tiên ứng viên có kinh nghiệm làm việc trong ngành in.+ Có đam mê và hiểu biết các kỹ năng cơ bản về nhiếp ảnh, quay, dựng videos… là lợi thế.+ Có kỹ năng lập kế hoạch và quản lý tiến độ công việc tốt.- Kinh nghiệm:+ Có ít nhất 2 năm kinh nghiệm làm trong lĩnh vực thiết kế quảng cáo, thiết kế thương hiệu, thiết kế HTML5…', NULL, 10000000.0000, 12000000.0000, 0, CAST(N'2020-06-22' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5030, 27, 2, 2, N'ntd13@gmail.com', N'0401818720', N'Phòng Hành Chính Nhân Sự', 0, NULL, N'Đã duyệt', 1, 860, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2845, N'Nhân Viên Marketing', N'Nhân viên', N'Nhân viên', 2, N'- Tổ chức và quản lý các chiến dịch quảng cáo và truyền thông trên kênh marketing-  Viết bài quảng cáo trên các kênh marketing- Hỗ trợ các hoạt động outbound/inbound marketing bằng cách thể hiện chuyên môn ở nhiều lĩnh vực khác nhau, bao gồm phát triển và tối ưu nội dung, quảng cáo, lên kế hoạch sự kiện, v.v...- Đảm nhận các nhiệm vụ độc lập trong kế hoạch marketing được phân công.', N'- có Kinh nghiệm về digital marketing hoặc chức vụ tương đương-Am hiểu sâu sắc các bộ phận marketing (bao gồm marketing truyền thống và digital marketing chẳng hạn như SEO và các phương tiện truyền thông xã hội) và các phương pháp nghiên cứu thị trường.- Sử dụng thành thạo phần mềm MS Office, phần mềm marketing (như Adobe Creative Suite & CRM) và công cụ SEO (Web analytics, Google Adwords...).Kiến thức về HTML, CSS và các công cụ phát triển web.- Kỹ năng giao tiếp và viết lách tốt- Tối ưu hoá công cụ tìm kiếm SEO- Làm thiết kế các logo hoặc thiết kế các hình ảnh nội dung được công ty yêu cầu', NULL, 8000000.0000, 15000000.0000, 0, CAST(N'2020-07-11' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5030, 27, 2, 0, N'ntd13@gmail.com', N'0404922748', N'Nhung', 0, NULL, N'Đã duyệt', 1, 546, 0)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2846, N'Nhân Viên Kế Toán Lương 10Tr / Tháng', N'Nhân viên', N'Nhân viên', 1, N'- Theo dõi công nợ ,quản lý tổng quát công nợ toàn công ty.- Hướng dẫn xử lý và hạch toán các nghiệp vụ kế toán- Thống kê và tổng hợp số liệu kế toán khi có yêu cầu- Lưu trữ dữ liệu kế toán theo quy định.- Thực hiện các công vệc kế toán- Cung cấp số liệu cho ban giám đốc khi có yêu cầu- Chi tiết công việc sẽ trao đổi cụ thể khi phỏng vấn', N'- Tốt nghiệp Cao đăng , Đại học chính quy chuyên ngành Kế toán- Kinh nghiệm: ít nhất 3 năm kinh nghiệm trở lên- Sử dụng máy vi tính thành thạo (Excel, Word, phần mềm kế toán Misa thành thạo).- Phẩm chất: trung thực, kiên trì, có sự nhìn nhận khách quan.- Nhanh nhẹn, giao tiếp tốt, hòa đồng với đồng nghiệp', NULL, 10000000.0000, 12000000.0000, 0, CAST(N'2020-07-15' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5030, 27, 2, 3, N'ntd13@gmail.com', N'0226896574', N'Anh Phan', 0, NULL, N'Đã duyệt', 1, 487, 0)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2847, N'Quản Lý Kỹ Thuật', N'Trưởng phòng', N'Quản lý', 1, N'- Quản lý kỹ thuật: Quản lý thi công và bảo trì hệ thống lạnh ( Kho lạnh , tủ showcase ) , điều hòa không khí, M&E.- Khảo sát thi công .- Kết hợp với bộ phận thiết kế lên phương án thiết kế , biện pháp , tiến độ thi công.- Quản lý, báo cáo tiến độ công việc.- Triển khai bản vẽ thi công , hoàn công.- Sắp xếp kỹ thuật đi bảo trì khi có sự cố.- Các công việc khác theo yêu cầu của cấp trên. (sẽ trao đổi chi tiết khi phỏng vấn). - Làm việc Tại Hà Nội và các tỉnh miền Bắc', N'- Có kinh nghiệm liên quan từ 4 năm trở lên.- Ưu tiên: Biết sử dụng CAD (AutoCad)', NULL, 14000000.0000, 16000000.0000, 0, CAST(N'2020-07-06' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5030, 27, 2, 4, N'ntd13@gmail.com', N'0435842248', N'Mr Hưng', 0, NULL, N'Đã duyệt', 1, 50, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2848, N'Nhân Viên Kinh Doanh Dự Án Làm Việc Tại Hà Nội', N'Nhân viên', N'Nhân viên', 1, N'- Xây dựng hồ sơ chào hàng cho dự án, lập phương án kinh doanh cho dự án. - Lập chính sách bán hàng cho dự án, tính toán phương án tài chính khả thi cho dự án - Tìm kiếm khách hàng dự án (Các nhà máy, các doanh nghiệp vừa và nhỏ, doanh nghiệp nước ngoài….)- Tư vấn, giới thiệu sản phẩm, thiết kế lên giải pháp phù hợp, báo giá, trình mẫu và chốt hợp đồng.- Báo cáo tiến độ và kết quả công việc theo tuần cho Quản lý và BGĐ.- Chủ động lên kế hoạch triển khai các công việc hoặc mục tiêu được giao.- Giữ mối quan hệ mật thiết với khách hàng bằng cách thường xuyên thăm hỏi, nhanh chóng phản hồi những yêu cầu của khách hàng.- Phối hợp với bộ phận kế toán, kỹ thuật để xây dựng giá của sản phẩm,….- Thực hiện các công việc khác theo phân công của cấp quản lý.', N'-Tốt nghiệp ngành Quản trị kinh doanh, Quản lý dự án- Có kinh nghiệm làm việc tại các chủ đầu tư Khu/ Cụm công nghiệp là một lợi thế. - Kỹ năng giao tiếp, thuyết trình, ứng xử , thuyết phục tốt- Các kỹ năng sử dụng máy tính cơ bản và các phần mềm quản lý.- Có thể đi công tác, gặp gỡ khách hàng - Kinh nghiệm 2-5 năm trong ngành', NULL, 15000000.0000, 20000000.0000, 0, CAST(N'2020-07-05' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5030, 27, 2, 2, N'ntd13@gmail.com', N'0779426972', N'Chị Quỳnh', 0, NULL, N'Đã duyệt', 1, 850, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2849, N'Quản Lý Kênh Mỹ Phẩm', N'Trưởng phòng', N'Quản lý', 1, N'- Chịu trách nhiệm chính trong việc quản lý và tăng doanh thu trên các kênh kinh doanh Online như TIKI, Lazada … (70% công việc). Các ứng viên có thể truy cập vào đường link này để biết thêm chi tiết mặt hàng sản phẩm sẽ kinh doanh: https://www.lazada.vn/shop/dwc/- Lên kế hoạch triển khai tăng doanh thu và quảng cáo sản phẩm tại các kênh Online cũng như các chương trình sự kiện về sản phẩm làm đẹp thường niên.- Hỗ trợ công việc giao tiếp với các đối tác và sử dụng tiếng anh để phiên dịch cho cấp trên.  (30% công việc)- Tất cả công việc đều thực hiện hoàn toàn trên máy tính và sử dụng tiếng Anh để giao tiếp trực tiếp với cấp trên.- Nộp báo cáo hàng tuần và tổng hợp báo cáo hàng tháng cho cấp trên bằng tiếng anh.', N'-Yêu cầu về bằng đại học, cao đẳng có tiếng Anh tốt và giao tiếp tốt trong công việc.-Yêu cầu về 1 số hiểu biết về mỹ phẩm và việc kinh doanh online.-Sử dụng vi tính thành thạo.-Có khả năng đi công tác các tỉnh thành, và tham gia các sự kiên về mỹ phẩm trong nước.', NULL, 10000000.0000, 15000000.0000, 0, CAST(N'2020-06-21' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5030, 27, 2, 2, N'ntd13@gmail.com', N'0157425432', N'Chị Phương Anh', 0, NULL, N'Đã duyệt', 1, 269, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2850, N'Kế Toán Tổng Hợp- Thu Nhập 10-12 Triệu', N'Nhân viên', N'Nhân viên', 1, N'► Xử lý, vào sổ sách kế toán các nghiệp vụ phát sinh như : mua bán vật tư, hàng hóa, các khoản thu chi tiền mặt, các khoản thanh toán qua ngân hàng ...► Theo dõi nhập xuất tồn hàng hóa ► Theo dõi, thanh toán các khoản công nợ đến hạn phải thu, phải trả► Cân đối các hóa đơn đầu vào đầu ra sao cho hợp lý► Lập các Báo cáo khác theo yêu cầu Ban Giám đốc► Lập các Báo cáo NXT , Báo cáo công nợ ► Cân đối hóa đơn đầu vào – đầu ra của tháng► Tính TNDN tạm tính theo quý► Nộp các khoản thuế phát sinh cho nhà nước (nếu có)► Kiểm tra, rà soát lại toàn bộ số liệu sổ sách kế toán trước khi làm Báo cáo năm ► Lập các Báo cáo cuối năm : + Quyết toán thuế TNDN và thuế TNCN năm+ Báo cáo tài chính năm► In sổ sách kế toán theo qui định', N'- Tốt nghiệp cao đẳng trở lên - Có 1 năm kinh nghiệm về vị trí kế toán tổng hợp- Cẩn thận trong công việc- Hoạt bát, nhanh nhẹn và năng động', NULL, 10000000.0000, 12000000.0000, 0, CAST(N'2020-07-09' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5030, 27, 2, 1, N'ntd13@gmail.com', N'0912405505', N'Quỳnh Chi', 0, NULL, N'Đã duyệt', 1, 942, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2851, N'Nhân Viên Tư Vấn Tín Dụng - Lương 10 Triệu Trở Lên', N'Nhân viên', N'Nhân viên', 3, N'- Sale được hỗ trợ data khách có nhu cầu để khai thác.- Giới thiệu, tư vấn, chăm sóc khách hàng sử dụng sản phẩm dịch vụ của SHB FC.- Hướng dẫn khách hàng hoàn tất thủ tục hồ sơ- Báo cáo trực tiếp leader.', N'- Từ 18 - 50 tuổi. Tốt nghiệp THPT trở lên, không nợ xấu.- Không yêu cầu kinh nghiệm, giờ giấc tự do, không check in, sẽ được training đầy đủ kỹ năng- Giờ giấc tự do, không cần đến văn phòng. - Chăm chỉ, chịu khó di chuyển, gắn bó lâu dài.- Ưu tiên các bạn đang làm tài chính Fecredit, Mcredit, LotteFinance, TPBFico, Vietcredit, Mirae Asset, Easy, Sale BĐS, Bảo Hiểm, tiếp thị...', NULL, 10000000.0000, 30000000.0000, 0, CAST(N'2020-07-05' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5031, 28, 1, 5, N'ntd14@gmail.com', N'0198930330', N'Mr. Nhớ', 0, NULL, N'Đã duyệt', 1, 986, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2852, N'Nhân Viên Kinh Doanh Lương Trên 10M', N'Nhân viên', N'Nhân viên', 2, N'Công ty TNHH Mefaco tham gia và hoạt động trong lĩnh vực thiết kế sản xuất, kinh doanh trang thiết bị y tế và cung cấp dịch vụ y tế. Nhằm mở rộng thị trường kinh doanh Công ty TNHH MEFACO cần tuyển dụng 5 nhân viên kinh doanh:Mô tả:- Tìm kiếm khách hàng qua nhiều kênh khác nhau, khai thác thị trường, phát triển kinh doanh- Tư vấn, hỗ trợ các khách hàng khi có nhu cầu sử dụng sản phẩm của công ty,- Tiến đến ký kết hợp đồng, theo dõi tiến độ hợp đồng, thanh toán và bàn giao hàng hóa cũng như nghiệm thu sản phẩm', N'- Tuổi từ : 20-35- Tốt nghiệp Cao đẳng trở lên chuyên ngành QTKD, kinh tế,…- Nắm vững các kiến thức cơ bản- Ham học hỏi, có tinh thần làm việc đồng đội- Giao tiếp tốt, nhanh nhẹn- Có trách nhiệm cao trong công việc Ưu tiên:+ Ứng viên có kinh nghiệm 1 năm trở lên (chưa có kinh nghiệm sẽ được đào tạo).', NULL, 7000000.0000, 12000000.0000, 0, CAST(N'2020-07-03' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5031, 28, 2, 5, N'ntd14@gmail.com', N'0618629347', N'Ms Loan', 0, NULL, N'Đã xóa', 1, 205, 0)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2853, N'[Hà Nội ] Kế Toán Kho Vận Chuyển - Logitic', N'Nhân viên', N'Nhân viên', 2, N'- Quản lí việc nhập kho hàng hóa , Xuất kho và tồn kho - Quản lí việc kiểm kê hàng hóa nhận được với list thực tế và Lên kế hoạch giao nhận hàng hóa cho khách hàng - Điều hành công việc trên kho hàng hóa - Quản lí việc xuất kho giao nhận hàng hóa cho khách- Các công việc khác theo sự phân công của cấp trên.', N'-  Giới tính : Nam -  Trình độ : Tốt nghiệp Đại học hoặc Cao đẳng kế toán-  Kinh nghiệm : từ 2 năm trở lên-  Có kinh nghiệm làm về logistic, Quản lí hàng hóa, Giao nhận hàng hóa, xuất nhập tồn kho là 1 lợi thế -  Độ tuổi Từ 25 tuổi trở lên -  Cẩn  thận , tỉ mỉ , làm việc có tinh thần trách nhiệm cao , có năng lực quản lí và chịu áp lực trong công việc -  Địa điểm làm việc : Bãi 81 - Cầu Thanh Trì - Hà Nội', NULL, 7000000.0000, 12000000.0000, 0, CAST(N'2020-07-10' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5031, 28, 2, 2, N'ntd14@gmail.com', N'0405699743', N'Ms Nguyện', 0, NULL, N'Đã duyệt', 1, 736, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2854, N'Trưởng Phòng Kinh Doanh', N'Trưởng phòng', N'Quản lý', 1, N'1. Quản lý kinh doanh- Xây dựng thương hiệu, hình ảnh công ty- Quảng bá, phát triển, mở rộng thị trường của các sản phẩm công ty đang làm phân phối: màn hình Led, thiết bị âm thanh, ánh sáng…- Xác định mục tiêu doanh thu. Lập kế hoạch và dự báo mức doanh thu hàng quý, hàng năm.- Phát triển chiến lược bán hàng để thu hút khách hàng mới. Có kế hoạch và triển khai kế hoạch tiếp cận các dự án.- Mở rộng mạng lưới phân phối trên toàn quốc.- Phân tích, đánh giá các dự án, đề xuất ý tưởng, phương thức hợp tác phát triển kinh doanh, xây dựng các chương trình tiếp cận khách hàng.- Xây dựng mối quan hệ với các đối tác để mở rộng khách hàng tiềm năng. Khai thác các mối quan hệ sẵn có của Công ty và tạo dựng mối quan hệ mới để tìm dự án, lập dự án và lấy được dự án, nâng cao hình ảnh và uy tín của công ty.- Phối hợp với nhân viên Digital marketing để xây dựng kế hoạch tạo ra khách hàng tiềm năng.- Theo dõi đối thủ cạnh tranh, các chỉ tiêu kinh tế và xu hướng của ngành.2. Quản lý nhân viên- Đặt mục tiêu, kế hoạch thực hiện và các tiêu chuẩn cho đại diện kinh doanh.- Hướng dẫn và đào tạo nhân viên kinh doanh.- Lập kế hoạch và triển khai các chương trình đào tạo.- Tuyển dụng và lựa chọn nhân viên kinh doanh mới.3. Quản lý nhu cầu khách hàng. Giải quyết vấn đề và các khiếu nại từ phía khách hàng liên quan đến bán hàng và dịch vụ.', N'- Tốt nghiệp ĐH chính quy các ngành ngoại thương, thương mại, kinh tế. Đã có kinh nghiệm ở vị trí tương đương.- Thành thạo về tin học văn phòng- Có hiểu biết trong ngành điện, điện tử, công nghệ thông tin là lợi thế.- Kỹ năng giao tiếp, đàm phán, thuyết trình, đào tạo, huấn luyện tốt.- Cương quyết, có năng lực quản lý bao quát tốt mọi vấn đề. Năng động, hoạt bát, có tinh thần trách nhiệm cao.- Sức khỏe tốt, chịu được áp lực cao trong công việc. Chấp nhận đi công tác nếu cần.', NULL, 15000000.0000, 30000000.0000, 0, CAST(N'2020-06-26' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5031, 28, 2, 0, N'ntd14@gmail.com', N'0673882517', N'Mrs Yến', 0, NULL, N'Đã duyệt', 1, 350, 0)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2855, N'Quản Lý Dự Án- Kỹ Sư Điện Công Nghiệp', N'Trưởng nhóm', N'Quản lý', 1, N'– Nhận thông tin và các hồ sơ giấy tờ về dự án từ ban giám đốc.– Triển khai kế hoạch thực hiện dự án mức A0, lập ban quản lý dự án và giao trách nhiệm cho từng thành viên để triển khai chi tiết mức A1.– Tổng hợp các kế hoạch chi tiết của thành viên ban QLDA, đối chiếu với dự toán kết hợp với việc dự đoán các nguy cơ, rủi ro có thể xảy ra để lên các phương án triển khai thích hợp cho từng trường hợp.– Phối hợp với các phòng ban liên quan triển khai thực hiện dự án. Theo dõi, giám sát và báo cáo, xử lý kịp thời nếu có bất kỳ sai lệch nào so với kế hoạch đã lên.– Tổng hợp và đánh giá chi phí, vật tư, tiến độ thực hiện theo từng bước trong kế hoạch với dự toán. Báo cáo và xử lý kịp thời nếu có phát sinh chi phí vượt quá định mức cho phép.– Trực tiếp liên lạc với người có trách nhiệm, giám sát kỹ thuật tại dự án hoặc trực tiếp ra công trường thi công để giám sát, đảm bảo dự án triển khai đúng kế hoạch và giải quyết vấn đề phát sinh nếu có.– Tập hợp và lưu trữ hồ sơ, giấy tờ cần thiết cho dự án để lưu tại công ty và cung cấp cho khách hàng. Phối hợp với các phòng ban chuẩn bị đầy đủ giấy tờ theo yêu cầu trong hợp đồng để tiến hành hoàn công và nghiệm thu.– Phối hợp với bộ phận kế toán đảm bảo việc thanh toán đúng hạn như trong hợp đồng.– Lập sổ tay QLDA để theo dõi tiến trinh thực hiện dự án và lập các báo cáo định kỳ theo quy định của công ty.– Tổng hợp số liệu khi dự án hoàn thành, lập báo cáo đánh giá kết quả thực hiện dự án- Tổ chức quản lý hệ thống điện trong toàn nhà máy.- Lập kế hoạch bảo trì, sửa chữa hệ thống điện nhà máy và máy móc sản xuất. cho toàn bộ các thiết bị điện (Bao gồm Máy phát điện, động cơ điện, tủ điện, hệ thống điện ... ) - Thực hiện triển khai các dự án tiết kiệm năng lượng cho hệ thống điện liên quan đến sản xuất.- Các công việc theo phân công của Ban giám đốc.', N'+ Tốt nghiệp Đại học hệ chính quy tại các trường công lập chuyên ngành Điện công nghiệp.+ Có kinh nghiệm quản lý dự án ít nhất 2-3 năm+ có chứng chỉ quản lsy dự án lợi thế+ Kinh nghiệm 1-3 năm kinh nghiệm trong lĩnh vực điện công nghiệp và tiết kiệm năng lượng tại các nhà máy sản xuất công nghiệp Đọc hiểu tài liệu kỹ thuật.+ Biết các phần mềm Autocad và MS Office. + Kỹ năng làm việc độc lập, phối hợp nhóm. Trung thực, làm việc chăm chỉ, có trách nhiệm vời công việc được giao. Ky năng tổ chức quản lý tốt. Những ứng viên làm qua các công ty sản xuất hoặc liên quan đến ngành ĐIỆN là một lợi thế.', NULL, 15000000.0000, 30000000.0000, 0, CAST(N'2020-06-30' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5031, 28, 2, 0, N'ntd14@gmail.com', N'0309088853', N'Ms Anh', 0, NULL, N'Đã duyệt', 1, 808, 0)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2856, N'Kỹ Thuật Sửa Máy Thêu Vi Tính', N'Nhân viên', N'Nhân viên', 2, N'- tìm ra nguyên nhân và sửa được các lỗi cơ bản của máy thêu vi tính.- Các công việc khác theo sự phân công của quản lý- Chi tiết cụ thể trao đổi khi phỏng vấn', N'- Yêu cầu Nam, tuổi từ 28 đến 45 tuổi- yêu cầu ứng viên đã có kinh nghiệm trong sửa chữa  máy thêu vi tính ít nhất 3 năm trở lên,', NULL, 7000000.0000, 15000000.0000, 0, CAST(N'2020-07-16' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5031, 28, 2, 0, N'ntd14@gmail.com', N'0294630939', N'Trần Thị Trang', 0, NULL, N'Đã duyệt', 1, 228, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2857, N'Nhân Viên Kinh Doanh Nội Thất', N'Nhân viên', N'Nhân viên', 5, N'- Giới thiệu tư vấn và thuyết phục các khách hàng sử dụng sản phẩm.- Duy trì, phát triển mối quan hệ hợp tác lâu dài với khách hàng đã có- Báo cáo thông tin, công việc về cho Trưởng Phòng/GĐ- Địa chỉ VP: Căn 1109 Toà S2.12 Vinhomes Ocean Park Gia Lâm. Hà Nội', N'- Đam mê kinh doanh và có tinh thần cầu tiến.- Kỹ năng giao tiếp, tư vấn và thuyết phục tốt.- Năng động, nhiệt huyết, ham học hỏi.- Ưu tiên ứng viên có kinh nghiệm telesales và làm sales trong lịch vực nội thất, xây dựng, bất động sản…- Chưa có kinh nghiệm được đào tạo', NULL, 10000000.0000, 15000000.0000, 0, CAST(N'2020-07-10' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5031, 28, 2, 0, N'ntd14@gmail.com', N'0843540795', N'Mr Hùng', 0, NULL, N'Đã duyệt', 1, 708, 0)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2858, N'Nhân Viên Vận Hành Máy SX Giấy Kháng Khuẩn', N'Nhân viên', N'Nhân viên', 3, N'+ Lắp đặt dây chuyền thiết bị mới theo chỉ đạo của Quản lý trực tiếp+ Trực tiếp thực hiện lắp đặt/ sửa chữa / bảo dưỡng máy móc, thiết bị+ Thực hiện chế độ báo cáo theo quy định và thực hiện các nhiệm vụ khác theo sự phân công của Quản lý trực tiếp', N'+ Ưu tiên người làm ở nhà máy nhựa, biết vận hành bảo trì hệ thống sản xuất nhựa+ Biết về PLC, servor motor, các loại cảm biến….+ Chịu trách nhiệm chính trong việc lắp đặt máy sản xuất.+ Sửa chữa các sự cố phát sinh hàng ngày trong toàn bộ khu vực sản xuất.+ Phân tích nguyên nhân gây lỗi sản phẩm liên quan đến kỹ thuật, chỉ đạo khắc phục và phương án phòng ngừa.+ Năng động, nhiệt tình với công việc+ Đi làm theo công trình tại TP. HCM và tỉnh lân cận', NULL, 10000000.0000, 15000000.0000, 0, CAST(N'2020-07-05' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5031, 28, 2, 1, N'ntd14@gmail.com', N'0725417577', N'Cao Văn Thưởng', 0, NULL, N'Đã duyệt', 1, 62, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2859, N'Nhân Viên Kinh Doanh (Nam)', N'Nhân viên', N'Nhân viên', 1, N'- Giới thiệu, tư vấn và bán sản phẩm cho khách hàng / Đại lý / cửa hàng.- Triển khai, tổ chức thực hiện kế hoạch kinh doanh, phát triển, duy trì và chăm sóc khách hàng.- Lập danh sách khách hàng, quản lý thông tin khách hàng hiện hữu và khách hàng tiềm năng.- Hiểu rõ sản phẩm, phối hợp với các nhóm hỗ trợ để xây dựng và triển khai các chương trình khuyến mãi kích thích kinh doanh và các chương trình chăm sóc khách hàng, giải quyết các khiếu nại của khách hàng- Khảo sát, đánh giá thị trường & khách hàng, đề xuất các kế hoạch phát triển sản phẩm mới- Cập nhật thông tin thị trường, thông tin đối thủ.- Đưa ra những thông tin về xu hướng thị trường trong thời gian tới để lập/ điều chỉnh kế hoạch kinh doanh thích hợp.- Thực hiện việc báo cáo tình hình và kết quả kinh doanh theo định kỳ được yêu cầu.- Công việc chủ yếu đi tham khảo thị trường, tiếp xúc với các cửa hàng, đại lý', N'- Kỹ năng lập kế hoạch kinh doanh, phân tích và phát triển thị trường.- Tốt Nghiệp : Đại học- Có kỹ năng giao tiếp tốt- Có khả năng thương thuyết và thuyết phục.- Nhạy bén trong công tác điều hành và xử lý tình huống.- Có sức khỏe tốt', NULL, 7000000.0000, 10000000.0000, 0, CAST(N'2020-07-16' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5031, 28, 2, 0, N'ntd14@gmail.com', N'0702366446', N'Mr Trọng', 0, NULL, N'Đã duyệt', 1, 598, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2860, N'hot Nữ Nhân Viên Tư Vấn Tại Văn Phòng - Không Yêu Cầu Kinh Nghiệm', N'Nhân viên', N'Nhân viên', 3, N'- Telesale tìm kiếm – tư vấn khách hàng theo data có sẵn- Tìm hiểu nhu cầu của khách hàng, tư vấn về sản phẩm của Công ty, đàm phán và ký kết hợp đồng- Chăm sóc khách hàng, hỗ trợ khách hàng trong quá trình thực hiện hợp đồng;- Thực hiện các yêu cầu công việc do cấp trên chỉ đạo.- Chi tiết công việc sẽ trao đổi khi phỏng vấn.', N'- Ham học hỏi, Máu lửa, Cầu tiến, Máu kiếm tiến và thích kiếm được thật nhiều tiền.- Nhiệt tình, năng động, cẩn thận và có trách nhiệm với công việc- Không ngại học cái mới, có khả năng làm việc nhóm, chịu được áp lực.- Ưu tiên các bạn có kinh nghiệm đã từng làm kinh doanh, hoặc CSKH. Không có kinh nghiệm sẽ được cầm tay chỉ việc nhiệt tình- Có LAPTOP cá nhân', NULL, 7000000.0000, 12000000.0000, 0, CAST(N'2020-06-25' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5031, 28, 2, 0, N'ntd14@gmail.com', N'0699819652', N'Ms Nhung', 0, NULL, N'Đã duyệt', 1, 877, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2861, N'Nhân Viên Kinh Doanh Lương Trên 20 Triệu', N'Nhân viên', N'Nhân viên', 5, N'+ Tìm hiểu nhu cầu của khách hàng, giải thích, tư vấn và hướng dẫn vấn đề liên quan đến sản phẩm cho khách hàng.+ Xây dựng, duy trì và phát triển mối quan hệ chặt chẽ với khách hàng mới, khách hàng hiện tại để đạt doanh số bán hàng cao.+ Tiếp nhận các thông tin của Khách hàng trong quá trình thực hiện hợp + Tìm kiếm khách hàng, đàm phán ký kết hợp đồng', N'- Tốt nghiệp bằng Đại học trở lên. - Nhiệt tình, có trách nhiệm với công việc, ưu tiên có kinh nghiệm từ 2 năm.- Có bằng lái xe ô tô là một lợi thế.', NULL, 0.0000, 0.0000, 1, CAST(N'2020-07-04' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5032, 29, 2, 1, N'ntd15@gmail.com', N'0850624646', N'Phòng Nhân Sự', 0, NULL, N'Đã duyệt', 1, 802, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2862, N'Chỉ Huy Trưởng Công Trình', N'Nhân viên', N'Nhân viên', 1, N'Giám sát thi công công trình nội thất, biệt thự, nhà phố,....;Theo dõi tiến độ thi công, đảm bảo về tiến độ và chất lượng công trình;Lập nhật ký công trình, biên bản hiện trường, biên bản phát sinh, vv.. Liên quan trong quá trình giám sát thi công; Tổng hợp các giấy tờ liên quan đến nghiệm thu kết thúc dự án;Kiểm tra khối lượng thực tế và xác nhận bản vẽ hoàn công;Kiểm tra hồ sơ thiết kế, bản vẽ kỹ thuật/ vật liệu;Giám sát chất lượng vật tư, thiết bị lắp đặt cho công trình;Kiểm tra biện pháp thi công, nhân lực thi công của nhà thầu phụ tại công trường;Quản lý các nhà thầu phụ, giám sát tiến độ và chất lượng thi công của thầu phụ;Thẩm định, nghiệm thu khối lượng và chất lượng của nhà thầu;Tổng hợp các giấy tờ liên quan đến nghiệm thu công trình và thực hiện công tác nghiệm thu công trình với các thầu phụ và chủ đầu tư;Phối hợp với các bộ phận thực hiện công tác bàn giao nghiệm thu với khách hàng;Phối hợp với các bộ phận liên quan thực hiện các thủ tục xin phép, cấp phép thi công dự án;Triển khai thực hiện các thủ tục ra vào công trường;Điều phối công tác vận chuyển và lắp đặt đồ nội thất;Thực hiện công tác bảo trì, bảo hành dự án;Kiểm tra việc bảo quản vật tư tại công trình đang thi công;Đảm bảo an toàn lao động, vệ sinh trong quá trình thi công công trình;Báo cáo tình trạng thi công hàng ngày tại công trình cho Tổ trưởng (tiến độ, chất lượng, khối lượng,...). Cảnh báo các vấn đề liên quan đến chất lượng, khối lượng và tiến độ công trình;Thực hiện các công việc khác theo yêu cầu của tổ trưởng và quản lý các cấp.', N'Kinh nghiệm 2 năm trở lên trong lĩnh vực giám sát thi công nội thất kiến trúc;Ưu tiên đã từng làm việc ở công ty xây dựng nội thất, am hiểu về các loại vật liệu nội thất;Tốt nghiệp Trung cấp trở lên các ngành Kỹ sư Xây dựng, Kiến trúc Nội thất;Am hiểu về các quy chuẩn, tiêu chuẩn, nghị định, thông tư, luật pháp liên quan đến xây dựng;Am hiểu tiêu chuẩn kỹ thuật của các loại vật tư, thiết bị xây dựng – lắp đặt công trình;Biết đọc, xử lý bản vẽ kỹ thuật thi công;', NULL, 15000000.0000, 20000000.0000, 0, CAST(N'2020-07-10' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5032, 29, 2, 2, N'ntd15@gmail.com', N'0525812155', N'Nguyễn Quang Bình', 0, NULL, N'Đã duyệt', 1, 292, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2863, N'Kế Toán Tổng Hợp Làm Việc Tại Phú Yên', N'Nhân viên', N'Nhân viên', 5, N'Kiểm tra đối chiếu số liệu giữa các đơn vị nội bộ, dữ liệu chi tiết và tổng hợp.- Kiểm tra các định khoản các nghiệp vụ phát sinh.- Kiểm tra sự cân đối giữa số liệu kế toán chi tiết và tổng hợp- Kiểm tra số dư cuối kỳ có hợp lý và Khớp đúng với các báo cáo chi tiết.-  Hạch toán thu nhập, chi phí, khấu hao,TSCĐ,công nợ, nghiệp vụ khác, thuế GTGT và báo cáo thuế khối văn phòng CT, lập quyết toán văn phòng cty.- Theo dõi công nợ khối văn phòng công ty, quản lý tổng quát công nợ toàn công ty. Xác định và đề xuất lập dự phòng hoặc xử lý công nợ phải thu khó đòi toàn công ty.- In sổ chi tiết và tổng hợp khối văn phòng, tổng hợp công ty theo qui định.- Lập báo cáo tài chính theo từng quí, 6 tháng, năm và các báo cáo giải trình chi tiết.- Hướng dẫn xử lý và hạch toán các nghiệp vụ kế toán.- Tham gia phối hợp công tác kiểm tra, kiểm kê tại các đơn vị cơ sở.-  Giải trình số liệu và cung cấp hồ sơ, số liệu  cho cơ quan thuế, kiểm toán, thanh tra kiểm tra theo yêu cầu của phụ trách phòng KT-TV.', N'- Có ít nhất 2 năm kinh nghiệm vị trí kế toán tổng hợp hoặc các vị trí tương đương- Cẩn thận, trung thực, nhanh nhẹn, nhiệt tình trong công việc;- Thành thạo tin học văn phòng (Word, Excel)- Biết sử dụng phần mềm kế toán,- Ưu tiên biết về lĩnh vực xây dựng', NULL, 12000000.0000, 15000000.0000, 0, CAST(N'2020-07-03' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5032, 29, 2, 0, N'ntd15@gmail.com', N'0816226343', N'Mss Diễm', 0, NULL, N'Đã duyệt', 1, 991, 0)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2864, N'Nhân Viên Tư Vấn Bảo Hiểm Cathay', N'Nhân viên', N'Nhân viên', 5, N'- Tìm kiếm và chăm sóc khách hàng tiềm năng- Tư vấn, Giải đáp thắc mắc và cung cấp thông tin cho khách hàng về sản phẩm.- Thiết lập mối quan hệ với các đối tác tiềm năng nhằm mở rộng hệ thống cung cấp khách hàng mới.', N'- Nữ tuổi từ 25 trở lên - Tốt nghiệp Trung Cấp, Cao đẳng trở lên (không phân biệt chuyên ngành)- Chưa có kinh nghiệm được đào tạo- Tự tin, nhanh nhẹn, vui vẻ, hòa đồng, làm việc được trong môi trường áp lực cao.', NULL, 8000000.0000, 20000000.0000, 0, CAST(N'2020-07-26' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5032, 29, 2, 0, N'ntd15@gmail.com', N'0972571862', N'Ms Uyên', 0, NULL, N'Đã duyệt', 1, 921, 0)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2865, N'Marketing Facebook Ads', N'Nhân viên', N'Nhân viên', 5, N'- Có kinh nghiệm chạy ads 3 tháng trở lên trong lĩnh vực bất kì hoặc Kinh nghiệm viết Content', N'- Độ tuổi từ 17 - 35t- Có laptop- Có kinh nghiệm chạy ads 3 tháng trở lên trong lĩnh vực bất kì hoặc Kinh nghiệm viết Content:+ Sản phẩm chất lượng,Ngân sách + Môi trường chuyên nghiệp,năng động+ngoại hình ưa nhìn,  giao tiếp tốt', NULL, 7000000.0000, 20000000.0000, 0, CAST(N'2020-07-15' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5032, 29, 2, 0, N'ntd15@gmail.com', N'0708335492', N'Ms Thảo', 0, NULL, N'Đã duyệt', 1, 882, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2866, N'Kế Toán Tổng Hợp Làm Việc Tại Hải Phòng', N'Nhân viên', N'Nhân viên', 1, N'- Kiểm tra, đối chiếu số liệu (Ngân hàng, công nợ…).- Làm bảng lương và các báo cáo thống kê.- Hàng kỳ lập các tờ khai thuế, báo cáo thuế theo quy định. Hạch toán các nghiệp vụ kinh tế phát sinh;- Quản lý và lưu trữ hồ sơ tài liệu kế toán, TSCĐ, Tập hợp các hóa đơn, chứng từ kế toán, kiểm tra tính hợp lý - hợp pháp của chứng từ kế toán;- Thực hiện các công việc khác do cấp trên giao.', N'- Nam nữ từ 25 tuổi trở lên, tốt nghiệp ĐH chuyên ngành tài chính – kế toán, ưu tiên các bạn biết Tiếng Anh là một lợi thế- Tối thiểu 2 năm kinh nghiệm làm kế toán trong công ty đầu tư, xây dựng.- Cẩn thận, trung thực, tỉ mỉ có tinh thần cầu tiến, chịu khó và có khả năng chịu được áp lực trong môi trường công việc;- Kỹ năng sử dụng máy tính tốt;- Kỹ năng giao tiếp, đàm phán, thuyết phục;- Khả năng làm việc độc lập và phối hợp nhóm;- Tinh thần trách nhiệm cao.- Nắm vững các quy định về thuế.', NULL, 5000000.0000, 10000000.0000, 0, CAST(N'2020-07-13' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5032, 29, 2, 2, N'ntd15@gmail.com', N'0232044030', N'Mrs Hương', 0, NULL, N'Đã duyệt', 1, 148, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2867, N'Kế Toán Bán Hàng_Kinh Nghiệm Ít Nhất 6 Tháng', N'Nhân viên', N'Nhân viên', 3, N'- Thu tiền sản phẩm từ nhân viên giao hàng.- Cập nhật thu – chi hàng ngày lên phần mềm kế toán.- Theo dõi công nợ của khách hàng.- Nghe điện thoại, nhận thông tin đặt hàng từ khách hàng.- Theo dõi, cập nhật thông tin đơn hàng.- Thời gian làm việc xoay ca: Ca 1: 7h30 – 16h30, Ca 2: 8h30 – 17h30, Ca 3: 10h00 – 19h00- Địa chỉ làm việc: Tại một trong 2 trung tâm của SAPUWA+ 195/27 Xô Viết Nghệ Tĩnh, Phường 17, Quận Bình Thạnh + 76 Hà Huy Tập, Khu Nam Thiên 1, Khu đô thị PMH,  P.Tân Phong, Quận 7 Lương : 6.500.000đ đến 7.500.000đ / tháng + trợ cấp cơm & giữ xe', N'- Khả năng giao tiếp tốt, đàm phán, thương lượng tốt.- Sử dụng tốt Excel.- Có kinh nghiệm kế toán bán hàng là lợi thế.- Tốt nghiệp trung cấp trở lên- Chịu khó, có khả năng chịu áp lực tốt.', NULL, 6000000.0000, 8000000.0000, 0, CAST(N'2020-07-11' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5032, 29, 1, 0, N'ntd15@gmail.com', N'0152867590', N'Phòng Nhân Sự', 0, NULL, N'Đã xóa', 1, 930, 0)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2868, N'Nhân Viên Kỹ Thuật (HCM - Hà Nội - Cần Thơ)', N'Nhân viên', N'Nhân viên', 10, N'- Khảo sát vị trí lắp đặt hệ thống, bảo trì, bảo dưỡng hệ thống sản phẩm của Công ty – được đào tạo chuyên môn.- Thực hiện các công việc khác do cấp trên giao- Cẩn thận, tỉ mỉ trong công việc- Chi tiết công việc sẽ trao đổi cụ thể khi phỏng vấn.Địa chỉ: 353/42 Phạm Ngũ Lão, Phường Phạm Ngũ Lão, Quận 1, Tp.HCMVăn phòng chính: 29 Nguyễn Thái Học, Phường Cầu Ông Lãnh, Quận 1, Tp.HCMVăn phòng 2: Cầu Bà Đắc, QL1A, ấp An Thái, xã An Cư, huyện Cái Bè, Tỉnh Tiền GiangVăn phòng 3: KV Thới An, phường Thuận An, Quận Thốt Nốt, Tp.Cần ThơVăn phòng 4: 354E Âu Cơ, phường Nhật Tân, Quận Tây Hồ, Tp.Hà NộiVăn phòng 5: Khu phố Bình Giang 01, phường Sơn Giang, thị xã Phước Long, Tỉnh Bình Phước', N'- Trung cấp Nghề trở lên, chuyên nghành+ Kỹ thuật điện+ Kỹ thuật cơ khí+ Ngành liên quan- Kinh nghiệm:+ Sinh viên mới ra trường hoặc kỹ thuật viên trái nghành sẽ được đào tạo học việc 02 tháng và hỗ trợ lương cơ bản.+ Số năm kinh nghiệm công tác với nghành kỹ thuật điện, kỹ thuật cơ khí là sẽ yếu tố xét lương bắt đầu- Có sức khỏe tốt, trung thực, nhiệt tình và có trách nhiệm trong công việc', NULL, 6000000.0000, 10000000.0000, 0, CAST(N'2020-07-07' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5032, 29, 2, 0, N'ntd15@gmail.com', N'0638331340', N'Chị An', 0, NULL, N'Đã duyệt', 1, 325, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2869, N'Kế Toán Tổng Hợp Ngành Xây Dựng', N'Trưởng phòng', N'Quản lý', 1, N'- Thu thập hoá đơn, chứng từ đầu vào, hoá đơn đầu ra để lên báo cáo;- Nhập liệu, kiểm tra các định khoản các nghiệp vụ phát sinh vào phần mềm kế toán;- Quản lý sổ sách chứng từ kế toán. Làm việc cơ quan thuế khi có phát sinh;- Thực hiện hạch toán kế toán các nghiệp vụ kinh tế phát sinh trong lĩnh vực được phân công;- Báo cáo tài chính, quyết toán thuế theo quy định;- Đảm bảo sổ sách kế toán và ghi nhận chính xác, phù hợp với chuẩn mực kế toán Việt Nam.', N'- Tuổi từ 25 đến 40;- Tốt nghiệp Đại học trở lên, chuyên ngành kế toán;- Nắm vững Luật Thuế, luật liên quan đến lĩnh vực xây dựng cơ bản,…- Có kinh nghiệm làm việc từ 06 năm trở lên, trong đó đã có ít nhất 2 năm làm vị trí tương đương (Ưu tiên có kinh nghiệm làm ở lĩnh vực xây dựng cơ bản) - Có năng lực nghiệp vụ kế toán đặc biệt là kế toán chuyên ngành xây dựng cơ bản, có khả năng tổng hợp, nắm vững chế độ kế toán;- Sử dụng máy vi tính thành thạo phần mềm excel, phần mềm kế toán;- Có khả năng làm việc độc lập và theo nhóm, chịu được áp lực công việc/làm thêm giờ, có tinh thần trách nhiệm cao;- Tác phong nhanh nhẹn, thái độ lịch sự, hòa nhã, thân thiện, kỹ năng giao tiếp và truyền đạt thông tin tốt;- Cẩn thận, trung thực, chăm chỉ, tận tâm và có trách nhiệm với công việc.', NULL, 25000000.0000, 30000000.0000, 0, CAST(N'2020-06-25' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5032, 29, 2, 5, N'ntd15@gmail.com', N'0329839159', N'Ms Thanh', 0, NULL, N'Đã duyệt', 1, 743, 0)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2870, N'Nhân Viên Customer Service -Chứng Từ Hàng Xuất', N'Nhân viên', N'Nhân viên', 2, N'- Liên hệ với khách hàng, hãng tàu, đại lý trong và ngoài nước.- Làm vận đơn ( đường biển, đường hàng không),các chứng từ và gởi các chứng từ theo qui trình- Xử lý dịch vụ vận tải giao nhận bằng đường biển, đường hàng không , đường bộ, khai thác Hải Quan, v..v...-Theo dõi tình trạng lô hàng va giải quyết các vấn đề phát sinh liên quan- Hỗ trợ bộ phận kinh doanh và hỗ trợ khách hàng.Nơi làm việc : 84 Nguyễn Đình Chính, phường 15, quận Phú Nhuận, Tp.HCM.', N'- Tốt nghiệp đại học, cao đẳng các ngành liên quan đến lĩnh vực xuất nhập khẩu như: Ngoại thương, Kinh Tế, Kinh doanh quốc tế, Giao Thông Vận tải, kinh tế đối ngoại.- Sử dụng Tiếng Anh trong giao dịch qua email, điện thoại, giao tiếp với người nước ngoài khi cần thiết. Nói và hiểu được chứng từ XNK vào bằng tiếng Anh,- Sử dụng tốt máy vi tính, e-mail. - Trung thực, siêng năng, chịu khó, có tinh thần cầu tiến- Nhanh nhẹn, năng động, cẩn thận, khả năng giao tiếp tốt- Kỹ năng đàm phán thuyết phục tốt; - Kỹ năng hợp tác làm việc nhóm tốt; - Kinh nghiệm làm cho fowarder tối thiểu 2 năm.', NULL, 7000000.0000, 10000000.0000, 0, CAST(N'2020-07-02' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5032, 29, 2, 4, N'ntd15@gmail.com', N'0216495995', N'Anh Kỳ', 0, NULL, N'Đã duyệt', 1, 94, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2871, N'Nhân Viên Graphic Designer', N'Nhân viên', N'Nhân viên', 5, N'- Thiết kế banner, promo, event, poster, graphic cho các dự án của công ty- Xây dựng các ý tưởng, thiết kế, sản xuất các visual content để đăng tải trên các kênh social của công ty (video, hình ảnh, infographic…) dựa trên mục tiêu và kế hoạch truyền thông thương hiệu của công ty khi cần.- Tham gia đóng góp và đưa ra sáng kiến đối với các kế hoạch của phòng Marketing- Thiết kế các ấn phẩm hỗ trợ cho hoạt động của các phòng ban khác khi được yêu cầu- Phát triển các ý tưởng sáng tạo cho các yếu tố hình ảnh của các chiến dịch quảng cáo- Các công việc khác do quản lý phân công', N'- Kinh nghiệm 2 năm trở lên- Sử dụng thành thạo Photoshop & Illustrator, ngoài ra: After effect, Premiere… là một lợi thế- Có kinh nghiệm về thiết kế banner, video animation- Edit Video- Gu thẩm mỹ tốt, Sáng tạo, khả năng sử dụng màu sắc, khả năng layout và typography tốt.- Cẩn thận, tỷ mỉ, có tinh thần trách nhiệm cao với công việc- Có khả năng làm việc độc lập và làm việc nhóm', NULL, 7000000.0000, 15000000.0000, 0, CAST(N'2020-07-14' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5033, 30, 2, 2, N'ntd16@gmail.com', N'0950189669', N'Chị Hòa', 0, NULL, N'Đã duyệt', 1, 978, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2872, N'Nhân Viên Bán Hàng - Tại Hải Dương', N'Nhân viên', N'Nhân viên', 5, N'- Làm việc tại văn phòng công ty giờ hàng chính- Tổ chức kênh bán hàng online (Google, Facebook, Zalo...)- Tìm kiếm, khai thác khách hàng / đại lý mới, chăm sóc khách hàng / đại lý cũ của Công ty .- Công việc khác theo yêu cầu.- Chi tiết trao đổi thêm khi phỏng vấn', N'- Tốt nghiệp trung cấp trở lên,- Có khả năng đàm phán, thuyết trình, làm việc nhóm.- Ưu tiên các bạn đã có kinh nghiệm, không có kinh nghiệm sẽ được đào tạo (kể cả những bạn mới ra trường)- Kỹ năng giao tiếp tốt.', NULL, 12000000.0000, 15000000.0000, 0, CAST(N'2020-06-21' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5033, 30, 2, 0, N'ntd16@gmail.com', N'0467506582', N'Trần Thủy', 0, NULL, N'Đã duyệt', 1, 509, 0)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2873, N'Nhân Viên Kinh Doanh Nội Thất Tại Hà Nội', N'Nhân viên', N'Nhân viên', 4, N'- Tìm kiếm khách hàng mới, triển khai công việc được trưởng phòng giao để tìm kiếm khách hàng và mở rộng thị trường : Lĩnh vực thiết kế và thi công  Nội thất gỗ tự nhiên ,( gỗ gõ đỏ, gỗ óc chó) .- Báo cáo công việc hàng ngày và lên kế hoạch thực hiện đầu việc được giao.- Chăm sóc khách hàng được giao, tư vấn khách hàng về thiết kế và các sản phẩm nội thất. Đàm phán chốt hợp đồng với khách hàng.- Quản lý đơn hàng: Giám sát tiến độ sản xuất, chất lượng sản phẩm trong quá trình sản xuất và lắp đặt. Chăm sóc khách hàng sau bán, hỗ trợ bảo hành cho khách hàng.- Cập nhật tin liên quan đến đối thủ cạnh tranh. Đề xuất ý tưởng kinh doanh mới- Làm các công việc khác theo sự phân công công việc của trưởng / phó phòng kinh doanh.', N'- Trình độ: trung cấp trở lên, chuyên ngành QTKD, Marketing, kinh tế, kiến trúc và các chuyên ngành liên quan.- Không yêu cầu kinh nghiệm.- Ứng viên nhanh nhẹn, có tinh thần làm việc độc lập, chịu được áp lực cao trong công việc.- Kỹ năng giao tiếp, thuyết trình, thiết lập mối quan hệ trong công việc.- Kỹ năng lập kế hoạch và tổ chức thực hiện công việc.- Kỹ năng tổng hợp, Phân tích số liệu và lập báo cáo', NULL, 4000000.0000, 6000000.0000, 0, CAST(N'2020-07-03' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5033, 30, 2, 0, N'ntd16@gmail.com', N'0325931026', N'Bùi Thu Huyền', 0, NULL, N'Đã duyệt', 1, 725, 0)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2874, N'Business System Analyst - IT (Thủ Dầu Một)', N'Nhân viên', N'Nhân viên', 1, N'• Work with operation closely to identify the IT solution and implementing any or all application(s) of the Supply Chain Management (SCM) suite within budget and a set timeline.                - SCM Suite would include WMS, TMS, FFS, Others where applicable • Join solutions design sessions with customers • Preparation of functional business specifications for development based on best practices. • Design and implement report analysis system for business requirement. • Testing and implement to UAT for new function . • Responsible for daily IT operation & System support including application, report issues and requirements. • To manage and troubleshoot problems/incidents promptly following established procedures. • To work on IT project as per assigned. • Rudimentary testing of development to Functional Business Requirements. OTHERS :                 - Productivity improvement.                 - Training and development materials for Super Users.                 - Product functionality recommendations.                 - Conduct testing', N'• Science/Information Technology or equivalent • Have 3 years of manage application system for logistics, daily work with BI analysis system, strong in MSSQL system • Business knowledge of supply chain execution in any or all of the following verticals:              o Distribution center services (warehouse operations)              o Transport management              o Freight forwarding. • Knowledge in any or all core supply chain application(s): WMS – preferably Exceed, TMS – preferably OTM, FFS – Cargowise 1 • Knowledge of ERP, or experience in ECom environment, Network & Infrastructure is an advantage. • Good English communication skills (written and verbal) • Work location: Mappletree Binh Duong Logistic Park Phase 1, 18 L2-1 Tao Luc 5 Street VSIPII, Hoa Phu Ward, Thu Dau Mot City, Binh Duong Province, Vietnam.', NULL, 15000000.0000, 25000000.0000, 0, CAST(N'2020-06-25' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5033, 30, 2, 3, N'ntd16@gmail.com', N'0332496555', N'Ms. Elly Tran (Trang)', 0, NULL, N'Đã duyệt', 1, 654, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2875, N'Phó Phòng Kỹ Thuật (Mức Lương 15- 20 Triệu / Tháng)', N'Quản lý', N'Quản lý', 1, N'1.1.Hỗ trợ Trưởng phòng tổ chức, điều hành hoạt động của phòng Kỹ thuật:-Thiết lập mục tiêu và giám sát, đánh giá toàn bộ kế hoạch hoạt động của phòng;-Đào tạo, hướng dẫn, phát triển và đánh giá hiệu quả công việc của CBNV trong phòng;-Quản lý và tổ chức thực hiện các mục tiêu về: 5S, Kaizen, an toàn của phòng;-Tham gia xây dựng các quy trình, quy định, tài liệu, biểu mẫu … liên quan đến lĩnh vực phụ trách trong toàn Công ty.1.2.Tổ chức thực hiện các vấn đề kỹ thuật phát sinh trong toàn nhà máy:-Cùng trưởng phòng giải quyết mọi vấn đề kỹ thuật phát sinh tại Nhà máy cơ khí;-Tham mưu cho Trưởng phòng, Ban giám đốc về các vấn đề: kỹ thuật; phương án công nghệ, giảm giá thành sản phẩm …-Chi tiết công việc sẽ trao đổi trong khi phỏng vấn.', N'-Nam, trên 30 tuổi,  Tốt nghiệp chính quy các trường Đại học thuộc khối kỹ thuật như: Bách khoa, Công nghiệp Hà Nội, Thái Nguyên, ĐHSPKT Hưng Yên ... chuyên ngành cơ khí chế tạo máy.-Có kinh nghiệm trên 5 năm tại vị trí trưởng/ phó phòng kỹ thuật của các nhà máy cơ khí có quy mô từ 300 người trở lên (ưu tiên làm trong doanh nghiệp FDI)-Đặc biệt phải am hiểu kỹ thuật về khuôn, dập, Hàn ...-Hiểu biết các công cụ quản lý sản xuất (5S, 4M, ISO)-Có kỹ năng quản lý tốt, nghiêm túc, ổn định.-Tự tin và tinh anh trong công việc-Có khả năng giao tiếp tốt, quyết đoán trong mọi tình huống.-Ưu tiên: Có trình độ tiếng anh giao tiếp và đọc tài liệu chuyên ngành;', NULL, 15000000.0000, 20000000.0000, 0, CAST(N'2020-06-25' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5033, 30, 2, 5, N'ntd16@gmail.com', N'0313812088', N'Ms Chúc', 0, NULL, N'Đã duyệt', 1, 583, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2876, N'Nhân Viên Marketing', N'Nhân viên', N'Nhân viên', 2, N'1. Marketing:- Hỗ trợ các manager trong team marketing trong việc lên kế hoạch và thực hiện các chương trình promotion nhằm tăng mức độ nhận diện thương hiệu của công ty và các công ty thành viên trong tập đoàn LOTTE- Các hoạt động brainstorm, lên ý tưởng, nghiên cứu và tìm kiếm thông tin, thiết kế proposal, quan hệ đối tác, thực hiện và báo cáo tình hình tiến độ của từng dự án - Lên ý tưởng và viết các nội dung đăng trên kênh social media, website,... của công ty.2. Customer Service:- Tiếp nhận và phản hồi  khách hàng trên các kênh fanpage, email và hotline của công ty; -  Hỗ trợ các công ty  trong tập đoàn LOTTE xử lý các vấn đề của khách hàng liên quan đến chương trình thành viên.3. Partnership:- Hỗ trợ các hoạt động quan hệ đối tác nhằm duy trì tương tác hiệu quả và bền vững giữa công ty và các công ty liên kết trong và ngoài tập đoàn.', N'- Tốt nghiệp Đại học chuyên ngành Marketing, Thương Mại, Truyền thông – Quảng cáo,  Quản trị kinh doanh hoặc các ngành liên quan - Có kinh nghiệm về Marketing trong lĩnh vực cùng ngành hoặc từng làm việc tại agency  là một lợi thế - Trình độ tiếng Anh từ khá trở lên ( đọc, viết tốt)- Biết nắm bắt xu hướng, có tương tác tốt trên cộng đồng mạng là 1 lợi thế : Facebook, Instagram ...- Sử dụng thành thạo công cụ Microsoft Office (Word, Excel, Power point)- Hiểu biết về Google Ads, Facebook Ads, youtube, instagram….- Kỹ năng phân tích, lên kế hoạch, quản lý thời gian tốt.- Có kĩ năng soạn thảo và làm proposal, planning tốt- Đam mê công việc, chịu khó học hỏi, có tinh thần cầu tiến và tinh thần đồng đội .', NULL, 10000000.0000, 12000000.0000, 0, CAST(N'2020-07-15' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5033, 30, 2, 0, N'ntd16@gmail.com', N'0679093129', N'Ms Nguyên', 0, NULL, N'Đã duyệt', 1, 860, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2877, N'Chuyên Viên Marketing', N'Nhân viên', N'Nhân viên', 1, N'•Hoạch định kế hoạch, phân tích và thực hiện hoạt động Digital Marketing, Social Media. Phụ trách phát triển các chương trình PR, quảng cáo trên trên các phương tiện truyền thông online để quảng bá thương hiệu và các sản phẩm của Công ty.•Thiết kế hình ảnh và video các ấn phẩm liên quan để phục vụ cho chương trình marketing.•Viết tin, bài quảng bá và chịu trách nhiệm quản lý nội dung website của Công ty.•Thu thập, phân tích thông tin thị trường, thông tin về sản phẩm, thông tin về các chương trình Digital Marketing, Social Media của đối thủ cạnh tranh.•Đề xuất và thực hiện các chương trình khuyến mại, truyền thông, quảng bá và xây dựng thương hiệu cho công ty và các sản phẩm của công ty•Thực hiện các công việc hỗ trợ khách hàng tiếp cận với sản phẩm trên Fanpage; website Công ty, giải đáp thông tin về sản phẩm cho khách hàng;•Tham gia các công việc khác theo yêu cầu của công ty.', N'•Trình độ: Tốt nghiệp cao đẳng chuyên ngành Marketing/ Kinh tế/ Công nghệ thông tin…hoặc các ngành có liên quan;.•Có khả năng giao tiếp, thuyết trình và thuyết phục tốt;•Có tinh thần trách nhiệm với công việc, nhiệt huyết•Có khả năng làm việc nhóm, và độc lập', NULL, 7000000.0000, 10000000.0000, 0, CAST(N'2020-06-23' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5033, 30, 2, 0, N'ntd16@gmail.com', N'0476992409', N'Chị Xinh', 0, NULL, N'Đã duyệt', 1, 571, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2878, N'Nhân Viên Tiếp Thị Bán Hàng [ Hà Nội]', N'Nhân viên', N'Nhân viên', 4, N'Tiếp tục làm việc với khách hàng cũ, tìm kiếm các kênh khách hàng và phát triển khách hàng mới. Soạn thảo, kiểm tra các điều khoản hợp đồng trước khi ký, xác nhận đơn đặt hàng và lịch giao hàng. Đề nghị khách hàng trả tiền hàng và yêu cầu công ty thanh toán tiền hoa hồng bán hàng từ Phòng kế toán của công ty. Báo cáo trực tiếp các hoạt động bán hàng hàng ngày cho Trưởng phòng và Ban Giám đốc. Lên kế hoạch bán hàng theo quý hay hàng năm.', N'+ Nam, tốt nghiệp đại học, cao đẳng, trung cấp ngành Marketing, kinh doanh, kinh tế, kỹ thuật. + Có kinh nghiêm làm việc trên 03 năm + Có thể giao tiếp bằng tiếng Anh hoặc tiếng Hàn Quốc là 1 lợi thế.', NULL, 12000000.0000, 15000000.0000, 0, CAST(N'2020-06-20' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5033, 30, 2, 0, N'ntd16@gmail.com', N'0231077345', N'Mr.Hậu', 0, NULL, N'Đã duyệt', 1, 947, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2879, N'Chuyên Viên Kế Toán Đối Soát', N'Nhân viên', N'Nhân viên', 3, N'- Đối soát phát hiện giao dịch lệch với ngân hàng, nhà mạng- Theo dõi xử lý giao dịch sau khi phát hiện lệch- Kiểm tra tính cân bằng tiền giữa hệ thống và ngân hàng, nhà mạng- Báo cáo tính cân bằng tiền trên hệ thống- Thực hiện các công việc khác theo sự phân công của quản lý', N'- Tốt nghiệp chuyên ngành Kế toán/Toán thống kê- Có ít nhất 06 tháng kinh nghiệm ở vị trí tương đương hoặc Kế toán Công nợ- Kỹ năng cần thiết: Kỹ năng giải quyết vấn đề, Kỹ năng giao tiếp- Sử dụng thành thạo Excel, sử dụng tốt phần mềm kế toán', NULL, 10000000.0000, 12000000.0000, 0, CAST(N'2020-07-02' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5033, 30, 2, 0, N'ntd16@gmail.com', N'0886415413', N'Mr.Trung', 0, NULL, N'Đã duyệt', 1, 793, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2880, N'Nhân Viên Cơ Khí Có Tay Nghề [ Hồ Chí Minh]', N'Nhân viên', N'Nhân viên', 5, N'- Hàn, tiện, Phay khung máy, chi tiết máy- Các công việc cơ khí khác: lắp ráp máy, khoan, taro...- Các công việc khác theo yêu cầu- Địa chỉ làm việc: Số 100 đường Hồng Sến, p. Long Thạnh Mỹ, Q9, Tp hồ chí minh', N'- Biết Tiện, Phay, Hàn-Trung thực, siêng năng, có tinh thần học hỏi- Làm việc ổn định, lâu dài- Có sức khỏe tốt', NULL, 7000000.0000, 10000000.0000, 0, CAST(N'2020-07-03' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5033, 30, 2, 1, N'ntd16@gmail.com', N'0621001181', N'Mr Khải', 0, NULL, N'Đã duyệt', 1, 467, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2881, N'Nhân Viên Thị Trường - Kinh Doanh Chế Phẩm Vi Sinh Công Nghệ Cao Nhật Bản Dùng Trong Nuôi Tôm', N'Nhân viên', N'Nhân viên', 20, N'- Ứng viên có thể chọn một trong các tỉnh sau để làm việc: Long An, Tiền Giang, Bến Tre, Trà Vinh, Sóc Trăng, Bạc Liêu, Cà Mau, Kiên Giang, Ninh Thuận, Bình Thuận, Khánh Hòa- Công việc đòi hỏi đi ngoài thị trường toàn thời gian.- Giới thiệu sản phẩm vi sinh công nghệ cao Nhật Bản đến với Đại lý phân phối và các trang trại nuôi tôm (B2B, B2C)  - Chăm sóc khách hàng hiện có, mở rộng thị trường và tìm kiếm khách hàng mới.   - Báo cáo công việc theo kế hoạch: Ngày/Tuần/Tháng/Quý', N'- Có xe gắn máy.- Có kiến thức về kinh doanh - Có khả năng làm độc lập, làm việc nhóm - Kỹ năng lập, tổ chức và thực hiện kế hoạch. - Kỹ năng giao tiếp, đàm phán.- Chịu khó, nhiệt tình với công việc - Chủ động và cầu tiến. - Ưu tiên sinh viên mới tốt nghiệp trung cấp, cao đẳng hoặc đại học. Không phân biệt ngành nghề.', NULL, 12000000.0000, 15000000.0000, 0, CAST(N'2020-06-30' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5034, 31, 2, 0, N'ntd17@gmail.com', N'0525231516', N'Phòng Nhân Sự', 0, NULL, N'Chưa duyệt', 1, 731, 0)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2882, N'Nhân Viên Kinh Doanh Tại Hà Nội', N'Nhân viên', N'Nhân viên', 10, N'- Tiếp nhận thông tin khách hàng tiềm năng và có sẵn nhu cầu từ Bộ phận tư vấn qua điện thoại (Telemarketing).- Sắp xếp đến gặp trực tiếp khách hàng mà Bộ phận tư vấn qua điện thoại (Telemarketing) đã hẹn khách hàng theo lịch hẹn có sẵn.- Không cần tự tìm kiếm Khách hàng, chỉ tập trung khai thác đúng đối tượng khách hàng có nhu cầu cần được tư vấn dịch vụ, tỉ lệ chốt khách hàng cao.- Hỗ trợ tư vấn, giải đáp thắc mắc cho khách hàng về các giải pháp liên quan đến tài chính, bảo hiểm.- Hỗ trợ Khách hàng ký Hợp đồng với công ty- Công việc chi tiết sẽ được trao đổi khi phỏng vấn THỜI GIAN VÀ ĐỊA ĐIỂM LÀM VIỆC- Thời gian làm việc: Giờ hành chính Thứ 2 –  Thứ 6 ( Nghỉ Thứ 7,Chủ nhật nghỉ)- Địa điểm làm việc: Văn phòng công ty hoặc di chuyển theo lịch hẹn của Bộ phận tư vấn qua điện thoại', N'- Nam/ Nữ tuổi từ 25 – 39 tuổi, tốt nghiệp THPT trở lên- Có phương tiện đi lại- Sức khỏe tốt, có thể gắn bó lâu dài- Yêu thích công việc tư vấn, giao tiếp và bán hàng trực tiếp- Khả năng nói chuyện lưu loát, thái độ tốt- Chăm chỉ nhiệt tình, hòa đồng', NULL, 15000000.0000, 20000000.0000, 0, CAST(N'2020-07-27' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5034, 31, 2, 0, N'ntd17@gmail.com', N'0484577403', N'Ms Nhung', 0, NULL, N'Đã duyệt', 1, 861, 0)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2883, N'Chuyên Viên Tư Vấn Đầu Tư BĐS (Tân Phú_Hồ Chí Minh) _Lương Trên 10 Triệu', N'Nhân viên', N'Nhân viên', 5, N'Tư vấn cho Khách Hàng có nhu cầu về sãn phẩm của chủ đầu tư!✔Triển khai Kế hoạch bán hàng và các chương trình bán hàng thuộc Dự án của CĐT✔ Tư vấn qua điện thoại hoặc trực tiếp tại công ty.✔ Làm việc tại văn phòng, được hổ trợ miễn phí và đào tạo bài bản chuyên nghiệp.✔ Cung cấp, tư vấn đầy đủ các thông tin về sản phẩm. Đàm phán, thương lượng và chốt hợp đồng với khách hàng.✔ Tất cả điều được hổ trợ từ bộ phận Quản lý, đồng nghiệp, bộ phận hổ trợ kinh doanh.✔ Chi tiết sẽ được trao đổi cụ thể khi phỏng vấn.', N'- Ngoại hình ưa Nhìn- Đam mê với công việc và có chí tiến thủ- Có mục tiêu rõ ràng. và chuyên nghiệp- Trình độ: trung cấp trở lên- Kinh nghiệm 1 năm trở lên', NULL, 15000000.0000, 20000000.0000, 0, CAST(N'2020-07-08' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5034, 31, 2, 1, N'ntd17@gmail.com', N'0928553364', N'Mr Long', 0, NULL, N'Đã duyệt', 1, 985, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2884, N'Kế Toán Tổng Hợp', N'Nhân viên', N'Nhân viên', 1, N'- Kiểm tra, đối chiếu số liệu (Ngân hàng, công nợ…).- Làm bảng lương và các báo cáo thống kê.- Hàng kỳ lập các tờ khai thuế, báo cáo thuế theo quy định. Hạch toán các nghiệp vụ kinh tế phát sinh;- Quản lý và lưu trữ hồ sơ tài liệu kế toán, Tập hợp các hóa đơn, chứng từ kế toán, kiểm tra tính hợp lý - hợp pháp của chứng từ kế toán;- Lập và nộp BCTC năm, BCTC nội bộ phục vụ yêu cầu quản lý của lãnh đạo ;- Tư vấn về các vấn đề liên quan về thuế- Lên quyết toán thuế thu nhập doanh nghiệp, thu nhập cá nhân;- Thực hiện các công việc khác do cấp trên giao.', N'- Nữ từ 25-32 tuổi, tốt nghiệp ĐH chuyên ngành tài chính – kế toán- Tối thiểu 3 năm kinh nghiệm làm kế toán trong công ty đầu tư, xây dựng, bất động sản.- Cẩn thận, trung thực, tỉ mỉ có tinh thần cầu tiến, chịu khó và có khả năng chịu được áp lực trong môi trường công việc;- Kỹ năng sử dụng máy tính tốt;- Kỹ năng giao tiếp, đàm phán, thuyết phục;- Khả năng làm việc độc lập và phối hợp nhóm;- Tinh thần trách nhiệm cao.- Nắm vững các quy định về thuế.', NULL, 8000000.0000, 12000000.0000, 0, CAST(N'2020-06-19' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5034, 31, 2, 0, N'ntd17@gmail.com', N'0995980061', N'Ms Ninh', 0, NULL, N'Đã duyệt', 1, 513, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2885, N'Giám Định Viên Hàng Hải', N'Nhân viên', N'Nhân viên', 5, N'Kiểm tra, điều tra và đánh giá mức độ tổn thất, đưa ra phương án khắc phục sự cố của tàu biển, tàu cá, tàu sông.', N'- Nhanh nhẹn.- giao tiếp tốt.- Thật thà.- Hòa đồng với mọi người', NULL, 7000000.0000, 10000000.0000, 0, CAST(N'2020-06-19' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5034, 31, 2, 0, N'ntd17@gmail.com', N'0399951359', N'Mr Hoàng Anh', 0, NULL, N'Đã duyệt', 1, 950, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2886, N'Nhân Viên Kế Toán Bán Hàng', N'Nhân viên', N'Nhân viên', 1, N'- Lập hóa đơn bán hàng.- Nhập phần mềm kế toán bán hàng (phần dữ liệu hàng hóa xuất bán cho khách, hàng khách trả, đổi lại nếu có..).- Theo dõi công nợ của khách hàng, đảm bảo số tiền công nợ là chính xác.- Quản lý sổ sách chứng từ kế toán.- Một số công việc hỗ trợ bán hàng cho nhân viên kinh daonh như: lập hợp đồng mua bán, danh mục chào hàng,...- Quản lý thu chi và hạch toán.', N'- Đã tốt nghiệp chuyên ngành Kế toán hoặc Sinh viên năm cuối chuyên ngành kế toán đã hoàn thành báo cáo thực tập- Chưa có kinh nghiệm sẽ được hướng dẫn, đào tạo.- Sử dụng thành thạo vi tính văn phòng: Word, Excel.- Có phương triện di chuyển cá nhân.- Làm việc chủ động, siêng năng, kiên trì.', NULL, 5000000.0000, 7000000.0000, 0, CAST(N'2020-06-22' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5034, 31, 2, 0, N'ntd17@gmail.com', N'0812775035', N'Ms Thy', 0, NULL, N'Đã duyệt', 1, 343, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2887, N'Kỹ Sư Xây Dựng - Bà Rịa Vũng Tàu', N'Nhân viên', N'Nhân viên', 5, N'Làm việc theo sự phân công của Tư Vấn Trưởng và Giám Đốc Dự Án- Tham gia kiểm tra bản vẽ thiết kế và bản vẽ shopdrawing.- Kiểm tra, giám sát và nghiệm thu vật liệu đầu vào; kỹ thuật thi công của các nhà thầu- Tham gia kiểm tra đánh giá khối lượng thanh toán cho các nhà thầu.- Tham gia kiểm tra hồ sơ chất lượng công trình và bản vẽ hoàn công theo quy định.- Giám sát và nhắc nhở tiến độ thi công của các nhà thầu.- Đề xuất phương án giải quyết vướng mắc về kỹ thuật tại công trường.- Báo cáo kịp thời các vấn đề vướng mắc, các sự cố liên quan đến thiết kế, liên quan đến công trường ngay khi phát hiện được cho Tư Vấn Trưởng và Giám Đốc Dự Án - Các công việc khác theo sự phân công', N'- Làm việc tại công trình Xuyên Mộc (tỉnh Bà Rịa Vũng Tàu)- TN Đại học trở lên chuyên ngành xây dựng dân dụng & CN- Hiểu biết về luật xây dựng và các thông tư, nghị định mới liên quan đến ngành xây dựng; Hiểu biết các kiến thức về giám sát và quản lý dự án.- Kỹ năng lập kế hoạch và tổ chức thực hiện;- Tối thiểu 03 năm kinh nghiệm.- Ưu tiên Có Chứng chỉ hành nghề giám sát- Chịu được áp lực công việc- Chịu khó, học hỏi, cầu tiến- Sử dụng thành thạo các phần mềm chuyên môn', NULL, 15000000.0000, 20000000.0000, 0, CAST(N'2020-07-17' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5034, 31, 2, 3, N'ntd17@gmail.com', N'0289016519', N'Ms.Oanh', 0, NULL, N'Đã duyệt', 1, 1004, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2888, N'Nhân Viên Kinh Doanh Lương 15 - 20 Triệu', N'Nhân viên', N'Nhân viên', 20, N'- Quản lý, lưu trữ thông tin các Dự án bất động sản mà Công ty phân phối- Chăm sóc khách hàng, thực hiện các thủ tục liên quan đến mua bán bất động sản- Khai thác nhu cầu, nắm bắt nhu cầu của khách hàng và định hướng nhu cầu của khách hàng vào các sản phẩm của công ty nhằm thực hiện hiện hóa việc ký kết hợp đồng dịch vụ.- Tiếp cận và thiết lập mối quan hệ với các đối tác tiềm năng nhằm mở rộng hệ thống cung cấp khách hàng mới.- Thường xuyên cập nhật thông tin thị trường BĐS, nhanh chóng nắm bắt nhu cầu của khách hàng.', N'- Nam - Nữ / Tuổi từ 18-32.- Tốt nghiệp THPT trở lên.- Có khả năng giao tiếp tốt, đàm phán khách hàng tốt.- Tự tin, nhanh nhẹn,vui vẻ, hòa đồng, làm việc được trong môi trường áp lực cao.- Các ứng viên chưa có kinh nghiệm sẽ được đào tạo chuyên nghiệp- Ưu tiên ứng viên đam mê kinh doanh, yêu thích bán hàng.- Có xe máy, Smatphone hoặc Laptop', NULL, 10000000.0000, 12000000.0000, 0, CAST(N'2020-06-25' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5034, 31, 2, 0, N'ntd17@gmail.com', N'0549099895', N'Chế Thị Xa Duyên', 0, NULL, N'Đã duyệt', 1, 177, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2889, N'Nhân Viên Tư Vấn - Hướng Dẫn Khách Hàng', N'Nhân viên', N'Nhân viên', 10, N'- Gọi điện thoại hoặc chat hỗ trợ khách hàng- Chăm sóc, hướng dẫn, tư vấn khách hàng mở tài khoản, nạp, rút tiền, xử lý phát sinh theo yêu cầu của khách hàng trên sàn giao dịch của công ty (có kịch bản hướng dẫn, có data khách hàng sẵn).- Làm việc bằng phần mềm MT4 (phần mềm giao dịch tài chính).- Làm việc theo đội nhóm, xây dựng hỗ trợ đội nhóm.- Cơ hội được phát huy tối đa năng lực bản thân, thăng tiến lên các vị trí cấp cao.', N'- Đã có kinh nghiệm về sale, telesale là một lợi thế Yêu cầu khác:- Nhanh nhẹn, chăm chỉ.- Có laptop cá nhân.', NULL, 10000000.0000, 15000000.0000, 0, CAST(N'2020-06-29' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5034, 31, 2, 0, N'ntd17@gmail.com', N'0350853279', N'Ms Hương', 0, NULL, N'Đã duyệt', 1, 437, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2890, N'Nhân Viên Thiết Kế - Mỹ Thuật In Ấn', N'Nhân viên', N'Nhân viên', 5, N'+ Thiết kế theo tiến độ đơn đặt hàng+ Thiết kế: Tem nhãn mác, tờ rơi, kẹp file, catalo, phong bì, catalogue,biểu mẫu, hộp giấy, túi xách...', N'- Có tư duy sáng tạo, linh hoạt trong thiết kế,- Năng động, nhạy bén trong công việc- Chịu được áp lực cao trong công việc.- Có kỹ năng thiết kế đồ họa, sử dụng các phần mềm thiết kế hiện hành thành thạo các phần mềm illustrator, photoshop, coreldraw...', NULL, 7000000.0000, 10000000.0000, 0, CAST(N'2020-07-05' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5034, 31, 2, 0, N'ntd17@gmail.com', N'0115371000', N'Mr Liêm', 0, NULL, N'Đã duyệt', 1, 122, 0)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2891, N'Nhân Viên Quản Lý Chăm Sóc Đại Lý', N'Nhân viên', N'Nhân viên', 10, N'- Quản lý khách hàng hiện tại và khai thác thêm khách hàng thuộc khu vực được phân công.- Triển khai kế hoạch kinh doanh, duy trì phát triển khách hàng cũ, tìm kiếm khách hàng mới, tham gia chào hàng và xúc tiến ký kết hợp đồng bán hàng.- Nghiên cứu và thu thập thông tin về thị trường và nhu cầu khách hàng. Tạo dựng các mối quan hệ với các cửa hàng phân phối sàn gỗ, công ty tư vấn thiết kế tiểu cảnh, các công trình xây dựng cao cấp,...- Thiết lập lộ trình, kế hoạch tiếp xúc khách hàng theo tuần, tháng. Hỗ trợ, chăm sóc hệ thống các khách hàng được thiết lập.- Theo dõi doanh thu, đánh giá các sản phẩm bán chạy, tiếp nhận yêu cầu từ khách hàng để kịp thời phản ánh về công ty và đưa ra các giải pháp thích hợp.- Đôn đốc quá trình bán hàng và thực hiện thu hồi công nợ khách hàng.', N'- Yêu cầu trình độ Trung cấp trở lên.- Ưu tiên ứng viên có kinh nghiệm tư vấn và bán sản phẩm sàn gỗ công nghiệp- Có khả năng nắm bắt công việc nhanh- Nhiệt tình, trung thực, nhanh nhẹn, xử lý tình huống linh hoạt, chủ động; Khả năng truyền đạt, giao tiếp, xây dựng mối quan hệ tốt.', NULL, 12000000.0000, 25000000.0000, 0, CAST(N'2020-07-06' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5035, 32, 2, 0, N'ntd18@gmail.com', N'0248074189', N'Ms. Hải Ly', 0, NULL, N'Đã duyệt', 1, 226, 0)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2892, N'Trưởng Nhóm Telesale - Lĩnh Vực Mỹ Phẩm', N'Trưởng nhóm', N'Quản lý', 2, N'• Liên hệ với khách hàng theo data được cung cấp để tư vấn, thuyết phục khách hàng sử dụng sản phẩm của công ty. Báo giá và tư vấn các sản phẩm phù hợp với từng khách hàng.• Xây dựng kịch bản Telesales, kịch bản bán hàng: bán chéo, bán thêm, đào tạo và phát triển đội nhóm telesales.• Đảm bảo doanh số kinh doanh do phòng đưa ra.• Hỗ trợ cùng Leader Marketing đưa ra chính sách và chương trình khuyến mãi và chính sách bán để thúc đẩy doanh thu.• Chăm sóc khách hàng trong quá trình sử dụng.• Quản lý nhóm sales.', N'• Năng lực bán hàng tốt, năng lực đào tạo nhân sự và quản lý đội nhóm.• Yêu cầu có kinh nghiệm trên 2 năm ở vị trí tương đương.• Có giọng nói rõ ràng, dễ nghe.• Kỹ năng xử lí tình huống tốt.• Dám nghĩ, dám làm, đưa ra đề xuất để cải thiện tình hình phát triển của đội ngũ bán hàng.• Ưu tiên: Đã từng làm các sản phẩm mỹ phẩm, làm đẹp', NULL, 20000000.0000, 30000000.0000, 0, CAST(N'2020-07-13' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5035, 32, 2, 2, N'ntd18@gmail.com', N'0162006566', N'Phó Anh', 0, NULL, N'Đã duyệt', 1, 179, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2893, N'Nhân Viên Kinh Doanh Mảng Cơ Khí', N'Nhân viên', N'Nhân viên', 5, N'- Tìm hiểu và mở rộng thị trường kinh doanh của công ty- Chăm sóc khách hàng cũ và mở rộng khách hàng mới- Trao đổi chi tiết công việc trong quá trình phỏng vấn.', N'- Yêu cầu trình độ Trung cấp trở lên.- Ưu tiên ứng viên có kinh nghiệm bán và tư vấn ít nhất 1 năm.- Có khả năng nắm bắt công việc nhanh, làm việc với áp lực cao- Nhiệt tình, trung thực, nhanh nhẹn, xử lý tình huống linh hoạt, chủ động; Khả năng truyền đạt, giao tiếp, xây dựng mối quan hệ tốt.', NULL, 10000000.0000, 20000000.0000, 0, CAST(N'2020-06-24' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5035, 32, 2, 0, N'ntd18@gmail.com', N'0823074209', N'Mr Hình', 0, NULL, N'Đã duyệt', 1, 848, 0)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2894, N'Nhân Viên Designer', N'Nhân viên', N'Nhân viên', 3, N'- Thiết kế các ấn phẩm PR, truyền thông Marketing: bộ nhận diện thương hiệu, voucher, ấn phẩm phục vụ sự kiện (backdrop, standee..)…- Phối hợp với bộ phận IT hoàn thiện thiết kế hệ thống website, app của công ty - Thiết kế các ấn phẩm trên kênh quảng cáo online, digital marketing: website, social media, fanpage, youtube… - Thiết kế hoặc outsource thiết kế mẫu mã, bao bì sản phẩm của công ty.', N'- Thành Thạo Illustrator , Photoshop, các phần mềm đồ họa phục vụ thiết kế.- Hiểu phong cách mỹ thuật,có khả năng lên concept ý tưởng- Ưu tiên ứng viên có thêm khả năng chỉnh sửa video- Luôn luôn trau dồi kiến thức phục vụ cho phát triển bản thân và công việc- Có trách nhiệm với công việc, có tinh thần học hỏi và tiếp thu ý kiến, năng động và chăm chỉ, gắn bó lâu dài với công ty', NULL, 10000000.0000, 15000000.0000, 0, CAST(N'2020-07-15' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5035, 32, 2, 0, N'ntd18@gmail.com', N'0705046619', N'Ms. Hường', 0, NULL, N'Đã duyệt', 1, 10, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2895, N'Kỹ Sư Điện Tự Động Hoá Tại Hà Nội', N'Nhân viên', N'Nhân viên', 2, N'- Làm việc trong môi trường chuyên nghiệp, phù hợp với các nghề: Điện, Tự động hóa, - Nghiên cứu, khảo sát, thiết kế, tư vấn, lắp đặt thiết bị điện, tự động hóa cho các nhà máy xí nghiệp.- Được hướng dẫn, hỗ trợ đào tạo trong và ngoài nước từ các chuyên gia của Công ty và các hãng sản xuất.', N'- Tốt nghiệp Cao đẳng, Đại Học chuyên ngành: Tự động hóa,  - Chịu khó, Cẩn thận, trung thực, ham học hỏi, linh hoạt sáng tạo trong công việc, tâm huyết gắn bó lâu dài với Công ty.- Có thể đi công tác theo yêu cầu của công việc Ưu tiên:  - Người có kinh nghiệm làm việc trong môi trường điện tự động hóa từ 2-3 năm trở lên. Tiếng anh giao tiếp thành thạo.', NULL, 8000000.0000, 12000000.0000, 0, CAST(N'2020-06-24' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5035, 32, 2, 0, N'ntd18@gmail.com', N'0595845747', N'Ms Bình', 0, NULL, N'Đã duyệt', 1, 776, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2896, N'Nhân Viên Kho', N'Nhân viên', N'Nhân viên', 1, N'- Sắp xếp bố trí nhà kho đảm bảo xuất nhập tồn, quản lý hàng hóa có trật tự và khoa học- Thông báo kịp thời cho các bộ phận liên quan về tình trạng hàng hóa tồn kho- Định kỳ kiểm kê kho theo quy định của công ty- Báo cáo tình hình kho hàng tuần, hàng tháng cho Giám Đốc- Đảm bảo an toàn phòng chống cháy nổ tại kho', N'Đại họcTiếng trung  hoặc tiếng Anh 4 kỹ năng,Có khái niệm về kho, biết sử dụng máy tính', NULL, 7000000.0000, 10000000.0000, 0, CAST(N'2020-06-27' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5035, 32, 2, 2, N'ntd18@gmail.com', N'0801707091', N'Ms Vân', 0, NULL, N'Đã duyệt', 1, 43, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2897, N'Nhân Viên Kinh Doanh Tại Vũ Tông Phan Thu Nhập Trên 12 Triệu / Tháng', N'Nhân viên', N'Nhân viên', 10, N'- Kinh doanh phân phối, dự án ngành gỗ nội thất, ván sàn, vật liệu hoàn thiện- Quản lý khách hàng hiện tại và khai thác thêm khách hàng thuộc khu vực được phân công.- Triển khai kế hoạch kinh doanh, duy trì phát triển khách hàng cũ, tìm kiếm khách hàng mới, tham gia chào hàng và xúc tiến ký kết hợp đồng bán hàng.- Nghiên cứu và thu thập thông tin về thị trường và nhu cầu khách hàng. Tạo dựng các mối quan hệ với các cửa hàng phân phối sàn gỗ, công ty tư vấn thiết kế tiểu cảnh, các công trình xây dựng cao cấp,...- Thiết lập lộ trình, kế hoạch tiếp xúc khách hàng theo tuần, tháng. Hỗ trợ, chăm sóc hệ thống các khách hàng được thiết lập.- Theo dõi doanh thu, đánh giá các sản phẩm bán chạy, tiếp nhận yêu cầu từ khách hàng để kịp thời phản ánh về công ty và đưa ra các giải pháp thích hợp.- Đôn đốc quá trình bán hàng và thực hiện thu hồi công nợ khách hàng.- Các công việc khác theo sự phân công của giám đốc', N'- Yêu cầu trình độ Trung cấp trở lên- Ưu tiên ứng viên có kinh nghiệm tư vấn và bán sản phẩm gỗ công nghiệp- Có khả năng nắm bắt công việc nhanh- Nhiệt tình, trung thực, nhanh nhẹn, xử lý tình huống linh hoạt, chủ động; Khả năng truyền đạt, giao tiếp, xây dựng mối quan hệ tốt', NULL, 12000000.0000, 25000000.0000, 0, CAST(N'2020-06-20' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5035, 32, 2, 0, N'ntd18@gmail.com', N'0786776801', N'Ms Trang', 0, NULL, N'Đã duyệt', 1, 14, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2898, N'Nhân Viên Kỹ Thuật Chuyên Về Máy CNC', N'Quản lý', N'Quản lý', 3, N'- Sửa chữa bảo dưỡng máy CNC - Thay thế trục Vít ME và Thanh ray dẫn hướng trong máy CNC- Gia công chi tiết, Có kiến thức về cơ khí chế tạo.- Đọc hiểu và thực hiện các bảng vẽ gia công- Trao đổi trực tiếp công việc khi phỏng vấn.', N'- Tốt nghiệp: Trung cấp, Cao đẳng, Đại học chuyên ngành cơ khí, điện, điện tự động hóa- Có sức khỏe tốt ,trung thực,có tinh thần trách nhiệm- Chăm chỉ ,chịu khó,ham học hỏi.- Ưu tiên các ứng viên ở gần và có kinh nghiệm lập trình,vận hành máy CNC', NULL, 10000000.0000, 14000000.0000, 0, CAST(N'2020-06-29' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5035, 32, 2, 2, N'ntd18@gmail.com', N'0308914715', N'Mr Quang / Ms Dung', 0, NULL, N'Đã duyệt', 1, 13, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2899, N'Nhân Viên Bóc Tách Khối Lượng (Nam)', N'Nhân viên', N'Nhân viên', 2, N'Trực tiếp bóc tách khối lượng cho các công trình dân dụng;Kiểm tra, xác nhận khối lượng thanh quyết toán;Có kinh nghiệm về bóc tách khối lượng và lập dự toán…', N'Tốt nghiệp Đại học có liên quan đến xây dựng, Kinh nghiệm 2-5 nămThành thạo sử dụng các phần mềm Autocad, Revit,...Ưu tiên nhanh nhẹn, giao tiếp với khách hàng tốtNhiệt tình, hoà đồng với đông nghiệp', NULL, 8000000.0000, 12000000.0000, 0, CAST(N'2020-06-28' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5035, 32, 2, 2, N'ntd18@gmail.com', N'0898280134', N'Ms Vân', 0, NULL, N'Đã duyệt', 1, 848, 0)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2900, N'Nhân Viên Kinh Doanh  (Làm Tại Thủ Đức_Hcm)', N'Nhân viên', N'Nhân viên', 10, N'•Đăng tin sản phẩm lên các website và mạng XH ( được hướng dẫn – đào tạo )•Tư vấn thông tin cho khách hàng qua điện thoại và  MXH online (làm việc tại văn phòng , chủ yếu làm online )•Hỗ trợ và Chăm sóc KH•Không áp Doanh số', N'•Bằng tốt nghiệp Trung Cấp trở lên•Không Cần Kinh nghiệm , sẽ được đào tạo từ cơ bản từ A-Z•Nếu chỉ Tốt Nghiệp THPT (Yêu cầu Có Kinh nghiệm )•Năng động , học hỏi , hòa đồng, Có chí cầu tiến•Thời gian làm việc 8 Tiếng giờ hành chính : từ 8h - 17h30 - Nghỉ trưa từ 12h-13h30•Làm việc từ Thứ 2 đến Thứ 6 , T7 , CN Nghỉ', NULL, 7000000.0000, 10000000.0000, 0, CAST(N'2020-07-14' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5035, 32, 2, 0, N'ntd18@gmail.com', N'0950896681', N'Anh Trung', 0, NULL, N'Đã duyệt', 1, 776, 0)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2901, N'Kỹ Sư Môi Trường Chuyên Bảo Trì Vận Hành Hệ Thống Xử Lý Nước Thải', N'Nhân viên', N'Nhân viên', 4, N'- Có khả năng đọc bản vẽ, Triển khai thi công lắp đặt đường ống, đấu nối tủ điện cho hệ thống xử lý nước thải, rác thải y tế, sinh hoạt theo bản vẽ ....- Lập kế hoạch công tác, báo cáo công tác bảo hành, bảo trì hệ thống xử lý nước thải, rác thải ....- Hỗ trợ các công việc khác về công tác hồ sơ nếu cần .....', N'- Tốt nghiệp đại học chuyên Công nghệ môi trường, Kỹ thuật môi trường, có kinh nghiệm làm việc từ 3 -5 năm- Chăm chỉ, trung thực, chịu được áp lực công việc cao- Có kỹ năng giao tiếp tốt, phối hợp công việc với các phòng ban- Chấp nhận đi công tác xa- Nắm rõ công nghệ MBBR, công nghệ AAO và 1 số công nghệ XLNT đang được sử dụng rộng rãi- Ưu tiên bạn Nam- Có khả năng xử lý hiện trường, xử lý các sự cố khi hệ thống xử lý nước thải và rác thải gặp phải- Bảo trì – bảo dưỡng toàn bộ hệ thống xử lý nước thải và hệ thống xử lý rác thải thải- Báo cáo công việc cho cấp trên', NULL, 10000000.0000, 15000000.0000, 0, CAST(N'2020-07-01' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5036, 33, 2, 0, N'ntd19@gmail.com', N'0660468321', N'P.Hcns', 0, NULL, N'Đã duyệt', 1, 321, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2902, N'hot Kỹ Thuật Hiện Trường', N'Nhân viên', N'Nhân viên', 4, N'-Xử lý kỹ thuật hiện trường;-Lập biện pháp tổ chức thi công và tiến độ thi công ngoài hiện trường;-Quản lý tiến độ và chất lượng thi công củɑ công trường;-Đọc hiểu bản vẽ, bóc tách khối lượng, lên kế hoạch vật tư.-Làm việc tại thành phố Hà Nội (nội thành và các huyện ngoại thành)', N'-Ưu tiên người có hộ khẩu Phú Xuyên.-Có kinh nghiệm ít nhất 02 năm làm các công việc tương tự. -Có sức khỏe đảm bảo đáp ứng công việc.-Đi phỏng vấn mang theo một bộ hồ sơ xin việc', NULL, 10000000.0000, 15000000.0000, 0, CAST(N'2020-07-06' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5036, 33, 2, 2, N'ntd19@gmail.com', N'0154765598', N'Mr. Tuyến', 0, NULL, N'Đã duyệt', 1, 861, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2903, N'Lái Xe 16 Chỗ Bằng D Mức Lương 8- 10 Triệu', N'Nhân viên', N'Khác', 1, N'- Lái xe 16 chỗ chở cán bộ CNV. - Buổi sáng lái xe từ Trần Đăng Ninh đón nhân viên chở về KCN Quang Minh.', N'- Có kinh nghiệm- Thật thà, nhanh nhẹn, chăm chỉ có khả năng gắn bó công việc lâu dài- Linh hoạt trong công việc, luôn sẵn sàng tiếp nhận chuyến xe đột xuất- Có khả năng làm việc dưới áp lực cao.', NULL, 8000000.0000, 10000000.0000, 0, CAST(N'2020-06-28' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5036, 33, 2, 0, N'ntd19@gmail.com', N'0601732006', N'Mr. Đạt', 0, NULL, N'Đã duyệt', 1, 65, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2904, N'Nhân Viên Vận Hành Máy CNC', N'Nhân viên', N'Nhân viên', 5, N'Vận hành máy phay CNC, gá sản phẩm chạy hàng trên máy   Vận hành máy tiện, máy mài   Làm việc từ thứ 2 đến thứ 7 theo giờ hành chính ( sáng từ 8h đến 12h,  chiều 1h đến 5h)   Công việc trao đổi cụ thể khi phỏng vấn   Ưu tiên ứng viên có kinh nghiệm trong lĩnh vực cơ khí.', N'Tốt nghiệp trung học trở lên   Biết sử dụng máy móc theo chuyên môn  Có kĩ năng, kiến thức, kinh nghiệm trong lĩnh vực cơ khí  Chăm chỉ, trung thực, nhiệt tình, yêu nghề', NULL, 5000000.0000, 9000000.0000, 0, CAST(N'2020-07-06' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5036, 33, 1, 0, N'ntd19@gmail.com', N'0128724719', N'Nhân Sự : Mr. Thông', 0, NULL, N'Đã duyệt', 1, 377, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2905, N'Giám Sát Công Trình (Kỹ Sư Cơ Điện Lạnh)', N'Quản lý', N'Quản lý', 10, N'- Khảo sát, thiết kế, bóc tách khối lượng, triển khai bản vẽ;- Hướng dẫn nhân công sản xuất, chạy máy- Lập dự toán và lập hồ sơ đấu thầu hệ thống lạnh, hệ thống điều hòa không khí;- Triển khai bản vẽ, theo dõi, giám sát, kiểm tra chất lượng thi công dự án;- Lập hồ sơ nghiệm thu, bản vẽ hoàn công, làm hồ sơ quyết toán;', N'- Tốt nghiệp chuyên ngành: Nhiệt/Điện lạnh/Cơ khí, HVAC hoặc chuyên ngành cơ điện M&E- Ưu tiên có kinh nghiệm làm việc lĩnh vực chuyên môn hoặc vị trí tương đương, ưu tiên kinh nghiệm thiết kế hệ thống M&E- Có hiểu biết hoặc kinh nghiệm về một trong các hệ thống cơ bản sau:+ Hệ thống điều hòa thông gió.+ Hệ thống cứu hỏa.+ Hệ thống khí nén.- Thành thạo AutoCAD', NULL, 10000000.0000, 12000000.0000, 0, CAST(N'2020-07-15' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5036, 33, 2, 0, N'ntd19@gmail.com', N'0652322921', N'Anh Năm', 0, NULL, N'Đã duyệt', 1, 254, 0)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2906, N'Nhân Viên Phòng Kinh Doanh Xuất Nhập Khẩu', N'Nhân viên', N'Nhân viên', 2, N'- Chuẩn bị và kê khai hàng hóa xuất khẩu- Tới làm việc với các ban ngành liên quan để hoàn thiện hồ sơ xuất khẩu.- Phối hợp cùng bộ phận kinh doanh trong việc chuẩn bị hàng hóa nguyên phụ liệu- Thực hiện các nhiệm vụ khác theo yêu cầu công việc và theo phân công của công tyĐịa điểm làm: Số 2 Tôn Thất Thuyết, Phường Mỹ Đình, Quận Nam Từ Liêm', N'- Giới tính: Nam- Trình độ học vấn: Cao đẳng trở lên các ngành Kinh doanh; Xuất nhập khẩu ; Kinh doanh thương mại; Kinh doanh quốc tế; Quản trị kinh doanh; Kế toán – Tài chính; Luật kinh tế và các ngành nghề khác có liên quan- Kinh nghiệm: Không yêu cầu- Độ tuổi từ 21- 30 tuổi- Chủ động và thích nghi nhanh với các yêu cầu công việc, có khả năng làm việc độc lập cũng như làm việc nhóm, sẵn sàng làm overtime vào những thời điểm gấp.- Chịu được áp lực công việc, trung thực có tinh thần trách nhiệm cao- Kỹ năng máy tính văn phòng word, excel- Ưu tiên kỹ năng làm đồ họa như corel, photoshop, illustrator- Ưu tiên am hiểu về IT để hỗ trợ các việc liên quan tới IT trong văn phòng- Biết tiếng Anh là một lợi thế', NULL, 7000000.0000, 14000000.0000, 0, CAST(N'2020-07-14' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5036, 33, 2, 0, N'ntd19@gmail.com', N'0853723147', N'Mr. Hiệp', 0, NULL, N'Đã duyệt', 1, 555, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2907, N'Chuyên Viên Tư Vấn Tài Chính (12-30Tr / Tháng)', N'Nhân viên', N'Nhân viên', 10, N'• Đại diện Công ty TNHH Manulife Việt Nam làm việc tại Hà Nội.• Độc quyền tiếp cận, khai thác nguồn khách hàng tiềm năng, có nhu cầu tham gia bảo hiểm nhân thọ do Công ty Manulife cung cấp và tư vấn về các gói sản phẩm bảo hiểm của Manulife cho khách hàng.• Thu thập thông tin khách hàng, hướng dẫn khách hàng chuẩn bị và nộp hồ sơ bảo hiểm của khách hàng về bộ phận thẩm định• Chăm sóc, hỗ trợ khách hàng chu đáo• Tham gia các buổi lập kế hoạch kinh doanh và báo cáo kết quả làm việc trực tiếp với quản lí.• Được đào tạo miễn phí các khóa học không yêu cầu kỹ năng nâng cao do Công ty Manulife tổ chức.• Thời gian làm việc: ( Partime / fulltime).', N'- Nam / Nữ tốt nghiệp THPT trở lên.- Tuổi từ 23 tuổi trở lên- Có đầy đủ hồ sơ cá nhân.- Tham gia đầy đủ các khóa đào tạo.- Ưu tiên các ứng viên có kinh nghiệm về lĩnh vựa CSKH, Bảo Hiểm, Bất Động Sản…', NULL, 15000000.0000, 20000000.0000, 0, CAST(N'2020-07-11' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5036, 33, 2, 0, N'ntd19@gmail.com', N'0453122105', N'Chị Hoa', 0, NULL, N'Đã duyệt', 1, 926, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2908, N'Kỹ Thuật Viên Cơ Khí', N'Quản lý', N'Quản lý', 5, N'- Công việc liên quan trong việc chế tạo máy, các kim loại tấm, máy cán tôn, máy cán kim loại tấm, máy tạo hình kim loại.- Trao đổi thêm khi phỏng vấn', N'- Nam từ 20-35 tuổi- Sức khỏe tốt- Tốt nghiệp Trung cấp trở lên', NULL, 7000000.0000, 12000000.0000, 0, CAST(N'2020-07-15' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5036, 33, 2, 0, N'ntd19@gmail.com', N'0146442971', N'Mr Bảo', 0, NULL, N'Chưa duyệt', 1, 440, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2909, N'Nhân Viên Kinh Doanh- Lương Cứng + Thưởng+ Hoa Hồng', N'Nhân viên', N'Nhân viên', 3, N'• Quản lý các khách hàng thường xuyên của công ty• Quản lý chuỗi bán hàng từ khi tiếp nhận yêu cầu báo giá cho đến khi kết thúc HĐ• Tìm kiếm mở rộng mạng lưới khách hàng mới', N'Kinh nghiệm: 3 năm trở lênTrình độ học vấn: Đại học trở lênChuyên môn: Kỹ thuật, Kinh tế, quản trị kinh doanh', NULL, 10000000.0000, 15000000.0000, 0, CAST(N'2020-07-10' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5036, 33, 2, 3, N'ntd19@gmail.com', N'0320458078', N'Ms Hà', 0, NULL, N'Đã duyệt', 1, 461, 0)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2910, N'Nhân Viên Phát Triển Thị Trường', N'Nhân viên', N'Nhân viên', 30, N'- Trực tiếp giới thiệu, chào bán & tư vấn sản phẩm tới các khách hàng;- Thu thập thông tin về khách hàng và nhu cầu của khách hàng;- Tìm kiếm và mở rộng quan hệ khách hàng, thực hiện chăm sóc, phát triển khách hàng;- Thực hiện các công việc khác theo yêu cầu của Ban Giám đốc/Trưởng phòng/cán bộ quản lý trực tiếp.', N'- Tốt nghiệp cao đẳng/đại học chuyên ngành ngân hàng, tài chính, bảo hiểm, lao động, có nhu cầu cộng tác lâu dài với Công ty;- Nhanh nhẹn, nhiệt tình, khả năng giao tiếp tốt; ứng viên có kỹ năng tổ chức nhóm tốt là 1 lợi thế;- Chấp nhận ứng viên mới ra trường, chưa có kinh nghiệm, sẽ được đào tạo bài bản từ đầu- Lý lịch rõ ràng.', NULL, 12000000.0000, 15000000.0000, 0, CAST(N'2020-06-27' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5036, 33, 2, 0, N'ntd19@gmail.com', N'0115733172', N'Ms. Phương', 0, NULL, N'Đã duyệt', 1, 177, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2911, N'Nhân Viên Telesales', N'Nhân viên', N'Nhân viên', 10, N'- Gọi điện tư vấn, chốt sale, bán các sản phẩm của Công ty theo data và kịch bản có sẵn.- Ngày làm việc: từ Thứ 2 đến thứ 7 (nghỉ Chủ Nhật, ngày lễ, Tết theo quy định nhà nước)+ Sáng: 8h00 – 12h00+ Chiều: 13h30 – 17h30- Có thể làm tăng giờ theo yêu cầu của công ty (sẽ được tính lương làm ngoài giờ theo quy định nhà nước)', N'- Thu nhập: Lương cứng: 7.000.000đ/tháng + 1tr Lương phụ trách nhiệm + Lương Kinh doanh + Thưởng.- Đóng BHXH, BHYT, BHTN sau khi ký hợp đồng chính thức- Thử việc: 60 ngày (Lương thử việc = 85% Lương cứng)- Ngày thanh toán lương: Ngày 15 của tháng làm việc kế tiếp gần nhất', NULL, 7000000.0000, 10000000.0000, 0, CAST(N'2020-07-16' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5037, 34, 2, 0, N'ntd20@gmail.com', N'0361269897', N'Ms Liên', 0, NULL, N'Đã duyệt', 1, 666, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2912, N'Nhân Viên Chăm Sóc Khách Hàng Tại Hà Nội', N'Nhân viên', N'Nhân viên', 5, N'- Khảo sát và phân tích đối thủ cạnh tranh và thị trường-Tham gia khảo sát thị trường KB- Đăng ký chạy chương trình sale với cơ quan quản lý+ Xây dựng và giám sát việc đảm bảo tiêu chuẩn khách hàng trên hệ thống theo các kênh+Tiêu chuẩn không gian dịch vụ+ Tiêu chuẩn dịch vụ bán hàng theo kênh+ Giám sát việc thực hiện các tiêu chuẩn trên+Xây dựng và giám sát việc thực hiện các chính sách chăm sóc khách hàng trên hệ thống+Chính sách thẻ VIP+Chương trình chăm sóc KH- Giám sát việc thực hiện các chính sách trên- Quà tặng, sự kiện theo chương trình chăm sóc khách hàng- Hoạt động truyền thông nội bộ theo định hướng- Thực hiện các công việc khác khi có yêu cầu từ Trưởng phòng và Công ty', N'- Nữ: Từ 25 đến 30 tuổi, sức khỏe tốt- Tốt nghiệp Đại Học chuyên ngành kinh tế, kinh doanh...- Tối thiểu kinh nghiệm 2 năm làm vị trí tương đương tại các công- Khả năng giao tiếp- Năng động, nhiệt tình, tận tâm với công việc- Khả năng làm việc độc lập và xử lý tình huống tốt- Sử dụng tiếng Anh và tin học văn phòng', NULL, 10000000.0000, 15000000.0000, 0, CAST(N'2020-07-08' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5037, 34, 2, 1, N'ntd20@gmail.com', N'0136593994', N'Phòng Hcns', 0, NULL, N'Đã duyệt', 1, 823, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2913, N'Kỹ Sư Môi Trường', N'Nhân viên', N'Nhân viên', 3, N'- Khảo sát hệ thống, thiết kế bản vẽ, bóc tách khối lượng vật tư, lập dự toán, ...cho công trình xử lý nước cấp, nước thải- Giám sát thi công công trình, đảm bảo tiến độ, chất lượng và hiệu quả cao- Làm hồ sơ nghiệm thu, thanh quyết toán công trình- Làm việc với Ban QLDA, TVGS, Chủ đầu tư, Chính quyền và nhân dân địa phương để công trình thi công hiệu quả', N'- Biết sử dụng Autocad 2D, 3D; đọc hiểu bản vẽ- Có tinh thần trách nhiệm cao, giải quyết vấn đề nhanh- Khả năng làm việc độc lập- Tốt nghiệp chuyên ngành môi trường , tuyển dụng vào các bộ phận: Vận hành, thicông lắp đặt, dự án.- Riêng ứng viên bộ phận dự án: Yêu cầu ứng viên có tiếng Anh chuyên ngành &giao tiếp.', NULL, 7000000.0000, 10000000.0000, 0, CAST(N'2020-06-30' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5037, 34, 2, 1, N'ntd20@gmail.com', N'0184050786', N'Ms. Nga', 0, NULL, N'Đã duyệt', 1, 637, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2914, N'[Hà Nội] Nhân Viên Bán Hàng', N'Nhân viên', N'Nhân viên', 2, N'- Tư vấn bán hàng: Mặt hàng là nhám (giấy giáp) + dịch vụ giao nhận vận chuyển- Test sản phẩm trực tiếp cho khách- Tư vấn công trình dự án, báo giá tới khách hàng- Giải thích thắc mắc của khách hàng liên quan tới sản phẩm.- Chăm sóc khách hàng- Giải quyết khiếu nại sản phẩm- Tiếp thị và gia hàng cho các đại lý của công ty- Công việc trao đổi cụ thể khi phỏng vấn', N'- Có tinh thần cầu tiến, ham học hỏi- Không yêu cầu bằng cấp và kinh nghiệm.- Có kinh nghiệm và kĩ năng giao tiếp tốt.- Ưu tiên những bạn đã và đang làm công việc sale, chăm sóc khách hàng, nhân viên kinh doanh- Ưu tiên có kiến thức, đam mê về các sản phẩm', NULL, 7000000.0000, 15000000.0000, 0, CAST(N'2020-06-24' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5037, 34, 2, 0, N'ntd20@gmail.com', N'0491473043', N'Anh Lịch', 0, NULL, N'Đã duyệt', 1, 182, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2915, N'Nhân Viên Kinh Doanh Sp Cửa Nhôm / Nhựa', N'Nhân viên', N'Nhân viên', 2, N'- Phát triển thị trường, tìm kiếm khách hàng để cung cấp các sản phẩm cửa nhôm, cửa nhựa, cửa kính- Xây dựng hệ thống khách hàng và đảm bảo doanh số kinh doanh hàng tháng.- Phát triển thị trường, thương hiệu, sản phẩm của Công ty đến các chủ đầu tư, đơn vị thi công và khách hàng trên phạm vi cả nước.- Xây dựng hệ thống khách hàng và hệ thống đại lý phân phối sản phẩm công ty.- Thời gian làm việc: 7h30 đến 5h00 từ Thứ Hai đến Thứ Bảy Địa chỉ làm việc: 99 Nguyễn Văn Tăng, Phường Thạnh Mỹ Lợi, Q9', N'- Tốt nghiệp Trung cấp trở lên các chuyên ngành Kinh tế, QTKD, xây dựng, cơ khí.- Độ tuổi: 23 - 30- Có kính nghiệm hoặc không kinh nghiệm đều được (Sẽ được training đầy đủ về sản phẩm và kỹ năng bán hàng)- Nhanh nhẹn, chịu khó, cầu tiến.', NULL, 7000000.0000, 10000000.0000, 0, CAST(N'2020-07-10' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5037, 34, 2, 0, N'ntd20@gmail.com', N'0337883884', N'Mr Kết', 0, NULL, N'Đã duyệt', 1, 904, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2916, N'Admin Dự Án Influencer Marketing', N'Quản lý', N'Quản lý', 2, N'- Quản lý/phê duyệt cộng tác viên, thành viên là influencer của hệ thống- Quản lý/phê duyệt chiến dịch của doanh nghiệp- Lập kế hoạch khai thác và phát triển kinh doanh dựa trên những influencer đăng ký hợp tác với hệ thống- Đề xuất các phương án quản trị influencer', N'- Có kinh nghiệm làm việc ở bất kỳ ngành nào tối thiểu 02 năm chính thức hoặc 01 năm trở lên trong lĩnh vực truyền thông/marketing.-- Tuổi: Từ 24 trở lên- Có máy tính xách tay- Trình độ: Cao đẳng trở lên- Tính cách: vui vẻ, dễ gần, trung thực', NULL, 7000000.0000, 12000000.0000, 0, CAST(N'2020-07-16' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5037, 34, 2, 0, N'ntd20@gmail.com', N'0456346924', N'Chị Nguyệt', 0, NULL, N'Đã duyệt', 1, 20, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2917, N'Nhân Viên Sap', N'Nhân viên', N'Nhân viên', 2, N'- Đào tạo nhân viên cách sử dụng, xử lý khi phát sinh sự cố từ hệ thống SAP/ERP- Hỗ trợ hệ thống SAP/ERP- Làm việc với tư vấn của hệ thống SAP/ERP từ đối tác cung cấp phần mềm để setup master data, giải quyết lỗi của hệ thống…- Quản lý user roll: tạo mới account, reset password, gán quyền cho user- Nhận yêu cầu: tìm lỗi và tư vấn giải pháp cho user.- Quản lí SAP KPI: kiểm tra hàng ngày, hướng dẫn và nhắc nhở người sử dụng theo dõi hệ thống một cách hiệu quả.- Hỗ trợ hệ thống SAP cho các bộ phận Thiết kế, Thu mua, Sales, Sản xuất, Kho, Logistic, …- Các công việc khác sẽ được trao đổi trong buổi phỏng vấn', N'- Tốt nghiệp Đại học, chuyên môn: software engineering (MIS- Management Information Systems), IT- Có trên 02 năm kinh nghiệm về quản trị hệ thống SAP BASIS- Có kinh nghiệm và am hiểu về hệ thống SAP/ ERP.- Sẵn sàng chia sẻ để làm việc thân thiện với End User.- Kiến thức về hệ quản trị CSDL: SQL Server.- Kiến thức về làm báo cáo bằng Crystal Report- Kỹ năng lập kế hoạch, quản lý và điều hành công việc cá nhân.- Khả năng giao tiếp, trình bày.- Có khả năng phân tích, xử lý các sự cố hệ thống SAP BASIS- Chịu được áp lực cao, sẵn sàng làm việc ngoài giờ để đảm bảo tiến độ công việc.- Khả năng làm việc độc lập và làm việc theo nhóm- Không ngại khó, sẵn sàng đi công tác- Có kinh nghiệm làm việc trong lĩnh vực thời trang là một lợi thế', NULL, 18000000.0000, 20000000.0000, 0, CAST(N'2020-06-27' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5037, 34, 2, 0, N'ntd20@gmail.com', N'0233396738', N'Ms Linh - Tp.Hcns', 0, NULL, N'Chưa duyệt', 1, 640, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2918, N'Nhân Viên Thiết Kế In Ấn Quảng Cáo - Hải Phòng', N'Nhân viên', N'Nhân viên', 1, N'- Brain-storm cùng team để đưa ra các ý tưởng sáng tạo cho nội dung chương trình theo thông tin quản lý- Thiết kế các ấn phẩm quảng cáo trên các kênh Digital Marketing (Website, Facebook,...giao diện website, video clip, tờ rơi, poster, banner,...)- Chịu trách nhiệm thiết kế, Key visual, Logo, FA... và các sản phẩm liên quan một cách sáng tạo và theo sát với nội dung, ý tưởng đã thống nhất với team. Điều chỉnh theo ý kiến và cho ra mẫu thiết kế hoàn chỉnh.- Chịu trách nhiệm làm việc với các nhà sản xuất, giám sát việc sản xuất để đảm bảo thực hiện đúng tiến độ của dự án.- Đóng góp và hỗ trợ cho các hoạt động của team và cùng tạo động lực để team hoàn thành dự án đem lại kết quả tốt nhất.- Có kinh nghiệm trong việc thiết kế digital ads, PR pages, outdoor/installation (2D & 3D).- Nẳm rõ các production & printing house, studio để theo dõi và chịu trách nhiệm về sản phẩm thiết kế.- Quản lý và lưu trữ toàn bộ dữ liệu phục vụ cho công việc thiết kế như: file thiết kế, thư viện ảnh, clipart...- Chụp hình và phối hợp cùng Marketing online để cập nhập tư liệu hình ảnh sản phẩm- Cập nhật các xu hướng thiết kế mới.', N'- Tốt nghiệp Đại học/ Cao đẳng chuyên ngành đồ họa, mỹ thuật, thiết kế công nghiệp.- Có ít nhất 3-5 năm kinh nghiệm trong lĩnh vực thiết kế quảng cáo, 3D, và có khả năng vẽ tay- Khả năng giao tiếp tốt- Có thể làm việc độc lập và làm việc nhóm, luôn có suy nghĩ tích cực và khả năng tự chủ trong công việc.- Trung thực, thành thật và giao tiếp lịch sự.', NULL, 10000000.0000, 15000000.0000, 0, CAST(N'2020-07-04' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5037, 34, 2, 3, N'ntd20@gmail.com', N'0494636595', N'Ms Ngọc Anh', 0, NULL, N'Đã duyệt', 1, 995, 0)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2919, N'Sales And Marketing Associate Job Summary', N'Nhân viên', N'Nhân viên', 1, N'We are seeking a highly skilled, adaptable, and reliable sales and marketing associate to help take our company to the next level. In this position, you will be part of a dynamic team of professionals who plan and implement our sales, marketing, and advertising activities. You should be familiar with analysis and market research, product and service promotion, and anticipation of customer behavior.Sales and Marketing Duties and Responsibilities- Manage daily administrative tasks- Conduct market research and identify new opportunities.- Meet Clients at managerial level  - Present, promote and sell products /services using solid arguments- Gather and analyze consumer behavior data (e.g. web traffic and rankings)- Generate reports on marketing and sales metrics - Some understanding with BRM and CRM - Contribute to collaborative efforts and organize promotional events- Coordinate with the marketing design and content teams to generate digital and print advertising material- Maintain and update impeccable records of marketing metrics and results of past campaigns- Prepare and deliver regular sales forecasting reports- Monitor and report competitors’ marketing and sales activities', N'- Attractive personal presentation - BS degree in marketing or relevant field- 1- 2 years experience as a marketing associate, marketing assistant, or similar position- Strong working knowledge of marketing and sales industry, including digital tools and techniques- Significant experience with SEO/SEM campaigns- Excellent computer skills, including Microsoft Office suite, web analytics, and Google AdWords - Some understanding with BRM and CRM - Exceptional communication and presentation skills', NULL, 8000000.0000, 10000000.0000, 0, CAST(N'2020-06-23' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5037, 34, 2, 1, N'ntd20@gmail.com', N'0206189521', N'Mr. Nick', 0, NULL, N'Đã duyệt', 1, 920, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2920, N'Nhân Viên Mua Hàng', N'Mới tốt nghiệp / Thực tập sinh', N'Nhân viên', 1, N'- Mua hàng theo yêu cầu của sản xuất- Lập báo cáo nhập - xuất - tồn hàng hóa- Các công việc khác theo yêu cầu của cấp trên', N'- Nam, tuổi từ 22 đến 30 không cần kinh nghiệm- Tốt nghiệp cao đẳng/đại học chuyên ngành kinh tế- Sử dụng tin học văn phòng thành thạo- Biết tiếng Anh hoặc tiếng Hàn- Công việc cụ thể trao đổi khi phỏng vấn', NULL, 0.0000, 0.0000, 1, CAST(N'2020-06-22' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5037, 34, 2, 0, N'ntd20@gmail.com', N'0621409746', N'Ms Hoa', 0, NULL, N'Đã duyệt', 1, 435, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2921, N'Nhân Viên Kinh Doanh Thu Nhập 20-30 Triệu', N'Nhân viên', N'Nhân viên', 10, N'- Phụ trách, quản lý khu vực được giao- Lập kế hoạch triển khai thị trường khu vực mình quản lý- Mở và chăm sóc hệ thống trên địa bàn mình được giao- Thường xuyên tìm hiểu thị trường, gia tăng mối quan hệ để có cơ sở mở đại lý- Công ty cần tuyển trên địa bàn các tỉnh sau: Hà Nội, Thái nguyên, Bắc Kan, Cao Bằng, Lạng Sơn, Bắc Giang, Bắc Ninh, Hải Phòng,Thái Bình, Đà Nẵng…', N'- Nam từ 22 tuổi trở lên- Có sức khỏe tốt- Tốt nghiệp từ trung cấp trở lên- Ưu tiên những ứng viên có kinh nghiệm và hiểu biết trong ngành VLXD nói chung và ngành sơn nói riêng (có thể trở thành Trưởng phòng kinh doanh nếu năng lực phù hợp)- Có kỹ năng giao tiếp, nhanh nhẹn, trung thực, nhiệt tình- Ưu tiên ứng viên là người bản địa,  có khả năng đi công tác tỉnh, có xe ô tô riêng ( tự lái)', NULL, 20000000.0000, 30000000.0000, 0, CAST(N'2020-07-04' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5038, 35, 2, 0, N'ntd21@gmail.com', N'0520211220', N'Mrs Sáu', 0, NULL, N'Đã duyệt', 1, 707, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2922, N'Nhân Viên Sale Admin - Bình Tân HCM', N'Nhân viên', N'Nhân viên', 3, N'- Hỗ trợ quản lý các tài khoản và khách hàng tiềm năng hiện có- Hỗ trợ chuẩn bị đề xuất, trích dẫn, hợp đồng kinh tế, biên bản họp, thỏa thuận, v.v.- Hỗ trợ dịch các ý tưởng, kế hoạch từ cuộc họp với khách hàng hoặc nhân viên- Hỗ trợ chuẩn bị báo cáo tiến độ dự án và các tài liệu dự án khác- Hợp tác với kế toán để theo dõi tiến độ dự án và thu tiền mặt- Hỗ trợ tổ chức các sự kiện bên trong & bên ngoài của công ty- Hỗ trợ cập nhật nội dung, bài đăng mới trên các kênh truyền thông- Sắp xếp chuyến công tác- Hỗ trợ trong các vấn đề nhân sự, và xây dựng môi trường làm việc tích cực- Thực hiện các nhiệm vụ hành chính khác khi có yêu cầu', N'- Có bằng cấp và chứng chỉ liên quan- 1 năm kinh nghiệm với trợ lý hoặc vị trí tương tự- Có khả năng giao tiếp tiếng Anh- Được tổ chức tốt, kỷ luật và có thể thực hiện dưới áp lực;- Sẵn sàng học hỏi, thích làm việc trong môi trường có nhịp độ nhanh- Kỹ năng giao tiếp bằng lời nói mạnh mẽ khi thường xuyên làm việc trực tiếp với khách hàng• Thử việc: 2 tháng.• Thời gian làm việc: Từ thứ Hai đến thứ Bảy, 8 giờ sáng đến 5 giờ chiều• Nơi làm việc: 1047 Quốc lộ 1A, Phường Bình Trị, Quận Bình Tân• Cơ hội đào tạo:Ms office (excel, word)Luyện kỹ năng giao tiếp tiếng anhCải thiện kỹ năng đàm phánPhát triển tiếp thị trực tuyếnĐồng nghiệp: Làm việc với đội ngũ nhân viên sáng tạo và trẻ trung• Ngày nghỉ: 1 ngày nghỉ mỗi tháng sau khi ký hợp đồng vĩnh viễn•Lợi ích:- Lương tháng- Bảo hiểm xã hội- 12 ngày phép / năm', NULL, 10000000.0000, 12000000.0000, 0, CAST(N'2020-07-17' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5038, 35, 2, 0, N'ntd21@gmail.com', N'0293562743', N'Ms Hương', 0, NULL, N'Đã duyệt', 1, 807, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2923, N'Nhân Viên Kinh Doanh Làm Việc Ở Bắc Giang', N'Nhân viên', N'Nhân viên', 2, N'- Tìm kiếm và phát triển thị trường, tư vấn và thuyết phục khách hàng sử dụng sản phẩm và dịch vụ của công ty.- Theo dõi, chăm sóc và duy trì mối quan hệ với khách hàng- Công việc sẽ trao đổi cụ thể khi phỏng vấn', N'- Tốt nghiệp cao đẳng trở lên.- Ưu tiên tốt nghiệp các ngành về kinh tế.- Có tinh thần trách nhiệm với công việc được giao và đáp ứng các công việc khác củacông ty.', NULL, 5000000.0000, 10000000.0000, 0, CAST(N'2020-07-11' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5038, 35, 2, 0, N'ntd21@gmail.com', N'0887327091', N'Anh Kỳ', 0, NULL, N'Đã duyệt', 1, 329, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2924, N'Nhân Viên Nhắc Phí Qua Điện Thoại - Lương Từ 10 - 15 Triệu / Tháng', N'Nhân viên', N'Nhân viên', 2, N'- Thực hiện cuộc gọi đến khách hàng để nhắc nhở/thuyết phục khách hàng thanh toán khoản nợ quá hạn- Cập nhật vào hệ thống lý do nợ tiền của khách hàng.- Nhắc nhở khách hàng về phí trễ hạn phát sinh nếu khách hàng chậm thanh toán.- Hướng dẫn khách hàng phương thức thanh toán qua ngân hàng và bưu điện...- Yêu cầu khách hàng cung cấp thông tin liên lạc mới (nếu có) và báo cho trưởng nhóm.- Báo cáo cho trưởng nhóm những trường hợp khách hàng có biểu hiện không đồng ý thanh toán hoặc không có thiện chí thanh toán hoặc có biểu hiện gian lận.- Cập nhật cho trưởng nhóm những trường hợp được miễn phí trễ hạn.- Gửi danh sách cho trưởng nhóm những khách hàng không có khả năng thu hồi và yêu cầu sự trợ giúp từ nhân viên thu hồi nợ trực tiếp.- Giải thích đầy đủ cho khách hàng những vấn đề liên quan đến số tiền trễ hạn.', N'- Nam/ Nữ- Trình độ học vấn: tốt nghiệp THPT trở lên.- Kinh nghiệm : Không yêu cầu - Năng động, có kỹ năng giao tiếp tốt qua điện thoại.- Kỹ năng giải quyết vấn đề, thuyết phục tốt, chịu áp lực cao trong công việc- Biết sử dụng vi tính văn phòng, email trong công việc.Ưu tiên: Ứng viên có kinh nghiệm làm việc trong lĩnh vực nhắc nợ khách hàng, thu hồi nợ qua điện thoại,có tối thiểu 1 năm kinh nghiệm làm việc 01 trong các vị trí như: telesale, bán hàng, dịch vụ khách hàng, tài chính ngân hàng, trực tổng đài.', NULL, 10000000.0000, 12000000.0000, 0, CAST(N'2020-07-08' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5038, 35, 2, 0, N'ntd21@gmail.com', N'0225835804', N'Mr Hải', 0, NULL, N'Đã duyệt', 1, 459, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2925, N'Chủ Trì Thiết Kế Kết Cấu', N'Nhân viên', N'Nhân viên', 1, N'Tiếp nhận thông tin đầu vào của các hồ sơ thiết kế (tiến độ, quy mô, chất lượng, yêu cầu của CĐT) đã được BGĐ phân công đảm nhận.Kiểm soát và điều phối các công việc:+ Họp triển khai thiết kế, lên kế hoạch tiến độ, giải quyết và nắm rõ về dự án do mình đảm nhận.+ Kiểm soát thông tin đầu vào của dự án, các thay đổi chi tiết trong quá trình thực hiện.+ Xử lý các yêu cầu chỉnh sửa từ thẩm tra hay từ BQL dự án CT.+ Hỗ trợ và hướng dẫn nhân viên thiết kế về các vấn đề kỹ thuật nhằm đáp ứng công việc và tiến độ.+ Họp với CĐT khi có yêu cầu hoặc phát sinh, giải trình các phương án thiết kế.', N'-Tốt nghiệp Đại học chính quy chuyên ngành Xây dựng.-Ít nhất 05 năm kinh nghiệm (Nam/Nữ). Độ tuổi: từ 28-35 tuổi.-Ưu tiên ứng viên biết tiếng Anh chuyên ngành.-Hiểu và vận dụng tốt các nguyên lý thiết kế, thông thạo cấu tạo kiến trúc và vật liệu.-Thành thạo phần mềm dự toán  G8, Autocad, Sap, Project và các phần mềm tin học liên quan.- Có khả năng bóc tách khối lượng chính xác, tính giá cả hợp lý.- Giao tiếp tốt, có khả năng thương lượng & đàm phán.-Có tinh thần trách nhiệm, chủ động trong công việc, chịu được cường độ làm việc cao.-Trung thực, năng động, sẵn sàng đi công tác xa.', NULL, 10000000.0000, 15000000.0000, 0, CAST(N'2020-06-21' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5038, 35, 2, 5, N'ntd21@gmail.com', N'0548864926', N'Ms Thúy Liễu', 0, NULL, N'Đã duyệt', 1, 356, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2926, N'Nhân Viên Facebook Ads', N'Nhân viên', N'Nhân viên', 3, N'- Xây dựng các kế hoach chạy quảng cáo facebook ads chi tiết. phù hợp với chiến dịch marketing của công ty- Trực tiếp triển khai các chiến dịch quảng cáo chuyển đổi trên facebook.- Theo dõi, phân tích, thống kê các số liệu, hiệu chỉnh và tối ưu các chiến dịch quảng cáo-  Không cần lo tài khoản và thẻ công ty sẽ lo tập trung lên camp và chạy', N'- Xây dựng các kế hoach chạy quảng cáo facebook ads chi tiết. phù hợp với chiến dịch marketing của công ty- Trực tiếp triển khai các chiến dịch quảng cáo chuyển đổi trên facebook.- Theo dõi, phân tích, thống kê các số liệu, hiệu chỉnh và tối ưu các chiến dịch quảng cáo-  Không cần lo tài khoản và thẻ công ty sẽ lo tập trung lên camp và chạy', NULL, 10000000.0000, 12000000.0000, 0, CAST(N'2020-07-15' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5038, 35, 2, 0, N'ntd21@gmail.com', N'0888917952', N'Nguyễn Vĩnh Hiệp', 0, NULL, N'Đã duyệt', 1, 717, 0)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2927, N'Nhân Viên Kinh Doanh Mức Lương 10-15 Triệu', N'Nhân viên', N'Nhân viên', 2, N'•Tư vấn, thuyết trình cho khách hàng về các dịch vụ thiết kế, thi công xây dựng công trình ( dân dụng, văn phòng, căn hộ, nhà phố, khách sạn, nhà hàng, showroom...v.v.)•Tiếp nhận chính xác các yêu cầu thiết kế, thi công của khách hàng và triển khai đến bộ phận thiết kế. •Phối hợp cùng các bộ phận dự toán giải trình các hạng mục báo giá thiết kế, thi công.•Phối hợp cùng các bộ phận khác để báo giá, tư vấn, thuyết phục khách hàng ký kết hợp đồng.•Theo dõi tiến độ thiết kế, thi công của các bộ phận liên quan.•Phối hợp cùng phòng Marketing để quảng bá thương hiệu, hình ảnh của công ty đến thị trường. •Thực hiện các nhiệm vụ khác theo yêu cầu công việc và theo phân công của quản lý các cấp.•Xây dựng, chăm sóc và duy trì mối quan hệ khách hàng.•Một số công việc khác sẽ trao đổi khi phỏng vấn.', N'•Tốt nghiệp Đại học, Cao Đẳng.•Đã có kinh nghiệm trong lĩnh vực Tư vấn và Chăm sóc khách hàng.•Ngoại hình ưa nhìn, khát khao trở thành chuyên gia tư vấn cho khách hàng trong lĩnh vực truyền thông và thương hiệu.•Ưu tiên ứng viên có ít nhất 01 năm kinh nghiệm làm tư vấn trong các lĩnh vực: Kiến trúc, Nội thất, Marketing, Dịch vụ quảng cáo, Thiết kế in ấn, Bất động sản ..... Đặc biệt các ứng viên đã từng làm cho các văn phòng tư vấn xây dựng.•Có niềm đam mê và yêu thích kinh doanh, chịu được áp lực công việc, gọi điện thường xuyên, làm việc ngoài giờ.•Kỹ năng giao tiếp, thuyết trình, đàm phán thuyết phục tốt.•Chịu được áp lực công việc, trung thực, có tinh thần trách nhiệm cao.•Chủ động và thích nghi nhanh với các yêu cầu công việc. Có khả năng làm việc độc lập cũng như làm việc nhóm, chịu được sức ép lớn, sẵn sàng làm overtime vào những thời điểm gấp của dự án.•Khả năng sử dụng máy vi tính thành thạo, sử dụng tốt các công cụ khai thác Internet.•Khả năng xử lý tình huống tốt, khéo léo, hiểu biết, trung thực.•Có mong muốn làm việc ổn định lâu dài.', NULL, 10000000.0000, 15000000.0000, 0, CAST(N'2020-06-22' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5038, 35, 2, 1, N'ntd21@gmail.com', N'0849819172', N'Mr. Vương', 0, NULL, N'Đã duyệt', 1, 536, 0)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2928, N'Nhân Viên Kinh Doanh Mức Lương 10-15 Triệu', N'Nhân viên', N'Nhân viên', 10, N'• Chịu trách nhiệm mọi hoạt động bán hàng và phát triển thị trường theo kế hoạch, khu vực được phân công, và chỉ tiêu bán hàng theo tháng, quý, năm.• Lập kế hoạch thực hiện chỉ tiêu bán hàng trong khu vực quản lý.• Chủ động lên kế hoạch đi công tác tỉnh để phát triển thị trường và tăng thêm nguồn khách hàng.• Tuân thủ nghiêm chỉnh các quy trình, chính sách của công ty về quản lý dữ liệu khách hàng, quy trình thanh toán & bán hàng.• Chịu trách nhiệm xây dựng hệ thống bán hàng.• Xây dựng mối quan hệ tốt, lâu dài với khách hàng và các phòng ban trong công ty.• Xử lý các khiếu nại, phàn nàn trong trường hợp cần thiết.• Nắm bắt thông tin về thị trường, thông tin đối thủ cạnh tranh nhằm xây dựng kế hoạch, đề xuất và kiểm soát các chương trình thúc đẩy bán hàng (khuyến mại, giảm giá, trưng bày…) trong khu vực mình quản lý nhằm tăng cường hình ảnh Công ty và tăng doanh số bán hàng.• Báo cáo hoạt động kinh doanh hàng tuần, hàng tháng, quý, năm. Tuân thủ nghiêm túc các quy trình trong hệ thống phần mềm quản lý của công ty.• Tham gia vào các chiến dịch marketing, quảng bá hình ảnh công ty.• Thực hiện công việc khác do trưởng bộ phận phân công.• Hỗ trợ khách hàng trong việc xác định nhu cầu xe và hoàn tất giấy tờ, thủ tục tài chính trong quá trình khách hàng sử dụng DV của Công ty.', N'- Độ tuổi từ 24-30 tuổi. Ưu tiên Nữ giới- Tốt nghiệp Đại học hoặc cao đẳng chuyên ngành kinh doanh.- Có kinh nghiệm bán hàng- Có hiểu biết về thị trường ôtô- Ưu tiên những ứng viên đã có kinh nghiệm dòng xe Toyota', NULL, 10000000.0000, 15000000.0000, 0, CAST(N'2020-06-20' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5038, 35, 2, 0, N'ntd21@gmail.com', N'0111512446', N'Ms. Minh Huyền', 0, NULL, N'Đã duyệt', 1, 370, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2929, N'Nhân Viên Bán Hàng Kênh Gt Tại Hà Nội', N'Nhân viên', N'Nhân viên', 4, N'- Học hỏi kiến thức về sản phẩm - Chăm sóc khách hàng theo tuyến cố định- Tư vấn bán sản phẩm phù hợp cho khách hàng.- Luôn mở mới khách trong tuyến - Trưng bày hàng hóa.- Thực hiện các yêu cầu công việc được giao từ cấp trên.- Chi tiết sẽ được trao đổi cụ thể trong buổi phỏng vấn.', N'- Ưu tiên có kinh nghiệm trong ngành FMCG, không biết được đào tạo.- Nói năng nhẹ nhàng, lưu loát, thái độ nhẫn nại, trung thực.- Nhanh nhẹn, ngăn nắp, sạch sẽ, làm việc khoa học.- Ưu tiên những người yêu thích công việc kinh doanh', NULL, 6000000.0000, 11000000.0000, 0, CAST(N'2020-07-10' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5038, 35, 2, 0, N'ntd21@gmail.com', N'0841261684', N'Mr Cường', 0, NULL, N'Đã duyệt', 1, 92, 0)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2930, N'Phụ Trách Kế Toán Làm Việc Tại Hải Phòng', N'Quản lý', N'Quản lý', 1, N'- Điều hành, quản lý nhân sự phòng Kế toán tài chính để thực hiện công tác tàichính - kế toán của Khách sạn trên cơ sở đảm bảo nguyên tắc tài chính theo Luật định.- Tổ chức bộ máy kế toán khoa học, tiết kiệm, hiệu quả, phân công nhiệm vụcho từng nhân viên trong bộ phận, hướng dẫn các kế toán thưc hiện, hoàn thành nhiệmvụ;- Chỉ đạo, hướng dẫn làm báo cáo tài chính nội bộ báo cáo cho ban lãnh đạo;- Cung cấp các sổ sách, số liệu phục vụ cho công tác thanh – kiểm toán của cơquan chức năng;- Thực hiện các công việc khác khi có yêu cầu của cấp trên; báo cáo trực tiếpvới ban Giám đốc khi có yêu cầu.Làm việc tại Khách sạn Lâm nghiệp Đồ Sơn tại Hải Phòng trực thuộc Tổng công ty Lâm nghiệp Việt Nam - công ty cổ phầnĐịa chỉ: Trung Tâm Khu 1, Phường Vạn Sơn, Đồ Sơn,TP. Hải Phòng', N'- Tốt nghiệp đại học chính quy chuyên ngành về Kế toán, Tài chính.- Có ít nhất 03 năm kinh nghiệm thực hiện nhiệm vụ về công tác tài chính, kếtoán.- Có kinh nghiệm từng làm kế toán tổng hợp hoặc kế toán trưởng tại các doanhnghiệp khác.- Am hiểu về Luật Kế toán và các chuẩn mực Kế toán theo quy định hiện hành- Có kỹ năng tổ chức, lập kế hoạch, kiểm tra báo cáo, đánh giá công tác thựchiện kế hoạch chi phí hàng tháng, quý, năm....- Có kỹ năng làm việc trực tiếp với các cơ quan ban ngành như: thuế, ngânhàng, đối tác… - Sử dụng tốt phần mềm tin học và phần mềm kế toán.- Ưu tiên các ứng viên:+ Đã thời gian thực hiện nhiệm vụ Kế toán trưởng tại các doanh nghiệp khác.+ Có kỹ năng, đọc hiểu và giao tiếp bằng tiếng Anh.+ Có hộ khẩu tại Hải Phòng.', NULL, 0.0000, 0.0000, 1, CAST(N'2020-07-08' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5038, 35, 2, 3, N'ntd21@gmail.com', N'0834009019', N'Mr Hùng', 0, NULL, N'Đã duyệt', 1, 714, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2931, N'Nhân Viên Kinh Doanh', N'Nhân viên', N'Nhân viên', 2, N'- Giới thiệu, tư vấn và thuyết phục các khách hàng sử sản phẩm/dịch vụ của doanh nghiệp- Tìm kiếm, duy trì và phát triển mạng lưới khách hàng và đối tác tiềm năng thuộc nhiều lĩnh vực (trực tiếp hoặc gián tiếp).- Trực tiếp thực hiện, đốc thúc thực hiện hợp đồng, bao gồm các thủ tục giao hàng, xuất hoá đơn, cùng khách hàng kiểm tra chất lượng sản phẩm giao.- Nhận và xử lý các khiếu nại của khách hàng về chất lượng sản phẩm, thời gian giao hàng….- Theo dõi quá trình thanh lý hợp đồng, hỗ trợ phòng kế toán đốc thúc công nợ, chỉ xong trách nhiệm khi khách hàng đã thanh toán xong.- Giải quyết các vấn đề và phàn nàn của khách hàng để đảm bảo độ hài lòng, tin cậy của khách theo đúng quy trình.- Báo cáo lên các cấp quản lý về nhu cầu, vấn đề, mối quan tâm của khách hàng; hoạt động của đối thủ cạnh tranh và tiềm năng trong việc phát triển kinh doanh sản phẩm/dịch vụ', N'Trình độ Cao đẳng trở lên chuyên ngành Quản trị kinh doanh, Kinh tế thương mại, Marketting.Sử dụng thành thạo vi tính văn phòng, các phần mềm liên quan đến công việc, làm việc độc lập.Nhiệt tình, năng động, độc lập, nhạy bén.Thúc đẩy kinh doanh, kỹ năng giao tiếp.Ưu tiên có kinh nghiệm kinh doanh ít nhất 2 năm trở lên.Quen thuộc với các phần mềm CRM là một lợi thếCó khả năng tự thúc đẩy, tự vạch định mục tiêu cụ thể và tập trung thực hiện mục tiêu.Kỹ năng liên quanKĩ năng giao tiếp tốtKĩ năng đàm phán và thuyết phụcKĩ năng phân tích tình huống, xử lí tình huống và ra quyết địnhKĩ năng tổ chức và quản lý thời gianKĩ năng quản trị mối quan hệ', NULL, 10000000.0000, 15000000.0000, 0, CAST(N'2020-07-13' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5039, 36, 2, 2, N'ntd22@gmail.com', N'0556315673', N'Ms Vân Anh', 0, NULL, N'Đã duyệt', 1, 565, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (2932, N'Nhân Viên Kinh Doanh Bất Động Sản', N'Nhân viên', N'Nhân viên', 10, N'- Maketting quảng bá thương hiệu công ty- Bán và môi giới các sản phẩm đất, nhà, thuê và cho thuê- Bán các sản phẩm do công ty đầu tư- Bán các dự án do công ty làm chủ đầu tư hoặc nhận phân phối', N'- Giới tính nam hoặc nữ đều được.- Độ tuổi từ 20- 25 tuổi.- Tốt nghiệp trung cấp trở lên, ưu tiền các ngành liên quan đến kinh tế, ngân hàng, kinh doanh, xã hội nhân văn, - Kinh nghiệm: không yêu cầu có kinh nghiệm trong lĩnh vực bất động sản nhưng có kinh nghiệm là một lợi thế.- Kỹ năng: Có kỹ năng ăn nói lưu loát, ưu tiên có giọng nói địa phương Quảng Nam, Đà Nẵng. Có sự đam mê trong lĩnh vực kinh doanh. Có ý thức nghề nghiệp thực sự. Có tư duy logic xử lý tình huống tốt.- Khả năng làm việc độc lập và làm việc nhóm với tinh thần trách nhiệm cao.', NULL, 10000000.0000, 30000000.0000, 0, CAST(N'2020-07-01' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5039, 36, 2, 0, N'ntd22@gmail.com', N'0421834055', N'Mr.Phi', 0, NULL, N'Đã duyệt', 1, 7, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (3005, N'Anh thắng tuyển dụng', N'Quản lý', N'Quản lý', 10, N'làm chủ', N'Tốt nghiệp Đại học chuyên ngành tài chính, ngân hàng, kế toán, kiểm toán hoặc các chuyên ngành liên quan.
Ưu tiên ứng viên có chứng chỉ CPA, ACCA hoặc CFA; có kiến thức về xuất nhập khẩu- lĩnh vực thanh toán quốc tế.
Có khả năng lập kế hoạch, báo cáo.
Có kỹ năng trình bày, làm việc đội nhóm, làm việc dưới áp lực cao.
Có khả năng phân tích và tổng hợp vấn đề. Nhạy bén với các biến động, xu hướng tài chính.
Có tính kiên nhẫn, cẩn thận, chính xác.
Đọc, hiểu bằng tiếng Anh tốt. "', N'Giao tiếp bằng tiếng anh thành thạo', 3000000.0000, 10000000.0000, 0, CAST(N'2020-06-30' AS Date), CAST(N'2026-10-06' AS Date), 0, 5026, 23, 2, 2, N'ntd9@gmail.com', N'0346006260', N'Anh thắn', 0, NULL, N'Đã duyệt', 1, 510, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (4005, N'Tuyển dụng Nhân viên Phiên dịch', N'Thông dịch viên', N'Nhân viên', 10, N'Thông thạo tiếng anh', N'Giao tiếp tốt, thái độ công việc chấp hành nghiêm chỉnh', N'Giao tiếp bằng tiếng anh thành thạo', NULL, 10000000.0000, 0, CAST(N'2020-07-01' AS Date), CAST(N'2026-10-06' AS Date), 0, 5026, 23, 2, 2, N'ntd9@gmail.com', N'0346006260', N'Nguyễn Thị Hoa', 0, NULL, N'Đã duyệt', 1, 404, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (5005, N'[Viện Big Data] Backend Engineer', N'Nhân viên', N'Nhân viên', 10, N'Tham gia thiết kế và phát triển hệ sinh thái phần mềm VinDr của VinBDI, trong đó bao gồm các giải pháp ứng dụng trí tuệ nhân tạo cho chẩn đoán hình ảnh y tế, bao gồm:
● Xây dựng RestAPI service cho hệ thống VinDR
● Kết hợp với team AI để xây dựng hệ thống chuẩn đoán
● Xây dựng tài liệu và unittest
● Hỗ trợ triển khai hệ thống tại cloud và tại hạ tầng khách hàng', N'● Có khả năng tập trung vào chi tiết và yêu cầu cao về chất lượng sản phẩm.
● Có +1 năm kinh nghiệm làm việc với Python và sử dụng thành thạo một Web Framework
● Có khả năng tìm hiểu và tuân thủ các convention và best practices của một dự án.
● Có kinh nghiệm sử dụng OpenAPI trong thiết kế và xây dựng RestAPI.
● Có kinh nghiệm làm việc và hiểu biết cơ bản về Docker.
● Sử dụng thành thạo Linux.', N'Docker, Api, Rest API, Linux, Python', 3000000.0000, 10000000.0000, 0, CAST(N'2020-03-07' AS Date), CAST(N'2026-10-06' AS Date), 0, 5026, 23, 2, 2, N'ntd9@gmail.com', N'0346006260', N'Phòng nhân sự', 0, NULL, N'Đã duyệt', 1, 1002, 0)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (6005, N'Project Management Specialist (10 Headcounts) - Working In Hanoi', N'Project Management', N'Quản lý', 10, N'- Accountable for the facilitating and recording cross-functional vehicle/program decisions. - Coordinate and resolve program conflicts. - Manage vehicle change control - Coordinate on-site cross functional resources to support launch. - Manage project timing. - Manage contract with engineering partners, part suppliers - Lead Management reviews. - Lead Plan for Every Part.', N'1) Education/ Professional knowledge: - Degree in Engineering (Mechanical/Electrical/Automobile)  2) Skill, Experience: - High level operational experience in the use of Microsoft PowerPoint - Proven ability to coordinate cross-functional teams. - Strong problem solving skills and the ability to grasp and effectively use technical concepts and technical information. - Highly motivated, self-starter, and active learner with strong interpersonal, communication, written and oral skills.', N'- Strong problem solving skills and the ability to grasp and effectively use technical concepts and technical information. - Highly motivated, self-starter, and active learner with strong interpersonal, communication, written and oral skills.', 5000000.0000, 10000000.0000, 0, CAST(N'2020-07-04' AS Date), CAST(N'2026-10-06' AS Date), 0, 5026, 23, 2, 1, N'ntd9@gmail.com', N'0346006260', N'Phòng nhân sự', 0, NULL, N'Đã duyệt', 1, 823, 0)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (6006, N'Java Developer - Ít Nhất 2 Năm Kinh Nghiệm (Up to $1,800)', N'Java Developer', N'Quản lý', 10, N'Tự hào là thành viên của tập đoàn công nghệ thông tin hàng đầu Nhật Bản: SystemEXE, chúng tôi đặt ra sứ mệnh cho mình, là cầu nối mang những tinh hoa, chất lượng của ngành CNTT Nhật Bản về với Việt Nam, qua đó tạo ra sự thành công vượt bậc cho người Việt - doanh nghiệp Việt - và xa hơn là đất nước Việt Nam. Hãy cùng SystemEXE Việt Nam tạo ra một tương lai thịnh vượng cùng IT, mang công nghệ Nhật - chất lượng Nhật đón nhận thành công Việt. Vì sao không cùng nhau trải nghiệm học tập và làm việc với tất cả khách hàng trong và ngoài nước? Bạn sẽ trở thành 1 Full-stack Developer. Sứ mệnh của Công Ty chúng tôi đang chờ bạn cùng xây dựng. Hãy gia nhập SystemEXE ngay hôm nay!', N'※ YÊU CẦU CÔNG VIỆC: ◆ Ít nhất 2 năm kinh nghiệm lập trình ngôn ngữ Java. ◆ Hiểu rõ ngôn ngữ lập trình Java/J2EE, Spring (Spring Core, Spring MVC, Spring Security, Spring Data). ◆ Biết một trong những công nghệ JavaScript: jQuery, ReactJS, Angular… ◆ Có khả năng lập trình với HTML5, CSS3, SASS. ◆ Sử dụng thành thạo DBMS: MS SQL, PostgreSQL, MySQL. ◆ Hiểu rõ OOP và design patterns. ◆ Có khả năng viết Unit testing: JUnit, Mock. ◆ Có kinh nghiệm Web Service, Micro-services.', N'※ CÁC PHÚC LỢI DÀNH CHO BẠN: ◆ Thưởng hè, thưởng theo lợi nhuận vào cuối năm, lương tháng 13... ◆ Hệ thống giải thưởng thường xuyên: Giải thưởng nỗ lực, giải thưởng nhân viên xuất sắc, nhân viên triển vọng, thưởng dự án thành công,... ◆ Được tư vấn và chia sẻ về phát triển năng lực và nghề nghiệp bản thân. ◆ Phụ cấp đi lại. ◆ Du lịch thường niên, teambuilding tổ chức định kỳ. ◆ Tham gia tiệc Công ty hàng tháng. ◆ 12+ ngày phép, 1 ngày nghỉ sinh nhật và các ngày nghỉ lễ theo quy định của pháp luật. ◆ Tham gia tất cả các loại bảo hiểm theo luật định, đóng bảo hiểm full lương. ◆ Khám sức khỏe định kỳ hàng năm. ◆ Tham gia các khóa kỹ năng mềm trong và ngoài Công ty. ◆ Đào tạo tiếng Nhật miễn phí, hỗ trợ thi chứng chỉ và thưởng khi đạt được chứng chỉ. ◆ Cơ hội Onsite Nhật Bản. ◆ Ngoài ra ở chi nhánh Hà Nội: nhân viên sẽ có cơ hội vào TP. HCM công tác thường xuyên, được offer mức lương cạnh tranh.', 3000000.0000, 5000000.0000, 0, CAST(N'2020-07-06' AS Date), CAST(N'2026-10-06' AS Date), 0, 5026, 24, 2, 2, N'ntd9@gmail.com', N'0287303910', N'Nguyễn Thị Hoa', 0, NULL, N'Đã duyệt', 1, 911, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (6007, N'Chuyên Viên Nhóm Định Lượng', N'Chuyên viên', N'Quản lý', 10, N'***MÔ TẢ CÔNG VIỆC Đây là một công việc hấp dẫn và đầy triển vọng phát triển nghề nghiệp, dành cho các ứng viên có khả năng cân bằng tính nhạy bén, sáng tạo với năng lực tư duy sâu sắc và tỉ mỉ dựa trên nền tảng tư duy toán học vững chắc. Công việc của bạn cụ thể như sau:  1. Xây dựng, theo dõi và bảo dưỡng các mô hình định lượng; Văn bản hóa các kết quả và hướng dẫn sử dụng mô hình trong hoạt động ứng dụng thực tiễn.  2. Nghiên cứu, đề xuất và phối hợp thực hiện các giải pháp mang tính định lượng nhằm nâng cao chất lượng quản trị rủi ro và hỗ trợ quyết định kinh doanh của Ngân hàng: a. Nghiên cứu mô hình hóa rủi ro: phát triển phương pháp luận, ứng dụng mô hình và tính toán tối ưu để lượng hóa rủi ro; nghiên cứu và ứng dụng các kỹ thuật kiểm định mô hình; phân tích dự báo các điều kiện kinh tế vĩ mô, kinh tế ngành trong môi trường giả thiết của các mô hình. b. Tính toán ứng dụng kết quả mô hình, theo dõi quá trình triển khai và văn bản hóa.  3. Tham gia tính toán thiết kế các sản phẩm, xây dựng các quy trình; phát triển và quản lý chất lượng hệ thống dữ liệu: a. Mô hình hóa nghiệp vụ; lập trình mô hình dự báo tài chính và mô hình hóa sản phẩm, nghiệp vụ đặc trưng. b. Khảo sát các nguồn dữ liệu, phát triển dữ liệu và thu thập, quản trị dữ liệu phục vụ xây dựng các mô hình trong các dự án nâng cao năng lực quản trị của Ngân hàng và các hoạt động nghiên cứu, phân tích khác. c. Quản trị dữ liệu và thực hiện các biện pháp, khuyến nghị nhằm nâng cao chất lượng dữ liệu. d. Lập trình các mẫu báo cáo quản lý chất lượng dữ liệu và các mẫu báo cáo thông tin quản lý trên các công cụ BI.  4. Nhiệm vụ khác: a. Thực hiện góp ý văn bản chính sách của VCB, các cơ quan quản lý liên quan trong lĩnh vực quản trị rủi ro. b. Tham gia các Dự án nâng cao năng lực quản trị của VCB. c. Tham gia các lớp đào tạo, tập huấn nghiệp vụ theo kế hoạch của phòng và VCB. d. Thực hiện các nhiệm vụ khác có liên quan do Lãnh đạo Phòng giao.', N'***Chúng tôi tìm kiếm ứng cử viên đáp ứng được tiêu chí sau: 1. Trình độ: a. Trình độ chuyên môn: Tốt nghiệp đại học loại Khá trở lên hoặc thạc sỹ chuyên ngành Toán, Kinh tế học, Toán tài chính, Toán kinh tế, Tài chính định lượng, Quản trị rủi ro, Phân tích kinh doanh, Khoa học dữ liệu, Kinh tế-tài chính ứng dụng hoặc các chuyên ngành có liên quan khác, tại các trường sau: - Học viện Ngân hàng. - Đại học Ngân hàng thành phố Hồ Chí Minh. - Đại học Kinh tế quốc dân. - Đại học Kinh tế thành phố Hồ Chí Minh. - Đại học Quốc gia. - Đại học Ngoại thương. - Học viện Tài chính. - Các trường ĐH trong và ngoài nước có uy tín. b. Trình độ ngoại ngữ: có các kĩ năng tiếng Anh thành thạo.  2. Độ tuổi: Không quá 35 tuổi tại thời điểm đăng ký tuyển dụng.  3. Kiến thức: Có nền tảng vững chắc về Xác suất-thống kê và Toán học trong kinh tế-tài chính.  4. Kỹ năng: - Có kỹ năng lập trình với các công cụ SAS, R, MATLAB, Ms-SQL,… - Có kỹ năng làm việc nhóm và làm việc độc lập. - Có kỹ năng tổ chức thực hiện, sắp xếp công việc. - Có kỹ năng trình bày và diễn giải những khái niệm kỹ thuật một cách dễ hiểu bằng cả ngôn ngữ tiếng Việt và tiếng Anh.  5. Khả năng: - Có khả năng nghiên cứu ứng dụng các phương pháp, kĩ thuật và mô hình toán tiên tiến. - Có khả năng thực thi triển khai các tính toán có mức độ phức tạp cao. - Có khả năng tư duy logic. - Có khả năng khái quát hóa và tư duy toàn cục.  6. Phẩm chất cá nhân: - Cẩn trọng, linh hoạt. - Trung thực, kỷ luật, trách nhiệm.  7. Các tiêu chí khác: - Sức khỏe tốt, chịu được áp lực công việc cao. - Có xác nhận lý lịch tư pháp.  8. Các tiêu chí ưu tiên: - Là học sinh chuyên khối Tự nhiên trong giai đoạn Trung học phổ thông, và/hoặc có các giải thưởng quốc gia, quốc tế. - Có các chứng chỉ quốc tế chuyên môn (FRM, PRM, CQF, CFA,...) - Có chứng chỉ tiếng Anh quốc tế IELTS trên 6.0 (hoặc tương đương).; - Có kinh nghiệm phân tích định lượng tại các tổ chức ngân hàng-tài chính/dịch vụ tài chính trong và ngoài nước. - Có thư giới thiệu từ giáo sư hoặc cấp trên ở nơi làm việc cũ.', N'Docker, Api, Rest API, Linux, Python', NULL, NULL, 1, CAST(N'2020-07-05' AS Date), CAST(N'2026-10-06' AS Date), 1, 5026, 24, 3, 3, N'ntd9@gmail.com', N'0346006260', N'Lý Thái Tổ', 0, NULL, N'Đã duyệt', 1, 79, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (7006, N'Assistant Partnership', N'Partnership', N'Quản lý', 10, N'Kỹ sư cơ: Thiết kế, báo giá, quản lý và giám sát tiến độ lắp đặt, kiểm tra chạy thử cho các hệ thống cơ: Hệ thống thông gió và điều hòa không khí (HVAC), Hệ thống cấp thoát nước (P & D), Hệ thống chữa cháy (FF) và các hệ thống phụ trợ khác cho các dự án phòng sạch và nhà công nghiệp

- Kỹ sử điện: Thiết kế, báo giá, quản lý và giám sát tiến độ lắp đặt kiểm tra chạy thử cho hệ thống điện và điều khiển: Hệ thống trạm biến áp, trạm trung thế, hạ thế cấp nguồn chính, BMS, hệ thống báo cháy, hệ thống an ninh, hệ thống điện thoại và dữ liệu, hệ thống chiếu sáng, hệ thống điều khiển, ...
Địa điểm làm việc: Hà Nội, Hồ Chí Minh và các dự án trên lãnh thổ Việt Nam', N'Tốt nghiệp Đại học chuyên ngành tài chính, ngân hàng, kế toán, kiểm toán hoặc các chuyên ngành liên quan.
Ưu tiên ứng viên có chứng chỉ CPA, ACCA hoặc CFA; có kiến thức về xuất nhập khẩu- lĩnh vực thanh toán quốc tế.
Có khả năng lập kế hoạch, báo cáo.
Có kỹ năng trình bày, làm việc đội nhóm, làm việc dưới áp lực cao.
Có khả năng phân tích và tổng hợp vấn đề. Nhạy bén với các biến động, xu hướng tài chính.
Có tính kiên nhẫn, cẩn thận, chính xác.
Đọc, hiểu bằng tiếng Anh tốt. "', N'Docker, Api, Rest API, Linux, Python', 3000000.0000, NULL, 0, CAST(N'2020-07-06' AS Date), CAST(N'2026-10-06' AS Date), 0, 5026, 23, 2, 2, N'ntd9@gmail.com', N'0987123133', N'Nguyễn Thị Hoa', 0, NULL, N'Đã duyệt', 1, 798, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (7007, N'Nhân viên kho vận
', N'Kho chủ', N'Quản lý', 10, N'Thông thạo tiếng anh', N'Tốt nghiệp Đại học chuyên ngành tài chính, ngân hàng, kế toán, kiểm toán hoặc các chuyên ngành liên quan.
Ưu tiên ứng viên có chứng chỉ CPA, ACCA hoặc CFA; có kiến thức về xuất nhập khẩu- lĩnh vực thanh toán quốc tế.
Có khả năng lập kế hoạch, báo cáo.
Có kỹ năng trình bày, làm việc đội nhóm, làm việc dưới áp lực cao.
Có khả năng phân tích và tổng hợp vấn đề. Nhạy bén với các biến động, xu hướng tài chính.
Có tính kiên nhẫn, cẩn thận, chính xác.
Đọc, hiểu bằng tiếng Anh tốt. "', N'Giao tiếp bằng tiếng anh thành thạo', NULL, 10000000.0000, 0, CAST(N'2019-02-03' AS Date), CAST(N'2026-10-06' AS Date), 1, 5026, 24, 1, 1, N'ntd9@gmail.com', N'0987123133', N'Nguyễn Thị Hoa', 0, NULL, N'Đã duyệt', 1, 13, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (7008, N'Studio Planning Assistant
', N'Chuyên viên', N'Quản lý', 10, N'Phiên dịch và dịch thuật giữa kỹ sư hệ thống Nhật Bản và Lập trình viên người Việt
Hỗ trợ cho các lập trình viên người Việt vừa đến Nhật trong công việc và đời sống như hỗ trợ thuê nhà ở, v.v.', N'・Người Việt Nam
・Thông thạo tiếng nhật
・Có chứng chỉ N1
・Tốt nghiệp đại học Việt Nam hay Nhật Bản
・Sống ở Tokyo
・Có kinh nghiệm phiên dịch
・Có khả năng truyền đạt với các kỹ sư lập trình
・Quan tâm đến lĩnh vực phần mềm máy tính', N'Docker, Api, Rest API, Linux, Python', NULL, 30000000.0000, 0, CAST(N'2020-07-06' AS Date), CAST(N'2026-10-06' AS Date), 1, 5026, 24, 1, 2, N'ntd9@gmail.com', N'0346006260', N'Phòng nhân sự', 0, NULL, N'Đã duyệt', 1, 152, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (7009, N'Nhân Viên Hỗ Trợ Nhập Liệu Và Lưu Trữ Hồ Sơ
', N'Kế toán viên', N'Quản lý', 10, N'Biết ngôn ngữ lập trình', N'Giao tiếp tốt, thái độ công việc chấp hành nghiêm chỉnh', N'Giao tiếp bằng tiếng anh thành thạo', NULL, NULL, 1, CAST(N'2020-01-06' AS Date), CAST(N'2026-10-06' AS Date), 1, 5026, 23, 1, 2, N'ntd9@gmail.com', N'0213313133', N'Nguyễn Nhân', 0, NULL, N'Đã duyệt', 1, 19, 0)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (7010, N'hân Viên Nhập lIệu Văn Phòng
', N'Assistiant', N'Quản lý', 10, N'Kỹ sư cơ: Thiết kế, báo giá, quản lý và giám sát tiến độ lắp đặt, kiểm tra chạy thử cho các hệ thống cơ: Hệ thống thông gió và điều hòa không khí (HVAC), Hệ thống cấp thoát nước (P & D), Hệ thống chữa cháy (FF) và các hệ thống phụ trợ khác cho các dự án phòng sạch và nhà công nghiệp

- Kỹ sử điện: Thiết kế, báo giá, quản lý và giám sát tiến độ lắp đặt kiểm tra chạy thử cho hệ thống điện và điều khiển: Hệ thống trạm biến áp, trạm trung thế, hạ thế cấp nguồn chính, BMS, hệ thống báo cháy, hệ thống an ninh, hệ thống điện thoại và dữ liệu, hệ thống chiếu sáng, hệ thống điều khiển, ...
Địa điểm làm việc: Hà Nội, Hồ Chí Minh và các dự án trên lãnh thổ Việt Nam', N'Tốt nghiệp Đại học chuyên ngành tài chính, ngân hàng, kế toán, kiểm toán hoặc các chuyên ngành liên quan.
Ưu tiên ứng viên có chứng chỉ CPA, ACCA hoặc CFA; có kiến thức về xuất nhập khẩu- lĩnh vực thanh toán quốc tế.
Có khả năng lập kế hoạch, báo cáo.
Có kỹ năng trình bày, làm việc đội nhóm, làm việc dưới áp lực cao.
Có khả năng phân tích và tổng hợp vấn đề. Nhạy bén với các biến động, xu hướng tài chính.
Có tính kiên nhẫn, cẩn thận, chính xác.
Đọc, hiểu bằng tiếng Anh tốt. "', N'Giao tiếp bằng tiếng anh thành thạo', NULL, 15000000.0000, 0, CAST(N'2020-01-06' AS Date), CAST(N'2026-10-06' AS Date), 1, 5026, 23, 1, 3, N'ntd9@gmail.com', N'0987123133', N'Trần Nam', 0, NULL, N'Đã duyệt', 1, 335, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (7011, N'Nhân viên R&D', N'Assistiant', N'Quản lý', 10, N'Tự hào là thành viên của tập đoàn công nghệ thông tin hàng đầu Nhật Bản: SystemEXE, chúng tôi đặt ra sứ mệnh cho mình, là cầu nối mang những tinh hoa, chất lượng của ngành CNTT Nhật Bản về với Việt Nam, qua đó tạo ra sự thành công vượt bậc cho người Việt - doanh nghiệp Việt - và xa hơn là đất nước Việt Nam. Hãy cùng SystemEXE Việt Nam tạo ra một tương lai thịnh vượng cùng IT, mang công nghệ Nhật - chất lượng Nhật đón nhận thành công Việt. Vì sao không cùng nhau trải nghiệm học tập và làm việc với tất cả khách hàng trong và ngoài nước? Bạn sẽ trở thành 1 Full-stack Developer. Sứ mệnh của Công Ty chúng tôi đang chờ bạn cùng xây dựng. Hãy gia nhập SystemEXE ngay hôm nay!', N'Giao tiếp tốt, thái độ công việc chấp hành nghiêm chỉnh', N'Docker, Api, Rest API, Linux, Python', NULL, NULL, 1, CAST(N'2019-02-03' AS Date), CAST(N'2026-10-06' AS Date), 1, 5026, 24, 1, 2, N'ntd9@gmail.com', N'0283973423', N'Vũ Tào', 0, NULL, N'Đã duyệt', 1, 229, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (7012, N'English Teacher''s Assistant (Vietnamese candidates only)', N'Tài chính viên', N'Quản lý', 10, N'Tham gia thiết kế và phát triển hệ sinh thái phần mềm VinDr của VinBDI, trong đó bao gồm các giải pháp ứng dụng trí tuệ nhân tạo cho chẩn đoán hình ảnh y tế, bao gồm:
● Xây dựng RestAPI service cho hệ thống VinDR
● Kết hợp với team AI để xây dựng hệ thống chuẩn đoán
● Xây dựng tài liệu và unittest
● Hỗ trợ triển khai hệ thống tại cloud và tại hạ tầng khách hàng', N'Tốt nghiệp Đại học chuyên ngành tài chính, ngân hàng, kế toán, kiểm toán hoặc các chuyên ngành liên quan.
Ưu tiên ứng viên có chứng chỉ CPA, ACCA hoặc CFA; có kiến thức về xuất nhập khẩu- lĩnh vực thanh toán quốc tế.
Có khả năng lập kế hoạch, báo cáo.
Có kỹ năng trình bày, làm việc đội nhóm, làm việc dưới áp lực cao.
Có khả năng phân tích và tổng hợp vấn đề. Nhạy bén với các biến động, xu hướng tài chính.
Có tính kiên nhẫn, cẩn thận, chính xác.
Đọc, hiểu bằng tiếng Anh tốt. "', N'Giao tiếp bằng tiếng anh thành thạo', NULL, 3000000.0000, 0, CAST(N'2020-07-05' AS Date), CAST(N'2026-10-06' AS Date), 1, 5026, 23, 1, 3, N'ntd9@gmail.com', N'0346006260', N'Nhật Phú', 0, NULL, N'Đã duyệt', 1, 720, 0)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (7013, N'Nhân Viên Hỗ Trợ Nhân Sự
', N'Hỗ trợ viên', N'Quản lý', 10, N'Tự hào là thành viên của tập đoàn công nghệ thông tin hàng đầu Nhật Bản: SystemEXE, chúng tôi đặt ra sứ mệnh cho mình, là cầu nối mang những tinh hoa, chất lượng của ngành CNTT Nhật Bản về với Việt Nam, qua đó tạo ra sự thành công vượt bậc cho người Việt - doanh nghiệp Việt - và xa hơn là đất nước Việt Nam. Hãy cùng SystemEXE Việt Nam tạo ra một tương lai thịnh vượng cùng IT, mang công nghệ Nhật - chất lượng Nhật đón nhận thành công Việt. Vì sao không cùng nhau trải nghiệm học tập và làm việc với tất cả khách hàng trong và ngoài nước? Bạn sẽ trở thành 1 Full-stack Developer. Sứ mệnh của Công Ty chúng tôi đang chờ bạn cùng xây dựng. Hãy gia nhập SystemEXE ngay hôm nay!', N'Tốt nghiệp Đại học chuyên ngành tài chính, ngân hàng, kế toán, kiểm toán hoặc các chuyên ngành liên quan.
Ưu tiên ứng viên có chứng chỉ CPA, ACCA hoặc CFA; có kiến thức về xuất nhập khẩu- lĩnh vực thanh toán quốc tế.
Có khả năng lập kế hoạch, báo cáo.
Có kỹ năng trình bày, làm việc đội nhóm, làm việc dưới áp lực cao.
Có khả năng phân tích và tổng hợp vấn đề. Nhạy bén với các biến động, xu hướng tài chính.
Có tính kiên nhẫn, cẩn thận, chính xác.
Đọc, hiểu bằng tiếng Anh tốt. "', N'※ CÁC PHÚC LỢI DÀNH CHO BẠN: ◆ Thưởng hè, thưởng theo lợi nhuận vào cuối năm, lương tháng 13... ◆ Hệ thống giải thưởng thường xuyên: Giải thưởng nỗ lực, giải thưởng nhân viên xuất sắc, nhân viên triển vọng, thưởng dự án thành công,... ◆ Được tư vấn và chia sẻ về phát triển năng lực và nghề nghiệp bản thân. ◆ Phụ cấp đi lại. ◆ Du lịch thường niên, teambuilding tổ chức định kỳ. ◆ Tham gia tiệc Công ty hàng tháng. ◆ 12+ ngày phép, 1 ngày nghỉ sinh nhật và các ngày nghỉ lễ theo quy định của pháp luật. ◆ Tham gia tất cả các loại bảo hiểm theo luật định, đóng bảo hiểm full lương. ◆ Khám sức khỏe định kỳ hàng năm. ◆ Tham gia các khóa kỹ năng mềm trong và ngoài Công ty. ◆ Đào tạo tiếng Nhật miễn phí, hỗ trợ thi chứng chỉ và thưởng khi đạt được chứng chỉ. ◆ Cơ hội Onsite Nhật Bản. ◆ Ngoài ra ở chi nhánh Hà Nội: nhân viên sẽ có cơ hội vào TP. HCM công tác thường xuyên, được offer mức lương cạnh tranh.', NULL, NULL, 1, CAST(N'2019-02-03' AS Date), CAST(N'2026-10-06' AS Date), 0, 5026, 23, 2, 1, N'ntd9@gmail.com', N'0283973423', N'Nam Nguyễn', 0, NULL, N'Đã duyệt', 1, 686, 0)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (7014, N'Personal Assistant to General Director( communicative English.', N'Director', N'Quản lý', 10, N'Tham gia thiết kế và phát triển hệ sinh thái phần mềm VinDr của VinBDI, trong đó bao gồm các giải pháp ứng dụng trí tuệ nhân tạo cho chẩn đoán hình ảnh y tế, bao gồm:
● Xây dựng RestAPI service cho hệ thống VinDR
● Kết hợp với team AI để xây dựng hệ thống chuẩn đoán
● Xây dựng tài liệu và unittest
● Hỗ trợ triển khai hệ thống tại cloud và tại hạ tầng khách hàng', N'Tốt nghiệp Đại học chuyên ngành tài chính, ngân hàng, kế toán, kiểm toán hoặc các chuyên ngành liên quan.
Ưu tiên ứng viên có chứng chỉ CPA, ACCA hoặc CFA; có kiến thức về xuất nhập khẩu- lĩnh vực thanh toán quốc tế.
Có khả năng lập kế hoạch, báo cáo.
Có kỹ năng trình bày, làm việc đội nhóm, làm việc dưới áp lực cao.
Có khả năng phân tích và tổng hợp vấn đề. Nhạy bén với các biến động, xu hướng tài chính.
Có tính kiên nhẫn, cẩn thận, chính xác.
Đọc, hiểu bằng tiếng Anh tốt. "', N'- Strong problem solving skills and the ability to grasp and effectively use technical concepts and technical information. - Highly motivated, self-starter, and active learner with strong interpersonal, communication, written and oral skills.', 5000000.0000, NULL, 0, CAST(N'2019-02-03' AS Date), CAST(N'2026-10-06' AS Date), 1, 5026, 23, 1, 1, N'ntd9@gmail.com', N'0283973423', N'Nam Nguyễn', 0, NULL, N'Đã duyệt', 1, 905, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (8018, N'SUNTECH VINA KCN Khắc Niệm Tuyển 500 Nữ CMND gốc đi làm ngay', N'Lao Động phổ thông ', N'Quản lý', NULL, N'TUYỂN 1000   NỮ

Làm dán QC Kiểm Hàng tại cty  SUNTECH - KCN Khắc Niệm 

Lương Cơ Bản : 4,000,000 VNĐ 

Phụ Cấp Thêm 1-1TR200K  

Luôn đảm bảo mức lương từ 7,5tr đến 9,5tr.

LIÊN HỆ : Ms Kỷ :  0562090000

CÔNG TY NUÔI CƠM 2 BỮA MIỄN PHÍ Ở CTY .

Công ty sẽ hỗ trợ tìm phòng trọ và không thu bất kì phí tuyển dụng nào của người lao động.', N'TUYỂN 1000   NỮ

Làm dán QC Kiểm Hàng tại cty  SUNTECH - KCN Khắc Niệm 

Lương Cơ Bản : 4,000,000 VNĐ 

Phụ Cấp Thêm 1-1TR200K  

Luôn đảm bảo mức lương từ 7,5tr đến 9,5tr.

LIÊN HỆ : Ms Kỷ :  0562090000

CÔNG TY NUÔI CƠM 2 BỮA MIỄN PHÍ Ở CTY .

Công ty sẽ hỗ trợ tìm phòng trọ và không thu bất kì phí tuyển dụng nào của người lao động.', N'TUYỂN 1000   NỮ

Làm dán QC Kiểm Hàng tại cty  SUNTECH - KCN Khắc Niệm 

Lương Cơ Bản : 4,000,000 VNĐ 

Phụ Cấp Thêm 1-1TR200K  

Luôn đảm bảo mức lương từ 7,5tr đến 9,5tr.

LIÊN HỆ : Ms Kỷ :  0562090000

CÔNG TY NUÔI CƠM 2 BỮA MIỄN PHÍ Ở CTY .

Công ty sẽ hỗ trợ tìm phòng trọ và không thu bất kì phí tuyển dụng nào của người lao động.', 10000000.0000, 20000000.0000, 0, CAST(N'2020-07-10' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5026, 23, 1, 0, N'ntd9@gmail.com', N'0346006260', N'dddd', 0, NULL, N'Đã duyệt', 1, 388, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (8020, N'Nhân Viên Kinh Doanh Nội Thất', N'Nhân Viên Kinh Doanh', N'Nhân viên', NULL, N'- Tìm hiểu và mở rộng thị trường kinh doanh của công ty
- Tư vấn và bán các sản phẩm công ty kinh doanh và được phân công phụ trách
- Phối hợp với các phòng ban trong công ty thực hiện các yêu cầu cung cấp dịch vụ cho khách hàng;
- Đề xuất các chính sách bán hàng, chương trình hậu mã
- Sản phẩm là nội thất nhập khẩu Châu Âu
- Trao đổi chi tiết công việc trong quá trình phỏng vấn.', N'- Yêu cầu trình độ Trung cấp trở lên.
- Ưu tiên ứng viên có kinh nghiệm bán và tư vấn nội thất ít nhất 1 năm.
- Có khả năng nắm bắt công việc nhanh, làm việc với áp lực cao
- Nhiệt tình, trung thực, nhanh nhẹn, xử lý tình huống linh hoạt, chủ động; Khả năng truyền đạt, giao tiếp, xây dựng mối quan hệ tốt.', N'1.CV cá nhân (mang theo khi PV)
2.Sơ yếu lý lịch bản gốc
3.Giấy khai sinh bản sao hoặc công chứng
4.Giấy khám sức khỏe có xác nhận có giá trị trong vòng 3 tháng
5.Chứng minh thư photo có xác nhận của địa phương
6.Bằng tốt nghiệp bản sao hoặc Giấy chứng nhận sinh viên (nếu có).', 1000.0000, 1100000.0000, 0, CAST(N'2020-07-13' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5026, 23, 2, 0, N'ntd9@gmail.com', N'0346006260', N'Anh Hưng Nè', 0, NULL, N'Chưa duyệt', 1, 943, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (8021, N'Chuyên Viên Hệ Thống', N'Chuyên Viên Hệ Thống', N'Nhân viên', NULL, N'- Đề xuất các Phòng ban các chương trình cải tiến hệ thống QLCL nhằm đảm bảo an toàn, lãng phí, nâng cao hiệu quả công việc;
- Đầu mối tiếp nhận và phản hồi thông tin đến các Đơn vị liên quan các chương trình cải tiến Hệ thống quản lý chất lượng của Công ty;
- Rà soát TLQL hiện hành, cập nhập điều chỉnh nội dung cho phù hợp thực tế định hướng phát triển theo quy định của Công ty;
- Tổ chức đánh giá nội bộ để xem xét áp dụng tài liệu các Đơn vị;
- Tổ chức đánh giá chứng nhận ISO;
- Phổ biến, triển khai đơn vị các tài liệu ban hành của Công ty;
- Phối hợp thực hiện xây dựng và thẩm định toàn bộ TLQL của Công ty thành viên', N'- Đề xuất các Phòng ban các chương trình cải tiến hệ thống QLCL nhằm đảm bảo an toàn, lãng phí, nâng cao hiệu quả công việc;
- Đầu mối tiếp nhận và phản hồi thông tin đến các Đơn vị liên quan các chương trình cải tiến Hệ thống quản lý chất lượng của Công ty;
- Rà soát TLQL hiện hành, cập nhập điều chỉnh nội dung cho phù hợp thực tế định hướng phát triển theo quy định của Công ty;
- Tổ chức đánh giá nội bộ để xem xét áp dụng tài liệu các Đơn vị;
- Tổ chức đánh giá chứng nhận ISO;
- Phổ biến, triển khai đơn vị các tài liệu ban hành của Công ty;
- Phối hợp thực hiện xây dựng và thẩm định toàn bộ TLQL của Công ty thành viên', N'- Đề xuất các Phòng ban các chương trình cải tiến hệ thống QLCL nhằm đảm bảo an toàn, lãng phí, nâng cao hiệu quả công việc;
- Đầu mối tiếp nhận và phản hồi thông tin đến các Đơn vị liên quan các chương trình cải tiến Hệ thống quản lý chất lượng của Công ty;
- Rà soát TLQL hiện hành, cập nhập điều chỉnh nội dung cho phù hợp thực tế định hướng phát triển theo quy định của Công ty;
- Tổ chức đánh giá nội bộ để xem xét áp dụng tài liệu các Đơn vị;
- Tổ chức đánh giá chứng nhận ISO;
- Phổ biến, triển khai đơn vị các tài liệu ban hành của Công ty;
- Phối hợp thực hiện xây dựng và thẩm định toàn bộ TLQL của Công ty thành viên', NULL, NULL, 1, CAST(N'2020-07-14' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5026, 23, 1, 0, N'ntd9@gmail.com', N'0987332231', N'Công Ty CP Đầu Tư Xây Dựng Trung Nam', 0, NULL, N'Đã duyệt', 1, 709, 0)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (8022, N'Nhân Viên Thiết Kế - Graphic Designer Lương Trên 10 Triệu', N'Nhân Viên Thiết Kế ', N'Nhân viên', NULL, N'- Quản lý, đảm bảo các yêu cầu thiết kế của khách hang.
- Thiết kế, xử lý file chế bản.
- Chịu trách nhiệm thiết kế toàn bộ các ấn phẩm quảng cáo Online và Offline khi Công ty có yêu cầu.
- Thực hiện mọi công việc: xóa, sửa, tách nền, cắt, ghép… các hình ảnh chất lượng (Tự sản xuất hay khai thác được) để sử dụng, phục vụ cho mục đích Kinh doanh và Marketing của Công ty.', N'+ Tốt nghiệp Cao đẳng, Đại học chuyên ngành thiết kế / Mỹ thuật công nghiệp…
+ Sáng tạo và có khả năng nghiên cứu, nắm bắt trend nhanh cùng các xu hướng thiết kế mới.
+ Am hiểu và sử dụng thành thạo công nghệ thiết kế bằng HTML5.
- Kỹ năng:
+ Sử dụng thành thạo các phần mềm thiết kế và chỉnh sửa hình ảnh chuyên nghiệp gồm Photoshop, AI, AE, Illustrator / Corel, Premiere
+ Ưu tiên ứng viên có kinh nghiệm làm việc trong ngành in.
+ Có đam mê và hiểu biết các kỹ năng cơ bản về nhiếp ảnh, quay, dựng videos… là lợi thế.
+ Có kỹ năng lập kế hoạch và quản lý tiến độ công việc tốt.
- Kinh nghiệm:
+ Có ít nhất 2 năm kinh nghiệm làm trong lĩnh vực thiết kế quảng cáo, thiết kế thương hiệu, thiết kế HTML5…', N'- Đơn xin việc.
- Sơ yếu lý lịch.
- Hộ khẩu, chứng minh nhân dân và giấy khám sức khỏe.
- Các bằng cấp có liên quan.', 10000000.0000, 12000000.0000, NULL, CAST(N'2020-07-22' AS Date), CAST(N'2026-10-06' AS Date), NULL, 1002, 1, 2, NULL, N'user111@gmail.com', N'0987332231', N'Phòng Hành Chính Nhân Sự', 0, NULL, N'Đã duyệt', NULL, 758, 0)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (9022, N'ádddddddddddddddddđ', N'dassss', N'Nhân viên', NULL, N'qưeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee', N'qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq
qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq
qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq
qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq', N'qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq
qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq
qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq
qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq', NULL, NULL, 1, CAST(N'2020-07-25' AS Date), CAST(N'2026-10-06' AS Date), NULL, 1, 1, 2, NULL, N'user111@gmail.com', N'0987332231', N'adssssss', 0, NULL, N'Đã xóa', NULL, 0, 0)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (9023, N'qqqqqqqqqe', N'eeeeeeeeee', N'Nhân viên', NULL, N'eeeeeeeeeeeeee', N'eeeeeeeeeeeeeeeeee', N'eeeeeeeeeeeeeeeeeeeee', NULL, NULL, 1, CAST(N'2020-07-25' AS Date), CAST(N'2026-10-06' AS Date), NULL, 1002, 1, 3, NULL, N'user111@gmail.com', N'0987332231', N'eeeeeeeeeeeeeee', 0, NULL, N'Đã xóa', NULL, 0, 0)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (9024, N'qqqqqqqqqe', N'eeeeeeeeee', N'Nhân viên', NULL, N'eeeeeeeeeeeeee', N'eeeeeeeeeeeeeeeeee', N'eeeeeeeeeeeeeeeeeeeee', NULL, NULL, 1, CAST(N'2020-07-05' AS Date), CAST(N'2026-10-06' AS Date), NULL, 1010, 1, 3, NULL, N'user111@gmail.com', N'0987332231', N'eeeeeeeeeeeeeee', 0, NULL, N'Đã xóa', NULL, 0, 0)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (9026, N'qqqqqqqqqqqqqqqqqqqqqqwwwwwwww', N'qqqqqqqqqqqqqqqq', N'Quản lý', NULL, N'qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq
qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq', N'qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq
qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq
qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq
qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq
qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq', N'qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq
qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq
qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq
qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq
', NULL, NULL, 1, CAST(N'2020-07-17' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5053, 1089, 2, NULL, N'asxi1998@gmail.com', N'031145254', N'qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq', 0, NULL, N'Đã xóa', NULL, 0, 0)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (9030, N'qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq', N'qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq', N'Quản lý', NULL, N'qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq', N'qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq', N'qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq', NULL, NULL, 1, CAST(N'2020-07-20' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5053, 1089, 3, NULL, N'asxi1998@gmail.com', N'0346006260', N'qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq', 0, NULL, N'Đã xóa', NULL, 0, 0)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (9032, N'qqqqqqqqqqqqqqqqqq', N'qqqqqqqqqqq', N'Nhân viên', NULL, N'qqqqqqqqqqqqqqqqq', N'qqqqqqqqqqqqqqqq', N'qqqqqqqqqqqqqqqqqqqq', NULL, NULL, 1, CAST(N'2020-07-15' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5053, 1089, 3, NULL, N'asxi1998@gmail.com', N'111111111', N'qqqqqqqqqqqqq', 0, NULL, N'Đã xóa', NULL, 0, 0)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (9033, N'qqqqqqqqqqqqqqqqqq', N'qqqqqqqqqqq', N'Nhân viên', NULL, N'qqqqqqqqqqqqqqqqq', N'qqqqqqqqqqqqqqqq', N'qqqqqqqqqqqqqqqqqqqq', NULL, NULL, 1, CAST(N'2020-07-25' AS Date), CAST(N'2026-10-06' AS Date), NULL, 5053, 1089, 3, NULL, N'asxi1998@gmail.com', N'111111111', N'qqqqqqqqqqqqq', 0, NULL, N'Đã xóa', NULL, 0, 0)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (10022, N'Giám Đốc Bộ Phận Phát Triển Thị Trường Người Việt Nam', N'Giám đốc', N'Quản lý', NULL, N'YÊU CẦU CÔNG VIỆC
*YÊU CẦU:

- Tốt nghiệp đại học các chuyên ngành kinh tế, quản trị kinh doanh,…
- Có tầm nhìn và chiến lược thương mại tốt, có định hướng, phương thức tiếp cận bán hàng để phát triển tại trị trường Việt Nam.
- Có kỹ năng tốt trong xây dựng và triển khai chiến lược bán hàng, phát triển thị trường
- Có kinh nghiệm từ 3-5 năm làm việc trong ngành sợi, từng làm việc trong lĩnh vực thời trang có thương hiệu hoặc các nhà máy may mặc, có kiến thức về lưu trình thời trang
- Có thể nhận biết được các loại vải, nhạy cảm với xu hướng thị trường thời trang, yêu thích phát triển sản phẩm
- Ngoại ngữ: thông thạo tiếng Anh hoặc tiếng Hoa hoặc tiếng Nhật. Giỏi giao tiếp tạo mối quan hệ tốt với khách hàng
- Làm việc nghiêm túc có trách nhiệm và trung thực.


*** Chế độ phúc lợi:
- Lương, thưởng hấp dẫn
- Chế độ BHXH, BHYT... theo quy định của công ty
- Môi trường làm việc thân thiện và năng động
- Thu nhập xứng đáng và hấp dẫn
- Cơ hội thăng tiến cao
Xem toàn bộ Yêu Cầu Công Việc', N'YÊU CẦU CÔNG VIỆC
*YÊU CẦU:

- Tốt nghiệp đại học các chuyên ngành kinh tế, quản trị kinh doanh,…
- Có tầm nhìn và chiến lược thương mại tốt, có định hướng, phương thức tiếp cận bán hàng để phát triển tại trị trường Việt Nam.
- Có kỹ năng tốt trong xây dựng và triển khai chiến lược bán hàng, phát triển thị trường
- Có kinh nghiệm từ 3-5 năm làm việc trong ngành sợi, từng làm việc trong lĩnh vực thời trang có thương hiệu hoặc các nhà máy may mặc, có kiến thức về lưu trình thời trang
- Có thể nhận biết được các loại vải, nhạy cảm với xu hướng thị trường thời trang, yêu thích phát triển sản phẩm
- Ngoại ngữ: thông thạo tiếng Anh hoặc tiếng Hoa hoặc tiếng Nhật. Giỏi giao tiếp tạo mối quan hệ tốt với khách hàng
- Làm việc nghiêm túc có trách nhiệm và trung thực.


*** Chế độ phúc lợi:
- Lương, thưởng hấp dẫn
- Chế độ BHXH, BHYT... theo quy định của công ty
- Môi trường làm việc thân thiện và năng động
- Thu nhập xứng đáng và hấp dẫn
- Cơ hội thăng tiến cao
Xem toàn bộ Yêu Cầu Công Việc', N'YÊU CẦU CÔNG VIỆC
*YÊU CẦU:

- Tốt nghiệp đại học các chuyên ngành kinh tế, quản trị kinh doanh,…
- Có tầm nhìn và chiến lược thương mại tốt, có định hướng, phương thức tiếp cận bán hàng để phát triển tại trị trường Việt Nam.
- Có kỹ năng tốt trong xây dựng và triển khai chiến lược bán hàng, phát triển thị trường
- Có kinh nghiệm từ 3-5 năm làm việc trong ngành sợi, từng làm việc trong lĩnh vực thời trang có thương hiệu hoặc các nhà máy may mặc, có kiến thức về lưu trình thời trang
- Có thể nhận biết được các loại vải, nhạy cảm với xu hướng thị trường thời trang, yêu thích phát triển sản phẩm
- Ngoại ngữ: thông thạo tiếng Anh hoặc tiếng Hoa hoặc tiếng Nhật. Giỏi giao tiếp tạo mối quan hệ tốt với khách hàng
- Làm việc nghiêm túc có trách nhiệm và trung thực.


*** Chế độ phúc lợi:
- Lương, thưởng hấp dẫn
- Chế độ BHXH, BHYT... theo quy định của công ty
- Môi trường làm việc thân thiện và năng động
- Thu nhập xứng đáng và hấp dẫn
- Cơ hội thăng tiến cao
Xem toàn bộ Yêu Cầu Công Việc', NULL, NULL, 1, CAST(N'2020-07-28' AS Date), CAST(N'2026-10-06' AS Date), NULL, 1003, 1, 1, NULL, N'user111@gmail.com', N'0987332231', N'Phòng HCNS', 0, NULL, N'Đã duyệt', NULL, 0, 0)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (10023, N'2111111', N'111111111', N'Nhân viên', NULL, N'1111111111111111', N'111111111111111111', N'111111111111111111', NULL, NULL, 1, CAST(N'2020-07-08' AS Date), CAST(N'2026-10-06' AS Date), NULL, 1003, 1, 2, NULL, N'user111@gmail.com', N'0987332231', N'1111111', 0, NULL, N'Đã xóa', NULL, 0, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (10024, N'Nhân Viên Triển Khai BIM (Không Cần Kinh Nghiệm - Nộp CV Bằng Tiếng Anh)', N'Nhân Viên Triển Khai BIM ', N'Nhân viên', NULL, N'YÊU CẦU CÔNG VIỆC
* Ưu tiên các bạn sinh viên chuẩn bị tốt nghiệp hoặc mới ra trường.
* Phương thức xét tuyển:
- Lọc hồ sơ
- Team leader và CEO phỏng vấn ứng viên được chọn
* Yêu cầu về bằng cấp:
- Cử nhân chuyên ngành Kỹ thuật kiến trúc hoặc tốt nghiệp cao đẳng với chuyên ngành như trên.
- Không yêu cầu kinh nghiệm
* Ưu tiên các ứng viên có các kỹ năng:
- Sử dụng tốt AutoCAD', N'YÊU CẦU CÔNG VIỆC
* Ưu tiên các bạn sinh viên chuẩn bị tốt nghiệp hoặc mới ra trường.
* Phương thức xét tuyển:
- Lọc hồ sơ
- Team leader và CEO phỏng vấn ứng viên được chọn
* Yêu cầu về bằng cấp:
- Cử nhân chuyên ngành Kỹ thuật kiến trúc hoặc tốt nghiệp cao đẳng với chuyên ngành như trên.
- Không yêu cầu kinh nghiệm
* Ưu tiên các ứng viên có các kỹ năng:
- Sử dụng tốt AutoCAD', N'YÊU CẦU CÔNG VIỆC
* Ưu tiên các bạn sinh viên chuẩn bị tốt nghiệp hoặc mới ra trường.
* Phương thức xét tuyển:
- Lọc hồ sơ
- Team leader và CEO phỏng vấn ứng viên được chọn
* Yêu cầu về bằng cấp:
- Cử nhân chuyên ngành Kỹ thuật kiến trúc hoặc tốt nghiệp cao đẳng với chuyên ngành như trên.
- Không yêu cầu kinh nghiệm
* Ưu tiên các ứng viên có các kỹ năng:
- Sử dụng tốt AutoCAD', NULL, NULL, 1, CAST(N'2020-07-18' AS Date), CAST(N'2026-10-06' AS Date), NULL, 1, 1, 2, NULL, N'user111@gmail.com', N'0987332231', N'HCNS', 0, NULL, N'Đã duyệt', NULL, 0, 0)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (11022, N'Quản lý hệ thống cửa hàng Cà Phê', N'Quản lý', N'Quản lý', NULL, N'Việc làm đầy áp lực', N'Nhanh nhẹn, có tầm nhìn, trình độ chuyên môn cao.', N'Kỹ năng giao tiếp, quản trị nhân sự', NULL, NULL, 1, CAST(N'2020-07-28' AS Date), CAST(N'2026-10-06' AS Date), NULL, 6053, 2089, 2, NULL, N'mrvanngoc182@gmail.com', N'0234124132', N'Mr. Hòa', 0, NULL, N'Đã duyệt', NULL, 0, 0)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (11023, N'ádas', N'ádadsa', N'Nhân viên', NULL, N'ádsada', N'dấd', N'dâd', NULL, NULL, 1, CAST(N'2020-07-28' AS Date), CAST(N'2026-10-06' AS Date), NULL, 6053, 2089, 2, NULL, N'mrvanngoc182@gmail.com', N'02132131312', N'adsada', 1, NULL, N'Chưa duyệt', NULL, 0, NULL)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (11024, N'11111111111111111111111', N'qqqqqqqqqqqqqq', N'Nhân viên', NULL, N'qqqqqqqqqqqqqqqqqqqqqqqqq', N'qqqqqqqqqqqqqqqqq', N'qqqqqqqqqqqqqqqqqqqqqqq', NULL, NULL, 1, CAST(N'2020-07-28' AS Date), CAST(N'2026-10-06' AS Date), NULL, 6053, 2089, 3, NULL, N'mrvanngoc182@gmail.com', N'1312312313', N'qqqqqqqq', 0, NULL, N'Đã xóa', NULL, 0, 0)
GO

INSERT [dbo].[DANGTINTUYENDUNG] ([ID_TINTUYENDUNG], [TIEUDE], [CHUCDANH], [CAPBAC], [SOLUONG], [MOTACONGVIEC], [YEUCAUCONGVIEC], [YEUCAUKYNANG], [MINLUONG], [MAXLUONG], [THUONGLUONG], [NGAYDANG], [NGAYHETHAN], [GIOITINH], [ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_LOAICONGVIEC], [KINHNGHIEM], [EMAILLIENHE], [SDTLIENHE], [NGUOILIENHE], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [DATASORT]) VALUES (11025, N'qqweeeeeeeeeeeeeeeeeeeeee', N'qeweqweqweqw', N'Nhân viên', NULL, N'qwe qdqwdq dqw
qwe qdqwdq dqw
qwe qdqwdq dqw
qwe qdqwdq dqw
qwe qdqwdq dqw', N'qwe qdqwdq dqw
qwe qdqwdq dqw
qwe qdqwdq dqw
qwe qdqwdq dqw
qwe qdqwdq dqw', N'qwe qdqwdq dqw
qwe qdqwdq dqw
qwe qdqwdq dqw
qwe qdqwdq dqw
qwe qdqwdq dqw', NULL, NULL, 1, CAST(N'2020-07-28' AS Date), CAST(N'2026-10-06' AS Date), NULL, 1003, 1, 2, NULL, N'user111@gmail.com', N'0987332231', N'P. HCNS', 0, NULL, N'Chưa duyệt', NULL, 0, NULL)
GO

SET IDENTITY_INSERT [dbo].[DANGTINTUYENDUNG] OFF
GO

SET IDENTITY_INSERT [dbo].[DIADIEMLAMVIEC] ON
GO

INSERT [dbo].[DIADIEMLAMVIEC] ([ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_THANHPHO], [DIACHI]) VALUES (1, 1, 79, N'26/11 Trần Văn Mười, ấp. Xuân Thới Đông 1, X. Xuân Thới Đông, H. Hóc Môn, Tp. Hồ Chí Minh')
GO

INSERT [dbo].[DIADIEMLAMVIEC] ([ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_THANHPHO], [DIACHI]) VALUES (2, 1, 79, N'1A10 Nguyễn Thái Sơn, P. 3, Q. Gò Vấp, Tp. Hồ Chí Minh')
GO

INSERT [dbo].[DIADIEMLAMVIEC] ([ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_THANHPHO], [DIACHI]) VALUES (3, 2, 79, N'Lầu 15, Phòng 1504, 2A-4A Tôn Đức Thắng, P. Bến Nghé, Q. 1, Tp. Hồ Chí Minh')
GO

INSERT [dbo].[DIADIEMLAMVIEC] ([ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_THANHPHO], [DIACHI]) VALUES (1002, 1, 79, N'Khu Công Nghiệp Tân Bình, Lô IV-18,Tây Thạnh, P. Tây Thạnh, Q. Tân Phú, Tp. Hồ Chí Minh')
GO

INSERT [dbo].[DIADIEMLAMVIEC] ([ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_THANHPHO], [DIACHI]) VALUES (1003, 1, 79, N'Số 29-30 Đường số 2, Khu Phố Hưng Gia 5, Phường Tân Phong, Quận 7 Tp. Hồ Chí Minh')
GO

INSERT [dbo].[DIADIEMLAMVIEC] ([ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_THANHPHO], [DIACHI]) VALUES (1010, 1, 79, N'Số 29-30 Đường số 2, Khu Phố Hưng Gia 5, Phường Tân Phong, Quận 7 Tp. Hồ Chí Minh')
GO

INSERT [dbo].[DIADIEMLAMVIEC] ([ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_THANHPHO], [DIACHI]) VALUES (2002, 1, 79, N'70/48/7 ấp 3, X. Nhị Bình, H. Hóc Môn, Tp. Hồ Chí Minh ')
GO

INSERT [dbo].[DIADIEMLAMVIEC] ([ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_THANHPHO], [DIACHI]) VALUES (3002, 2, 79, N'92 Nguyễn Thị Nhỏ, P. 15, Q. 11, Tp. Hồ Chí Minh')
GO

INSERT [dbo].[DIADIEMLAMVIEC] ([ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_THANHPHO], [DIACHI]) VALUES (3003, 2, 79, N'1901 Sai Gon Trade Center, 37 Tôn Đức Thắng, P . Bến Nghé, Q. 1, Tp. Hồ Chí Minh')
GO

INSERT [dbo].[DIADIEMLAMVIEC] ([ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_THANHPHO], [DIACHI]) VALUES (4002, 25, 79, N'77 An Dương Vương, P. An Lạc A, Q. Bình Tân, Tp. Hồ Chí Minh')
GO

INSERT [dbo].[DIADIEMLAMVIEC] ([ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_THANHPHO], [DIACHI]) VALUES (4003, 25, 1, N'198 Trần Quang Khải, Phường Lý Thái Tổ, Quận Hoàn Kiếm, Hà Nội')
GO

INSERT [dbo].[DIADIEMLAMVIEC] ([ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_THANHPHO], [DIACHI]) VALUES (5003, 86, 75, N'140 Lê trọng tấn, Quận tân phú, TP HCM')
GO

INSERT [dbo].[DIADIEMLAMVIEC] ([ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_THANHPHO], [DIACHI]) VALUES (5004, 86, 1, N'Tầng 1, Toà nhà IC, Số 82 Duy Tân, P. Dịch Vọng Hậu, Cầu Giấy, Hà Nội')
GO

INSERT [dbo].[DIADIEMLAMVIEC] ([ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_THANHPHO], [DIACHI]) VALUES (5005, 6, 1, N'360 Kim Mã Q. Ba Đình')
GO

INSERT [dbo].[DIADIEMLAMVIEC] ([ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_THANHPHO], [DIACHI]) VALUES (5006, 6, 1, N'40 Cát Linh, Q. Đống Đa')
GO

INSERT [dbo].[DIADIEMLAMVIEC] ([ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_THANHPHO], [DIACHI]) VALUES (5007, 6, 1, N' 44B Lý Thường Kiệt, Q. Hoàn Kiếm')
GO

INSERT [dbo].[DIADIEMLAMVIEC] ([ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_THANHPHO], [DIACHI]) VALUES (5008, 7, 75, N'Số 45, Hẻm 34, Tổ 6, KP 3, P. Trảng Dài, TP. Biên Hoà, Đồng Nai')
GO

INSERT [dbo].[DIADIEMLAMVIEC] ([ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_THANHPHO], [DIACHI]) VALUES (5009, 7, 75, N'232 Quốc Lộ 1K, P. Bửu Long, TP. Biên Hòa, Đồng Nai')
GO

INSERT [dbo].[DIADIEMLAMVIEC] ([ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_THANHPHO], [DIACHI]) VALUES (5010, 7, 75, N'152/14/2 Tổ 6, Ấp Bình Lâm, Xã Lộc An, H. Long Thành, Đồng Nai')
GO

INSERT [dbo].[DIADIEMLAMVIEC] ([ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_THANHPHO], [DIACHI]) VALUES (5011, 8, 79, N'29 Lê Duẩn, P. Bến Nghé, Q. 1, Tp. Hồ Chí Minh')
GO

INSERT [dbo].[DIADIEMLAMVIEC] ([ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_THANHPHO], [DIACHI]) VALUES (5012, 8, 79, N' Tòa Star Building 33TER-33BIS, Mạc Đĩnh Chi, Q. 1, Tp. Hồ Chí Minh')
GO

INSERT [dbo].[DIADIEMLAMVIEC] ([ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_THANHPHO], [DIACHI]) VALUES (5013, 8, 79, N'Số 60 Nguyễn Đình Chiểu, P. Đăk, Q. 1, Tp. Hồ Chí Minh')
GO

INSERT [dbo].[DIADIEMLAMVIEC] ([ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_THANHPHO], [DIACHI]) VALUES (5014, 9, 79, N' 2-4 Lưu Văn Lang, P. Bến Thành, Q. 1, Tp. Hồ Chí Minh')
GO

INSERT [dbo].[DIADIEMLAMVIEC] ([ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_THANHPHO], [DIACHI]) VALUES (5017, 10, 70, N' 239 QL14 P.Tân Xuân, Thị xã Đồng Xoài')
GO

INSERT [dbo].[DIADIEMLAMVIEC] ([ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_THANHPHO], [DIACHI]) VALUES (5020, 11, 48, N' 60 Mẹ Nhu, phường Thanh Khê Tây, quận Thanh Khê, thành phố Đà Nẵng.')
GO

INSERT [dbo].[DIADIEMLAMVIEC] ([ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_THANHPHO], [DIACHI]) VALUES (5023, 12, 48, N'Ðường số 2, KCN Hòa Cầm, quận Cẩm Lệ, TP. Đà Nẵng.')
GO

INSERT [dbo].[DIADIEMLAMVIEC] ([ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_THANHPHO], [DIACHI]) VALUES (5026, 23, 79, NULL)
GO

INSERT [dbo].[DIADIEMLAMVIEC] ([ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_THANHPHO], [DIACHI]) VALUES (5027, 24, 79, NULL)
GO

INSERT [dbo].[DIADIEMLAMVIEC] ([ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_THANHPHO], [DIACHI]) VALUES (5028, 25, 79, NULL)
GO

INSERT [dbo].[DIADIEMLAMVIEC] ([ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_THANHPHO], [DIACHI]) VALUES (5029, 26, 48, N'96 đường Yết Kiêu, phường Thọ Quang, quận Sơn Trà, thành phố Đà Nẵng.')
GO

INSERT [dbo].[DIADIEMLAMVIEC] ([ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_THANHPHO], [DIACHI]) VALUES (5030, 27, 48, N'Đường số 11B, KCN Thanh Vinh, quận Liên Chiểu, TP. Đà Nẵng.')
GO

INSERT [dbo].[DIADIEMLAMVIEC] ([ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_THANHPHO], [DIACHI]) VALUES (5031, 28, 1, N'58 Tây Hồ, Q. Tây Hồ')
GO

INSERT [dbo].[DIADIEMLAMVIEC] ([ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_THANHPHO], [DIACHI]) VALUES (5032, 29, 1, N' 25 Ngọc Khánh, Q. Ba Đình')
GO

INSERT [dbo].[DIADIEMLAMVIEC] ([ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_THANHPHO], [DIACHI]) VALUES (5033, 30, 2, N'40 Hoàng Diệu, phường Phước Ninh,')
GO

INSERT [dbo].[DIADIEMLAMVIEC] ([ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_THANHPHO], [DIACHI]) VALUES (5034, 31, 2, N'234 Nguyễn Văn Linh')
GO

INSERT [dbo].[DIADIEMLAMVIEC] ([ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_THANHPHO], [DIACHI]) VALUES (5035, 32, 72, N'26 Bạch Đằng, phường Thạch Thang')
GO

INSERT [dbo].[DIADIEMLAMVIEC] ([ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_THANHPHO], [DIACHI]) VALUES (5036, 33, 72, N' 2 Phan Đình Phùng')
GO

INSERT [dbo].[DIADIEMLAMVIEC] ([ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_THANHPHO], [DIACHI]) VALUES (5037, 34, 72, N'đường Võ Nguyên Giáp')
GO

INSERT [dbo].[DIADIEMLAMVIEC] ([ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_THANHPHO], [DIACHI]) VALUES (5038, 35, 79, N'số 92 đường 2 Tháng 9, phường Bình Thuận')
GO

INSERT [dbo].[DIADIEMLAMVIEC] ([ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_THANHPHO], [DIACHI]) VALUES (5039, 36, 79, N'8/1-8/3 Nguyễn Huy Tưởng, P. 6, Q. Bình Thạnh, Tp. Hồ Chí Minh')
GO

INSERT [dbo].[DIADIEMLAMVIEC] ([ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_THANHPHO], [DIACHI]) VALUES (5040, 37, 80, N' số 346, đường 2 Tháng 9, phường Hòa Cường Bắc')
GO

INSERT [dbo].[DIADIEMLAMVIEC] ([ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_THANHPHO], [DIACHI]) VALUES (5041, 38, 20, N' 308 đường 2 Tháng 9, phường Hòa Cường Bắc')
GO

INSERT [dbo].[DIADIEMLAMVIEC] ([ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_THANHPHO], [DIACHI]) VALUES (5042, 39, 20, N' 122 đường 2 Tháng 9, phường Bình Thuận')
GO

INSERT [dbo].[DIADIEMLAMVIEC] ([ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_THANHPHO], [DIACHI]) VALUES (5044, 77, 25, N'192 Xuân Thủy, phường Khuê Trung')
GO

INSERT [dbo].[DIADIEMLAMVIEC] ([ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_THANHPHO], [DIACHI]) VALUES (5045, 78, 10, N' 97 Phan Châu Trinh, phường Phước Ninh,')
GO

INSERT [dbo].[DIADIEMLAMVIEC] ([ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_THANHPHO], [DIACHI]) VALUES (5047, 80, 11, N'số 27 đường Bùi Chát, phường Hòa Khánh Bắc')
GO

INSERT [dbo].[DIADIEMLAMVIEC] ([ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_THANHPHO], [DIACHI]) VALUES (5048, 81, 12, N' số 92 đường 2 Tháng 9, phường Bình Thuận')
GO

INSERT [dbo].[DIADIEMLAMVIEC] ([ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_THANHPHO], [DIACHI]) VALUES (5049, 82, 14, N' 339 Nguyễn Lương Bằng, phường Hòa Khánh Bắc')
GO

INSERT [dbo].[DIADIEMLAMVIEC] ([ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_THANHPHO], [DIACHI]) VALUES (5050, 83, 15, N' 2 Bạch Đằng, phường Thạch Thang')
GO

INSERT [dbo].[DIADIEMLAMVIEC] ([ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_THANHPHO], [DIACHI]) VALUES (5051, 86, 17, N' Lô X1- Khu Biệt thự Đảo xanh, phường Hòa Cường Bắc')
GO

INSERT [dbo].[DIADIEMLAMVIEC] ([ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_THANHPHO], [DIACHI]) VALUES (5052, 87, 56, N' 23 Phan Đình Phùng, phường Hải Châu 1')
GO

INSERT [dbo].[DIADIEMLAMVIEC] ([ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_THANHPHO], [DIACHI]) VALUES (5053, 1089, 1, N'182 / 49 Thoại Ngọc Hầu, Phú Thạnh, Tân Phú, Hồ Chí Minh')
GO

INSERT [dbo].[DIADIEMLAMVIEC] ([ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_THANHPHO], [DIACHI]) VALUES (6053, 2089, 1, N'Hoàng kim giáp, Hà Nội')
GO

INSERT [dbo].[DIADIEMLAMVIEC] ([ID_DIADIEMLAMVIEC], [ID_NHATUYENDUNG], [ID_THANHPHO], [DIACHI]) VALUES (6054, 2089, 1, N'Hoàng kim giáp, Hà Nội')
GO

SET IDENTITY_INSERT [dbo].[DIADIEMLAMVIEC] OFF
GO

SET IDENTITY_INSERT [dbo].[KINHNGHIEMLAMVIEC] ON
GO

INSERT [dbo].[KINHNGHIEMLAMVIEC] ([ID_KINHNGHIEMLAMVIEC], [ID_UNGVIEN], [CHUCDANH], [TENCONGTY], [CHUCVU], [THOIGIANBATDAU], [THOIGIANKETTHUC], [CONGVIECHIENTAI], [GHICHU]) VALUES (5, 7, N'coder', N'DPS', N'nhân viên', CAST(N'2019-01-01' AS Date), CAST(N'2020-08-01' AS Date), 0, N'good')
GO

INSERT [dbo].[KINHNGHIEMLAMVIEC] ([ID_KINHNGHIEMLAMVIEC], [ID_UNGVIEN], [CHUCDANH], [TENCONGTY], [CHUCVU], [THOIGIANBATDAU], [THOIGIANKETTHUC], [CONGVIECHIENTAI], [GHICHU]) VALUES (6, 1, N'coder', N'TMA', N'nhân viên', CAST(N'2020-01-01' AS Date), CAST(N'2020-04-01' AS Date), 0, N'Luôn hoàn thành công việc đúng thời gian')
GO

INSERT [dbo].[KINHNGHIEMLAMVIEC] ([ID_KINHNGHIEMLAMVIEC], [ID_UNGVIEN], [CHUCDANH], [TENCONGTY], [CHUCVU], [THOIGIANBATDAU], [THOIGIANKETTHUC], [CONGVIECHIENTAI], [GHICHU]) VALUES (3003, 8, N'Nhân viên', N'Đại Phát Slution', N'Nhân viên', CAST(N'2020-08-01' AS Date), NULL, 1, N'Hoàn thành tốt')
GO

INSERT [dbo].[KINHNGHIEMLAMVIEC] ([ID_KINHNGHIEMLAMVIEC], [ID_UNGVIEN], [CHUCDANH], [TENCONGTY], [CHUCVU], [THOIGIANBATDAU], [THOIGIANKETTHUC], [CONGVIECHIENTAI], [GHICHU]) VALUES (3004, 1, N'Developer', N'Công ty cổ phần Xây dựng & Công nghiệp NSN', N'Nhân viên', CAST(N'2020-01-01' AS Date), NULL, 1, N'Công ty CP Xây dựng và Công nghiệp NSN được thành lập ngày 10/12/2003 với ngành nghề chính là xây dựng Công nghiệp và Cơ điện, sau quá trình hình thành và phát triển, hiện nay NSN đã trở thành nhà thầu chuyên nghiệp hàng đầu, mở rộng các lĩnh vực kinh doanh: Tư vấn thiết kế, thi công xây dựng các công trình công')
GO

INSERT [dbo].[KINHNGHIEMLAMVIEC] ([ID_KINHNGHIEMLAMVIEC], [ID_UNGVIEN], [CHUCDANH], [TENCONGTY], [CHUCVU], [THOIGIANBATDAU], [THOIGIANKETTHUC], [CONGVIECHIENTAI], [GHICHU]) VALUES (3005, 1018, N'DEV front-end', N'Công ty TNHH COTO TECH', N'Nhân viên', CAST(N'2020-01-01' AS Date), NULL, 1, N'Hoàn thành các công việc theo đúng tiến độ
')
GO

SET IDENTITY_INSERT [dbo].[KINHNGHIEMLAMVIEC] OFF
GO

SET IDENTITY_INSERT [dbo].[KYNANGUNGVIEN] ON
GO

INSERT [dbo].[KYNANGUNGVIEN] ([ID_KYNANGCANHAN], [TENKYNANG], [MOTAKYNANG], [ID_UNGVIEN]) VALUES (1, N'Lập trình C#', N'Biết lập trình trên ngôn ngữ C#', 1)
GO

INSERT [dbo].[KYNANGUNGVIEN] ([ID_KYNANGCANHAN], [TENKYNANG], [MOTAKYNANG], [ID_UNGVIEN]) VALUES (4, N'Lập trình Angular', N'Kỹ năng cao', 1)
GO

INSERT [dbo].[KYNANGUNGVIEN] ([ID_KYNANGCANHAN], [TENKYNANG], [MOTAKYNANG], [ID_UNGVIEN]) VALUES (10, N'Lập trình Java', N'OPP hướng đối tượng
', 1)
GO

INSERT [dbo].[KYNANGUNGVIEN] ([ID_KYNANGCANHAN], [TENKYNANG], [MOTAKYNANG], [ID_UNGVIEN]) VALUES (1002, N'Lập trình Angular', N'Có khả năng viết code TypeScrip', 1018)
GO

INSERT [dbo].[KYNANGUNGVIEN] ([ID_KYNANGCANHAN], [TENKYNANG], [MOTAKYNANG], [ID_UNGVIEN]) VALUES (2002, N'Lập trình Java', N'Code hướng đối tượng ổn áp', 1018)
GO

SET IDENTITY_INSERT [dbo].[KYNANGUNGVIEN] OFF
GO

SET IDENTITY_INSERT [dbo].[LOAICONGVIEC] ON
GO

INSERT [dbo].[LOAICONGVIEC] ([ID_LOAICONGVIEC], [TENLOAICONGVIEC]) VALUES (1, N'Công việc bán thời gian')
GO

INSERT [dbo].[LOAICONGVIEC] ([ID_LOAICONGVIEC], [TENLOAICONGVIEC]) VALUES (2, N'Công việc toàn thời gian')
GO

INSERT [dbo].[LOAICONGVIEC] ([ID_LOAICONGVIEC], [TENLOAICONGVIEC]) VALUES (3, N'Việc làm từ xa')
GO

SET IDENTITY_INSERT [dbo].[LOAICONGVIEC] OFF
GO

INSERT [dbo].[LOAICONGVIECUNGVIEN] ([ID_UNGVIEN], [ID_LOAICONGVIEC]) VALUES (1, 1)
GO

INSERT [dbo].[LOAICONGVIECUNGVIEN] ([ID_UNGVIEN], [ID_LOAICONGVIEC]) VALUES (1, 2)
GO

INSERT [dbo].[LOAICONGVIECUNGVIEN] ([ID_UNGVIEN], [ID_LOAICONGVIEC]) VALUES (1, 3)
GO

INSERT [dbo].[LOAICONGVIECUNGVIEN] ([ID_UNGVIEN], [ID_LOAICONGVIEC]) VALUES (2, 1)
GO

INSERT [dbo].[LOAICONGVIECUNGVIEN] ([ID_UNGVIEN], [ID_LOAICONGVIEC]) VALUES (3, 1)
GO

INSERT [dbo].[LOAICONGVIECUNGVIEN] ([ID_UNGVIEN], [ID_LOAICONGVIEC]) VALUES (3, 2)
GO

INSERT [dbo].[LOAICONGVIECUNGVIEN] ([ID_UNGVIEN], [ID_LOAICONGVIEC]) VALUES (3, 3)
GO

INSERT [dbo].[LOAICONGVIECUNGVIEN] ([ID_UNGVIEN], [ID_LOAICONGVIEC]) VALUES (4, 1)
GO

INSERT [dbo].[LOAICONGVIECUNGVIEN] ([ID_UNGVIEN], [ID_LOAICONGVIEC]) VALUES (6, 3)
GO

INSERT [dbo].[LOAICONGVIECUNGVIEN] ([ID_UNGVIEN], [ID_LOAICONGVIEC]) VALUES (7, 2)
GO

INSERT [dbo].[LOAICONGVIECUNGVIEN] ([ID_UNGVIEN], [ID_LOAICONGVIEC]) VALUES (8, 3)
GO

INSERT [dbo].[LOAICONGVIECUNGVIEN] ([ID_UNGVIEN], [ID_LOAICONGVIEC]) VALUES (9, 2)
GO

INSERT [dbo].[LOAICONGVIECUNGVIEN] ([ID_UNGVIEN], [ID_LOAICONGVIEC]) VALUES (10, 1)
GO

INSERT [dbo].[LUUCONGVIEC] ([ID_UNGVIEN], [ID_TINTUYENDUNG], [NGAYLUU]) VALUES (1, 1005, CAST(N'2020-07-13' AS Date))
GO

INSERT [dbo].[LUUCONGVIEC] ([ID_UNGVIEN], [ID_TINTUYENDUNG], [NGAYLUU]) VALUES (1, 7008, CAST(N'2020-07-14' AS Date))
GO

INSERT [dbo].[LUUCONGVIEC] ([ID_UNGVIEN], [ID_TINTUYENDUNG], [NGAYLUU]) VALUES (1, 7010, CAST(N'2020-07-14' AS Date))
GO

INSERT [dbo].[LUUCONGVIEC] ([ID_UNGVIEN], [ID_TINTUYENDUNG], [NGAYLUU]) VALUES (8, 2017, CAST(N'2020-07-09' AS Date))
GO

INSERT [dbo].[LUUCONGVIEC] ([ID_UNGVIEN], [ID_TINTUYENDUNG], [NGAYLUU]) VALUES (1018, 2745, CAST(N'2020-08-10' AS Date))
GO

INSERT [dbo].[LUUCONGVIEC] ([ID_UNGVIEN], [ID_TINTUYENDUNG], [NGAYLUU]) VALUES (1018, 2782, CAST(N'2020-07-21' AS Date))
GO

INSERT [dbo].[LUUCONGVIEC] ([ID_UNGVIEN], [ID_TINTUYENDUNG], [NGAYLUU]) VALUES (1018, 2853, CAST(N'2020-08-10' AS Date))
GO

INSERT [dbo].[LUUCONGVIEC] ([ID_UNGVIEN], [ID_TINTUYENDUNG], [NGAYLUU]) VALUES (1018, 2854, CAST(N'2020-07-21' AS Date))
GO

INSERT [dbo].[LUUCONGVIEC] ([ID_UNGVIEN], [ID_TINTUYENDUNG], [NGAYLUU]) VALUES (1018, 2868, CAST(N'2020-07-21' AS Date))
GO

INSERT [dbo].[LUUUNGVIEN] ([ID_NHATUYENDUNG], [ID_UNGVIEN], [NGAYLUU]) VALUES (1, 1, CAST(N'2020-08-11' AS Date))
GO

INSERT [dbo].[LUUUNGVIEN] ([ID_NHATUYENDUNG], [ID_UNGVIEN], [NGAYLUU]) VALUES (1, 2, CAST(N'2020-07-16' AS Date))
GO

INSERT [dbo].[LUUUNGVIEN] ([ID_NHATUYENDUNG], [ID_UNGVIEN], [NGAYLUU]) VALUES (1, 7, CAST(N'2020-07-23' AS Date))
GO

INSERT [dbo].[LUUUNGVIEN] ([ID_NHATUYENDUNG], [ID_UNGVIEN], [NGAYLUU]) VALUES (1, 8, CAST(N'2020-07-22' AS Date))
GO

SET IDENTITY_INSERT [dbo].[NGANH] ON
GO

INSERT [dbo].[NGANH] ([ID_NGANH], [TENNGANH]) VALUES (1, N'IT - Phần mềm')
GO

INSERT [dbo].[NGANH] ([ID_NGANH], [TENNGANH]) VALUES (2, N'IT-Phần cứng/Mạng')
GO

INSERT [dbo].[NGANH] ([ID_NGANH], [TENNGANH]) VALUES (3, N'Thiết kế đồ họa')
GO

INSERT [dbo].[NGANH] ([ID_NGANH], [TENNGANH]) VALUES (4, N'Kế toán')
GO

INSERT [dbo].[NGANH] ([ID_NGANH], [TENNGANH]) VALUES (5, N'Xây dựng')
GO

INSERT [dbo].[NGANH] ([ID_NGANH], [TENNGANH]) VALUES (6, N'Kiến trúc/Thiết kế nội thất')
GO

INSERT [dbo].[NGANH] ([ID_NGANH], [TENNGANH]) VALUES (7, N'Bất động sản')
GO

INSERT [dbo].[NGANH] ([ID_NGANH], [TENNGANH]) VALUES (8, N'Viễn Thông')
GO

INSERT [dbo].[NGANH] ([ID_NGANH], [TENNGANH]) VALUES (9, N'Truyền hình/Truyền thông/Báo chí ')
GO

INSERT [dbo].[NGANH] ([ID_NGANH], [TENNGANH]) VALUES (10, N'Mỹ Thuật/Nghệ Thuật/Thiết Kế')
GO

INSERT [dbo].[NGANH] ([ID_NGANH], [TENNGANH]) VALUES (11, N'Quảng cáo/Khuyến mãi/Đối ngoại')
GO

INSERT [dbo].[NGANH] ([ID_NGANH], [TENNGANH]) VALUES (12, N'Internet/Online Media')
GO

INSERT [dbo].[NGANH] ([ID_NGANH], [TENNGANH]) VALUES (13, N'In ấn/ Xuất bản ')
GO

INSERT [dbo].[NGANH] ([ID_NGANH], [TENNGANH]) VALUES (14, N'Ngân hàng')
GO

INSERT [dbo].[NGANH] ([ID_NGANH], [TENNGANH]) VALUES (15, N'Kiểm toán ')
GO

INSERT [dbo].[NGANH] ([ID_NGANH], [TENNGANH]) VALUES (16, N'Tài chính/Đầu tư')
GO

INSERT [dbo].[NGANH] ([ID_NGANH], [TENNGANH]) VALUES (17, N'Chứng khoán')
GO

INSERT [dbo].[NGANH] ([ID_NGANH], [TENNGANH]) VALUES (18, N'Bảo hiểm')
GO

INSERT [dbo].[NGANH] ([ID_NGANH], [TENNGANH]) VALUES (19, N'Hàng không/Du lịch')
GO

INSERT [dbo].[NGANH] ([ID_NGANH], [TENNGANH]) VALUES (20, N'Nhà hàng/Khách sạn')
GO

INSERT [dbo].[NGANH] ([ID_NGANH], [TENNGANH]) VALUES (21, N'Điện/Điện tử')
GO

INSERT [dbo].[NGANH] ([ID_NGANH], [TENNGANH]) VALUES (22, N'Cơ khí')
GO

INSERT [dbo].[NGANH] ([ID_NGANH], [TENNGANH]) VALUES (23, N'Hóa học/Hóa sinh')
GO

INSERT [dbo].[NGANH] ([ID_NGANH], [TENNGANH]) VALUES (24, N'Môi trường/Xử lý chất thải ')
GO

INSERT [dbo].[NGANH] ([ID_NGANH], [TENNGANH]) VALUES (25, N'Bác sĩ')
GO

INSERT [dbo].[NGANH] ([ID_NGANH], [TENNGANH]) VALUES (26, N'Y sĩ/Hộ lý')
GO

INSERT [dbo].[NGANH] ([ID_NGANH], [TENNGANH]) VALUES (27, N'Dược sĩ')
GO

INSERT [dbo].[NGANH] ([ID_NGANH], [TENNGANH]) VALUES (28, N'Nhân sự')
GO

INSERT [dbo].[NGANH] ([ID_NGANH], [TENNGANH]) VALUES (29, N'Vận chuyển')
GO

INSERT [dbo].[NGANH] ([ID_NGANH], [TENNGANH]) VALUES (30, N'Sản xuất')
GO

INSERT [dbo].[NGANH] ([ID_NGANH], [TENNGANH]) VALUES (31, N'Phân phối Thiết bị')
GO

INSERT [dbo].[NGANH] ([ID_NGANH], [TENNGANH]) VALUES (32, N'Tự động hóa/Ô tô')
GO

INSERT [dbo].[NGANH] ([ID_NGANH], [TENNGANH]) VALUES (33, N'Kinh doanh')
GO

SET IDENTITY_INSERT [dbo].[NGANH] OFF
GO

INSERT [dbo].[NGANHNGHEMONGMUON] ([ID_NGANH], [ID_CONGVIECMONGMUON]) VALUES (1, 2031)
GO

INSERT [dbo].[NGANHNGHEMONGMUON] ([ID_NGANH], [ID_CONGVIECMONGMUON]) VALUES (1, 2084)
GO

INSERT [dbo].[NGANHNGHEMONGMUON] ([ID_NGANH], [ID_CONGVIECMONGMUON]) VALUES (1, 4088)
GO

INSERT [dbo].[NGANHNGHEMONGMUON] ([ID_NGANH], [ID_CONGVIECMONGMUON]) VALUES (2, 2044)
GO

INSERT [dbo].[NGANHNGHEMONGMUON] ([ID_NGANH], [ID_CONGVIECMONGMUON]) VALUES (2, 2084)
GO

INSERT [dbo].[NGANHNGHEMONGMUON] ([ID_NGANH], [ID_CONGVIECMONGMUON]) VALUES (2, 4088)
GO

INSERT [dbo].[NGANHNGHEMONGMUON] ([ID_NGANH], [ID_CONGVIECMONGMUON]) VALUES (3, 1028)
GO

INSERT [dbo].[NGANHNGHEMONGMUON] ([ID_NGANH], [ID_CONGVIECMONGMUON]) VALUES (3, 4088)
GO

INSERT [dbo].[NGANHNGHEMONGMUON] ([ID_NGANH], [ID_CONGVIECMONGMUON]) VALUES (4, 1028)
GO

INSERT [dbo].[NGANHNGHEMONGMUON] ([ID_NGANH], [ID_CONGVIECMONGMUON]) VALUES (12, 33)
GO

INSERT [dbo].[NGANHNGHEMONGMUON] ([ID_NGANH], [ID_CONGVIECMONGMUON]) VALUES (16, 31)
GO

INSERT [dbo].[NGANHNGHEMONGMUON] ([ID_NGANH], [ID_CONGVIECMONGMUON]) VALUES (18, 31)
GO

INSERT [dbo].[NGANHNGHEMONGMUON] ([ID_NGANH], [ID_CONGVIECMONGMUON]) VALUES (20, 33)
GO

INSERT [dbo].[NGANHNGHEMONGMUON] ([ID_NGANH], [ID_CONGVIECMONGMUON]) VALUES (21, 31)
GO

INSERT [dbo].[NGANHNGHEMONGMUON] ([ID_NGANH], [ID_CONGVIECMONGMUON]) VALUES (22, 33)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (1, 1)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (1, 2)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (1, 3)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2, 3)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2, 10)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2, 13)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (3, 9)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (3, 12)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (3, 26)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (1005, 3)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (1005, 23)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (1005, 32)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2014, 7)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2014, 12)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2014, 32)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2015, 11)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2015, 13)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2015, 30)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2016, 3)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2016, 9)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2016, 14)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2017, 4)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2017, 12)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2017, 30)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2733, 11)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2733, 23)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2733, 25)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2734, 6)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2734, 28)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2735, 5)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2735, 21)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2735, 24)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2736, 3)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2736, 18)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2736, 29)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2737, 6)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2737, 20)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2737, 29)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2738, 2)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2738, 9)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2738, 15)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2739, 8)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2739, 9)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2739, 12)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2740, 14)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2740, 16)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2740, 26)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2741, 6)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2741, 10)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2741, 14)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2742, 11)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2742, 13)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2742, 16)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2743, 9)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2743, 14)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2743, 31)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2744, 10)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2744, 31)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2744, 33)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2745, 20)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2745, 21)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2745, 24)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2746, 11)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2746, 23)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2746, 26)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2747, 4)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2747, 13)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2747, 18)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2748, 8)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2748, 21)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2748, 31)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2749, 12)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2749, 13)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2749, 30)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2750, 4)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2750, 19)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2750, 29)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2751, 4)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2751, 13)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2751, 27)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2752, 12)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2752, 17)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2752, 26)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2753, 17)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2753, 27)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2753, 32)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2754, 9)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2754, 12)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2754, 15)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2755, 17)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2755, 28)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2755, 33)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2756, 11)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2756, 22)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2756, 32)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2757, 8)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2757, 14)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2757, 15)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2758, 2)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2759, 25)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2759, 29)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2759, 33)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2760, 1)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2760, 2)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2760, 25)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2761, 2)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2761, 7)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2761, 26)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2762, 3)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2762, 26)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2762, 27)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2763, 19)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2763, 20)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2763, 22)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2764, 7)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2764, 18)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2764, 21)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2765, 2)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2765, 10)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2765, 24)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2766, 5)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2766, 12)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2766, 32)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2767, 2)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2767, 24)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2768, 5)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2768, 17)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2768, 27)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2769, 10)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2769, 20)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2769, 21)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2770, 7)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2770, 14)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2770, 17)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2771, 20)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2771, 28)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2771, 31)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2772, 23)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2772, 24)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2772, 31)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2773, 7)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2773, 18)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2773, 19)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2774, 18)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2774, 26)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2774, 32)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2775, 1)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2775, 14)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2775, 30)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2776, 22)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2776, 25)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2776, 28)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2777, 4)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2777, 13)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2777, 26)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2778, 11)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2778, 17)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2778, 26)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2779, 10)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2779, 20)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2779, 24)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2780, 5)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2780, 21)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2780, 28)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2781, 8)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2781, 11)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2781, 18)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2782, 2)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2782, 5)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2782, 18)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2783, 23)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2783, 26)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2783, 30)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2784, 8)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2784, 18)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2784, 30)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2785, 14)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2785, 18)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2785, 33)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2786, 13)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2786, 18)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2786, 19)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2787, 18)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2787, 25)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2787, 30)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2788, 8)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2788, 13)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2788, 30)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2789, 9)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2789, 12)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2789, 32)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2790, 6)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2790, 14)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2790, 26)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2791, 19)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2791, 20)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2791, 21)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2792, 5)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2792, 12)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2792, 29)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2793, 6)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2793, 11)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2793, 18)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2794, 11)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2794, 14)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2794, 33)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2832, 11)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2832, 15)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2832, 30)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2833, 1)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2833, 14)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2833, 19)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2834, 10)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2834, 23)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2834, 27)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2835, 13)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2835, 28)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2836, 7)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2836, 8)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2836, 30)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2837, 4)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2837, 7)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2837, 24)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2838, 8)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2838, 21)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2838, 32)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2839, 17)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2839, 23)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2839, 29)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2840, 9)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2840, 10)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2840, 14)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2841, 17)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2841, 30)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2841, 33)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2842, 4)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2842, 6)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2842, 21)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2843, 9)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2843, 30)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2843, 33)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2844, 8)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2844, 24)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2844, 32)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2845, 4)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2845, 15)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2845, 27)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2846, 14)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2846, 15)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2846, 16)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2847, 9)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2847, 11)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2847, 21)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2848, 2)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2848, 19)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2848, 31)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2849, 3)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2849, 26)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2849, 30)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2850, 20)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2850, 22)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2850, 33)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2851, 12)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2851, 24)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2851, 31)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2852, 27)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2852, 28)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2853, 16)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2853, 25)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2853, 30)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2854, 1)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2854, 2)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2854, 23)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2855, 3)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2855, 6)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2855, 29)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2856, 7)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2856, 14)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2856, 21)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2857, 17)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2857, 19)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2857, 33)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2858, 13)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2858, 18)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2859, 14)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2859, 19)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2859, 31)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2860, 11)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2860, 13)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2860, 16)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2861, 10)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2861, 15)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2861, 27)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2862, 15)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2862, 18)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2862, 31)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2863, 1)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2863, 26)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2863, 27)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2864, 5)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2864, 29)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2864, 32)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2865, 8)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2865, 31)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2865, 32)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2866, 1)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2866, 5)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2866, 18)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2867, 9)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2867, 11)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2867, 31)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2868, 2)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2868, 6)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2868, 15)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2869, 22)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2869, 26)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2869, 33)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2870, 7)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2870, 16)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2870, 30)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2871, 10)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2871, 11)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2871, 17)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2872, 7)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2872, 16)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2872, 27)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2873, 3)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2873, 15)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2873, 21)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2874, 2)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2874, 11)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2874, 29)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2875, 4)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2875, 11)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2875, 20)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2876, 11)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2876, 14)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2876, 28)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2877, 16)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2877, 22)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2877, 27)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2878, 6)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2878, 16)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2878, 27)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2879, 14)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2879, 16)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2879, 21)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2880, 13)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2880, 14)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2880, 22)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2881, 4)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2881, 13)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2881, 25)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2882, 10)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2882, 27)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2883, 10)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2883, 17)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2883, 29)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2884, 14)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2884, 27)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2885, 4)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2885, 6)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2885, 25)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2886, 4)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2886, 8)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2886, 33)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2887, 17)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2887, 28)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2887, 33)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2888, 3)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2888, 7)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2888, 15)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2889, 13)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2889, 22)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2889, 33)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2890, 4)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2890, 22)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2890, 24)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2891, 8)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2891, 17)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2891, 28)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2892, 16)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2892, 18)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2892, 24)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2893, 20)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2893, 27)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2893, 30)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2894, 6)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2894, 10)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2894, 19)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2895, 14)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2895, 18)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2895, 29)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2896, 3)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2896, 12)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2896, 24)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2897, 5)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2897, 19)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2897, 31)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2898, 16)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2898, 21)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2898, 33)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2899, 6)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2899, 23)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2899, 31)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2900, 5)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2900, 19)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2900, 29)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2901, 17)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2901, 27)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2901, 32)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2902, 3)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2902, 18)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2902, 25)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2903, 20)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2903, 21)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2903, 22)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2904, 5)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2904, 13)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2904, 20)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2905, 6)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2905, 30)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2905, 32)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2906, 4)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2906, 20)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2906, 27)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2907, 1)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2907, 7)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2907, 29)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2908, 10)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2908, 17)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2908, 18)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2909, 12)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2909, 14)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2909, 28)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2910, 22)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2910, 26)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2910, 32)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2911, 8)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2911, 24)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2911, 32)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2912, 12)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2912, 23)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2912, 25)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2913, 2)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2913, 20)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2913, 21)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2914, 12)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2914, 21)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2914, 22)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2915, 12)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2915, 14)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2915, 27)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2916, 12)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2916, 18)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2916, 30)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2917, 16)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2917, 18)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2917, 25)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2918, 13)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2918, 17)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2918, 21)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2919, 1)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2919, 9)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2919, 31)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2920, 2)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2920, 9)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2920, 28)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2921, 13)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2921, 19)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2921, 23)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2922, 24)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2922, 25)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2922, 27)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2923, 3)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2923, 23)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2923, 31)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2924, 11)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2924, 15)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2924, 19)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2925, 1)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2925, 25)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2925, 32)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2926, 19)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2926, 31)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2927, 18)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2927, 20)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2927, 30)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2928, 12)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2928, 19)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2928, 33)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2929, 8)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2929, 14)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2929, 29)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2930, 17)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2930, 29)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2930, 30)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2931, 19)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2931, 22)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2931, 30)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2932, 1)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2932, 7)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (2932, 17)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (3005, 4)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (3005, 18)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (3005, 19)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (4005, 6)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (4005, 13)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (4005, 31)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (5005, 2)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (5005, 9)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (5005, 13)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (6005, 8)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (6005, 13)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (6005, 17)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (6006, 4)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (6006, 24)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (6006, 28)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (6007, 8)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (6007, 25)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (6007, 30)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (7006, 6)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (7006, 25)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (7006, 27)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (7007, 2)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (7007, 11)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (7007, 12)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (7008, 1)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (7008, 3)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (7008, 17)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (7009, 3)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (7009, 19)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (7010, 10)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (7010, 22)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (7010, 31)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (7011, 15)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (7011, 16)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (7011, 30)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (7012, 21)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (7012, 23)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (7012, 25)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (7013, 2)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (7013, 6)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (7013, 18)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (7014, 9)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (7014, 16)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (8018, 13)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (8018, 19)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (8018, 24)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (8020, 16)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (8020, 24)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (8020, 31)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (8021, 25)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (8021, 33)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (8022, 3)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (8022, 6)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (9022, 17)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (9022, 22)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (9023, 7)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (9024, 7)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (9026, 7)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (9026, 17)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (9032, 18)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (9032, 19)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (9033, 18)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (9033, 19)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (10022, 18)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (10022, 33)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (10023, 18)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (10024, 1)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (10024, 9)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (10024, 13)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (11022, 28)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (11023, 11)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (11023, 31)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (11024, 17)
GO

INSERT [dbo].[NGANHTUYENDUNG] ([ID_TINTUYENDUNG], [ID_NGANH]) VALUES (11025, 27)
GO

SET IDENTITY_INSERT [dbo].[NGOAINGU] ON
GO

INSERT [dbo].[NGOAINGU] ([ID_NGOAINGU], [TENNGOAINGU]) VALUES (1, N'ANH')
GO

INSERT [dbo].[NGOAINGU] ([ID_NGOAINGU], [TENNGOAINGU]) VALUES (2, N'PHÁP ')
GO

INSERT [dbo].[NGOAINGU] ([ID_NGOAINGU], [TENNGOAINGU]) VALUES (3, N'MỸ')
GO

INSERT [dbo].[NGOAINGU] ([ID_NGOAINGU], [TENNGOAINGU]) VALUES (4, N'NHẬT')
GO

INSERT [dbo].[NGOAINGU] ([ID_NGOAINGU], [TENNGOAINGU]) VALUES (5, N'TRUNG QUỐC')
GO

INSERT [dbo].[NGOAINGU] ([ID_NGOAINGU], [TENNGOAINGU]) VALUES (6, N'NGA')
GO

INSERT [dbo].[NGOAINGU] ([ID_NGOAINGU], [TENNGOAINGU]) VALUES (7, N'TÂY BAN NHA')
GO

SET IDENTITY_INSERT [dbo].[NGOAINGU] OFF
GO

INSERT [dbo].[NGOAINGUUNGVIEN] ([ID_NGOAINGU], [ID_UNGVIEN], [NGHE], [NOI], [DOC], [VIET], [COBAN], [GHICHU]) VALUES (1, 1, 0, 0, 0, 0, 1, N'Có khả năng đọc hiểu tài liệu tiếng anh')
GO

INSERT [dbo].[NGOAINGUUNGVIEN] ([ID_NGOAINGU], [ID_UNGVIEN], [NGHE], [NOI], [DOC], [VIET], [COBAN], [GHICHU]) VALUES (1, 7, 0, 0, 0, 0, 1, N'xem anime')
GO

INSERT [dbo].[NGOAINGUUNGVIEN] ([ID_NGOAINGU], [ID_UNGVIEN], [NGHE], [NOI], [DOC], [VIET], [COBAN], [GHICHU]) VALUES (1, 8, 1, 1, 0, 0, 1, N'Tôi có thể làm điều gì đó như thế này bằng JavaScript/jQuery không? ... Sẽ rất gọn gàng nếu các chuỗi vốn là các mảng ký tự trong javascript để chúng ta không ...')
GO

INSERT [dbo].[NGOAINGUUNGVIEN] ([ID_NGOAINGU], [ID_UNGVIEN], [NGHE], [NOI], [DOC], [VIET], [COBAN], [GHICHU]) VALUES (1, 10, 0, 0, 0, 0, 1, NULL)
GO

INSERT [dbo].[NGOAINGUUNGVIEN] ([ID_NGOAINGU], [ID_UNGVIEN], [NGHE], [NOI], [DOC], [VIET], [COBAN], [GHICHU]) VALUES (1, 1018, 0, 0, 0, 0, 1, N'Có khả năng đọc hiểu tài liệu chuyên ngành')
GO

INSERT [dbo].[NGOAINGUUNGVIEN] ([ID_NGOAINGU], [ID_UNGVIEN], [NGHE], [NOI], [DOC], [VIET], [COBAN], [GHICHU]) VALUES (4, 7, 1, 1, 0, 1, 0, N'chém gió với tây')
GO

INSERT [dbo].[NGOAINGUUNGVIEN] ([ID_NGOAINGU], [ID_UNGVIEN], [NGHE], [NOI], [DOC], [VIET], [COBAN], [GHICHU]) VALUES (5, 7, 1, 0, 0, 0, 1, N'abcd')
GO

SET IDENTITY_INSERT [dbo].[NHATUYENDUNG] ON
GO

INSERT [dbo].[NHATUYENDUNG] ([ID_NHATUYENDUNG], [EMAIL], [MATKHAU], [LOGO], [SHOWLOGO], [TENCONGTY], [TENVIETTAT], [NGANHNGHEHOATDONG], [QUYMOCONGTY], [DIACHI], [SDT], [WEBSITE], [BANNERCONGTY], [VIDEOGIOITHIEU], [HINHANH], [GIOITHIEUCONGTY], [NGAYDANGKY], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [SOCIALLOGIN]) VALUES (1, N'user111@gmail.com', N'111111', N'Resources\Images\congtyAT.png', 1, N'CÔNG TY TNHH IN ẤN QUẢNG CÁO CAO THIÊN MINH', N'TMQC', 3, 20, N'D6/180 Quốc Lộ 50, Ấp 4, Xã Phong Phú, Huyện Bình Chánh, Thành phố Hồ Chí Minh
', N'0987332231', N'http://trangvangtructuyen.vn/cong-ty-tnhh-in-an-quang-cao-cao-thien-minh.html', N'Resources\Images\banner_congtyAT.png', N'https://www.youtube.com/watch?v=9yu9lraVxfI', N'Resources\Images\hinhanh_congtyAT.jpg', N'Công ty Cao Thiên Minh là một trong những công ty  hàng đầu chuyên in trên mọi chất liệu như: Decal, PP, Hiflex, Canvas, Backlist film, ..... Đặc biệt in trên vải, nón, in folder, Name card, bao thư ......là những sản phẩm mới của Cao Thiên Minh. Trên 10 năm qua công ty chúng tôi đã xây dựng uy tín trên thị trường TPHCM cũng như các tỉnh lân cận. Cao Thiên Minh đã và đang nỗ lực không ngừng làm cầu nối tin cậy đưa thương hiệu sản phẩm và dịch vụ khách hàng đến với người tiêu dùng. Chúng tôi mong rằng quý khách hàng luôn luôn đồng hành cùng chúng tôi trong quá trình phát triển: "Thành công của các bạn là mục tiêu của chúng tôi".,,', CAST(N'2020-06-30' AS Date), 0, NULL, NULL, 1, 104, 0)
GO

INSERT [dbo].[NHATUYENDUNG] ([ID_NHATUYENDUNG], [EMAIL], [MATKHAU], [LOGO], [SHOWLOGO], [TENCONGTY], [TENVIETTAT], [NGANHNGHEHOATDONG], [QUYMOCONGTY], [DIACHI], [SDT], [WEBSITE], [BANNERCONGTY], [VIDEOGIOITHIEU], [HINHANH], [GIOITHIEUCONGTY], [NGAYDANGKY], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [SOCIALLOGIN]) VALUES (2, N'grear@gmail.com', N'111111', N'Resources\Images\GARMAENT_CONGTY.jpg', 1, N'CÔNG TY TNHH GEARMENT', N'GEARMENT', 13, 1000, N'150 Bis Lê Thị Hồng Gấm, Phường Cầu Ông Lãnh, Quận 1, Thành phố Hồ Chí Minh', N'0789123567', N' www.fashiongarments.com', N'Resources\Images\BANNER_GARMENT.png', N'https://www.youtube.com/watch?v=9yu9lraVxfI', N'Resources\Images\CONGTY_2.jpg', N'Công ty TNHH Fashion Garments 2 là công ty với 100% vốn đầu tư nước ngoài, chuyên sản xuất và xuất khẩu hàng may mặc hàng đầu Việt Nam.', CAST(N'2020-06-02' AS Date), 0, NULL, NULL, 1, 109, 0)
GO

INSERT [dbo].[NHATUYENDUNG] ([ID_NHATUYENDUNG], [EMAIL], [MATKHAU], [LOGO], [SHOWLOGO], [TENCONGTY], [TENVIETTAT], [NGANHNGHEHOATDONG], [QUYMOCONGTY], [DIACHI], [SDT], [WEBSITE], [BANNERCONGTY], [VIDEOGIOITHIEU], [HINHANH], [GIOITHIEUCONGTY], [NGAYDANGKY], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [SOCIALLOGIN]) VALUES (3, N'ntd1@gmail.com', N'111111', N'Resources\Images\cpn_toanphat.png', 1, N'CÔNG TY TNHH SẢN XUẤT VÀ THƯƠNG MẠI GIẤY QUỐC TOÀN PHÁT
', N'TPC', 13, 100, N'50/1/28/32 Nguyễn Quý Yêm, Khu phố 4, Phường An Lạc, Quận Bình Tân, Thành phố Hồ Chí Minh', N'0346006260', N'http://www.daitoanphat.vn/#', N'Resources\Images\congtytoanphat_banner.jpg', N'https://www.youtube.com/watch?v=9yu9lraVxfI', N'Resources\Images\congtytoanphat.jpg', N'Công Ty TNHH SX TM Bao Bì Giấy ĐẠI TOÀN PHÁT là công ty sản xuất thùng carton có uy tín tại thị trường Tp Hồ Chí Minh. ĐẠI TOÀN PHÁT sở hữu một đội ngũ nhân sự giàu kinh nghiệm với một phong thái làm việc nghiêm túc, nhiệt tình và hiệu quả.', CAST(N'2020-06-02' AS Date), 0, NULL, NULL, 1, 102, 0)
GO

INSERT [dbo].[NHATUYENDUNG] ([ID_NHATUYENDUNG], [EMAIL], [MATKHAU], [LOGO], [SHOWLOGO], [TENCONGTY], [TENVIETTAT], [NGANHNGHEHOATDONG], [QUYMOCONGTY], [DIACHI], [SDT], [WEBSITE], [BANNERCONGTY], [VIDEOGIOITHIEU], [HINHANH], [GIOITHIEUCONGTY], [NGAYDANGKY], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [SOCIALLOGIN]) VALUES (6, N'ntd2@gmail.com', N'111111', N'Resources\Images\logo6.jpg', 1, N'CÔNG TY CỔ PHẦN THƯƠNG MẠI XUẤT NHẬP KHẨU & CHẾ BIẾN HẢI SẢN ĐẠI DƯƠNG XANH

', N'TPT', 6, 10, N'BIG C PANDORA', N'0356767343', N'congty1.com', N'Resources\Images\9-bí-quyết-thu-hút-nhân-tài-công-nghệ-1920x1080_13052019.jpg', N'https://www.youtube.com/watch?v=9yu9lraVxfI', N'Resources\Images\unnamed.jpg', N'    Lorem ipsum dolor sit amet consectetur adipisicing elit. Eum amet ea cum? Iusto, aliquam! Fugit, ullam natus doloremque aperiam asperiores, consequatur corrupti laborum iste cumque harum, earum dicta quis id temporibus! Quibusdam voluptates asperiores eveniet sed impedit culpa expedita alias minima voluptatum sint aspernatur nam, neque, voluptatibus esse, perspiciatis adipisci temporibus laboriosam? Nulla illum explicabo culpa. Blanditiis corrupti et eaque tempore sed accusamus doloremque quibusdam assumenda molestias ratione temporibus officia mollitia consequatur voluptate reprehenderit possimus laboriosam porro quaerat, soluta praesentium perspiciatis voluptatem rerum. Modi voluptatum iusto dolorum in quos. Molestiae adipisci ducimus dolores dolor harum sed ipsa architecto quaerat iure?', CAST(N'2020-06-02' AS Date), 0, NULL, NULL, 2, 113, 0)
GO

INSERT [dbo].[NHATUYENDUNG] ([ID_NHATUYENDUNG], [EMAIL], [MATKHAU], [LOGO], [SHOWLOGO], [TENCONGTY], [TENVIETTAT], [NGANHNGHEHOATDONG], [QUYMOCONGTY], [DIACHI], [SDT], [WEBSITE], [BANNERCONGTY], [VIDEOGIOITHIEU], [HINHANH], [GIOITHIEUCONGTY], [NGAYDANGKY], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [SOCIALLOGIN]) VALUES (7, N'ntd3@gmail.com', N'111111', N'Resources\Images\logo7.jpg', 1, N'Công ty phát triển công nghệ Hồng Đức', N'DDX', 3, 20, N'27 Hồ Văn Huê, Phường 09, Quận Phú Nhuận, Thành phố Hồ Chí Minh
', N'0355539845', N'congty1.com', N'Resources\Images\29570974_1218221574976941_9039675224670053342_n.jpg', N'https://www.youtube.com/watch?v=9yu9lraVxfI', N'Resources\Images\unnamed.jpg', N'    Lorem ipsum dolor sit amet consectetur adipisicing elit. Eum amet ea cum? Iusto, aliquam! Fugit, ullam natus doloremque aperiam asperiores, consequatur corrupti laborum iste cumque harum, earum dicta quis id temporibus! Quibusdam voluptates asperiores eveniet sed impedit culpa expedita alias minima voluptatum sint aspernatur nam, neque, voluptatibus esse, perspiciatis adipisci temporibus laboriosam? Nulla illum explicabo culpa. Blanditiis corrupti et eaque tempore sed accusamus doloremque quibusdam assumenda molestias ratione temporibus officia mollitia consequatur voluptate reprehenderit possimus laboriosam porro quaerat, soluta praesentium perspiciatis voluptatem rerum. Modi voluptatum iusto dolorum in quos. Molestiae adipisci ducimus dolores dolor harum sed ipsa architecto quaerat iure?', CAST(N'2020-06-02' AS Date), 0, NULL, NULL, 2, 120, 0)
GO

INSERT [dbo].[NHATUYENDUNG] ([ID_NHATUYENDUNG], [EMAIL], [MATKHAU], [LOGO], [SHOWLOGO], [TENCONGTY], [TENVIETTAT], [NGANHNGHEHOATDONG], [QUYMOCONGTY], [DIACHI], [SDT], [WEBSITE], [BANNERCONGTY], [VIDEOGIOITHIEU], [HINHANH], [GIOITHIEUCONGTY], [NGAYDANGKY], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [SOCIALLOGIN]) VALUES (8, N'ntd4@gmail.com', N'111111', N'Resources\Images\logo8.jpg', 1, N'CÔNG TY TNHH ĐẦU TƯ PHÁT TRIỂN KHẢI THỊNH
', N'KT', 9, 30, N'235 Nguyễn Thái Bình, Phường 4, Quận Tân Bình, Thành phố Hồ Chí Minh', N'0356895232', N'congty1.com', N'Resources\Images\1543285604615-1543228480311-Banner1.jpg', N'https://www.youtube.com/watch?v=9yu9lraVxfI', N'Resources\Images\oZ80BDa.jpg', N'
    Lorem ipsum dolor sit amet consectetur adipisicing elit. Eum amet ea cum? Iusto, aliquam! Fugit, ullam natus doloremque aperiam asperiores, consequatur corrupti laborum iste cumque harum, earum dicta quis id temporibus! Quibusdam voluptates asperiores eveniet sed impedit culpa expedita alias minima voluptatum sint aspernatur nam, neque, voluptatibus esse, perspiciatis adipisci temporibus laboriosam? Nulla illum explicabo culpa. Blanditiis corrupti et eaque tempore sed accusamus doloremque quibusdam assumenda molestias ratione temporibus officia mollitia consequatur voluptate reprehenderit possimus laboriosam porro quaerat, soluta praesentium perspiciatis voluptatem rerum. Modi voluptatum iusto dolorum in quos. Molestiae adipisci ducimus dolores dolor harum sed ipsa architecto quaerat iure?', CAST(N'2020-06-02' AS Date), 0, NULL, NULL, 2, 122, 0)
GO

INSERT [dbo].[NHATUYENDUNG] ([ID_NHATUYENDUNG], [EMAIL], [MATKHAU], [LOGO], [SHOWLOGO], [TENCONGTY], [TENVIETTAT], [NGANHNGHEHOATDONG], [QUYMOCONGTY], [DIACHI], [SDT], [WEBSITE], [BANNERCONGTY], [VIDEOGIOITHIEU], [HINHANH], [GIOITHIEUCONGTY], [NGAYDANGKY], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [SOCIALLOGIN]) VALUES (9, N'ntd5@gmail.com', N'111111', N'Resources\Images\tcons_logo.jpg', 1, N'CÔNG TY CỔ PHẦN ĐẦU TƯ XÂY DỰNG TCONS
', N'Tcons', 22, 50, N'Số 23 Cửu Long, Phường 2, Quận Tân Bình, Thành phố Hồ Chí Minh', N'2121800254', N'congty1.com', N'Resources\Images\tcons_banner.jpg', N'https://www.youtube.com/watch?v=9yu9lraVxfI', N'Resources\Images\tcons3.jpg', N'
    Lorem ipsum dolor sit amet consectetur adipisicing elit. Eum amet ea cum? Iusto, aliquam! Fugit, ullam natus doloremque aperiam asperiores, consequatur corrupti laborum iste cumque harum, earum dicta quis id temporibus! Quibusdam voluptates asperiores eveniet sed impedit culpa expedita alias minima voluptatum sint aspernatur nam, neque, voluptatibus esse, perspiciatis adipisci temporibus laboriosam? Nulla illum explicabo culpa. Blanditiis corrupti et eaque tempore sed accusamus doloremque quibusdam assumenda molestias ratione temporibus officia mollitia consequatur voluptate reprehenderit possimus laboriosam porro quaerat, soluta praesentium perspiciatis voluptatem rerum. Modi voluptatum iusto dolorum in quos. Molestiae adipisci ducimus dolores dolor harum sed ipsa architecto quaerat iure?', CAST(N'2020-06-22' AS Date), 0, NULL, NULL, 2, 118, 0)
GO

INSERT [dbo].[NHATUYENDUNG] ([ID_NHATUYENDUNG], [EMAIL], [MATKHAU], [LOGO], [SHOWLOGO], [TENCONGTY], [TENVIETTAT], [NGANHNGHEHOATDONG], [QUYMOCONGTY], [DIACHI], [SDT], [WEBSITE], [BANNERCONGTY], [VIDEOGIOITHIEU], [HINHANH], [GIOITHIEUCONGTY], [NGAYDANGKY], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [SOCIALLOGIN]) VALUES (10, N'ntd6@gmail.com', N'111111', N'Resources\Images\platinum_logo.jpg', 1, N'CÔNG TY TNHH CÔNG NGHỆ SỐ PLATINUM', N'PLATINUM', 21, 10, N'93/16 Đường Nguyễn Thị Tú, Phường Bình Hưng Hòa B, Quận Bình Tân, Thành phố Hồ Chí Minh', N'0981412135', N'congty1.com', N'Resources\Images\platinum_banner.jpg', N'https://www.youtube.com/watch?v=9yu9lraVxfI', N'Resources\Images\PLATINUM1.JPG', N'
    Lorem ipsum dolor sit amet consectetur adipisicing elit. Eum amet ea cum? Iusto, aliquam! Fugit, ullam natus doloremque aperiam asperiores, consequatur corrupti laborum iste cumque harum, earum dicta quis id temporibus! Quibusdam voluptates asperiores eveniet sed impedit culpa expedita alias minima voluptatum sint aspernatur nam, neque, voluptatibus esse, perspiciatis adipisci temporibus laboriosam? Nulla illum explicabo culpa. Blanditiis corrupti et eaque tempore sed accusamus doloremque quibusdam assumenda molestias ratione temporibus officia mollitia consequatur voluptate reprehenderit possimus laboriosam porro quaerat, soluta praesentium perspiciatis voluptatem rerum. Modi voluptatum iusto dolorum in quos. Molestiae adipisci ducimus dolores dolor harum sed ipsa architecto quaerat iure?', CAST(N'2020-04-01' AS Date), 0, NULL, NULL, 2, 121, 0)
GO

INSERT [dbo].[NHATUYENDUNG] ([ID_NHATUYENDUNG], [EMAIL], [MATKHAU], [LOGO], [SHOWLOGO], [TENCONGTY], [TENVIETTAT], [NGANHNGHEHOATDONG], [QUYMOCONGTY], [DIACHI], [SDT], [WEBSITE], [BANNERCONGTY], [VIDEOGIOITHIEU], [HINHANH], [GIOITHIEUCONGTY], [NGAYDANGKY], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [SOCIALLOGIN]) VALUES (11, N'ntd7@gmail.com', N'111111', N'Resources\Images\anvuongphat_logo.jpg', 1, N' CÔNG TY TNHH THƯƠNG MẠI SẢN XUẤT VƯỢNG PHÁT', N'AVP', 7, 100, N'193 Cô Giang, quận 1', N'0925656412', N'congty1.com', N'Resources\Images\anvuongphat_banner.jpg', N'https://www.youtube.com/watch?v=9yu9lraVxfI', N'Resources\Images\anvuongphat.jpg', N'
    Lorem ipsum dolor sit amet consectetur adipisicing elit. Eum amet ea cum? Iusto, aliquam! Fugit, ullam natus doloremque aperiam asperiores, consequatur corrupti laborum iste cumque harum, earum dicta quis id temporibus! Quibusdam voluptates asperiores eveniet sed impedit culpa expedita alias minima voluptatum sint aspernatur nam, neque, voluptatibus esse, perspiciatis adipisci temporibus laboriosam? Nulla illum explicabo culpa. Blanditiis corrupti et eaque tempore sed accusamus doloremque quibusdam assumenda molestias ratione temporibus officia mollitia consequatur voluptate reprehenderit possimus laboriosam porro quaerat, soluta praesentium perspiciatis voluptatem rerum. Modi voluptatum iusto dolorum in quos. Molestiae adipisci ducimus dolores dolor harum sed ipsa architecto quaerat iure?', CAST(N'2020-06-08' AS Date), 0, NULL, NULL, 2, 107, 0)
GO

INSERT [dbo].[NHATUYENDUNG] ([ID_NHATUYENDUNG], [EMAIL], [MATKHAU], [LOGO], [SHOWLOGO], [TENCONGTY], [TENVIETTAT], [NGANHNGHEHOATDONG], [QUYMOCONGTY], [DIACHI], [SDT], [WEBSITE], [BANNERCONGTY], [VIDEOGIOITHIEU], [HINHANH], [GIOITHIEUCONGTY], [NGAYDANGKY], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [SOCIALLOGIN]) VALUES (12, N'ntd8@gmail.com', N'111111', N'Resources\Images\bimgroup_logo.jpg', 1, N'BIM Group', N'BIM', 7, 1000, N'46A Đinh Công Tráng, phường Tân Định, quận 1', N'0986332355', N'https://bimgroup.com', N'Resources\Images\bimgroup_banner.jpg', N'https://www.youtube.com/watch?v=9yu9lraVxfI', N'Resources\Images\bimgroup.jpg', N'
    Lorem ipsum dolor sit amet consectetur adipisicing elit. Eum amet ea cum? Iusto, aliquam! Fugit, ullam natus doloremque aperiam asperiores, consequatur corrupti laborum iste cumque harum, earum dicta quis id temporibus! Quibusdam voluptates asperiores eveniet sed impedit culpa expedita alias minima voluptatum sint aspernatur nam, neque, voluptatibus esse, perspiciatis adipisci temporibus laboriosam? Nulla illum explicabo culpa. Blanditiis corrupti et eaque tempore sed accusamus doloremque quibusdam assumenda molestias ratione temporibus officia mollitia consequatur voluptate reprehenderit possimus laboriosam porro quaerat, soluta praesentium perspiciatis voluptatem rerum. Modi voluptatum iusto dolorum in quos. Molestiae adipisci ducimus dolores dolor harum sed ipsa architecto quaerat iure?', CAST(N'2020-05-10' AS Date), 0, NULL, NULL, 2, 129, 0)
GO

INSERT [dbo].[NHATUYENDUNG] ([ID_NHATUYENDUNG], [EMAIL], [MATKHAU], [LOGO], [SHOWLOGO], [TENCONGTY], [TENVIETTAT], [NGANHNGHEHOATDONG], [QUYMOCONGTY], [DIACHI], [SDT], [WEBSITE], [BANNERCONGTY], [VIDEOGIOITHIEU], [HINHANH], [GIOITHIEUCONGTY], [NGAYDANGKY], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [SOCIALLOGIN]) VALUES (23, N'ntd9@gmail.com', N'111111', N'Resources\Images\cargill_logo.jpg', 1, N'CARGILL VIETNAM', N'CARGILL', 6, 15000, N'11Floor, Maple Tree Business Center, 1060 Nguyen Van Linh, District 7, HCMC, Vietnam', N'0284161515', N'https://www.cargill.com.vn/', N'Resources\Images\cargill_banner.jpg', N'https://www.youtube.com/watch?v=9yu9lraVxfI', N'Resources\Images\cargill2.jpg', N'CARGILL là công ty tư nhân lớn nhất thế giới trong nhiều năm qua và là tập đoàn hàng đầu của Mỹ ở lĩnh vực nông nghiệp, thực phẩm, sản phẩm công nghiệp và dịch vụ tài chính.  

Thành lập năm 1865, đến nay Cargill có hơn 150.000 nhân viên làm việc trên 70 quốc gia khắp các châu lục.', CAST(N'2020-05-10' AS Date), 0, NULL, NULL, 2, 111, 0)
GO

INSERT [dbo].[NHATUYENDUNG] ([ID_NHATUYENDUNG], [EMAIL], [MATKHAU], [LOGO], [SHOWLOGO], [TENCONGTY], [TENVIETTAT], [NGANHNGHEHOATDONG], [QUYMOCONGTY], [DIACHI], [SDT], [WEBSITE], [BANNERCONGTY], [VIDEOGIOITHIEU], [HINHANH], [GIOITHIEUCONGTY], [NGAYDANGKY], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [SOCIALLOGIN]) VALUES (24, N'ntd10@gmail.com', N'111111', N'Resources\Images\tuonglai_logo.jpg', 1, N'CÔNG TY TNHH ĐẦU TƯ VÀ PHÁT TRIỂN CÔNG NGHỆ TƯƠNG LAI
', N'MST', 6, 500, N'Tầng 3, toà nhà VTC Online, số 18, đường Tam Trinh, Phường Minh Khai, Quận Hai Bà Trưng, TP. Hà Nội, Việt Nam', N'0287303912', N'http://nexttech.asia/', N'Resources\Images\tuonglai_banner.jpg', N'https://www.youtube.com/watch?v=9yu9lraVxfI', N'Resources\Images\tuonglai2.jpg', N'Chào mừng bạn đến với NEXTTECH GROUP,

NextTech (www.nexttech.asia) là một tập hợp hơn 20 công ty công nghệ khởi nghiệp tại Việt Nam từ năm 2001 với tiền thân là PeaceSoft-group. NextTech chuyên doanh các dịch vụ Điện tử hoá cuộc sống trong nhiều lĩnh vực như: Chợ điện tử, Mua bán xuyên biên giới, Cổng thanh toán, Ví điện tử, Thanh toán di động, Cho vay tiêu dùng, Sàn giao dịch tiền mã hoá, Hậu cần kho vận, Chuyển phát hàng hoá, Đào tạo công nghệ, Du lịch trực tuyến v.v… với nhiều đơn vị thành viên lớn nhất Việt Nam hoặc tiên phong trong khu vực Đông Nam Á.

Hệ sinh thái NextTech hiện có 600 nhân viên làm việc tại 7 quốc gia trong 3 lĩnh vực chính: Thương mại điện tử, Công nghệ tài chính và Dịch vụ hậu cần; với sản lượng giao dịch năm 2017 ước đạt 500 triệu USD và doanh thu 90 triệu USD. NextTech đang hoạt động tại 8 văn phòng trên toàn cầu ở Hà Nội, Hồ Chí Minh (Việt Nam), Bangkok (Thái Lan), Kuala Lumpur (Malaysia), Jakarta (Indonesia), Manila (Philippines), San Jose (bang California, Mỹ) và Quảng Châu (Trung Quốc).

Vào tháng 11 năm 2017, NextTech vinh dự được bình chọn là 1 trong 10 doanh nghiệp có ảnh hưởng lớn nhất đến sự phát triển của Internet tại Việt Nam trong vòng 10 năm. ', CAST(N'2020-05-10' AS Date), 0, NULL, NULL, 2, 104, 0)
GO

INSERT [dbo].[NHATUYENDUNG] ([ID_NHATUYENDUNG], [EMAIL], [MATKHAU], [LOGO], [SHOWLOGO], [TENCONGTY], [TENVIETTAT], [NGANHNGHEHOATDONG], [QUYMOCONGTY], [DIACHI], [SDT], [WEBSITE], [BANNERCONGTY], [VIDEOGIOITHIEU], [HINHANH], [GIOITHIEUCONGTY], [NGAYDANGKY], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [SOCIALLOGIN]) VALUES (25, N'ntd11@gmail.com', N'111111', N'Resources\Images\_logo.jpg', 1, N'First Alliances Vietnam''s Leading HR Specialist', N'First Alliances', 1, 499, N'37 Ton Duc Thang, Dist.1', N'0312323913', N'https://www.fa.net.vn/', N'Resources\Images\alliances_banner.jpg', N'https://www.youtube.com/watch?v=9yu9lraVxfI', N'Resources\Images\alliances2.jpg', N'Established in 1998, First Alliances is the most well-known as the leading HR consultancy in Viet Nam. We are proud to be the pioneer and constantly contributing to upgrade the Staffing & Recruiting landscape of Vietnam. After approximately two decades in the business, First Alliances has established on nationwide basics with offices based in Hanoi and Ho Chi Minh City. Our reputable in Executive Search and Selection field is highly recognized among MNCs and local giants. First Alliances’ mission is to provide tailored recruitment services to our local and international clients’ ambitions to actively acquire and retain talent. Our well-established database, industry-centric insights, and a dynamic team of well-trained recruitment professionals have enabled us to successfully bring together talented candidates and reputable employers over the last 17 years. Message from First Alliances', CAST(N'2020-06-02' AS Date), 0, NULL, NULL, 2, 118, 0)
GO

INSERT [dbo].[NHATUYENDUNG] ([ID_NHATUYENDUNG], [EMAIL], [MATKHAU], [LOGO], [SHOWLOGO], [TENCONGTY], [TENVIETTAT], [NGANHNGHEHOATDONG], [QUYMOCONGTY], [DIACHI], [SDT], [WEBSITE], [BANNERCONGTY], [VIDEOGIOITHIEU], [HINHANH], [GIOITHIEUCONGTY], [NGAYDANGKY], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [SOCIALLOGIN]) VALUES (26, N'ntd12@gmail.com', N'111111', N'Resources\Images\heneken_logo.jpg', 1, N'HEINEKEN VIETNAM BREWERY', N'HEINEKEN', 20, 2000, N'Lầu 18&19 Tòa nhà Vietcombank, 5 Công Trường Mê Linh, Quận 1', N'0321321312', N'https://www.heineken.com/vn', N'Resources\Images\heneken_banner.jpg', N'https://www.youtube.com/watch?v=9yu9lraVxfI', N'Resources\Images\heneken3.jpg', N'Heineken Vietnam Brewery, was established on December 9th, 1991 by Saigon Trading Group (SATRA) and Asia Pacific Breweries Ltd. (APBL) – now Heineken Asia Pacific Pte. Ltd. (HEINEKEN Asia Pacific).

The brewery covering an area of 12.7 hectares in Thoi An Ward, District 12, HCMC is one of the most modern breweries in Southeast Asia. For a number of years, Heineken Vietnam Brewery has been one of the top corporate tax payers in the city and is recognized as one of the most effective businesses in Ho Chi Minh City. Heineken Vietnam Brewery has also contributed significantly to creating employment opportunities, hiring around 1,600 locals and creating thousands of jobs for suppliers and partners in Vietnam. The company is active in social activities, human resources development and leads the way in environmental protection.

Apart from the innovative brewery in District 12, Heineken Vietnam Brewery has 100% ownership of breweries in Danang, Quang Nam, Tien Giang and Vung Tau. Currently, Heineken Vietnam Brewery has a broad and impressive portfolio of beers which includes Heineken, Tiger, Tiger Crystal, Desperados, Biere Larue, Biere Larue Export, BGI and Bivina in Vietnam.', CAST(N'2020-05-10' AS Date), 0, NULL, NULL, 2, 127, 0)
GO

INSERT [dbo].[NHATUYENDUNG] ([ID_NHATUYENDUNG], [EMAIL], [MATKHAU], [LOGO], [SHOWLOGO], [TENCONGTY], [TENVIETTAT], [NGANHNGHEHOATDONG], [QUYMOCONGTY], [DIACHI], [SDT], [WEBSITE], [BANNERCONGTY], [VIDEOGIOITHIEU], [HINHANH], [GIOITHIEUCONGTY], [NGAYDANGKY], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [SOCIALLOGIN]) VALUES (27, N'ntd13@gmail.com', N'111111', N'Resources\Images\austdoor_logo.jpg', 1, N'CÔNG TY CỔ PHẦN TẬP ĐOÀN AUSTDOOR
', N'LDPT', 16, 5000, N'Trụ sở chính: Số 35A, Đường số 1, Trần Thái Tông, Dịch Vọng, Cầu Giấy, Hà Nội', N'0213213133', N'www.austdoorgroup.vn', N'Resources\Images\austdoor_banner.jpg', N'https://www.youtube.com/watch?v=9yu9lraVxfI', N'Resources\Images\austdoor2.jpg', N'Công ty Cổ phần Tập đoàn Austdoor là một trong những Nhà sản xuất và cung cấp các giải pháp về cửa & vật liệu xây dựng công nghệ mới hàng đầu tại Việt Nam. Trải qua hơn 17 năm nỗ lực xây dựng và phát triển không ngừng góp phần bảo vệ hàng triệu ngôi nhà & tô đẹp thêm cho kiến trúc Việt Nam, Tập đoàn Austdoor hiện đang tiếp tục đẩy mạnh hoạt động sản xuất kinh doanh tại nhiều quốc gia và vùng lãnh thổ trên thế giới.

Bằng sản phẩm chất lượng cao, dịch vụ khách hàng ngày càng hoàn thiện, Tập đoàn Austdoor đã vinh dự nhận những giải thưởng của nhiều tổ chức, ban ngành uy tín: Cúp Vàng Sản phẩm và Dịch vụ xuất sắc; Giải thưởng Thương hiệu nổi tiếng quốc gia; Huy chương Vàng Chất lượng sản phẩm ngành Xây Dựng...

Austdoor luôn mong muốn hợp tác thành công và trở thành người bạn đồng hành Đáng tin cậy của mỗi đối tác, mỗi khách hàng, và của cả cộng đồng, cùng gắn kết sức mạnh, tài trí của người Việt, nỗ lực không ngừng với phương châm sáng tạo vì cuộc sống.', CAST(N'2020-05-10' AS Date), 0, NULL, NULL, 2, 130, 0)
GO

INSERT [dbo].[NHATUYENDUNG] ([ID_NHATUYENDUNG], [EMAIL], [MATKHAU], [LOGO], [SHOWLOGO], [TENCONGTY], [TENVIETTAT], [NGANHNGHEHOATDONG], [QUYMOCONGTY], [DIACHI], [SDT], [WEBSITE], [BANNERCONGTY], [VIDEOGIOITHIEU], [HINHANH], [GIOITHIEUCONGTY], [NGAYDANGKY], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [SOCIALLOGIN]) VALUES (28, N'ntd14@gmail.com', N'111111', N'Resources\Images\apex_logo.jpg', 1, N'CÔNG TY TNHH APEX PLASTECH VIỆT NAM
', N'Apex', 4, 200, N'Lô A65/II - A72/II, Đường Số 4, KCN Vĩnh Lộc, P. Bình Hưng Hòa B, Q. Bình Tân, TP. HCM', N'0231552333', N'www.Apex.vn', N'Resources\Images\apex_banner.jpg', N'https://www.youtube.com/watch?v=9yu9lraVxfI', N'Resources\Images\apex1.jpg', N'Công Ty TNHH Apex Plastech là công ty có 100% vốn đầu tư từ Thái Lan, được thành lập từ năm 1985, có trụ sở chính tại Băng Cốc, Thái Lan. Apex Plastech dẫn đầu trong nghành công nghiệp nhựa công nghiệp tại Thái Lan và bắt đầu mở rộng quy mô nhà máy sang Việt Nam từ năm 2017. Apex Plastech chuyên sản xuất, gia công sản phẩm từ nhựa, bao gồm: Chai nhựa, đò chơi nhựa, gia công thiết bị điện tử nhựa, gia công thiết bị kỹ thuật bằng nhựa,..
Chúng tôi cam kết mang đến cho quý khách sản phẩm với chất lượng tốt và giá cả cạnh tranh', CAST(N'2020-05-10' AS Date), 0, NULL, NULL, 2, 120, 0)
GO

INSERT [dbo].[NHATUYENDUNG] ([ID_NHATUYENDUNG], [EMAIL], [MATKHAU], [LOGO], [SHOWLOGO], [TENCONGTY], [TENVIETTAT], [NGANHNGHEHOATDONG], [QUYMOCONGTY], [DIACHI], [SDT], [WEBSITE], [BANNERCONGTY], [VIDEOGIOITHIEU], [HINHANH], [GIOITHIEUCONGTY], [NGAYDANGKY], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [SOCIALLOGIN]) VALUES (29, N'ntd15@gmail.com', N'111111', N'Resources\Images\reeracoen_logo.jpg', 1, N'CÔNG TY TNHH REERACOEN VIỆT NAM
', N'REERACOEN', 12, 300, N'Cao ốc Zen Plaza, số 54-56 Nguyễn Trãi, phường Bến Thành, Quận 1', N'0986332355', N'https://www.reeracoacom.vn/', N'Resources\Images\reeracoen_banner.jpg', N'https://www.youtube.com/watch?v=9yu9lraVxfI', N'Resources\Images\reeracoen1.jpg', N'Công Ty TNHH Apex Plastech là công ty có 100% vốn đầu tư từ Thái Lan, được thành lập từ năm 1985, có trụ sở chính tại Băng Cốc, Thái Lan. Apex Plastech dẫn đầu trong nghành công nghiệp nhựa công nghiệp tại Thái Lan và bắt đầu mở rộng quy mô nhà máy sang Việt Nam từ năm 2017. Apex Plastech chuyên sản xuất, gia công sản phẩm từ nhựa, bao gồm: Chai nhựa, đò chơi nhựa, gia công thiết bị điện tử nhựa, gia công thiết bị kỹ thuật bằng nhựa,..
Chúng tôi cam kết mang đến cho quý khách sản phẩm với chất lượng tốt và giá cả cạnh tranh', CAST(N'2020-05-10' AS Date), 0, NULL, NULL, 2, 130, 0)
GO

INSERT [dbo].[NHATUYENDUNG] ([ID_NHATUYENDUNG], [EMAIL], [MATKHAU], [LOGO], [SHOWLOGO], [TENCONGTY], [TENVIETTAT], [NGANHNGHEHOATDONG], [QUYMOCONGTY], [DIACHI], [SDT], [WEBSITE], [BANNERCONGTY], [VIDEOGIOITHIEU], [HINHANH], [GIOITHIEUCONGTY], [NGAYDANGKY], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [SOCIALLOGIN]) VALUES (30, N'ntd16@gmail.com', N'111111', N'Resources\Images\hamsa_logo.jpg', 1, N'HAMSA TECHNOLOGIES', N'HAMSA', 1, 99, N'Tòa nhà B4, 21 Phạm Ngọc Thạch, Phường Kim Liên, Quận Đống Đa, Thành phố Hà Nội', N'0553234324', N'https://hamsa.co/', N'Resources\Images\hamsa_banner.jpg', N'https://www.youtube.com/watch?v=9yu9lraVxfI', N'Resources\Images\hamsa.jpg', N'Hamsa Technologies is a leading e-commerce solution company in Vietnam, working with multiple e-commerce platforms. Since 2014 till now, Hamsa did deliver customized e-commerce solutions to more than 1000 clients from all over the world.

We can’t do that without a strong team of unique culture. A happy team makes great services. Great services make clients satisfied. That’s a simple philosophy we have believed and lived with, from Day One.', CAST(N'2020-05-10' AS Date), 0, NULL, NULL, 2, 114, 0)
GO

INSERT [dbo].[NHATUYENDUNG] ([ID_NHATUYENDUNG], [EMAIL], [MATKHAU], [LOGO], [SHOWLOGO], [TENCONGTY], [TENVIETTAT], [NGANHNGHEHOATDONG], [QUYMOCONGTY], [DIACHI], [SDT], [WEBSITE], [BANNERCONGTY], [VIDEOGIOITHIEU], [HINHANH], [GIOITHIEUCONGTY], [NGAYDANGKY], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [SOCIALLOGIN]) VALUES (31, N'ntd17@gmail.com', N'111111', N'Resources\Images\fwd-logo.jpg', 1, N'FWD VIETNAM LIFE INSURANCE COMPANY LIMITED', N'FWD', 17, 400, N'Tầng 11, Diamond Plaza, 34 Lê Duẩn, Bến Nghé, Quận 1, Hồ Chí Minh', N'0234413124', N'https://www.fwd.com.vn/', N'Resources\Images\FWD.jpg', N'https://www.youtube.com/watch?v=9yu9lraVxfI', N'Resources\Images\fwd2.jpg', N'FWD Việt Nam được thành lập vào năm 2016 và là thành viên của Tập đoàn bảo hiểm FWD - có phạm vi hoạt động rộng khắp Châu Á. Với chiến lược ưu tiên phát triển công nghệ số, FWD Việt Nam là công ty bảo hiểm khác biệt, có nền tảng vững chắc dựa trên các thế mạnh riêng có: sản phẩm đột phá, hệ thống phân phối tập trung vào chất lượng, số hóa mọi quy trình và chiến lược thương hiệu khác biệt.

FWD Việt Nam liên tục là một trong những công ty bảo hiểm nhân thọ có tốc độ tăng trưởng nhanh nhất thị trường.

Để biết thêm thông tin, vui lòng truy cập website www.fwd.com.vn.


Thông tin về Tập đoàn bảo hiểm FWD

FWD là tập đoàn bảo hiểm có hoạt động kinh doanh tại Hồng Kông, Ma Cao, Thái Lan, Indonesia, Philippines, Singapore, Việt Nam, Nhật Bản và Malaysia. FWD cung cấp sản phẩm bảo hiểm nhân thọ, bảo hiểm sức khỏe, bảo hiểm phi nhân thọ, bảo hiểm trợ cấp người lao động và dòng sản phẩm Takaful theo luật Shariah.
', CAST(N'2020-05-10' AS Date), 0, NULL, NULL, 2, 132, 0)
GO

INSERT [dbo].[NHATUYENDUNG] ([ID_NHATUYENDUNG], [EMAIL], [MATKHAU], [LOGO], [SHOWLOGO], [TENCONGTY], [TENVIETTAT], [NGANHNGHEHOATDONG], [QUYMOCONGTY], [DIACHI], [SDT], [WEBSITE], [BANNERCONGTY], [VIDEOGIOITHIEU], [HINHANH], [GIOITHIEUCONGTY], [NGAYDANGKY], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [SOCIALLOGIN]) VALUES (32, N'ntd18@gmail.com', N'111111', N'Resources\Images\skflogo.jpg', 1, N'SKF VIETNAM CO. LTD', N'SKF', 7, 99, N'Room 701, 7th Floor, Plaza Thang Long, 105 Lang Ha, Lang Ha Ward, Dong Da District, Hanoi | R2, 4Fl., Crescent Plaza, 105 Ton Dat Tien, Tan Phu ward, Dist.7, HCMC', N'0925656412', N'https://www.skf.com/', N'Resources\Images\skf1.jpg', N'https://www.youtube.com/watch?v=9yu9lraVxfI', N'Resources\Images\skf2.jpg', N'You may know SKF as the world leading supplier of products and solutions in the rolling bearing business. In addition, we are one of the leading players in seals, mechatronics, lubrication systems, and services which include technical support, maintenance services, condition monitoring and training. The inherent skills and competence of our around 40 000 employees help us to fulfil our objective of creating more intelligent, sustainable and innovative customer solutions. SKF is a truly global company with presence in more than 130 countries, serving an extensive range of industries and customers worldwide.
SKF officially presented in Vietnam since 1991 with the head office in Ho Chi Minh City and the branch in Hanoi. Especially after more than 28 years, we established our distribution network with 35 Authorized Distributors throughout the country.', CAST(N'2020-05-10' AS Date), 0, NULL, NULL, 2, 122, 0)
GO

INSERT [dbo].[NHATUYENDUNG] ([ID_NHATUYENDUNG], [EMAIL], [MATKHAU], [LOGO], [SHOWLOGO], [TENCONGTY], [TENVIETTAT], [NGANHNGHEHOATDONG], [QUYMOCONGTY], [DIACHI], [SDT], [WEBSITE], [BANNERCONGTY], [VIDEOGIOITHIEU], [HINHANH], [GIOITHIEUCONGTY], [NGAYDANGKY], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [SOCIALLOGIN]) VALUES (33, N'ntd19@gmail.com', N'111111', N'Resources\Images\nguyenphat_logo.png', 1, N'CÔNG TY CỔ PHẦN LOGISTICS NGUYỄN PHÁT', N'NP', 29, 499, N'151/1 Võ Văn Tần, Phường 6, Quận 3, TP. HCM', N'2121800254', N'https://www.np.com/', N'Resources\Images\nguyenphat_banner.jpg', N'https://www.youtube.com/watch?v=9yu9lraVxfI', N'Resources\Images\nguyenphat2.jpg', N'Công ty Cổ phần Logistics Nguyễn Phát thành lập từ năm 1996, hoạt động trong lĩnh vực vận tải hàng hóa container tại các Cảng TP. HCM và các tỉnh lân cận, vận tải hàng đông lạnh bằng container tuyến trục Bắc – Nam, chuyển phát nhanh hàng hóa nội địa. Hoạt động trong lĩnh vực cho thuê kho bãi, tổng diện tích lên đến 80.000m2 tại các quận, huyện trên TP. HCM.  Hơn 20 năm phát triển, hiện tại Công ty Nguyễn Phát đã trở thành đơn vị vận tải hàng đầu Việt Nam với 200 xe Container lạnh, đầu kéo Container, xe tải các loại. Đội ngũ gần 300 nhân viên hoạt động tại các chi nhánh của công ty. Hiện là đối tác vận tải chính của các công ty nổi tiếng như Unilever, Kido, Kangaroo, DHL, Indo Trans Logistics (ITL),... Chiến lược của Nguyễn Phát là phát triển thêm các dịch vụ như phát chuyển nhanh bưu phẩm, bưu kiện, dịch vụ giao nhận quốc tế, hoàn thiện chuỗi cung ứng dịch vụ Logistics nhằm đáp ứng tối đa nhu cầu của khách hàng.', CAST(N'2020-05-10' AS Date), 0, NULL, NULL, 2, 129, 0)
GO

INSERT [dbo].[NHATUYENDUNG] ([ID_NHATUYENDUNG], [EMAIL], [MATKHAU], [LOGO], [SHOWLOGO], [TENCONGTY], [TENVIETTAT], [NGANHNGHEHOATDONG], [QUYMOCONGTY], [DIACHI], [SDT], [WEBSITE], [BANNERCONGTY], [VIDEOGIOITHIEU], [HINHANH], [GIOITHIEUCONGTY], [NGAYDANGKY], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [SOCIALLOGIN]) VALUES (34, N'ntd20@gmail.com', N'111111', N'Resources\Images\awc_logo.png', 1, N'AWC CO., LTD', N'AWC', 24, 24, N'89L Nguyễn Ảnh Thủ, Tổ 32, KP3, Phường Hiệp Thành, Quận 12, Tp HCM', N'0246674571', N'https://www.AWC.com/', N'Resources\Images\awc-banner.jfif', N'https://www.youtube.com/watch?v=9yu9lraVxfI', N'Resources\Images\acw.jpeg', N'AWC Co., Ltd - Công ty mẹ tại Malaysia, được thành lập tại Việt Nam từ năm 2007.

Thế mạnh của AWC tại thị trường Việt nam bao gồm:
- Thiết kế, lập dự án, thi công xây dựng, cung cấp thiết bị, lắp đặt M&E, vận hành thử, nghiệm thu, bảo hành và bảo trì nhiều hệ thống xử lý nước cấp và nước thải.
- Vận hành, bảo trì định kỳ nhiều hệ thống xử lý nước thải, nước cấp cho các nhà máy và khu công nghiệp, khu dân cư.
- Cung cấp dịch vụ hỗ trợ 24/7 xử lý sự cố các hệ thống xử lý môi trường.

Khách hàng tiêu biểu của AWC gồm nhiều công ty đa quốc gia như: Intel, First solar, Bayer, SP Setia,...', CAST(N'2020-05-10' AS Date), 0, NULL, NULL, 2, 102, 0)
GO

INSERT [dbo].[NHATUYENDUNG] ([ID_NHATUYENDUNG], [EMAIL], [MATKHAU], [LOGO], [SHOWLOGO], [TENCONGTY], [TENVIETTAT], [NGANHNGHEHOATDONG], [QUYMOCONGTY], [DIACHI], [SDT], [WEBSITE], [BANNERCONGTY], [VIDEOGIOITHIEU], [HINHANH], [GIOITHIEUCONGTY], [NGAYDANGKY], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [SOCIALLOGIN]) VALUES (35, N'ntd21@gmail.com', N'111111', N'Resources\Images\pharvina-logo.png', 1, N'CÔNG TY CỔ PHẦN DƯỢC PHẨM PHARVINA', N'Pharvina', 27, 99, N'Số 101, Láng Hạ, quận Đống Đa, Thành phố Hà Nội', N'0463299997', N'https://pharvina.com/', N'Resources\Images\pharvina-banner.png', N'https://www.youtube.com/watch?v=9yu9lraVxfI', N'Resources\Images\pharvina3.jpg', N'PHARVINA là doanh nghiệp nghiên cứu, sản xuất thực phẩm chức năng
PHARVINA kết hợp các bài thuốc cổ phương, các cây thuốc quý hiếm tại Việt Nam và các vùng lãnh thổ khác trên thế giới nhằm tạo ra các sản phẩm thực phẩm chức năng cao cấp có nguồn gốc thiên nhiên kết hợp với tinh hoa của khoa học và công nghệ hiện đại. Các sản phẩm của PHARVINA luôn được đầu tư giá trị chất xám cao, được xây dựng bởi những chuyên gia, ban cố vấn, lãnh đạo có chuyên môn, đội ngũ nhân viên tâm huyết gắn bó sâu sắc với nghề', CAST(N'2020-05-10' AS Date), 0, NULL, NULL, 2, 124, 0)
GO

INSERT [dbo].[NHATUYENDUNG] ([ID_NHATUYENDUNG], [EMAIL], [MATKHAU], [LOGO], [SHOWLOGO], [TENCONGTY], [TENVIETTAT], [NGANHNGHEHOATDONG], [QUYMOCONGTY], [DIACHI], [SDT], [WEBSITE], [BANNERCONGTY], [VIDEOGIOITHIEU], [HINHANH], [GIOITHIEUCONGTY], [NGAYDANGKY], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [SOCIALLOGIN]) VALUES (36, N'ntd22@gmail.com', N'111111', N'Resources\Images\mitlab-logo.png', 1, N'CÔNG TY TNHH THIẾT BỊ MINH TÂM (MITALAB CO., LTD)', N'Mitalab', 31, 499, N'Tòa nhà Mitalab 76 Giảng Võ, Đống Đa, Hà Nội.', N'0463299997', N'http://mitalab.vn', N'Resources\Images\mitlab-banner.jpg', N'https://www.youtube.com/watch?v=9yu9lraVxfI', N'Resources\Images\mitlab5.jpg', N'Mitalab Co.,Ltd là nhà phân phối thiết bị xét nghiệm y tế độc quyền tại Việt Nam cho các hãng lớn uy tín trên thế giới như Beckman Coulter, BioSystems, IL, Dialab...

Thành lập từ năm 2001, Mitalab đã có 15 năm kinh nghiệm trong lĩnh vực thiết bị y tế. Chúng tôi chuyên phân phối các loại thiết bị và hóa chất xét nghiệm, phần mềm quản lý cho các khoa xét nghiệm; cài đặt hóa chất, bảo trì, bảo dưỡng và sửa chữa các loại máy xét nghiệm tại các bệnh viện và phòng xét nghiệm trên toàn quốc.', CAST(N'2020-05-10' AS Date), 0, NULL, NULL, 2, 126, 0)
GO

INSERT [dbo].[NHATUYENDUNG] ([ID_NHATUYENDUNG], [EMAIL], [MATKHAU], [LOGO], [SHOWLOGO], [TENCONGTY], [TENVIETTAT], [NGANHNGHEHOATDONG], [QUYMOCONGTY], [DIACHI], [SDT], [WEBSITE], [BANNERCONGTY], [VIDEOGIOITHIEU], [HINHANH], [GIOITHIEUCONGTY], [NGAYDANGKY], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [SOCIALLOGIN]) VALUES (37, N'ntd23@gmail.com', N'111111', N'Resources\Images\vinhhung-logo.png', 1, N'Công Ty Cổ Phần Giấy CP ( Chi Nhánh Công Ty Cổ Phần Giấy Phùng Vĩnh Hưng )', N'CP', 30, 499, N'C8, C9, C10-1 đường N5B, KCN lê Minh Xuân 3, xã Lê Minh Xuân, Bình chánh', N'0463299997', N'http://cp.vn', N'Resources\Images\vinhhung-banner.png', N'https://www.youtube.com/watch?v=9yu9lraVxfI', N'Resources\Images\vinhhung6.jfif', N'Chúng tôi luôn tiên phong trong việc gắn sự phát triển bền vững của công ty với lợi ích người tiêu dùng và trách nhiệm với môi trường. Vì vậy, việc triển khai những sản phẩm chất lượng, an toàn và thân thiện với môi trường được chú trọng: từ giấy in sách bảo vệ mắt, giấy an toàn thực phẩm cho đến các sản phẩm giấy được sản xuất từ rừng trồng có quản lý. Chúng tôi cũng tự hào là nhà thương mại giấy đầu tiên tại Việt Nam đạt chứng nhận FSC-CoC – tiêu chuẩn về chuỗi hành trình sản phẩm do Hội đồng quản trị rừng thế giới ban hành.

Mỗi thành viên trong tập thể Phùng Vĩnh Hưng/ CP Paper đều nhận được sự hướng dẫn, hỗ trợ nhiệt tình từ đồng nghiệp và cấp trên. Chúng tôi luôn tạo môi trường mở với thông tin trao đổi đa chiều. Các thành viên được lắng nghe, ghi nhận kịp thời, tạo điều kiện, trao cơ hội để hoàn thiện bản thân, phát triển nghề nghiệp song hành cùng sự phát triển chung của công ty.

Đến với chúng tôi, bạn sẽ chung tay vào hành trình “Tạo dựng niềm tin từ uy tín” cho chính thương hiệu cá nhân của mình và thương hiệu chung của công ty Việt Nam trên thị trường trong và ngoài nước.', CAST(N'2020-05-10' AS Date), 0, NULL, NULL, 2, 128, 0)
GO

INSERT [dbo].[NHATUYENDUNG] ([ID_NHATUYENDUNG], [EMAIL], [MATKHAU], [LOGO], [SHOWLOGO], [TENCONGTY], [TENVIETTAT], [NGANHNGHEHOATDONG], [QUYMOCONGTY], [DIACHI], [SDT], [WEBSITE], [BANNERCONGTY], [VIDEOGIOITHIEU], [HINHANH], [GIOITHIEUCONGTY], [NGAYDANGKY], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [SOCIALLOGIN]) VALUES (38, N'ntd24@gmail.com', N'111111', N'Resources\Images\tas-logo.jfif', 1, N'Công ty TNHH TAS Vietnam', N'TAS', 2, 24, N'Toà nhà PaxSky, 123 Nguyễn Đình Chiểu, Phường 6, Quận 3, Thành phố Hồ Chí Minh', N'0246674571', N'http://www.kk-tas.co.jp', N'Resources\Images\tas-banner.jfif', N'https://www.youtube.com/watch?v=9yu9lraVxfI', N'Resources\Images\51919782_2190703350996657_1759878813929439232_o(1).jpg', N'Công ty TNHH TAS Vietnam được đầu tư 100% vốn từ Công ty TAS Nhật Bản.
TAS Vietnam hiện đã và đang phát triển các ứng dụng di động đáp ứng trên hai nền tảng iOS và Android, các ứng dụng web. Các ứng dụng này được sử dụng tại thị trường Nhật Bản.

Với đội ngũ nhân viên thân thiện, môi trường làm việc năng động, đến với TAS Vietnam bạn sẽ có cơ hội phát triển bản thân và thăng tiến trong tương lai.', CAST(N'2020-05-10' AS Date), 0, NULL, NULL, 2, 30, 0)
GO

INSERT [dbo].[NHATUYENDUNG] ([ID_NHATUYENDUNG], [EMAIL], [MATKHAU], [LOGO], [SHOWLOGO], [TENCONGTY], [TENVIETTAT], [NGANHNGHEHOATDONG], [QUYMOCONGTY], [DIACHI], [SDT], [WEBSITE], [BANNERCONGTY], [VIDEOGIOITHIEU], [HINHANH], [GIOITHIEUCONGTY], [NGAYDANGKY], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [SOCIALLOGIN]) VALUES (39, N'ntd25@gmail.com', N'111111', N'Resources\Images\vin-logo.jfif', 1, N'Vingroup Big Data Institute (VinBDI) ', N'VinBDI', 1, 300, N'Tầng 5 - Tower 1, Times City 458 Minh Khai, quận Hai Bà Trưng, Hà Nội, Việt Nam', N'0232141311', N'http://www.vinbdi.com', N'Resources\Images\vin.jpg', N'https://www.youtube.com/watch?v=9yu9lraVxfI', N'Resources\Images\vin3.jpg', N'The Vingroup Big Data Institute(VinBDI) was established by Vingroup in August 2018, aiming to conduct high-impact research on important areas of Big Data and Artificial Intelligence (AI). The institute promotes fundamental research as well as investigating novel and highly-applicable technologies. 

VinBDI is seeking highly qualified applicants for the position of Research Scientist who conducts research & development in the areas of Machine Learning (ML) with particular focus on Deep Learning (DL), ​Computer Vision (CV), ​Natural Language/Speech Understanding (NLP), and Medical Image Analysis. You will work on some of the most challenging technical problems to create new AI-based solutions. After building prototypes that demonstrate the promise of your work, you will collaborate with the software engineering team to integrate your work into products as well as to publish the work in top-tier journals/conferences.', CAST(N'2020-05-10' AS Date), 0, NULL, NULL, 2, 25, 0)
GO

INSERT [dbo].[NHATUYENDUNG] ([ID_NHATUYENDUNG], [EMAIL], [MATKHAU], [LOGO], [SHOWLOGO], [TENCONGTY], [TENVIETTAT], [NGANHNGHEHOATDONG], [QUYMOCONGTY], [DIACHI], [SDT], [WEBSITE], [BANNERCONGTY], [VIDEOGIOITHIEU], [HINHANH], [GIOITHIEUCONGTY], [NGAYDANGKY], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [SOCIALLOGIN]) VALUES (77, N'ntd26@gmail.com', N'111111', N'Resources\Images\thanhxuan-logo.png', 1, N'Toyota Thanh Xuân', N'TX', 32, 300, N'Tầng 5 - Tower 1, Times City 458 Minh Khai, quận Hai Bà Trưng, Hà Nội, Việt Nam', N'0463299997', N'www.toyotathanhxuan.com.vn', N'Resources\Images\thanhxuan-banner1.jpg', N'https://www.youtube.com/watch?v=9yu9lraVxfI', N'Resources\Images\thanhxuan1.jpg', N'Công ty TNHH Toyota Thanh Xuân được UBND Thành phố Hà Nội cấp giấy phép Đầu tư ngày 11 tháng 5 năm 2011, sau một năm triển khai việc xây dựng và hoàn thiện cở sở hạ tầng ngày 10/12/2012 đã chính thức đi vào hoạt động.
Là thành viên trẻ trong gia đình Toyota Việt Nam, tuy nhiên Toyota Thanh Xuân có thể khẳng định và tự hào đã xây dựng và hoàn thiện được đội ngũ nhân viên, kỹ thuật viên có kiến thức,có trình độ, đầy nhiệt huyết và lòng nhiệt tình. Với phương châm hoạt động “Con người chính là nguồn lực quan trọng và quý giá nhất” Toyota Thanh Xuân sẽ không ngừng đầu tư và phát triển “Nguồn lực Con người” với hy vọng đó sẽ là nguồn lực mạnh mẽ và tiên quyết để xây dựng Công ty ngày từng bước hoàn thiện và phát triển lớn mạnh.', CAST(N'2020-05-10' AS Date), 0, NULL, NULL, 1, 124, 0)
GO

INSERT [dbo].[NHATUYENDUNG] ([ID_NHATUYENDUNG], [EMAIL], [MATKHAU], [LOGO], [SHOWLOGO], [TENCONGTY], [TENVIETTAT], [NGANHNGHEHOATDONG], [QUYMOCONGTY], [DIACHI], [SDT], [WEBSITE], [BANNERCONGTY], [VIDEOGIOITHIEU], [HINHANH], [GIOITHIEUCONGTY], [NGAYDANGKY], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [SOCIALLOGIN]) VALUES (78, N'ntd27@gmail.com', N'111111', N'Resources\Images\haivan-logo.jpg', 1, N'CÔNG TY TNHH THƯƠNG MẠI DỊCH VỤ XÂY DỰNG HẢI VÂN', N'HV', 5, 30, N'39/2A Phú Định, Phường 16, Quận 8, Thành phố Hồ Chí Minh
', N'0463212313', N'http://www.haivan.com', N'Resources\Images\haivan.jfif', N'https://www.youtube.com/watch?v=9yu9lraVxfI', N'Resources\Images\haivan5.jpg', N'Công ty TNHH Hải Vân chuyên công trình xây dựng. Sở hữu đội ngũ kỹ sư, kiến trúc sư trẻ, năng động, sáng tạo và luôn hết mình trong mọi công trình, công ty cung cấp các dịch vụ xây dựng phần thô hay trọn gói biệt thự, nhà phố, căn hộ, chung cư đến thiết kế nội thất cho quán cà phê, khách sạn, resort. Chất lượng thi công luôn được đánh giá là tỉ mỉ cẩn thận, an toàn, và đúng tiến độ.', CAST(N'2020-05-10' AS Date), 0, NULL, NULL, 2, 127, 0)
GO

INSERT [dbo].[NHATUYENDUNG] ([ID_NHATUYENDUNG], [EMAIL], [MATKHAU], [LOGO], [SHOWLOGO], [TENCONGTY], [TENVIETTAT], [NGANHNGHEHOATDONG], [QUYMOCONGTY], [DIACHI], [SDT], [WEBSITE], [BANNERCONGTY], [VIDEOGIOITHIEU], [HINHANH], [GIOITHIEUCONGTY], [NGAYDANGKY], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [SOCIALLOGIN]) VALUES (79, N'ntd28@gmail.com', N'111111', N'Resources\Images\kimma-logo.jpg', 1, N' shop Thời trang nữ cao cấp Elise Kim Mã', N'KM', 33, 24, N'53 Cao Thắng, quận 3', N'0122464321', N'https://elise.vn/', N'Resources\Images\km-banner.jpg', N'https://www.youtube.com/watch?v=9yu9lraVxfI', N'Resources\Images\km.jpg', N'Elise được ra đời từ sự đam mê kinh doanh và thời trang. Vượt qua nhiều thử thách, Elise đã trở thành công ty có tiềm năng bậc nhất của ngành thời trang Việt Nam. Công ty được nhiều tập đoàn thời trang lớn quốc tế đánh giá cao và chọn làm đối tác hợp tác lâu dài.
Với người tiêu dùng trong nước, Elise được biết đến như thương hiệu thời trang hàng đầu. Đặc biệt, khách hàng bị quyến rũ bởi phong cách hiện đại, sang trọng của Elise ngang tầm với các thương hiệu nổi tiếng thế giới.', CAST(N'2020-05-10' AS Date), 0, NULL, NULL, 2, 115, 0)
GO

INSERT [dbo].[NHATUYENDUNG] ([ID_NHATUYENDUNG], [EMAIL], [MATKHAU], [LOGO], [SHOWLOGO], [TENCONGTY], [TENVIETTAT], [NGANHNGHEHOATDONG], [QUYMOCONGTY], [DIACHI], [SDT], [WEBSITE], [BANNERCONGTY], [VIDEOGIOITHIEU], [HINHANH], [GIOITHIEUCONGTY], [NGAYDANGKY], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [SOCIALLOGIN]) VALUES (80, N'ntd29@gmail.com', N'111111', N'Resources\Images\sichien-logo.png', 1, N'	
Siêu thị điện máy Sỹ Chiến', N'SC', 33, 23, N'23 Minh Tân, quận 7', N'0213213213', N'dienmaysychien.com', N'Resources\Images\sichien-banner.jpg', N'https://www.youtube.com/watch?v=9yu9lraVxfI', N'Resources\Images\sichien3.jpg', N'Điện Máy Chính Hãng tại Diễn Châu - Điện máy giá tốt nhất trên cả nước - Điện máy Trả góp lãi suất thấp - Điện máy Uy tín nhất - Điện Máy Sỹ Chiến cam kết 100% hàng chính hãng.', CAST(N'2020-05-10' AS Date), 0, NULL, NULL, 2, 118, 0)
GO

INSERT [dbo].[NHATUYENDUNG] ([ID_NHATUYENDUNG], [EMAIL], [MATKHAU], [LOGO], [SHOWLOGO], [TENCONGTY], [TENVIETTAT], [NGANHNGHEHOATDONG], [QUYMOCONGTY], [DIACHI], [SDT], [WEBSITE], [BANNERCONGTY], [VIDEOGIOITHIEU], [HINHANH], [GIOITHIEUCONGTY], [NGAYDANGKY], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [SOCIALLOGIN]) VALUES (81, N'ntd30@gmail.com', N'111111', N'Resources\Images\koffmann-logo.png', 1, N'Cửa thép vân gỗ Koff Mann', N'KM', 33, 89, N'77 Nguyễn Viết Xuân Tp. Vinh - Tỉnh Nghệ An', N'0213213321', N'https://koffmann.com', N'Resources\Images\koffmann-banner.png', N'https://www.youtube.com/watch?v=9yu9lraVxfI', N'Resources\Images\koffmann3.jpg', N'Là doanh nghiệp chuyên sản xuất và cung cấp các sản phẩm: cửa thép vân gỗ, cửa thép chống cháy, cửa sổ vân gỗ
cửa Luxury, cửa Luxury thủy lực – vách kính, phụ kiện cửa các loại.', CAST(N'2020-05-10' AS Date), 0, NULL, NULL, 2, 108, 0)
GO

INSERT [dbo].[NHATUYENDUNG] ([ID_NHATUYENDUNG], [EMAIL], [MATKHAU], [LOGO], [SHOWLOGO], [TENCONGTY], [TENVIETTAT], [NGANHNGHEHOATDONG], [QUYMOCONGTY], [DIACHI], [SDT], [WEBSITE], [BANNERCONGTY], [VIDEOGIOITHIEU], [HINHANH], [GIOITHIEUCONGTY], [NGAYDANGKY], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [SOCIALLOGIN]) VALUES (82, N'ks@gmail.com', N'111111', N'Resources\Images\cokhisaigon-logo.jfif', 1, N'Cơ Khí Công Nghiệp Sài Gòn', N'cksg', 22, 200, N'191 Bà Triệu, quận Cầu giấy, Hà Nội, Việt Nam', N'0213131133', N'https://cokhisaigon.com', N'Resources\Images\cokhisaigon-banner.jpg', N'https://www.youtube.com/watch?v=9yu9lraVxfI', N'Resources\Images\cokhisaigon2.jpg', N'Công ty TNHH Cơ Khí Công Nghiệp Sài Gòn (CCSC) là một trong những doanh nghiệp chuyên thiết kế, chế tạo máy công nghiệp phục vụ cho ngành thép tại Việt Nam.Các sản phẩm đã đưa vào hoạt động:- Máy cán tôn sóng vuông, sóng tròn, sóng giả ngói các loại.- Máy cán vòm, máy dập vòm, máy chấn máng xối- Máy cán cửa sắt, cửa cuốn các loại- Máy cán xà gồ C, Z, Máy xả băng thép.', CAST(N'2020-05-10' AS Date), 0, NULL, NULL, 2, 109, 0)
GO

INSERT [dbo].[NHATUYENDUNG] ([ID_NHATUYENDUNG], [EMAIL], [MATKHAU], [LOGO], [SHOWLOGO], [TENCONGTY], [TENVIETTAT], [NGANHNGHEHOATDONG], [QUYMOCONGTY], [DIACHI], [SDT], [WEBSITE], [BANNERCONGTY], [VIDEOGIOITHIEU], [HINHANH], [GIOITHIEUCONGTY], [NGAYDANGKY], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [SOCIALLOGIN]) VALUES (83, N'ntd31@gmail.com', N'111111', N'Resources\Images\tech-logo.png', 1, N'Ngân Hàng Thương Mại Cổ Phần Kỹ Thương Việt Nam', N'Tech', 14, 500, N'191 Bà Triệu, quận Hai Bà Trưng, Hà Nội, Việt Nam', N'0213131133', N'https://techcombankjobs.com/', N'Resources\Images\tech-banner.jpg', N'https://www.youtube.com/watch?v=9yu9lraVxfI', N'Resources\Images\tech3.png', N'Ngân hàng thương mại cổ phần Kỹ Thương Việt Nam là một ngân hàng thương mại cổ phần của Việt Nam, được thành lập năm 1993 với số vốn ban đầu 20 tỷ đồng. Trụ sở chính của Techcombank được đặt tại 191 phố Bà Triệu, Hà Nội.', CAST(N'2020-06-02' AS Date), 0, NULL, NULL, 1, 118, 0)
GO

INSERT [dbo].[NHATUYENDUNG] ([ID_NHATUYENDUNG], [EMAIL], [MATKHAU], [LOGO], [SHOWLOGO], [TENCONGTY], [TENVIETTAT], [NGANHNGHEHOATDONG], [QUYMOCONGTY], [DIACHI], [SDT], [WEBSITE], [BANNERCONGTY], [VIDEOGIOITHIEU], [HINHANH], [GIOITHIEUCONGTY], [NGAYDANGKY], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [SOCIALLOGIN]) VALUES (86, N'ntd32@gmail.com', N'111111', N'Resources\Images\vnpt-logo.jpg', 1, N'Tập đoàn Bưu chính viễn thông Việt Nam', N'vnpt', 8, 2000, N'Tầng 7 Tòa nhà Bưu Điện, 271 Nguyễn Văn Linh, Phường Vĩnh Trung, Quận Thanh Khê, Đà Nẵng
', N'0213131133', N'vnpt.vn', N'Resources\Images\vnpt-banner.jpg', N'https://www.youtube.com/watch?v=9yu9lraVxfI', N'Resources\Images\vnpt2.jpg', N'Tập đoàn Bưu chính Viễn thông Việt Nam là một doanh nghiệp nhà nước chuyên đầu tư, sản xuất, kinh doanh trong lĩnh vực bưu chính và viễn thông tại Việt Nam.', CAST(N'2020-07-10' AS Date), 0, NULL, NULL, 1, 114, 0)
GO

INSERT [dbo].[NHATUYENDUNG] ([ID_NHATUYENDUNG], [EMAIL], [MATKHAU], [LOGO], [SHOWLOGO], [TENCONGTY], [TENVIETTAT], [NGANHNGHEHOATDONG], [QUYMOCONGTY], [DIACHI], [SDT], [WEBSITE], [BANNERCONGTY], [VIDEOGIOITHIEU], [HINHANH], [GIOITHIEUCONGTY], [NGAYDANGKY], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [SOCIALLOGIN]) VALUES (87, N'vinamilk@gmail.com', N'111111', N'Resources\Images\vinamilk-logo.jfif', 1, N'CÔNG TY CỔ PHẦN SỮA VIỆT NAM - VINAMILK', N'Vinamilk', 30, 600, N'Tầng 7 Tòa nhà Bưu Điện, 271 Nguyễn Văn Linh, Phường Vĩnh Trung, Quận Thanh Khê, Đà Nẵng
', N'0213131133', N'https://www.vinamilk.com.vn/', N'Resources\Images\vinamilk-banner.jpg', N'https://www.youtube.com/watch?v=9yu9lraVxfI', N'Resources\Images\vinamilk4.jpg', N'Công ty Cổ phần Sữa Việt Nam, tên khác: Vinamilk, mã chứng khoán HOSE: VNM, là một công ty sản xuất, kinh doanh sữa và các sản phẩm từ sữa cũng như thiết bị máy móc liên quan tại Việt Nam. Theo thống kê của Chương trình Phát triển Liên Hiệp Quốc, đây là công ty lớn thứ 15 tại Việt Nam vào năm 2007.', CAST(N'2020-07-16' AS Date), 0, NULL, NULL, 1, 110, 0)
GO

INSERT [dbo].[NHATUYENDUNG] ([ID_NHATUYENDUNG], [EMAIL], [MATKHAU], [LOGO], [SHOWLOGO], [TENCONGTY], [TENVIETTAT], [NGANHNGHEHOATDONG], [QUYMOCONGTY], [DIACHI], [SDT], [WEBSITE], [BANNERCONGTY], [VIDEOGIOITHIEU], [HINHANH], [GIOITHIEUCONGTY], [NGAYDANGKY], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [SOCIALLOGIN]) VALUES (1089, N'asxi1998@gmail.com', N'asxi1998@gmail.com', N'Resources\Images\company.jpg', NULL, N'AAAAAAAAAAAAAAAAA', N'TPA', 6, 100, N'asxi1998@gmail.com', NULL, N'1111111111111', N'Resources\Images\alliances_banner.jpg', N'https://www.youtube.com/watch?v=9yu9lraVxfI', N'Resources\Images\alliances.jpg', N'Công ty Cổ phần Sữa Việt Nam, tên khác: Vinamilk, mã chứng khoán HOSE: VNM, là một công ty sản xuất, kinh doanh sữa và các sản phẩm từ sữa cũng như thiết bị máy móc liên quan tại Việt Nam. Theo thống kê của Chương trình Phát triển Liên Hiệp Quốc, đây là công ty lớn thứ 15 tại Việt Nam vào năm 2007.', CAST(N'2020-07-25' AS Date), 0, NULL, NULL, 1, 10, 0)
GO

INSERT [dbo].[NHATUYENDUNG] ([ID_NHATUYENDUNG], [EMAIL], [MATKHAU], [LOGO], [SHOWLOGO], [TENCONGTY], [TENVIETTAT], [NGANHNGHEHOATDONG], [QUYMOCONGTY], [DIACHI], [SDT], [WEBSITE], [BANNERCONGTY], [VIDEOGIOITHIEU], [HINHANH], [GIOITHIEUCONGTY], [NGAYDANGKY], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [DOUUTIEN], [LUOTXEM], [SOCIALLOGIN]) VALUES (2089, N'mrvanngoc182@gmail.com', N'111111', N'Resources\Images\company.jpg', NULL, N'Công ty TNHH Viva', N'VVS', 33, 200, N'HCM', NULL, NULL, N'Resources/Images/banner_new.jpg', N'https://www.youtube.com/watch?v=9yu9lraVxfI', N'Resources\Images\alliances.jpg', N'Công ty Cổ phần Sữa Việt Nam, tên khác: Vinamilk, mã chứng khoán HOSE: VNM, là một công ty sản xuất, kinh doanh sữa và các sản phẩm từ sữa cũng như thiết bị máy móc liên quan tại Việt Nam. Theo thống kê của Chương trình Phát triển Liên Hiệp Quốc, đây là công ty lớn thứ 15 tại Việt Nam vào năm 2007.', CAST(N'2020-07-28' AS Date), 0, NULL, NULL, 1, 1, 0)
GO

SET IDENTITY_INSERT [dbo].[NHATUYENDUNG] OFF
GO

INSERT [dbo].[NOILAMVIECMONGMUON] ([ID_CONGVIECMONGMUON], [ID_THANHPHO]) VALUES (31, 1)
GO

INSERT [dbo].[NOILAMVIECMONGMUON] ([ID_CONGVIECMONGMUON], [ID_THANHPHO]) VALUES (33, 1)
GO

INSERT [dbo].[NOILAMVIECMONGMUON] ([ID_CONGVIECMONGMUON], [ID_THANHPHO]) VALUES (1028, 1)
GO

INSERT [dbo].[NOILAMVIECMONGMUON] ([ID_CONGVIECMONGMUON], [ID_THANHPHO]) VALUES (1028, 4)
GO

INSERT [dbo].[NOILAMVIECMONGMUON] ([ID_CONGVIECMONGMUON], [ID_THANHPHO]) VALUES (2031, 1)
GO

INSERT [dbo].[NOILAMVIECMONGMUON] ([ID_CONGVIECMONGMUON], [ID_THANHPHO]) VALUES (2044, 1)
GO

INSERT [dbo].[NOILAMVIECMONGMUON] ([ID_CONGVIECMONGMUON], [ID_THANHPHO]) VALUES (2084, 1)
GO

INSERT [dbo].[NOILAMVIECMONGMUON] ([ID_CONGVIECMONGMUON], [ID_THANHPHO]) VALUES (2084, 2)
GO

INSERT [dbo].[NOILAMVIECMONGMUON] ([ID_CONGVIECMONGMUON], [ID_THANHPHO]) VALUES (2084, 79)
GO

INSERT [dbo].[NOILAMVIECMONGMUON] ([ID_CONGVIECMONGMUON], [ID_THANHPHO]) VALUES (4087, 1)
GO

INSERT [dbo].[NOILAMVIECMONGMUON] ([ID_CONGVIECMONGMUON], [ID_THANHPHO]) VALUES (4087, 48)
GO

INSERT [dbo].[NOILAMVIECMONGMUON] ([ID_CONGVIECMONGMUON], [ID_THANHPHO]) VALUES (4087, 79)
GO

INSERT [dbo].[NOILAMVIECMONGMUON] ([ID_CONGVIECMONGMUON], [ID_THANHPHO]) VALUES (4088, 1)
GO

INSERT [dbo].[NOILAMVIECMONGMUON] ([ID_CONGVIECMONGMUON], [ID_THANHPHO]) VALUES (4088, 48)
GO

INSERT [dbo].[NOILAMVIECMONGMUON] ([ID_CONGVIECMONGMUON], [ID_THANHPHO]) VALUES (4088, 79)
GO

SET IDENTITY_INSERT [dbo].[PHUCLOI] ON
GO

INSERT [dbo].[PHUCLOI] ([ID_PHUCLOI], [TENPHUCLOI]) VALUES (1, N'Thưởng')
GO

INSERT [dbo].[PHUCLOI] ([ID_PHUCLOI], [TENPHUCLOI]) VALUES (2, N'Thi đua')
GO

INSERT [dbo].[PHUCLOI] ([ID_PHUCLOI], [TENPHUCLOI]) VALUES (3, N'Du lịch')
GO

INSERT [dbo].[PHUCLOI] ([ID_PHUCLOI], [TENPHUCLOI]) VALUES (4, N'Khám sức khỏe')
GO

INSERT [dbo].[PHUCLOI] ([ID_PHUCLOI], [TENPHUCLOI]) VALUES (5, N'Thư viện')
GO

INSERT [dbo].[PHUCLOI] ([ID_PHUCLOI], [TENPHUCLOI]) VALUES (6, N'Team Building')
GO

INSERT [dbo].[PHUCLOI] ([ID_PHUCLOI], [TENPHUCLOI]) VALUES (7, N'Nghỉ phép')
GO

INSERT [dbo].[PHUCLOI] ([ID_PHUCLOI], [TENPHUCLOI]) VALUES (8, N'Laptop')
GO

INSERT [dbo].[PHUCLOI] ([ID_PHUCLOI], [TENPHUCLOI]) VALUES (9, N'Trợ cấp đi lại')
GO

INSERT [dbo].[PHUCLOI] ([ID_PHUCLOI], [TENPHUCLOI]) VALUES (10, N'Đào tạo')
GO

INSERT [dbo].[PHUCLOI] ([ID_PHUCLOI], [TENPHUCLOI]) VALUES (11, N'Điện thoại')
GO

INSERT [dbo].[PHUCLOI] ([ID_PHUCLOI], [TENPHUCLOI]) VALUES (12, N'Căn tin')
GO

INSERT [dbo].[PHUCLOI] ([ID_PHUCLOI], [TENPHUCLOI]) VALUES (13, N'Khác')
GO

SET IDENTITY_INSERT [dbo].[PHUCLOI] OFF
GO

SET IDENTITY_INSERT [dbo].[QUANTRIVIEN] ON
GO

INSERT [dbo].[QUANTRIVIEN] ([ID_QTV], [MATKHAU], [EMAIL], [SDT]) VALUES (1, N'admin', N'admin@gmail.com', N'0346006260')
GO

INSERT [dbo].[QUANTRIVIEN] ([ID_QTV], [MATKHAU], [EMAIL], [SDT]) VALUES (2, N'a', N'use', N'0164652323')
GO

SET IDENTITY_INSERT [dbo].[QUANTRIVIEN] OFF
GO

SET IDENTITY_INSERT [dbo].[THANHPHO] ON
GO

INSERT [dbo].[THANHPHO] ([ID_THANHPHO], [TENTHANHPHO]) VALUES (1, N'Thành phố Hà Nội')
GO

INSERT [dbo].[THANHPHO] ([ID_THANHPHO], [TENTHANHPHO]) VALUES (2, N'Tỉnh Hà Giang')
GO

INSERT [dbo].[THANHPHO] ([ID_THANHPHO], [TENTHANHPHO]) VALUES (4, N'Tỉnh Cao Bằng')
GO

INSERT [dbo].[THANHPHO] ([ID_THANHPHO], [TENTHANHPHO]) VALUES (6, N'Tỉnh Bắc Kạn')
GO

INSERT [dbo].[THANHPHO] ([ID_THANHPHO], [TENTHANHPHO]) VALUES (8, N'Tỉnh Tuyên Quang')
GO

INSERT [dbo].[THANHPHO] ([ID_THANHPHO], [TENTHANHPHO]) VALUES (10, N'Tỉnh Lào Cai')
GO

INSERT [dbo].[THANHPHO] ([ID_THANHPHO], [TENTHANHPHO]) VALUES (11, N'Tỉnh Điện Biên')
GO

INSERT [dbo].[THANHPHO] ([ID_THANHPHO], [TENTHANHPHO]) VALUES (12, N'Tỉnh Lai Châu')
GO

INSERT [dbo].[THANHPHO] ([ID_THANHPHO], [TENTHANHPHO]) VALUES (14, N'Tỉnh Sơn La')
GO

INSERT [dbo].[THANHPHO] ([ID_THANHPHO], [TENTHANHPHO]) VALUES (15, N'Tỉnh Yên Bái')
GO

INSERT [dbo].[THANHPHO] ([ID_THANHPHO], [TENTHANHPHO]) VALUES (17, N'Tỉnh Hoà Bình')
GO

INSERT [dbo].[THANHPHO] ([ID_THANHPHO], [TENTHANHPHO]) VALUES (19, N'Tỉnh Thái Nguyên')
GO

INSERT [dbo].[THANHPHO] ([ID_THANHPHO], [TENTHANHPHO]) VALUES (20, N'Tỉnh Lạng Sơn')
GO

INSERT [dbo].[THANHPHO] ([ID_THANHPHO], [TENTHANHPHO]) VALUES (22, N'Tỉnh Quảng Ninh')
GO

INSERT [dbo].[THANHPHO] ([ID_THANHPHO], [TENTHANHPHO]) VALUES (24, N'Tỉnh Bắc Giang')
GO

INSERT [dbo].[THANHPHO] ([ID_THANHPHO], [TENTHANHPHO]) VALUES (25, N'Tỉnh Phú Thọ')
GO

INSERT [dbo].[THANHPHO] ([ID_THANHPHO], [TENTHANHPHO]) VALUES (26, N'Tỉnh Vĩnh Phúc')
GO

INSERT [dbo].[THANHPHO] ([ID_THANHPHO], [TENTHANHPHO]) VALUES (27, N'Tỉnh Bắc Ninh')
GO

INSERT [dbo].[THANHPHO] ([ID_THANHPHO], [TENTHANHPHO]) VALUES (30, N'Tỉnh Hải Dương')
GO

INSERT [dbo].[THANHPHO] ([ID_THANHPHO], [TENTHANHPHO]) VALUES (31, N'Thành phố Hải Phòng')
GO

INSERT [dbo].[THANHPHO] ([ID_THANHPHO], [TENTHANHPHO]) VALUES (33, N'Tỉnh Hưng Yên')
GO

INSERT [dbo].[THANHPHO] ([ID_THANHPHO], [TENTHANHPHO]) VALUES (34, N'Tỉnh Thái Bình')
GO

INSERT [dbo].[THANHPHO] ([ID_THANHPHO], [TENTHANHPHO]) VALUES (35, N'Tỉnh Hà Nam')
GO

INSERT [dbo].[THANHPHO] ([ID_THANHPHO], [TENTHANHPHO]) VALUES (36, N'Tỉnh Nam Định')
GO

INSERT [dbo].[THANHPHO] ([ID_THANHPHO], [TENTHANHPHO]) VALUES (37, N'Tỉnh Ninh Bình')
GO

INSERT [dbo].[THANHPHO] ([ID_THANHPHO], [TENTHANHPHO]) VALUES (38, N'Tỉnh Thanh Hóa')
GO

INSERT [dbo].[THANHPHO] ([ID_THANHPHO], [TENTHANHPHO]) VALUES (40, N'Tỉnh Nghệ An')
GO

INSERT [dbo].[THANHPHO] ([ID_THANHPHO], [TENTHANHPHO]) VALUES (42, N'Tỉnh Hà Tĩnh')
GO

INSERT [dbo].[THANHPHO] ([ID_THANHPHO], [TENTHANHPHO]) VALUES (44, N'Tỉnh Quảng Bình')
GO

INSERT [dbo].[THANHPHO] ([ID_THANHPHO], [TENTHANHPHO]) VALUES (45, N'Tỉnh Quảng Trị')
GO

INSERT [dbo].[THANHPHO] ([ID_THANHPHO], [TENTHANHPHO]) VALUES (46, N'Tỉnh Thừa Thiên Huế')
GO

INSERT [dbo].[THANHPHO] ([ID_THANHPHO], [TENTHANHPHO]) VALUES (48, N'Thành phố Đà Nẵng')
GO

INSERT [dbo].[THANHPHO] ([ID_THANHPHO], [TENTHANHPHO]) VALUES (49, N'Tỉnh Quảng Nam')
GO

INSERT [dbo].[THANHPHO] ([ID_THANHPHO], [TENTHANHPHO]) VALUES (51, N'Tỉnh Quảng Ngãi')
GO

INSERT [dbo].[THANHPHO] ([ID_THANHPHO], [TENTHANHPHO]) VALUES (52, N'Tỉnh Bình Định')
GO

INSERT [dbo].[THANHPHO] ([ID_THANHPHO], [TENTHANHPHO]) VALUES (54, N'Tỉnh Phú Yên')
GO

INSERT [dbo].[THANHPHO] ([ID_THANHPHO], [TENTHANHPHO]) VALUES (56, N'Tỉnh Khánh Hòa')
GO

INSERT [dbo].[THANHPHO] ([ID_THANHPHO], [TENTHANHPHO]) VALUES (58, N'Tỉnh Ninh Thuận')
GO

INSERT [dbo].[THANHPHO] ([ID_THANHPHO], [TENTHANHPHO]) VALUES (60, N'Tỉnh Bình Thuận')
GO

INSERT [dbo].[THANHPHO] ([ID_THANHPHO], [TENTHANHPHO]) VALUES (62, N'Tỉnh Kon Tum')
GO

INSERT [dbo].[THANHPHO] ([ID_THANHPHO], [TENTHANHPHO]) VALUES (64, N'Tỉnh Gia Lai')
GO

INSERT [dbo].[THANHPHO] ([ID_THANHPHO], [TENTHANHPHO]) VALUES (66, N'Tỉnh Đắk Lắk')
GO

INSERT [dbo].[THANHPHO] ([ID_THANHPHO], [TENTHANHPHO]) VALUES (67, N'Tỉnh Đắk Nông')
GO

INSERT [dbo].[THANHPHO] ([ID_THANHPHO], [TENTHANHPHO]) VALUES (68, N'Tỉnh Lâm Đồng')
GO

INSERT [dbo].[THANHPHO] ([ID_THANHPHO], [TENTHANHPHO]) VALUES (70, N'Tỉnh Bình Phước')
GO

INSERT [dbo].[THANHPHO] ([ID_THANHPHO], [TENTHANHPHO]) VALUES (72, N'Tỉnh Tây Ninh')
GO

INSERT [dbo].[THANHPHO] ([ID_THANHPHO], [TENTHANHPHO]) VALUES (74, N'Tỉnh Bình Dương')
GO

INSERT [dbo].[THANHPHO] ([ID_THANHPHO], [TENTHANHPHO]) VALUES (75, N'Tỉnh Đồng Nai')
GO

INSERT [dbo].[THANHPHO] ([ID_THANHPHO], [TENTHANHPHO]) VALUES (77, N'Tỉnh Bà Rịa - Vũng Tàu')
GO

INSERT [dbo].[THANHPHO] ([ID_THANHPHO], [TENTHANHPHO]) VALUES (79, N'Thành phố Hồ Chí Minh')
GO

INSERT [dbo].[THANHPHO] ([ID_THANHPHO], [TENTHANHPHO]) VALUES (80, N'Tỉnh Long An')
GO

INSERT [dbo].[THANHPHO] ([ID_THANHPHO], [TENTHANHPHO]) VALUES (82, N'Tỉnh Tiền Giang')
GO

INSERT [dbo].[THANHPHO] ([ID_THANHPHO], [TENTHANHPHO]) VALUES (83, N'Tỉnh Bến Tre')
GO

INSERT [dbo].[THANHPHO] ([ID_THANHPHO], [TENTHANHPHO]) VALUES (84, N'Tỉnh Trà Vinh')
GO

INSERT [dbo].[THANHPHO] ([ID_THANHPHO], [TENTHANHPHO]) VALUES (86, N'Tỉnh Vĩnh Long')
GO

INSERT [dbo].[THANHPHO] ([ID_THANHPHO], [TENTHANHPHO]) VALUES (87, N'Tỉnh Đồng Tháp')
GO

INSERT [dbo].[THANHPHO] ([ID_THANHPHO], [TENTHANHPHO]) VALUES (89, N'Tỉnh An Giang')
GO

INSERT [dbo].[THANHPHO] ([ID_THANHPHO], [TENTHANHPHO]) VALUES (91, N'Tỉnh Kiên Giang')
GO

INSERT [dbo].[THANHPHO] ([ID_THANHPHO], [TENTHANHPHO]) VALUES (92, N'Thành phố Cần Thơ')
GO

INSERT [dbo].[THANHPHO] ([ID_THANHPHO], [TENTHANHPHO]) VALUES (93, N'Tỉnh Hậu Giang')
GO

INSERT [dbo].[THANHPHO] ([ID_THANHPHO], [TENTHANHPHO]) VALUES (94, N'Tỉnh Sóc Trăng')
GO

INSERT [dbo].[THANHPHO] ([ID_THANHPHO], [TENTHANHPHO]) VALUES (95, N'Tỉnh Bạc Liêu')
GO

INSERT [dbo].[THANHPHO] ([ID_THANHPHO], [TENTHANHPHO]) VALUES (96, N'Tỉnh Cà Mau')
GO

SET IDENTITY_INSERT [dbo].[THANHPHO] OFF
GO

SET IDENTITY_INSERT [dbo].[TRINHDO] ON
GO

INSERT [dbo].[TRINHDO] ([ID_TRINHDO], [TENTRINHDO], [GIATRI]) VALUES (1, N'TRUNG CẤP', 2)
GO

INSERT [dbo].[TRINHDO] ([ID_TRINHDO], [TENTRINHDO], [GIATRI]) VALUES (2, N'CAO ĐẲNG', 3)
GO

INSERT [dbo].[TRINHDO] ([ID_TRINHDO], [TENTRINHDO], [GIATRI]) VALUES (3, N'ĐẠI HỌC', 4)
GO

INSERT [dbo].[TRINHDO] ([ID_TRINHDO], [TENTRINHDO], [GIATRI]) VALUES (1002, N'TRUNG HỌC', 1)
GO

SET IDENTITY_INSERT [dbo].[TRINHDO] OFF
GO

INSERT [dbo].[TRINHDOTUYENDUNG] ([ID_TUYENDUNG], [ID_TRINHDO]) VALUES (1, 1)
GO

INSERT [dbo].[TRINHDOTUYENDUNG] ([ID_TUYENDUNG], [ID_TRINHDO]) VALUES (1, 2)
GO

INSERT [dbo].[TRINHDOTUYENDUNG] ([ID_TUYENDUNG], [ID_TRINHDO]) VALUES (3, 2)
GO

INSERT [dbo].[TRINHDOTUYENDUNG] ([ID_TUYENDUNG], [ID_TRINHDO]) VALUES (1, 3)
GO

INSERT [dbo].[TRINHDOTUYENDUNG] ([ID_TUYENDUNG], [ID_TRINHDO]) VALUES (2, 3)
GO

INSERT [dbo].[TRINHDOTUYENDUNG] ([ID_TUYENDUNG], [ID_TRINHDO]) VALUES (3, 3)
GO

INSERT [dbo].[UNGTUYEN] ([ID_UNGVIEN], [ID_CONGVIEC], [NGAYUNGTUYEN], [DAXEM]) VALUES (1, 1, CAST(N'2020-07-12' AS Date), 0)
GO

INSERT [dbo].[UNGTUYEN] ([ID_UNGVIEN], [ID_CONGVIEC], [NGAYUNGTUYEN], [DAXEM]) VALUES (1, 1005, CAST(N'2020-07-12' AS Date), 0)
GO

INSERT [dbo].[UNGTUYEN] ([ID_UNGVIEN], [ID_CONGVIEC], [NGAYUNGTUYEN], [DAXEM]) VALUES (1, 7010, CAST(N'2020-07-12' AS Date), 0)
GO

INSERT [dbo].[UNGTUYEN] ([ID_UNGVIEN], [ID_CONGVIEC], [NGAYUNGTUYEN], [DAXEM]) VALUES (1, 8020, CAST(N'2020-07-13' AS Date), 0)
GO

INSERT [dbo].[UNGTUYEN] ([ID_UNGVIEN], [ID_CONGVIEC], [NGAYUNGTUYEN], [DAXEM]) VALUES (1, 11022, CAST(N'2020-07-30' AS Date), 0)
GO

INSERT [dbo].[UNGTUYEN] ([ID_UNGVIEN], [ID_CONGVIEC], [NGAYUNGTUYEN], [DAXEM]) VALUES (3, 3005, CAST(N'2020-07-07' AS Date), 0)
GO

INSERT [dbo].[UNGTUYEN] ([ID_UNGVIEN], [ID_CONGVIEC], [NGAYUNGTUYEN], [DAXEM]) VALUES (8, 1, CAST(N'2020-07-12' AS Date), 0)
GO

INSERT [dbo].[UNGTUYEN] ([ID_UNGVIEN], [ID_CONGVIEC], [NGAYUNGTUYEN], [DAXEM]) VALUES (1015, 7008, CAST(N'2020-07-09' AS Date), 0)
GO

INSERT [dbo].[UNGTUYEN] ([ID_UNGVIEN], [ID_CONGVIEC], [NGAYUNGTUYEN], [DAXEM]) VALUES (1016, 3005, CAST(N'2020-07-10' AS Date), 0)
GO

INSERT [dbo].[UNGTUYEN] ([ID_UNGVIEN], [ID_CONGVIEC], [NGAYUNGTUYEN], [DAXEM]) VALUES (1018, 2016, CAST(N'2020-08-10' AS Date), 0)
GO

INSERT [dbo].[UNGTUYEN] ([ID_UNGVIEN], [ID_CONGVIEC], [NGAYUNGTUYEN], [DAXEM]) VALUES (1018, 2745, CAST(N'2020-07-28' AS Date), 0)
GO

INSERT [dbo].[UNGTUYEN] ([ID_UNGVIEN], [ID_CONGVIEC], [NGAYUNGTUYEN], [DAXEM]) VALUES (1018, 2756, CAST(N'2020-07-21' AS Date), 0)
GO

INSERT [dbo].[UNGTUYEN] ([ID_UNGVIEN], [ID_CONGVIEC], [NGAYUNGTUYEN], [DAXEM]) VALUES (1018, 2850, CAST(N'2020-07-28' AS Date), 0)
GO

INSERT [dbo].[UNGTUYEN] ([ID_UNGVIEN], [ID_CONGVIEC], [NGAYUNGTUYEN], [DAXEM]) VALUES (1018, 2869, CAST(N'2020-07-28' AS Date), 0)
GO

INSERT [dbo].[UNGTUYEN] ([ID_UNGVIEN], [ID_CONGVIEC], [NGAYUNGTUYEN], [DAXEM]) VALUES (1018, 2874, CAST(N'2020-08-10' AS Date), 0)
GO

INSERT [dbo].[UNGTUYEN] ([ID_UNGVIEN], [ID_CONGVIEC], [NGAYUNGTUYEN], [DAXEM]) VALUES (1018, 2889, CAST(N'2020-07-28' AS Date), 0)
GO

INSERT [dbo].[UNGTUYEN] ([ID_UNGVIEN], [ID_CONGVIEC], [NGAYUNGTUYEN], [DAXEM]) VALUES (1018, 8021, CAST(N'2020-07-28' AS Date), 0)
GO

INSERT [dbo].[UNGTUYEN] ([ID_UNGVIEN], [ID_CONGVIEC], [NGAYUNGTUYEN], [DAXEM]) VALUES (1018, 10022, NULL, 1)
GO

SET IDENTITY_INSERT [dbo].[UNGVIEN] ON
GO

INSERT [dbo].[UNGVIEN] ([ID_UNGVIEN], [EMAIL], [MATKHAU], [HINHANH], [HO], [TEN], [DIACHI], [GIOITINH], [DOCTHAN], [NGAYSINH], [SDT], [NGAYTAO], [ID_THANHPHO], [URL_CV], [LUONG], [KINHNGHIEM], [MOTA], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [LUOTXEM], [NGANHMONGMUON], [CAPBACMONGMUON], [THOIGIANLAMVIECMONGMUON], [MUCTIEUNGHENGHIEP], [VITRIUNGTUYEN], [SOCIALLOGIN]) VALUES (1, N'user@gmail.com', N'111111', N'Resources\Images\ungvien10.JPG', N'Nguyễn', N'Thắng', N'Lê Trọng Tấn', 1, 1, CAST(N'1998-01-07' AS Date), N'0346006260', CAST(N'2020-07-20' AS Date), 46, N'Resources\Images\Hung-N-TopCV.vn-180720.155942.pdf', 10000000.0000, 3, N'MỤC TIÊU NGHỀ NGHIỆP
Mục tiêu ngắn hạn:- Sau khi được nhận vào công ty, tiếp tục học tập kinh nghiệm làm việc từ các anh chị, sau 3 tháng trở thành nhân viên chính thức của công ty và có khả năng đảm nhận tốt các công việc được giao.- Sau 6 tháng thành thạo các kỹ năng, bắt đầu trở thành nhân viên lập trình chuyên nghiệp của công ty.
Mục tiêu dài hạn:- Sau 3 năm đầu tiên sẽ nâng cao được trình độ của bản thân và có một mức lương tốt- Sau 5 - 6 năm sẽ trở thành một Team Leader của nhóm phát triển phần mềm- Gắn bó lâu dài với công ty và giúp công ty phát triển trở thành một công ty lớn về công nghệ..', 0, NULL, N'Đang tìm việc', 100, 11, N'Nhân viên', N'Công việc toàn thời gian', N'Mục tiêu ngắn hạn:

Hoàn thành khóa học chứng chỉ Kế toán trưởng trong 2 năm tới
Có khả năng giao tiếp với khách nước ngoài trong 1 năm tới
Mục tiêu dài hạn:
', N'Lập trình front-end', 0)
GO

INSERT [dbo].[UNGVIEN] ([ID_UNGVIEN], [EMAIL], [MATKHAU], [HINHANH], [HO], [TEN], [DIACHI], [GIOITINH], [DOCTHAN], [NGAYSINH], [SDT], [NGAYTAO], [ID_THANHPHO], [URL_CV], [LUONG], [KINHNGHIEM], [MOTA], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [LUOTXEM], [NGANHMONGMUON], [CAPBACMONGMUON], [THOIGIANLAMVIECMONGMUON], [MUCTIEUNGHENGHIEP], [VITRIUNGTUYEN], [SOCIALLOGIN]) VALUES (2, N'abcd@gmail.com', N'111111', N'Resources\Images\ungvien1.JPG', N'Hoàng', N'Lê', N'Bình Hưng Hòa', 1, 0, CAST(N'1999-12-12' AS Date), N'0295808553', CAST(N'2020-07-20' AS Date), 79, N'Resources\Images\DATN2020.039_NTBichNgan - Nguyen Hong Vu.pdf', 2000.0000, 1, N'Vui vẻ năng động có tinh thần học hỏi trong công việc.
Có tinh thần cầu tiến ', 0, NULL, N'Đang tìm việc', 100, 1, N'Nhân Viên', N'Công việc toàn thời gian', N'Mục tiêu ngắn hạn: Làm font-end cắt html từ pdf.
Mục tiêu dài hạn: Làm leader nhóm web dsadasdasdsada', NULL, 0)
GO

INSERT [dbo].[UNGVIEN] ([ID_UNGVIEN], [EMAIL], [MATKHAU], [HINHANH], [HO], [TEN], [DIACHI], [GIOITINH], [DOCTHAN], [NGAYSINH], [SDT], [NGAYTAO], [ID_THANHPHO], [URL_CV], [LUONG], [KINHNGHIEM], [MOTA], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [LUOTXEM], [NGANHMONGMUON], [CAPBACMONGMUON], [THOIGIANLAMVIECMONGMUON], [MUCTIEUNGHENGHIEP], [VITRIUNGTUYEN], [SOCIALLOGIN]) VALUES (3, N'thang123@gmail.com', N'111111', N'Resources\Images\ungvien2.JPG', N'Lê', N'Hậu', N'Tân bình', 0, 1, CAST(N'2000-12-25' AS Date), N'0732754609', CAST(N'2020-07-20' AS Date), 79, N'Resources\Images\DATN2020.039_NTBichNgan - Nguyen Hong Vu.pdf', 10000.0000, 6, N'Vui vẻ năng động có tinh thần học hỏi trong công việc.
Có tinh thần cầu tiến ', 0, NULL, N'Đang tìm việc', 100, 1, N'Nhân Viên', N'Công việc toàn thời gian', N'Mục tiêu ngắn hạn: Làm font-end cắt html từ pdf.
Mục tiêu dài hạn: Làm leader nhóm web dsadasdasdsada', NULL, 0)
GO

INSERT [dbo].[UNGVIEN] ([ID_UNGVIEN], [EMAIL], [MATKHAU], [HINHANH], [HO], [TEN], [DIACHI], [GIOITINH], [DOCTHAN], [NGAYSINH], [SDT], [NGAYTAO], [ID_THANHPHO], [URL_CV], [LUONG], [KINHNGHIEM], [MOTA], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [LUOTXEM], [NGANHMONGMUON], [CAPBACMONGMUON], [THOIGIANLAMVIECMONGMUON], [MUCTIEUNGHENGHIEP], [VITRIUNGTUYEN], [SOCIALLOGIN]) VALUES (4, N'hello@gmail.com', N'111111', N'Resources\Images\ungvien3.JPG', N'Hải', N'Tiến', N'Tân quý', 0, 1, CAST(N'1995-01-27' AS Date), N'0413432187', CAST(N'2020-07-20' AS Date), 79, N'Resources\Images\DATN2020.039_NTBichNgan - Nguyen Hong Vu.pdf', 3000.0000, 4, N'Vui vẻ năng động có tinh thần học hỏi trong công việc.
Có tinh thần cầu tiến ', 0, NULL, N'Đang tìm việc', 100, 5, N'Nhân Viên', N'Công việc toàn thời gian', N'Mục tiêu ngắn hạn: Làm font-end cắt html từ pdf.
Mục tiêu dài hạn: Làm leader nhóm web dsadasdasdsada', NULL, 0)
GO

INSERT [dbo].[UNGVIEN] ([ID_UNGVIEN], [EMAIL], [MATKHAU], [HINHANH], [HO], [TEN], [DIACHI], [GIOITINH], [DOCTHAN], [NGAYSINH], [SDT], [NGAYTAO], [ID_THANHPHO], [URL_CV], [LUONG], [KINHNGHIEM], [MOTA], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [LUOTXEM], [NGANHMONGMUON], [CAPBACMONGMUON], [THOIGIANLAMVIECMONGMUON], [MUCTIEUNGHENGHIEP], [VITRIUNGTUYEN], [SOCIALLOGIN]) VALUES (6, N'ungvien1@gmail.com', N'111111', N'Resources\Images\ungvien4.JPG', N'Lê', N'Kim', N'Đường 27', 0, 0, CAST(N'2000-10-20' AS Date), N'0947857073', CAST(N'2020-07-20' AS Date), 79, N'Resources\Images\DATN2020.039_NTBichNgan - Nguyen Hong Vu.pdf', 0.0000, 7, N'Vui vẻ năng động có tinh thần học hỏi trong công việc.
Có tinh thần cầu tiến ', 0, NULL, N'Đang tìm việc', 100, 20, N'Nhân Viên', N'Công việc toàn thời gian', N'Mục tiêu ngắn hạn: Làm font-end cắt html từ pdf.
Mục tiêu dài hạn: Làm leader nhóm web dsadasdasdsada', NULL, 0)
GO

INSERT [dbo].[UNGVIEN] ([ID_UNGVIEN], [EMAIL], [MATKHAU], [HINHANH], [HO], [TEN], [DIACHI], [GIOITINH], [DOCTHAN], [NGAYSINH], [SDT], [NGAYTAO], [ID_THANHPHO], [URL_CV], [LUONG], [KINHNGHIEM], [MOTA], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [LUOTXEM], [NGANHMONGMUON], [CAPBACMONGMUON], [THOIGIANLAMVIECMONGMUON], [MUCTIEUNGHENGHIEP], [VITRIUNGTUYEN], [SOCIALLOGIN]) VALUES (7, N'ungvien2@gmail.com', N'111111', N'Resources\Images\ungvien5.JPG', N'Hồ', N'Quang TIến', N'Lê Trọng Tấn', 1, 1, CAST(N'1998-07-01' AS Date), N'0346006260', CAST(N'2020-07-20' AS Date), 79, N'Resources\Images\DATN2020.039_NTBichNgan - Nguyen Hong Vu.pdf', 0.0000, 10, N'Rất yêu các công việc của tôi', 0, NULL, N'Đang tìm việc', 100, 5, N'Nhân Viên', N'Công việc toàn thời gian', N'Mục tiêu ngắn hạn: Làm font-end cắt html từ pdf.
Mục tiêu dài hạn: Làm leader nhóm web dsadasdasdsada', NULL, 0)
GO

INSERT [dbo].[UNGVIEN] ([ID_UNGVIEN], [EMAIL], [MATKHAU], [HINHANH], [HO], [TEN], [DIACHI], [GIOITINH], [DOCTHAN], [NGAYSINH], [SDT], [NGAYTAO], [ID_THANHPHO], [URL_CV], [LUONG], [KINHNGHIEM], [MOTA], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [LUOTXEM], [NGANHMONGMUON], [CAPBACMONGMUON], [THOIGIANLAMVIECMONGMUON], [MUCTIEUNGHENGHIEP], [VITRIUNGTUYEN], [SOCIALLOGIN]) VALUES (8, N'thang@gmail.com', N'111111', N'Resources\Images\ungvien6.JPG', N'Trần ', N'Nhung', N'182 / 49 Thoại Ngọc Hầu, Phú Thạnh, Tân Phú, Hồ Chí Minh', 1, 1, CAST(N'1998-07-01' AS Date), N'0346006260', CAST(N'2020-07-20' AS Date), 79, N'Resources\Images\DATN2020.039_NTBichNgan - Nguyen Hong Vu.pdf', 0.0000, 10, N'Tôi có thể làm điều gì đó như thế này bằng JavaScript/jQuery không? ... Sẽ rất gọn gàng nếu các chuỗi vốn là các mảng ký tự trong javascript để chúng ta không ...', 0, NULL, N'Đang tìm việc', 100, 2, N'Nhân Viên', N'Công việc toàn thời gian', N'Mục tiêu ngắn hạn: Làm font-end cắt html từ pdf.
Mục tiêu dài hạn: Làm leader nhóm web dsadasdasdsada', NULL, 0)
GO

INSERT [dbo].[UNGVIEN] ([ID_UNGVIEN], [EMAIL], [MATKHAU], [HINHANH], [HO], [TEN], [DIACHI], [GIOITINH], [DOCTHAN], [NGAYSINH], [SDT], [NGAYTAO], [ID_THANHPHO], [URL_CV], [LUONG], [KINHNGHIEM], [MOTA], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [LUOTXEM], [NGANHMONGMUON], [CAPBACMONGMUON], [THOIGIANLAMVIECMONGMUON], [MUCTIEUNGHENGHIEP], [VITRIUNGTUYEN], [SOCIALLOGIN]) VALUES (9, N'ungvien3@gmail.com', N'111111', N'Resources\Images\ungvien7.JPG', N'Lê', N'Hải Kim', N'182 / 49 Thoại Ngọc Hầu, Phú Thạnh, Tân Phú, Hồ Chí Minh', 0, 1, CAST(N'2000-01-01' AS Date), N'0692640800', CAST(N'2020-07-20' AS Date), 79, N'Resources\Images\DATN2020.039_NTBichNgan - Nguyen Hong Vu.pdf', 0.0000, 6, N'Vui vẻ năng động có tinh thần học hỏi trong công việc.
Có tinh thần cầu tiến ', 0, NULL, N'Đang tìm việc', 100, 25, N'Quản lý', N'Công việc toàn thời gian', N'Mục tiêu ngắn hạn: Làm font-end cắt html từ pdf.
Mục tiêu dài hạn: Làm leader nhóm web dsadasdasdsada', NULL, 0)
GO

INSERT [dbo].[UNGVIEN] ([ID_UNGVIEN], [EMAIL], [MATKHAU], [HINHANH], [HO], [TEN], [DIACHI], [GIOITINH], [DOCTHAN], [NGAYSINH], [SDT], [NGAYTAO], [ID_THANHPHO], [URL_CV], [LUONG], [KINHNGHIEM], [MOTA], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [LUOTXEM], [NGANHMONGMUON], [CAPBACMONGMUON], [THOIGIANLAMVIECMONGMUON], [MUCTIEUNGHENGHIEP], [VITRIUNGTUYEN], [SOCIALLOGIN]) VALUES (10, N'ungvien4@gmail.com', N'111111', N'Resources\Images\ungvien8.JPG', N'Nguyễn', N'Nhung Trần', N'Lê Trọng Tấn', 1, 1, CAST(N'1998-07-01' AS Date), N'0346006260', CAST(N'2020-07-20' AS Date), 79, N'Resources\Images\DATN2020.039_NTBichNgan - Nguyen Hong Vu.pdf', 0.0000, 3, N'Vui vẻ năng động có tinh thần học hỏi trong công việc.
Có tinh thần cầu tiến ', 0, NULL, N'Đang tìm việc', 100, 4, N'Quản lý', N'Công việc toàn thời gian', N'Mục tiêu ngắn hạn: Làm font-end cắt html từ pdf.
Mục tiêu dài hạn: Làm leader nhóm web dsadasdasdsada', NULL, 0)
GO

INSERT [dbo].[UNGVIEN] ([ID_UNGVIEN], [EMAIL], [MATKHAU], [HINHANH], [HO], [TEN], [DIACHI], [GIOITINH], [DOCTHAN], [NGAYSINH], [SDT], [NGAYTAO], [ID_THANHPHO], [URL_CV], [LUONG], [KINHNGHIEM], [MOTA], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [LUOTXEM], [NGANHMONGMUON], [CAPBACMONGMUON], [THOIGIANLAMVIECMONGMUON], [MUCTIEUNGHENGHIEP], [VITRIUNGTUYEN], [SOCIALLOGIN]) VALUES (1010, N'ungvien5@gmail.com', N'111111', N'Resources\Images\ungvien9.JPG', N'Hồ', N'Thị Mỹ', N'182 / 49 Thoại Ngọc Hầu, Phú Thạnh, Tân Phú, Hồ Chí Minh', 1, 0, CAST(N'2000-01-01' AS Date), N'0752118506', CAST(N'2020-07-20' AS Date), 48, N'Resources\Images\DATN2020.039_NTBichNgan - Nguyen Hong Vu.pdf', 0.0000, 4, N'Vui vẻ năng động có tinh thần học hỏi trong công việc.
Có tinh thần cầu tiến ', 0, NULL, N'Đang tìm việc', 0, 11, N'Quản lý', N'Công việc toàn thời gian', N'Mục tiêu ngắn hạn: Làm font-end cắt html từ pdf.
Mục tiêu dài hạn: Làm leader nhóm web dsadasdasdsada', NULL, 0)
GO

INSERT [dbo].[UNGVIEN] ([ID_UNGVIEN], [EMAIL], [MATKHAU], [HINHANH], [HO], [TEN], [DIACHI], [GIOITINH], [DOCTHAN], [NGAYSINH], [SDT], [NGAYTAO], [ID_THANHPHO], [URL_CV], [LUONG], [KINHNGHIEM], [MOTA], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [LUOTXEM], [NGANHMONGMUON], [CAPBACMONGMUON], [THOIGIANLAMVIECMONGMUON], [MUCTIEUNGHENGHIEP], [VITRIUNGTUYEN], [SOCIALLOGIN]) VALUES (1011, N'ungvien6@gmail.com', N'111111', N'Resources\Images\ungvien11.JPG', N'Hà', N'Quang Tần', N'182 / 49 Thoại Ngọc Hầu, Phú Thạnh, Tân Phú, Hồ Chí Minh', 0, 1, CAST(N'2000-01-01' AS Date), N'0432361849', CAST(N'2020-07-20' AS Date), 48, N'Resources\Images\DATN2020.039_NTBichNgan - Nguyen Hong Vu.pdf', 0.0000, 6, N'Vui vẻ năng động có tinh thần học hỏi trong công việc.
Có tinh thần cầu tiến ', 0, NULL, N'Đang tìm việc', 0, 2, N'Quản lý', N'Công việc toàn thời gian', N'Mục tiêu ngắn hạn: Làm font-end cắt html từ pdf.
Mục tiêu dài hạn: Làm leader nhóm web dsadasdasdsada', NULL, 0)
GO

INSERT [dbo].[UNGVIEN] ([ID_UNGVIEN], [EMAIL], [MATKHAU], [HINHANH], [HO], [TEN], [DIACHI], [GIOITINH], [DOCTHAN], [NGAYSINH], [SDT], [NGAYTAO], [ID_THANHPHO], [URL_CV], [LUONG], [KINHNGHIEM], [MOTA], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [LUOTXEM], [NGANHMONGMUON], [CAPBACMONGMUON], [THOIGIANLAMVIECMONGMUON], [MUCTIEUNGHENGHIEP], [VITRIUNGTUYEN], [SOCIALLOGIN]) VALUES (1012, N'ungvien7@gmail.com', N'111111', N'Resources\Images\user.jpg', N'Trần ', N'Thúy Vân', N'182 / 49 Thoại Ngọc Hầu, Phú Thạnh, Tân Phú, Hồ Chí Minh', 1, 0, CAST(N'2000-01-01' AS Date), N'0528094812', CAST(N'2020-07-20' AS Date), 48, N'Resources\Images\DATN2020.039_NTBichNgan - Nguyen Hong Vu.pdf', 0.0000, 3, N'Vui vẻ năng động có tinh thần học hỏi trong công việc.
Có tinh thần cầu tiến ', 0, NULL, N'Đang tìm việc', 0, 7, N'Quản lý', N'Công việc toàn thời gian', N'Mục tiêu ngắn hạn: Làm font-end cắt html từ pdf.
Mục tiêu dài hạn: Làm leader nhóm web dsadasdasdsada', NULL, 0)
GO

INSERT [dbo].[UNGVIEN] ([ID_UNGVIEN], [EMAIL], [MATKHAU], [HINHANH], [HO], [TEN], [DIACHI], [GIOITINH], [DOCTHAN], [NGAYSINH], [SDT], [NGAYTAO], [ID_THANHPHO], [URL_CV], [LUONG], [KINHNGHIEM], [MOTA], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [LUOTXEM], [NGANHMONGMUON], [CAPBACMONGMUON], [THOIGIANLAMVIECMONGMUON], [MUCTIEUNGHENGHIEP], [VITRIUNGTUYEN], [SOCIALLOGIN]) VALUES (1013, N'ungvien8@gmail.com', N'111111', N'Resources\Images\user.jpg', N'Trần', N'Thúy Kiều', N'182 / 49 Thoại Ngọc Hầu, Phú Thạnh, Tân Phú, Hồ Chí Minh', 0, 1, CAST(N'2000-01-01' AS Date), N'0719499406', CAST(N'2020-07-20' AS Date), 48, N'Resources\Images\DATN2020.039_NTBichNgan - Nguyen Hong Vu.pdf', 0.0000, 2, N'Vui vẻ năng động có tinh thần học hỏi trong công việc.
Có tinh thần cầu tiến ', 0, NULL, N'Đang tìm việc', 0, 7, N'Quản lý', N'Công việc toàn thời gian', N'Mục tiêu ngắn hạn: Làm font-end cắt html từ pdf.
Mục tiêu dài hạn: Làm leader nhóm web dsadasdasdsada', NULL, 0)
GO

INSERT [dbo].[UNGVIEN] ([ID_UNGVIEN], [EMAIL], [MATKHAU], [HINHANH], [HO], [TEN], [DIACHI], [GIOITINH], [DOCTHAN], [NGAYSINH], [SDT], [NGAYTAO], [ID_THANHPHO], [URL_CV], [LUONG], [KINHNGHIEM], [MOTA], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [LUOTXEM], [NGANHMONGMUON], [CAPBACMONGMUON], [THOIGIANLAMVIECMONGMUON], [MUCTIEUNGHENGHIEP], [VITRIUNGTUYEN], [SOCIALLOGIN]) VALUES (1014, N'ungvien9@gmail.com', N'111111', N'Resources\Images\user.jpg', N'Hồ', N'Hải', N'182 / 49 Thoại Ngọc Hầu, Phú Thạnh, Tân Phú, Hồ Chí Minh', 1, 0, CAST(N'2000-01-01' AS Date), N'0682309744', CAST(N'2020-07-20' AS Date), 48, N'Resources\Images\DATN2020.039_NTBichNgan - Nguyen Hong Vu.pdf', 0.0000, 7, N'Tư duy tốt, mong muốn làm việc trong môi trường năng động, có thể phát huy hết năng lực của bản thân
Được làm viêc lâu dài, và làm đúng công việc mong muốn', 0, NULL, N'Đang tìm việc', 0, 9, N'Quản lý', N'Công việc toàn thời gian', N'Mục tiêu ngắn hạn: Làm font-end cắt html từ pdf.
Mục tiêu dài hạn: Làm leader nhóm web dsadasdasdsada', NULL, 0)
GO

INSERT [dbo].[UNGVIEN] ([ID_UNGVIEN], [EMAIL], [MATKHAU], [HINHANH], [HO], [TEN], [DIACHI], [GIOITINH], [DOCTHAN], [NGAYSINH], [SDT], [NGAYTAO], [ID_THANHPHO], [URL_CV], [LUONG], [KINHNGHIEM], [MOTA], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [LUOTXEM], [NGANHMONGMUON], [CAPBACMONGMUON], [THOIGIANLAMVIECMONGMUON], [MUCTIEUNGHENGHIEP], [VITRIUNGTUYEN], [SOCIALLOGIN]) VALUES (1015, N'ungvien10@gmail.com', N'111111', N'Resources\Images\user.jpg', N'Trần ', N'Văn Học', N'182 / 49 Thoại Ngọc Hầu, Phú Thạnh, Tân Phú, Hồ Chí Minh', 0, 1, CAST(N'2000-01-01' AS Date), N'0217690942', CAST(N'2020-07-20' AS Date), 48, N'Resources\Images\DATN2020.039_NTBichNgan - Nguyen Hong Vu.pdf', 0.0000, 9, N'Tư duy tốt, mong muốn làm việc trong môi trường năng động, có thể phát huy hết năng lực của bản thân
Được làm viêc lâu dài, và làm đúng công việc mong muốn', 0, NULL, N'Đang tìm việc', 0, 8, N'Quản lý', N'Công việc toàn thời gian', N'Mục tiêu ngắn hạn: Làm font-end cắt html từ pdf.
Mục tiêu dài hạn: Làm leader nhóm web dsadasdasdsada', NULL, 0)
GO

INSERT [dbo].[UNGVIEN] ([ID_UNGVIEN], [EMAIL], [MATKHAU], [HINHANH], [HO], [TEN], [DIACHI], [GIOITINH], [DOCTHAN], [NGAYSINH], [SDT], [NGAYTAO], [ID_THANHPHO], [URL_CV], [LUONG], [KINHNGHIEM], [MOTA], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [LUOTXEM], [NGANHMONGMUON], [CAPBACMONGMUON], [THOIGIANLAMVIECMONGMUON], [MUCTIEUNGHENGHIEP], [VITRIUNGTUYEN], [SOCIALLOGIN]) VALUES (1016, N'ungvien11@gmail.com', N'111111', N'Resources\Images\user.jpg', N'Lê', N'Hoàng', N'182 / 49 Thoại Ngọc Hầu, Phú Thạnh, Tân Phú, Hồ Chí Minh', 1, 0, CAST(N'2000-01-01' AS Date), N'0175993186', CAST(N'2020-07-20' AS Date), 48, N'Resources\Images\DATN2020.039_NTBichNgan - Nguyen Hong Vu.pdf', 0.0000, 8, N'Tư duy tốt, mong muốn làm việc trong môi trường năng động, có thể phát huy hết năng lực của bản thân
Được làm viêc lâu dài, và làm đúng công việc mong muốn', 0, NULL, N'Đang tìm việc', 0, 9, N'Quản lý', N'Công việc toàn thời gian', N'Mục tiêu ngắn hạn: Làm font-end cắt html từ pdf.
Mục tiêu dài hạn: Làm leader nhóm web dsadasdasdsada', NULL, 0)
GO

INSERT [dbo].[UNGVIEN] ([ID_UNGVIEN], [EMAIL], [MATKHAU], [HINHANH], [HO], [TEN], [DIACHI], [GIOITINH], [DOCTHAN], [NGAYSINH], [SDT], [NGAYTAO], [ID_THANHPHO], [URL_CV], [LUONG], [KINHNGHIEM], [MOTA], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [LUOTXEM], [NGANHMONGMUON], [CAPBACMONGMUON], [THOIGIANLAMVIECMONGMUON], [MUCTIEUNGHENGHIEP], [VITRIUNGTUYEN], [SOCIALLOGIN]) VALUES (1017, N'hiad@gmail.com', N'111111', N'Resources/Images/user.jpg', N'Nguyễn', N'Ngọc', N'182 / 49 Thoại Ngọc Hầu, Phú Thạnh, Tân Phú, Hồ Chí Minh', 0, 1, CAST(N'2000-01-01' AS Date), N'0714019192', CAST(N'2020-07-20' AS Date), 48, N'Resources\Images\DATN2020.039_NTBichNgan - Nguyen Hong Vu.pdf', 0.0000, 0, N'Tư duy tốt, mong muốn làm việc trong môi trường năng động, có thể phát huy hết năng lực của bản thân
Được làm viêc lâu dài, và làm đúng công việc mong muốn', 0, NULL, N'Đang tìm việc', 0, 8, N'Quản lý', N'Công việc toàn thời gian', N'Mục tiêu ngắn hạn: Làm font-end cắt html từ pdf.
Mục tiêu dài hạn: Làm leader nhóm web dsadasdasdsada', NULL, 0)
GO

INSERT [dbo].[UNGVIEN] ([ID_UNGVIEN], [EMAIL], [MATKHAU], [HINHANH], [HO], [TEN], [DIACHI], [GIOITINH], [DOCTHAN], [NGAYSINH], [SDT], [NGAYTAO], [ID_THANHPHO], [URL_CV], [LUONG], [KINHNGHIEM], [MOTA], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [LUOTXEM], [NGANHMONGMUON], [CAPBACMONGMUON], [THOIGIANLAMVIECMONGMUON], [MUCTIEUNGHENGHIEP], [VITRIUNGTUYEN], [SOCIALLOGIN]) VALUES (1018, N'cutygialai@gmail.com', N'123456', N'https://lh3.googleusercontent.com/a-/AOh14Ggq6e92XlFKbXY9O_gkosQYX3L-v6LCfkAAZwb_=s96-c', N'NguyễnThanhThắng', N'', N'182 / 49 Thoại Ngọc Hầu, Phú Thạnh, Tân Phú, Hồ Chí Minh', 1, 1, CAST(N'1998-07-01' AS Date), N'0819367103', CAST(N'2020-07-20' AS Date), 79, N'Resources\Images\Phat-N-TopCV.vn-180720.155916.pdf', 10000000.0000, 3, N'Vui vẻ năng động có tinh thần học hỏi trong công việc.
Có tinh thần cầu tiến ', 0, NULL, N'Đang cập nhật', 0, 1, N'Nhân viên', N'Công việc toàn thời gian', N'Mục tiêu ngắn hạn: Làm font-end cắt html từ pdf.
Mục tiêu dài hạn: Làm leader nhóm web dsadasdasdsada', N'Nhân Viên DEV Front-end Angular', 1)
GO

INSERT [dbo].[UNGVIEN] ([ID_UNGVIEN], [EMAIL], [MATKHAU], [HINHANH], [HO], [TEN], [DIACHI], [GIOITINH], [DOCTHAN], [NGAYSINH], [SDT], [NGAYTAO], [ID_THANHPHO], [URL_CV], [LUONG], [KINHNGHIEM], [MOTA], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [LUOTXEM], [NGANHMONGMUON], [CAPBACMONGMUON], [THOIGIANLAMVIECMONGMUON], [MUCTIEUNGHENGHIEP], [VITRIUNGTUYEN], [SOCIALLOGIN]) VALUES (1019, N'thanhthang1798@gmail.com', N'123456', N'https://lh3.googleusercontent.com/a-/AOh14GhWTbkyjL8hbtb4so8XQ5d1OjUQuoizwoOSum4R=s96-c', N'nguyễn', N'Thắng', N'182 / 49 Thoại Ngọc Hầu, Phú Thạnh, Tân Phú, Hồ Chí Minh', 0, 1, CAST(N'2000-01-01' AS Date), N'0871076970', CAST(N'2020-07-20' AS Date), 48, N'Resources\Images\DATN2020.039_NTBichNgan - Nguyen Hong Vu.pdf', 0.0000, 9, N'Tư duy tốt, mong muốn làm việc trong môi trường năng động, có thể phát huy hết năng lực của bản thân
Được làm viêc lâu dài, và làm đúng công việc mong muốn', 0, NULL, N'Đang tìm việc', 0, NULL, NULL, N'Công việc toàn thời gian', N'Mục tiêu ngắn hạn: Làm font-end cắt html từ pdf.
Mục tiêu dài hạn: Làm leader nhóm web dsadasdasdsada', NULL, 1)
GO

INSERT [dbo].[UNGVIEN] ([ID_UNGVIEN], [EMAIL], [MATKHAU], [HINHANH], [HO], [TEN], [DIACHI], [GIOITINH], [DOCTHAN], [NGAYSINH], [SDT], [NGAYTAO], [ID_THANHPHO], [URL_CV], [LUONG], [KINHNGHIEM], [MOTA], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [LUOTXEM], [NGANHMONGMUON], [CAPBACMONGMUON], [THOIGIANLAMVIECMONGMUON], [MUCTIEUNGHENGHIEP], [VITRIUNGTUYEN], [SOCIALLOGIN]) VALUES (1020, N'2001160630@ittc.edu.vn', N'123456', N'https://lh3.googleusercontent.com/-2-yPd7ABWyM/AAAAAAAAAAI/AAAAAAAAAAA/AMZuucmDu0bOGgtKJz7WQz39Yn6zvAnFyg/s96-c/photo.jpg', N'Trần', N'Hải', N'182 / 49 Thoại Ngọc Hầu, Phú Thạnh, Tân Phú, Hồ Chí Minh', 1, 0, CAST(N'2000-01-01' AS Date), N'0434420799', CAST(N'2020-07-20' AS Date), 48, N'Resources\Images\DATN2020.039_NTBichNgan - Nguyen Hong Vu.pdf', 0.0000, 8, N'Tư duy tốt, mong muốn làm việc trong môi trường năng động, có thể phát huy hết năng lực của bản thân
Được làm viêc lâu dài, và làm đúng công việc mong muốn', 0, NULL, N'Đang tìm việc', 0, NULL, NULL, N'Công việc toàn thời gian', N'Mục tiêu ngắn hạn: Làm font-end cắt html từ pdf.
Mục tiêu dài hạn: Làm leader nhóm web dsadasdasdsada', NULL, 1)
GO

INSERT [dbo].[UNGVIEN] ([ID_UNGVIEN], [EMAIL], [MATKHAU], [HINHANH], [HO], [TEN], [DIACHI], [GIOITINH], [DOCTHAN], [NGAYSINH], [SDT], [NGAYTAO], [ID_THANHPHO], [URL_CV], [LUONG], [KINHNGHIEM], [MOTA], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [LUOTXEM], [NGANHMONGMUON], [CAPBACMONGMUON], [THOIGIANLAMVIECMONGMUON], [MUCTIEUNGHENGHIEP], [VITRIUNGTUYEN], [SOCIALLOGIN]) VALUES (1021, N'nu@gmail.com', N'User123', N'Resources/Images/user.jpg', N'Phạm Thị', N'Nụ', N'8D Điện Biên Phủ', 1, 0, CAST(N'1984-08-05' AS Date), N'0312411345', CAST(N'2020-07-20' AS Date), 1, N'Resources\Images\DATN2020.039_NTBichNgan - Nguyen Hong Vu.pdf', 7000000.0000, 11, N'Tư duy tốt, mong muốn làm việc trong môi trường năng động, có thể phát huy hết năng lực của bản thân
Được làm viêc lâu dài, và làm đúng công việc mong muốn', 0, NULL, N'Đang cập nhật', 100, 30, N'Nhân viên', N'Công việc toàn thời gian', N'Mục tiêu ngắn hạn: Làm font-end cắt html từ pdf.
Mục tiêu dài hạn: Làm leader nhóm web dsadasdasdsada', NULL, 0)
GO

INSERT [dbo].[UNGVIEN] ([ID_UNGVIEN], [EMAIL], [MATKHAU], [HINHANH], [HO], [TEN], [DIACHI], [GIOITINH], [DOCTHAN], [NGAYSINH], [SDT], [NGAYTAO], [ID_THANHPHO], [URL_CV], [LUONG], [KINHNGHIEM], [MOTA], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [LUOTXEM], [NGANHMONGMUON], [CAPBACMONGMUON], [THOIGIANLAMVIECMONGMUON], [MUCTIEUNGHENGHIEP], [VITRIUNGTUYEN], [SOCIALLOGIN]) VALUES (1022, N'kimthoa@gmail.com', N'User123', N'Resources/Images/user.jpg', N'Lê Thị Kim', N'Thoa', N'Số 88, Ấp Long Giêng, Xã Phước Hậu, Huyện Cần Giuộc, Tỉnh Long An', 0, 0, CAST(N'1998-01-19' AS Date), N'0312413141', CAST(N'2020-07-20' AS Date), 80, N'Resources\Images\DATN2020.039_NTBichNgan - Nguyen Hong Vu.pdf', 5000000.0000, 0, N'Là một sinh viên mới ra trường chưa có kinh nghiệm nên em mong muốn tìm được một môi trường làm việc chuyên nghiệp, vui vẻ để có thể học hỏi, phát triển và cống hiến những gì mình đã học. Và có cơ hội thăng tiến trong tương lai.', 0, NULL, N'Đang tìm việc', 100, 15, N'Nhân viên', N'Công việc toàn thời gian', N'Mục tiêu ngắn hạn: Làm font-end cắt html từ pdf.
Mục tiêu dài hạn: Làm leader nhóm web dsadasdasdsada', NULL, 0)
GO

INSERT [dbo].[UNGVIEN] ([ID_UNGVIEN], [EMAIL], [MATKHAU], [HINHANH], [HO], [TEN], [DIACHI], [GIOITINH], [DOCTHAN], [NGAYSINH], [SDT], [NGAYTAO], [ID_THANHPHO], [URL_CV], [LUONG], [KINHNGHIEM], [MOTA], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [LUOTXEM], [NGANHMONGMUON], [CAPBACMONGMUON], [THOIGIANLAMVIECMONGMUON], [MUCTIEUNGHENGHIEP], [VITRIUNGTUYEN], [SOCIALLOGIN]) VALUES (1023, N'vanlong@gmail.com', N'User123', N'Resources/Images/user.jpg', N'Đoàn Văn', N'Long', N'Hoàn Long - Yên Mỹ - Hưng Yên', 1, 1, CAST(N'1998-04-18' AS Date), N'0524234321', CAST(N'2020-07-20' AS Date), 33, N'Resources\Images\DATN2020.039_NTBichNgan - Nguyen Hong Vu.pdf', 7000000.0000, 1, N'Ham học hỏi, chăm chỉ và có trách nhiệm với công việc
 Kỹ năng giao tiếp
 Kỹ năng làm việc theo nhóm', 0, NULL, N'Đang cập nhật', 100, 31, N'Nhân viên', N'Công việc toàn thời gian', N'Mục tiêu ngắn hạn: Làm font-end cắt html từ pdf.
Mục tiêu dài hạn: Làm leader nhóm web dsadasdasdsada', NULL, 0)
GO

INSERT [dbo].[UNGVIEN] ([ID_UNGVIEN], [EMAIL], [MATKHAU], [HINHANH], [HO], [TEN], [DIACHI], [GIOITINH], [DOCTHAN], [NGAYSINH], [SDT], [NGAYTAO], [ID_THANHPHO], [URL_CV], [LUONG], [KINHNGHIEM], [MOTA], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [LUOTXEM], [NGANHMONGMUON], [CAPBACMONGMUON], [THOIGIANLAMVIECMONGMUON], [MUCTIEUNGHENGHIEP], [VITRIUNGTUYEN], [SOCIALLOGIN]) VALUES (1024, N'bao@gmail.com', N'User123', N'Resources/Images/user.jpg', N'Đỗ Hoàng', N'Bảo', N'182 / 49 Thoại Ngọc Hầu, Phú Thạnh, Tân Phú, Hồ Chí Minh', 1, 0, CAST(N'1997-01-22' AS Date), N'0543531314', CAST(N'2020-07-20' AS Date), 79, N'Resources\Images\DATN2020.039_NTBichNgan - Nguyen Hong Vu.pdf', 5000000.0000, 3, N' Kỹ năng tổ chức
 Kỹ năng làm việc theo nhóm
 Kỹ năng lãnh đạo
 Kỹ năng thuyết trình
 Giải quyết vấn đề
 Lập kế hoạch', 0, NULL, N'Đang tìm việc', 0, 32, N'Nhân viên', N'Công việc toàn thời gian', N'Mục tiêu ngắn hạn: Làm font-end cắt html từ pdf.
Mục tiêu dài hạn: Làm leader nhóm web dsadasdasdsada', NULL, 0)
GO

INSERT [dbo].[UNGVIEN] ([ID_UNGVIEN], [EMAIL], [MATKHAU], [HINHANH], [HO], [TEN], [DIACHI], [GIOITINH], [DOCTHAN], [NGAYSINH], [SDT], [NGAYTAO], [ID_THANHPHO], [URL_CV], [LUONG], [KINHNGHIEM], [MOTA], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [LUOTXEM], [NGANHMONGMUON], [CAPBACMONGMUON], [THOIGIANLAMVIECMONGMUON], [MUCTIEUNGHENGHIEP], [VITRIUNGTUYEN], [SOCIALLOGIN]) VALUES (1027, N'user1@gmail.com', N'User123', N'Resources/Images/user.jpg', N'Trần', N'Hoàng', N'182 / 49 Thoại Ngọc Hầu, Phú Thạnh, Tân Phú, Hồ Chí Minh', 0, 1, CAST(N'1987-08-28' AS Date), N'0123213213', CAST(N'2020-07-20' AS Date), 79, N'Resources\Images\DATN2020.039_NTBichNgan - Nguyen Hong Vu.pdf', 700000.0000, 1, N'ôi có khả năng xử lý nhiều nhiệm vụ trong một lúc và có thể hoàn thành đúng hạn các nhiệm vụ được giao. Ngoài ra tôi là người đáng tin cậy và người quản lý thời gian tuyệt vời, một trong thế mạnh quan trọng của tôi là giao tiếp, xây dựng mối quan hệ mạnh mẽ với mọi người để đạt được kết quả công việc tốt nhất. ', 0, NULL, N'Đang cập nhật', 100, 1, N'Nhân viên', N'Công việc toàn thời gian', N'Mục tiêu ngắn hạn: Làm font-end cắt html từ pdf.
Mục tiêu dài hạn: Làm leader nhóm web dsadasdasdsada', NULL, 0)
GO

INSERT [dbo].[UNGVIEN] ([ID_UNGVIEN], [EMAIL], [MATKHAU], [HINHANH], [HO], [TEN], [DIACHI], [GIOITINH], [DOCTHAN], [NGAYSINH], [SDT], [NGAYTAO], [ID_THANHPHO], [URL_CV], [LUONG], [KINHNGHIEM], [MOTA], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [LUOTXEM], [NGANHMONGMUON], [CAPBACMONGMUON], [THOIGIANLAMVIECMONGMUON], [MUCTIEUNGHENGHIEP], [VITRIUNGTUYEN], [SOCIALLOGIN]) VALUES (1028, N'user789@gmail.com', N'User123', N'Resources/Images/user.jpg', N'Lê', N'Nhàn', N'182 / 49 Thoại Ngọc Hầu, Phú Thạnh, Tân Phú, Hồ Chí Minh', 0, 1, CAST(N'1993-06-28' AS Date), N'0435430131', CAST(N'2020-07-20' AS Date), 79, N'Resources\Images\DATN2020.039_NTBichNgan - Nguyen Hong Vu.pdf', 900000.0000, 1, N'ôi có khả năng xử lý nhiều nhiệm vụ trong một lúc và có thể hoàn thành đúng hạn các nhiệm vụ được giao. Ngoài ra tôi là người đáng tin cậy và người quản lý thời gian tuyệt vời, một trong thế mạnh quan trọng của tôi là giao tiếp, xây dựng mối quan hệ mạnh mẽ với mọi người để đạt được kết quả công việc tốt nhất. ', 0, NULL, N'Đang cập nhật', 100, 2, N'Nhân viên', N'Công việc toàn thời gian', N'Mục tiêu ngắn hạn: Làm font-end cắt html từ pdf.
Mục tiêu dài hạn: Làm leader nhóm web dsadasdasdsada', NULL, 0)
GO

INSERT [dbo].[UNGVIEN] ([ID_UNGVIEN], [EMAIL], [MATKHAU], [HINHANH], [HO], [TEN], [DIACHI], [GIOITINH], [DOCTHAN], [NGAYSINH], [SDT], [NGAYTAO], [ID_THANHPHO], [URL_CV], [LUONG], [KINHNGHIEM], [MOTA], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [LUOTXEM], [NGANHMONGMUON], [CAPBACMONGMUON], [THOIGIANLAMVIECMONGMUON], [MUCTIEUNGHENGHIEP], [VITRIUNGTUYEN], [SOCIALLOGIN]) VALUES (1029, N'user678@gmail.com', N'User123', N'Resources/Images/user.jpg', N'Hà', N'Văn Nữ', N'182 / 49 Thoại Ngọc Hầu, Phú Thạnh, Tân Phú, Hồ Chí Minh', 0, 1, CAST(N'1998-03-28' AS Date), N'0132213134', CAST(N'2020-07-20' AS Date), 79, N'Resources\Images\DATN2020.039_NTBichNgan - Nguyen Hong Vu.pdf', 700000.0000, 1, N'ôi có khả năng xử lý nhiều nhiệm vụ trong một lúc và có thể hoàn thành đúng hạn các nhiệm vụ được giao. Ngoài ra tôi là người đáng tin cậy và người quản lý thời gian tuyệt vời, một trong thế mạnh quan trọng của tôi là giao tiếp, xây dựng mối quan hệ mạnh mẽ với mọi người để đạt được kết quả công việc tốt nhất. ', 0, NULL, N'Đang tìm việc', 100, 3, N'Nhân viên', N'Công việc toàn thời gian', N'Mục tiêu ngắn hạn: Làm font-end cắt html từ pdf.
Mục tiêu dài hạn: Làm leader nhóm web dsadasdasdsada', NULL, 0)
GO

INSERT [dbo].[UNGVIEN] ([ID_UNGVIEN], [EMAIL], [MATKHAU], [HINHANH], [HO], [TEN], [DIACHI], [GIOITINH], [DOCTHAN], [NGAYSINH], [SDT], [NGAYTAO], [ID_THANHPHO], [URL_CV], [LUONG], [KINHNGHIEM], [MOTA], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [LUOTXEM], [NGANHMONGMUON], [CAPBACMONGMUON], [THOIGIANLAMVIECMONGMUON], [MUCTIEUNGHENGHIEP], [VITRIUNGTUYEN], [SOCIALLOGIN]) VALUES (1030, N'user132@gmail.com', N'User123', N'Resources/Images/user.jpg', N'Nguyễn', N'Tuấn', N'182 / 49 Thoại Ngọc Hầu, Phú Thạnh, Tân Phú, Hồ Chí Minh', 0, 1, CAST(N'1997-03-03' AS Date), N'0543530131', CAST(N'2020-07-20' AS Date), 79, N'Resources\Images\DATN2020.039_NTBichNgan - Nguyen Hong Vu.pdf', 700000.0000, 1, N'ôi có khả năng xử lý nhiều nhiệm vụ trong một lúc và có thể hoàn thành đúng hạn các nhiệm vụ được giao. Ngoài ra tôi là người đáng tin cậy và người quản lý thời gian tuyệt vời, một trong thế mạnh quan trọng của tôi là giao tiếp, xây dựng mối quan hệ mạnh mẽ với mọi người để đạt được kết quả công việc tốt nhất. ', 0, NULL, N'Đang tìm việc', 100, 4, N'Nhân viên', N'Công việc toàn thời gian', N'Mục tiêu ngắn hạn: Làm font-end cắt html từ pdf.
Mục tiêu dài hạn: Làm leader nhóm web dsadasdasdsada', NULL, 0)
GO

INSERT [dbo].[UNGVIEN] ([ID_UNGVIEN], [EMAIL], [MATKHAU], [HINHANH], [HO], [TEN], [DIACHI], [GIOITINH], [DOCTHAN], [NGAYSINH], [SDT], [NGAYTAO], [ID_THANHPHO], [URL_CV], [LUONG], [KINHNGHIEM], [MOTA], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [LUOTXEM], [NGANHMONGMUON], [CAPBACMONGMUON], [THOIGIANLAMVIECMONGMUON], [MUCTIEUNGHENGHIEP], [VITRIUNGTUYEN], [SOCIALLOGIN]) VALUES (1031, N'user4@gmail.com', N'User123', N'Resources/Images/user.jpg', N'Võ', N'Thiên Tài', N'182 / 49 Thoại Ngọc Hầu, Phú Thạnh, Tân Phú, Hồ Chí Minh', 0, 1, CAST(N'1996-12-28' AS Date), N'0312321031', CAST(N'2020-07-20' AS Date), 79, N'Resources\Images\DATN2020.039_NTBichNgan - Nguyen Hong Vu.pdf', 400000.0000, 1, N'ôi có khả năng xử lý nhiều nhiệm vụ trong một lúc và có thể hoàn thành đúng hạn các nhiệm vụ được giao. Ngoài ra tôi là người đáng tin cậy và người quản lý thời gian tuyệt vời, một trong thế mạnh quan trọng của tôi là giao tiếp, xây dựng mối quan hệ mạnh mẽ với mọi người để đạt được kết quả công việc tốt nhất. ', 0, NULL, N'Đang cập nhật', 100, 5, N'Quản lý', N'Công việc toàn thời gian', N'Mục tiêu ngắn hạn: Làm font-end cắt html từ pdf.
Mục tiêu dài hạn: Làm leader nhóm web dsadasdasdsada', NULL, 0)
GO

INSERT [dbo].[UNGVIEN] ([ID_UNGVIEN], [EMAIL], [MATKHAU], [HINHANH], [HO], [TEN], [DIACHI], [GIOITINH], [DOCTHAN], [NGAYSINH], [SDT], [NGAYTAO], [ID_THANHPHO], [URL_CV], [LUONG], [KINHNGHIEM], [MOTA], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [LUOTXEM], [NGANHMONGMUON], [CAPBACMONGMUON], [THOIGIANLAMVIECMONGMUON], [MUCTIEUNGHENGHIEP], [VITRIUNGTUYEN], [SOCIALLOGIN]) VALUES (1032, N'user324@gmail.com', N'User123', N'Resources/Images/user.jpg', N'Phan', N'Văn Tân', N'182 / 49 Thoại Ngọc Hầu, Phú Thạnh, Tân Phú, Hồ Chí Minh', 0, 1, CAST(N'1991-11-02' AS Date), N'0435435032', CAST(N'2020-07-20' AS Date), 79, N'Resources\Images\DATN2020.039_NTBichNgan - Nguyen Hong Vu.pdf', 500000.0000, 1, N'ôi có khả năng xử lý nhiều nhiệm vụ trong một lúc và có thể hoàn thành đúng hạn các nhiệm vụ được giao. Ngoài ra tôi là người đáng tin cậy và người quản lý thời gian tuyệt vời, một trong thế mạnh quan trọng của tôi là giao tiếp, xây dựng mối quan hệ mạnh mẽ với mọi người để đạt được kết quả công việc tốt nhất. ', 0, NULL, N'Đang tìm việc', 100, 4, N'Quản lý', N'Công việc toàn thời gian', N'Mục tiêu ngắn hạn: Làm font-end cắt html từ pdf.
Mục tiêu dài hạn: Làm leader nhóm web dsadasdasdsada', NULL, 0)
GO

INSERT [dbo].[UNGVIEN] ([ID_UNGVIEN], [EMAIL], [MATKHAU], [HINHANH], [HO], [TEN], [DIACHI], [GIOITINH], [DOCTHAN], [NGAYSINH], [SDT], [NGAYTAO], [ID_THANHPHO], [URL_CV], [LUONG], [KINHNGHIEM], [MOTA], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [LUOTXEM], [NGANHMONGMUON], [CAPBACMONGMUON], [THOIGIANLAMVIECMONGMUON], [MUCTIEUNGHENGHIEP], [VITRIUNGTUYEN], [SOCIALLOGIN]) VALUES (1033, N'user542@gmail.com', N'User123', N'Resources/Images/user.jpg', N'Gian ', N'Sơ Ảnh', N'182 / 49 Thoại Ngọc Hầu, Phú Thạnh, Tân Phú, Hồ Chí Minh', 0, 1, CAST(N'1997-07-28' AS Date), N'0345435345', CAST(N'2020-07-20' AS Date), 79, N'Resources\Images\DATN2020.039_NTBichNgan - Nguyen Hong Vu.pdf', 700000.0000, 1, N'ôi có khả năng xử lý nhiều nhiệm vụ trong một lúc và có thể hoàn thành đúng hạn các nhiệm vụ được giao. Ngoài ra tôi là người đáng tin cậy và người quản lý thời gian tuyệt vời, một trong thế mạnh quan trọng của tôi là giao tiếp, xây dựng mối quan hệ mạnh mẽ với mọi người để đạt được kết quả công việc tốt nhất. ', 0, NULL, N'Đang tìm việc', 100, 8, N'Nhân viên', N'Công việc toàn thời gian', N'Mục tiêu ngắn hạn: Làm font-end cắt html từ pdf.
Mục tiêu dài hạn: Làm leader nhóm web dsadasdasdsada', NULL, 0)
GO

INSERT [dbo].[UNGVIEN] ([ID_UNGVIEN], [EMAIL], [MATKHAU], [HINHANH], [HO], [TEN], [DIACHI], [GIOITINH], [DOCTHAN], [NGAYSINH], [SDT], [NGAYTAO], [ID_THANHPHO], [URL_CV], [LUONG], [KINHNGHIEM], [MOTA], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [LUOTXEM], [NGANHMONGMUON], [CAPBACMONGMUON], [THOIGIANLAMVIECMONGMUON], [MUCTIEUNGHENGHIEP], [VITRIUNGTUYEN], [SOCIALLOGIN]) VALUES (1034, N'user3123@gmail.com', N'User123', N'Resources/Images/user.jpg', N'Vũ', N'Duy Hàn', N'182 / 49 Thoại Ngọc Hầu, Phú Thạnh, Tân Phú, Hồ Chí Minh', 0, 1, CAST(N'1993-03-22' AS Date), N'0345435343', CAST(N'2020-07-20' AS Date), 79, N'Resources\Images\DATN2020.039_NTBichNgan - Nguyen Hong Vu.pdf', 1100000.0000, 1, N'ôi có khả năng xử lý nhiều nhiệm vụ trong một lúc và có thể hoàn thành đúng hạn các nhiệm vụ được giao. Ngoài ra tôi là người đáng tin cậy và người quản lý thời gian tuyệt vời, một trong thế mạnh quan trọng của tôi là giao tiếp, xây dựng mối quan hệ mạnh mẽ với mọi người để đạt được kết quả công việc tốt nhất. ', 0, NULL, N'Đang tìm việc', 100, 7, N'Quản lý', N'Công việc toàn thời gian', N'Mục tiêu ngắn hạn: Làm font-end cắt html từ pdf.
Mục tiêu dài hạn: Làm leader nhóm web dsadasdasdsada', NULL, 0)
GO

INSERT [dbo].[UNGVIEN] ([ID_UNGVIEN], [EMAIL], [MATKHAU], [HINHANH], [HO], [TEN], [DIACHI], [GIOITINH], [DOCTHAN], [NGAYSINH], [SDT], [NGAYTAO], [ID_THANHPHO], [URL_CV], [LUONG], [KINHNGHIEM], [MOTA], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [LUOTXEM], [NGANHMONGMUON], [CAPBACMONGMUON], [THOIGIANLAMVIECMONGMUON], [MUCTIEUNGHENGHIEP], [VITRIUNGTUYEN], [SOCIALLOGIN]) VALUES (1035, N'user213@gmail.com', N'User123', N'Resources/Images/user.jpg', N'Nguyễn', N'Trần Trung Quân', N'182 / 49 Thoại Ngọc Hầu, Phú Thạnh, Tân Phú, Hồ Chí Minh', 0, 1, CAST(N'1998-03-28' AS Date), N'0435654564', CAST(N'2020-07-20' AS Date), 79, N'Resources\Images\DATN2020.039_NTBichNgan - Nguyen Hong Vu.pdf', 800000.0000, 1, N'ôi có khả năng xử lý nhiều nhiệm vụ trong một lúc và có thể hoàn thành đúng hạn các nhiệm vụ được giao. Ngoài ra tôi là người đáng tin cậy và người quản lý thời gian tuyệt vời, một trong thế mạnh quan trọng của tôi là giao tiếp, xây dựng mối quan hệ mạnh mẽ với mọi người để đạt được kết quả công việc tốt nhất. ', 0, NULL, N'Đang tìm việc', 100, 9, N'Quản lý', N'Công việc toàn thời gian', N'Mục tiêu ngắn hạn: Làm font-end cắt html từ pdf.
Mục tiêu dài hạn: Làm leader nhóm web dsadasdasdsada', NULL, 0)
GO

INSERT [dbo].[UNGVIEN] ([ID_UNGVIEN], [EMAIL], [MATKHAU], [HINHANH], [HO], [TEN], [DIACHI], [GIOITINH], [DOCTHAN], [NGAYSINH], [SDT], [NGAYTAO], [ID_THANHPHO], [URL_CV], [LUONG], [KINHNGHIEM], [MOTA], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [LUOTXEM], [NGANHMONGMUON], [CAPBACMONGMUON], [THOIGIANLAMVIECMONGMUON], [MUCTIEUNGHENGHIEP], [VITRIUNGTUYEN], [SOCIALLOGIN]) VALUES (1036, N'user321@gmail.com', N'User123', N'Resources/Images/user.jpg', N'Nguyễn', N'Thành Tài', N'182 / 49 Thoại Ngọc Hầu, Phú Thạnh, Tân Phú, Hồ Chí Minh', 0, 1, CAST(N'1993-03-13' AS Date), N'0345342421', CAST(N'2020-07-20' AS Date), 79, N'Resources\Images\DATN2020.039_NTBichNgan - Nguyen Hong Vu.pdf', 700000.0000, 1, N'ôi có khả năng xử lý nhiều nhiệm vụ trong một lúc và có thể hoàn thành đúng hạn các nhiệm vụ được giao. Ngoài ra tôi là người đáng tin cậy và người quản lý thời gian tuyệt vời, một trong thế mạnh quan trọng của tôi là giao tiếp, xây dựng mối quan hệ mạnh mẽ với mọi người để đạt được kết quả công việc tốt nhất. ', 0, NULL, N'Đang tìm việc', 100, 2, N'Nhân viên', N'Công việc toàn thời gian', N'Mục tiêu ngắn hạn: Làm font-end cắt html từ pdf.
Mục tiêu dài hạn: Làm leader nhóm web dsadasdasdsada', NULL, 0)
GO

INSERT [dbo].[UNGVIEN] ([ID_UNGVIEN], [EMAIL], [MATKHAU], [HINHANH], [HO], [TEN], [DIACHI], [GIOITINH], [DOCTHAN], [NGAYSINH], [SDT], [NGAYTAO], [ID_THANHPHO], [URL_CV], [LUONG], [KINHNGHIEM], [MOTA], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [LUOTXEM], [NGANHMONGMUON], [CAPBACMONGMUON], [THOIGIANLAMVIECMONGMUON], [MUCTIEUNGHENGHIEP], [VITRIUNGTUYEN], [SOCIALLOGIN]) VALUES (1037, N'user2@gmail.com', N'User123', N'Resources/Images/user.jpg', N'Hạ', N'Tý Sửu', N'182 / 49 Thoại Ngọc Hầu, Phú Thạnh, Tân Phú, Hồ Chí Minh', 0, 1, CAST(N'1992-03-04' AS Date), N'0131241312', CAST(N'2020-07-20' AS Date), 79, N'Resources\Images\DATN2020.039_NTBichNgan - Nguyen Hong Vu.pdf', 700000.0000, 1, N'ôi có khả năng xử lý nhiều nhiệm vụ trong một lúc và có thể hoàn thành đúng hạn các nhiệm vụ được giao. Ngoài ra tôi là người đáng tin cậy và người quản lý thời gian tuyệt vời, một trong thế mạnh quan trọng của tôi là giao tiếp, xây dựng mối quan hệ mạnh mẽ với mọi người để đạt được kết quả công việc tốt nhất. ', 0, NULL, N'Đang tìm việc', 100, 11, N'Nhân viên', N'Công việc toàn thời gian', N'Mục tiêu ngắn hạn: Làm font-end cắt html từ pdf.
Mục tiêu dài hạn: Làm leader nhóm web dsadasdasdsada', NULL, 0)
GO

INSERT [dbo].[UNGVIEN] ([ID_UNGVIEN], [EMAIL], [MATKHAU], [HINHANH], [HO], [TEN], [DIACHI], [GIOITINH], [DOCTHAN], [NGAYSINH], [SDT], [NGAYTAO], [ID_THANHPHO], [URL_CV], [LUONG], [KINHNGHIEM], [MOTA], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [LUOTXEM], [NGANHMONGMUON], [CAPBACMONGMUON], [THOIGIANLAMVIECMONGMUON], [MUCTIEUNGHENGHIEP], [VITRIUNGTUYEN], [SOCIALLOGIN]) VALUES (1038, N'2001160231@ittc.edu.vn', N'123456', N'https://lh3.googleusercontent.com/-lvnPOEKAAAs/AAAAAAAAAAI/AAAAAAAAAAA/AMZuucltz0_KJLvNevEUM1uKlq5OtOtqww/s96-c/photo.jpg', N'HỔ', N'ĐOÀNPHI', N'182 / 49 Thoại Ngọc Hầu, Phú Thạnh, Tân Phú, Hồ Chí Minh', 1, 0, CAST(N'1992-03-04' AS Date), N'0131241312', CAST(N'2020-07-28' AS Date), 79, NULL, 700000.0000, 1, N'ôi có khả năng xử lý nhiều nhiệm vụ trong một lúc và có thể hoàn thành đúng hạn các nhiệm vụ được giao. Ngoài ra tôi là người đáng tin cậy và người quản lý thời gian tuyệt vời, một trong thế mạnh quan trọng của tôi là giao tiếp, xây dựng mối quan hệ mạnh mẽ với mọi người để đạt được kết quả công việc tốt nhất. ', 0, NULL, N'Đang tìm việc', 0, 11, N'Nhân viên', N'Công việc toàn thời gian', N'Mục tiêu ngắn hạn: Làm font-end cắt html từ pdf.
Mục tiêu dài hạn: Làm leader nhóm web dsadasdasdsada', NULL, 1)
GO

INSERT [dbo].[UNGVIEN] ([ID_UNGVIEN], [EMAIL], [MATKHAU], [HINHANH], [HO], [TEN], [DIACHI], [GIOITINH], [DOCTHAN], [NGAYSINH], [SDT], [NGAYTAO], [ID_THANHPHO], [URL_CV], [LUONG], [KINHNGHIEM], [MOTA], [ISDELETE], [CONTENTDELETE], [TRANGTHAI], [LUOTXEM], [NGANHMONGMUON], [CAPBACMONGMUON], [THOIGIANLAMVIECMONGMUON], [MUCTIEUNGHENGHIEP], [VITRIUNGTUYEN], [SOCIALLOGIN]) VALUES (1039, N'mrvanngoc181@gmail.com', N'111111', N'Resources/Images/user.jpg', N'Vinh', N'Ngọc', N'182 / 49 Thoại Ngọc Hầu, Phú Thạnh, Tân Phú, Hồ Chí Minh', 1, 0, CAST(N'1992-03-04' AS Date), N'0131241312', CAST(N'2020-07-28' AS Date), 79, NULL, 700000.0000, 1, N'ôi có khả năng xử lý nhiều nhiệm vụ trong một lúc và có thể hoàn thành đúng hạn các nhiệm vụ được giao. Ngoài ra tôi là người đáng tin cậy và người quản lý thời gian tuyệt vời, một trong thế mạnh quan trọng của tôi là giao tiếp, xây dựng mối quan hệ mạnh mẽ với mọi người để đạt được kết quả công việc tốt nhất. ', 0, NULL, N'Đang tìm việc', 0, 11, N'Nhân viên', N'Công việc toàn thời gian', N'Mục tiêu ngắn hạn: Làm font-end cắt html từ pdf.
Mục tiêu dài hạn: Làm leader nhóm web dsadasdasdsada', NULL, 0)
GO

SET IDENTITY_INSERT [dbo].[UNGVIEN] OFF
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING ON
GO

/****** Object:  StoredProcedure [dbo].[GETALLTHANHPHO]    Script Date: 1/17/2024 4:27:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[GETTHANHPHO]    Script Date: 1/17/2024 4:27:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[SPNGANHTUYENDUNG]    Script Date: 1/17/2024 4:27:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

USE [master]
GO

ALTER DATABASE [JEEJOB] SET  READ_WRITE
GO

ALTER DATABASE [JEEJOB] SET QUERY_STORE (OPERATION_MODE = READ_WRITE, CLEANUP_POLICY = (STALE_QUERY_THRESHOLD_DAYS = 30), DATA_FLUSH_INTERVAL_SECONDS = 900, INTERVAL_LENGTH_MINUTES = 60, MAX_STORAGE_SIZE_MB = 1000, QUERY_CAPTURE_MODE = AUTO, SIZE_BASED_CLEANUP_MODE = AUTO, MAX_PLANS_PER_QUERY = 200)

GO

--Syntax Error: Incorrect syntax near WAIT_STATS_CAPTURE_MODE.
--ALTER DATABASE [JEEJOB] SET QUERY_STORE (OPERATION_MODE = READ_WRITE, CLEANUP_POLICY = (STALE_QUERY_THRESHOLD_DAYS = 30), DATA_FLUSH_INTERVAL_SECONDS = 900, INTERVAL_LENGTH_MINUTES = 60, MAX_STORAGE_SIZE_MB = 1000, QUERY_CAPTURE_MODE = AUTO, SIZE_BASED_CLEANUP_MODE = AUTO, MAX_PLANS_PER_QUERY = 200, WAIT_STATS_CAPTURE_MODE = ON)



GO
