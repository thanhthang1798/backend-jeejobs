﻿CREATE TABLE [dbo].[LUUUNGVIEN](
	[ID_NHATUYENDUNG] [int] NOT NULL,
	[ID_UNGVIEN] [int] NOT NULL,
	[NGAYLUU] [date] NULL,
 CONSTRAINT [PK_LUUUNGVIEN] PRIMARY KEY CLUSTERED 
(
	[ID_NHATUYENDUNG] ASC,
	[ID_UNGVIEN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[LUUUNGVIEN]  WITH CHECK ADD  CONSTRAINT [FK_LUUUNGVIEN_NHATUYENDUNG] FOREIGN KEY([ID_NHATUYENDUNG])
REFERENCES [dbo].[NHATUYENDUNG] ([ID_NHATUYENDUNG])
GO

ALTER TABLE [dbo].[LUUUNGVIEN] CHECK CONSTRAINT [FK_LUUUNGVIEN_NHATUYENDUNG]
GO
ALTER TABLE [dbo].[LUUUNGVIEN]  WITH CHECK ADD  CONSTRAINT [FK_LUUUNGVIEN_UNGVIEN] FOREIGN KEY([ID_UNGVIEN])
REFERENCES [dbo].[UNGVIEN] ([ID_UNGVIEN])
GO

ALTER TABLE [dbo].[LUUUNGVIEN] CHECK CONSTRAINT [FK_LUUUNGVIEN_UNGVIEN]