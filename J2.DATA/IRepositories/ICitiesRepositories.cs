﻿using J2.DATA.DBEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J2.DATA.IRepositories
{
    public interface ICitiesRepositories : IRepository<Thanhpho>
    {
        IList<Thanhpho> RawDataGetAll();
    }
}
