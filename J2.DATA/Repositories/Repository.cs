﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using J2.DATA.DBEntities;
using Dapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Data.SqlClient;
using static Dapper.SqlMapper;
using J2.DATA.IRepositories;

namespace J2.DATA.Repositories
{
    public class Repository<T> : IRepository<T> where T : class 
    {
        #region Variables

        string MsgCode, MsgMessage, MsgErrorCode;
        private readonly DBEntities.DBEntities _context;
        #endregion

        #region Constructors

        /// <summary>
        /// Constructor
        /// </summary>
        protected Repository(DBEntities.DBEntities context)
        {
            _context = context;
        }

        #endregion

        #region Get Data

        /// <summary>
        /// Get all item
        /// </summary>
        /// <returns></returns>
        public IList<T> GetAll()
        {
            return _context.Set<T>().ToList();
        }

        /// <summary>
        /// Get List object
        /// </summary>
        /// <param name="navigationProperties">parameter</param>
        /// <returns>Return IList objects</returns>
        public IList<T> GetAll(params Expression<Func<T, object>>[] navigationProperties)
        {
            try
            {
                List<T> list;
                IQueryable<T> dbQuery = _context.Set<T>();

                //Apply eager loading
                foreach (Expression<Func<T, object>> navigationProperty in navigationProperties)
                    dbQuery = dbQuery.Include(navigationProperty);

                list = dbQuery
                    .AsNoTracking()
                    .ToList();
                TotalRows = list.Count();
                return list;
            }
            catch
            {
                throw;
            }
        }

        public IList<T> GetAll(int startRow, int maxRow, params Expression<Func<T, object>>[] navigationProperties)
        {
            try
            {
                List<T> list;
                IQueryable<T> dbQuery = _context.Set<T>();

                //Apply eager loading
                foreach (Expression<Func<T, object>> navigationProperty in navigationProperties)
                    dbQuery = dbQuery.Include(navigationProperty);

                list = dbQuery
                    .AsNoTracking()
                    .ToList();
                TotalRows = list.Count();
                return list.Skip(startRow).Take(maxRow).ToList();
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Get list object by some conditions 
        /// </summary>
        /// <param name="where">Where conditions</param>
        /// <param name="navigationProperties"></param>
        /// <returns></returns>
        public IList<T> GetList(Func<T, bool> where, params Expression<Func<T, object>>[] navigationProperties)
        {
            try
            {
                List<T> list;
                IQueryable<T> dbQuery = _context.Set<T>();

                //Apply eager loading
                foreach (Expression<Func<T, object>> navigationProperty in navigationProperties)
                    dbQuery = dbQuery.Include(navigationProperty);

                list = dbQuery
                    .AsNoTracking()
                    .Where(where)
                    .ToList();
                TotalRows = list.Count();
                return list;
            }
            catch
            {
                throw;
            }
        }

        public IList<T> GetList(int startRow, int maxRow, Func<T, bool> where, params Expression<Func<T, object>>[] navigationProperties)
        {
            try
            {
                List<T> list;
                IQueryable<T> dbQuery = _context.Set<T>();

                //Apply eager loading
                foreach (Expression<Func<T, object>> navigationProperty in navigationProperties)
                    dbQuery = dbQuery.Include(navigationProperty);

                list = dbQuery
                    .AsNoTracking()
                    .Where(where)
                    .ToList();
                TotalRows = list.Count();
                return list.Skip(startRow).Take(maxRow).ToList();
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Get single object
        /// </summary>
        /// <param name="where">Where some conditions</param>
        /// <param name="navigationProperties"></param>
        /// <returns>Return Object </returns>
        public T GetSingle(Func<T, bool> where, params Expression<Func<T, object>>[] navigationProperties)
        {
            try
            {
                T item = null;
                IQueryable<T> dbQuery = _context.Set<T>();

                //Apply eager loading
                foreach (Expression<Func<T, object>> navigationProperty in navigationProperties)
                    dbQuery = dbQuery.Include(navigationProperty);

                item = dbQuery
                    .AsNoTracking() //Don't track any changes for the selected item
                    .FirstOrDefault(where); //Apply where clause
                TotalRows = 1;
                return item;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Get object by object ID
        /// </summary>
        /// <param name="ID">object ID</param>
        /// <returns>Return only object</returns>
        public T GetByID(object ID)
        {
            try
            {
                return _context.Set<T>().Find(ID);
            }
            catch
            {
                throw;
            }
        }



        /// <summary>
        /// Add message
        /// </summary>
        /// <param name="Code">Message Code</param>
        /// <param name="Message">Message Text</param>
        public void AddMessage(string Code, string Message, string ErrorCode = "")
        {
            MsgCode = Code;
            MsgMessage = Message;
            MsgErrorCode = ErrorCode;
        }

        public string getMsgCode()
        {
            return MsgCode;
        }

        public string getMessage()
        {
            return MsgMessage;
        }

        public string getErrorCode()
        {
            return MsgErrorCode;
        }

        #endregion

        #region Insert, Update, Delete

        /// <summary>
        /// Add new item
        /// </summary>
        /// <param name="item">Object</param>
        /// <returns>True/False</returns>
        public void Add(T item)
        {
            _context.Set<T>().Add(item);
        }

        /// <summary>
        /// Add news multi items
        /// </summary>
        /// <param name="items">list items</param>
        /// <returns>True/False</returns>
        public void AddRange(List<T> items)
        {
            _context.Set<T>().AddRange(items);
        }

        /// <summary>
        /// Update item
        /// </summary>
        /// <param name="item">Item</param>
        /// <returns>True/False</returns>
        public void Update(T item)
        {
            _context.Entry(item).State = EntityState.Modified;
            _context.Attach(item);
        }

        /// <summary>
        /// Update list items
        /// </summary>
        /// <param name="items">List items</param>
        /// <returns>True/False</returns>
        public void Update(List<T> items)
        {
            foreach (T item in items)
            {
                _context.Entry(item).State = EntityState.Modified;
                _context.Attach(item);
            }
        }

        /// <summary>
        /// Delete item by ID
        /// </summary>
        /// <param name="ID">Item ID</param>
        /// <returns>True/False</returns>
        public void Delete(T entity)
        {
            _context.Set<T>().Remove(entity);
        }

        /// <summary>
        /// Delete multi items
        /// </summary>
        /// <param name="items">List items</param>
        /// <returns>True/False</returns>
        public void Delete(List<T> entitys)
        {
            _context.Set<T>().RemoveRange(entitys);
        }

        #endregion



        #region Get & Set

        public int TotalRows { get; set; }
        public string LangID { get; set; }

        #endregion

        #region Private 

        private string GetTableName()
        {
            //using (var db = new DBEntities.DBEntities())
            //{
            //    var objectContext = ((IObjectContextAdapter)db).ObjectContext;
            //    var sql = objectContext.CreateObjectSet<T>().ToTraceString();
            //    var regex = new Regex(@"FROM\s+(?<table>.+)\s+AS");
            //    var match = regex.Match(sql);

            //    return match.Groups["table"].Value;
            //}
            return "TableName";
        }

        public string GetMessage(string Code, string Lang)
        {
            throw new NotImplementedException();
        }

        public string GetMessage()
        {
            throw new NotImplementedException();
        }

        public int GetMaxSortOrder(Func<T, int> columnSelector)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}