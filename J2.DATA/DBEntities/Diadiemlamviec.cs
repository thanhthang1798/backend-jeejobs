﻿using System;
using System.Collections.Generic;

namespace J2.DATA.DBEntities;

public partial class Diadiemlamviec
{
    public int IdDiadiemlamviec { get; set; }

    public int? IdNhatuyendung { get; set; }

    public int? IdThanhpho { get; set; }

    public string? Diachi { get; set; }

    public virtual ICollection<Dangtintuyendung> Dangtintuyendungs { get; set; } = new List<Dangtintuyendung>();

    public virtual Nhatuyendung? IdNhatuyendungNavigation { get; set; }

    public virtual Thanhpho? IdThanhphoNavigation { get; set; }
}
