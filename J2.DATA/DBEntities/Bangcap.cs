﻿using System;
using System.Collections.Generic;

namespace J2.DATA.DBEntities;

public partial class Bangcap
{
    public int IdBangcap { get; set; }

    public int? IdUngvien { get; set; }

    public int? IdTrinhdo { get; set; }

    public string? Chuyennganh { get; set; }

    public string? Tentruong { get; set; }

    public string? Tenbangcap { get; set; }

    public DateTime? Ngaybatdau { get; set; }

    public DateTime? Ngayketthuc { get; set; }

    public string? Ghichu { get; set; }

    public virtual Trinhdo? IdTrinhdoNavigation { get; set; }

    public virtual Ungvien? IdUngvienNavigation { get; set; }
}
