﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace J2.DATA.DBEntities;

public partial class DBEntities : DbContext
{
    //public DBEntities()
    //{
    //}

    public DBEntities(DbContextOptions<DBEntities> options)
        : base(options)
    {
    }

    public virtual DbSet<Bangcap> Bangcaps { get; set; }

    public virtual DbSet<Congviecmongmuon> Congviecmongmuons { get; set; }

    public virtual DbSet<Dangtintuyendung> Dangtintuyendungs { get; set; }

    public virtual DbSet<Diadiemlamviec> Diadiemlamviecs { get; set; }

    public virtual DbSet<Kinhnghiemlamviec> Kinhnghiemlamviecs { get; set; }

    public virtual DbSet<Kynangungvien> Kynangungviens { get; set; }

    public virtual DbSet<Loaicongviec> Loaicongviecs { get; set; }

    public virtual DbSet<Luucongviec> Luucongviecs { get; set; }

    public virtual DbSet<Luuungvien> Luuungviens { get; set; }

    public virtual DbSet<Nganh> Nganhs { get; set; }

    public virtual DbSet<Nganhungvien> Nganhungviens { get; set; }

    public virtual DbSet<Ngoaingu> Ngoaingus { get; set; }

    public virtual DbSet<Ngoainguungvien> Ngoainguungviens { get; set; }

    public virtual DbSet<Nhatuyendung> Nhatuyendungs { get; set; }

    public virtual DbSet<Phucloi> Phuclois { get; set; }

    public virtual DbSet<Phucloicongviecmongmuon> Phucloicongviecmongmuons { get; set; }

    public virtual DbSet<Phucloituyendung> Phucloituyendungs { get; set; }

    public virtual DbSet<Quantrivien> Quantriviens { get; set; }

    public virtual DbSet<Thanhpho> Thanhphos { get; set; }

    public virtual DbSet<Thanhphoungvien> Thanhphoungviens { get; set; }

    public virtual DbSet<Trinhdo> Trinhdos { get; set; }

    public virtual DbSet<Ungtuyen> Ungtuyens { get; set; }

    public virtual DbSet<Ungvien> Ungviens { get; set; }

    public virtual DbSet<VwRand> VwRands { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
        => optionsBuilder.UseSqlServer("Server=localhost, 1433;Database=JEEJOB;Trusted_Connection=True;Encrypt=false;TrustServerCertificate=true");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Bangcap>(entity =>
        {
            entity.HasKey(e => e.IdBangcap);

            entity.ToTable("BANGCAP");

            entity.Property(e => e.IdBangcap).HasColumnName("ID_BANGCAP");
            entity.Property(e => e.Chuyennganh)
                .HasMaxLength(50)
                .HasColumnName("CHUYENNGANH");
            entity.Property(e => e.Ghichu)
                .HasColumnType("ntext")
                .HasColumnName("GHICHU");
            entity.Property(e => e.IdTrinhdo).HasColumnName("ID_TRINHDO");
            entity.Property(e => e.IdUngvien).HasColumnName("ID_UNGVIEN");
            entity.Property(e => e.Ngaybatdau)
                .HasColumnType("date")
                .HasColumnName("NGAYBATDAU");
            entity.Property(e => e.Ngayketthuc)
                .HasColumnType("date")
                .HasColumnName("NGAYKETTHUC");
            entity.Property(e => e.Tenbangcap)
                .HasMaxLength(50)
                .HasColumnName("TENBANGCAP");
            entity.Property(e => e.Tentruong)
                .HasMaxLength(200)
                .HasColumnName("TENTRUONG");

            entity.HasOne(d => d.IdTrinhdoNavigation).WithMany(p => p.Bangcaps)
                .HasForeignKey(d => d.IdTrinhdo)
                .HasConstraintName("FK_BANGCAP_TRINHDO");

            entity.HasOne(d => d.IdUngvienNavigation).WithMany(p => p.Bangcaps)
                .HasForeignKey(d => d.IdUngvien)
                .HasConstraintName("FK_BANGCAP_UNGVIEN");
        });

        modelBuilder.Entity<Congviecmongmuon>(entity =>
        {
            entity.HasKey(e => e.IdCongviecmongmuon);

            entity.ToTable("CONGVIECMONGMUON");

            entity.Property(e => e.IdCongviecmongmuon).HasColumnName("ID_CONGVIECMONGMUON");
            entity.Property(e => e.Capbac)
                .HasMaxLength(50)
                .HasColumnName("CAPBAC");
            entity.Property(e => e.IdLoaicongviec).HasColumnName("ID_LOAICONGVIEC");
            entity.Property(e => e.IdNganh).HasColumnName("ID_NGANH");
            entity.Property(e => e.IdThanhpho).HasColumnName("ID_THANHPHO");
            entity.Property(e => e.IdUngvien).HasColumnName("ID_UNGVIEN");
            entity.Property(e => e.Luong)
                .HasColumnType("money")
                .HasColumnName("LUONG");
            entity.Property(e => e.Ngaytao)
                .HasColumnType("date")
                .HasColumnName("NGAYTAO");
            entity.Property(e => e.Noikhac)
                .HasComment("Nơi làm việc khác ngoài những nơi đã chọn")
                .HasColumnName("NOIKHAC");
            entity.Property(e => e.Tenvieclam)
                .HasMaxLength(500)
                .HasColumnName("TENVIECLAM");
            entity.Property(e => e.Thongbao).HasColumnName("THONGBAO");
            entity.Property(e => e.Thuongluong).HasColumnName("THUONGLUONG");

            entity.HasOne(d => d.IdLoaicongviecNavigation).WithMany(p => p.Congviecmongmuons)
                .HasForeignKey(d => d.IdLoaicongviec)
                .HasConstraintName("FK_CONGVIECMONGMUON_LOAICONGVIEC");

            entity.HasOne(d => d.IdUngvienNavigation).WithMany(p => p.Congviecmongmuons)
                .HasForeignKey(d => d.IdUngvien)
                .HasConstraintName("FK_CONGVIECMONGMUON_UNGVIEN");

            entity.HasMany(d => d.IdThanhphos).WithMany(p => p.IdCongviecmongmuons)
                .UsingEntity<Dictionary<string, object>>(
                    "Noilamviecmongmuon",
                    r => r.HasOne<Thanhpho>().WithMany()
                        .HasForeignKey("IdThanhpho")
                        .OnDelete(DeleteBehavior.ClientSetNull)
                        .HasConstraintName("FK_NOILAMVIECMONGMUON_THANHPHO"),
                    l => l.HasOne<Congviecmongmuon>().WithMany()
                        .HasForeignKey("IdCongviecmongmuon")
                        .OnDelete(DeleteBehavior.ClientSetNull)
                        .HasConstraintName("FK_NOILAMVIECMONGMUON_CONGVIECMONGMUON"),
                    j =>
                    {
                        j.HasKey("IdCongviecmongmuon", "IdThanhpho");
                        j.ToTable("NOILAMVIECMONGMUON");
                        j.IndexerProperty<int>("IdCongviecmongmuon").HasColumnName("ID_CONGVIECMONGMUON");
                        j.IndexerProperty<int>("IdThanhpho").HasColumnName("ID_THANHPHO");
                    });
        });

        modelBuilder.Entity<Dangtintuyendung>(entity =>
        {
            entity.HasKey(e => e.IdTintuyendung);

            entity.ToTable("DANGTINTUYENDUNG");

            entity.Property(e => e.IdTintuyendung).HasColumnName("ID_TINTUYENDUNG");
            entity.Property(e => e.Capbac)
                .HasMaxLength(200)
                .HasColumnName("CAPBAC");
            entity.Property(e => e.Chucdanh)
                .HasMaxLength(200)
                .HasColumnName("CHUCDANH");
            entity.Property(e => e.Contentdelete).HasColumnName("CONTENTDELETE");
            entity.Property(e => e.Datasort).HasColumnName("DATASORT");
            entity.Property(e => e.Douutien).HasColumnName("DOUUTIEN");
            entity.Property(e => e.Emaillienhe)
                .HasMaxLength(50)
                .IsUnicode(false)
                .HasColumnName("EMAILLIENHE");
            entity.Property(e => e.Gioitinh).HasColumnName("GIOITINH");
            entity.Property(e => e.IdDiadiemlamviec).HasColumnName("ID_DIADIEMLAMVIEC");
            entity.Property(e => e.IdLoaicongviec).HasColumnName("ID_LOAICONGVIEC");
            entity.Property(e => e.IdNhatuyendung).HasColumnName("ID_NHATUYENDUNG");
            entity.Property(e => e.Isdelete).HasColumnName("ISDELETE");
            entity.Property(e => e.Kinhnghiem).HasColumnName("KINHNGHIEM");
            entity.Property(e => e.Luotxem).HasColumnName("LUOTXEM");
            entity.Property(e => e.Maxluong)
                .HasColumnType("money")
                .HasColumnName("MAXLUONG");
            entity.Property(e => e.Minluong)
                .HasColumnType("money")
                .HasColumnName("MINLUONG");
            entity.Property(e => e.Motacongviec).HasColumnName("MOTACONGVIEC");
            entity.Property(e => e.Ngaydang)
                .HasColumnType("date")
                .HasColumnName("NGAYDANG");
            entity.Property(e => e.Ngayhethan)
                .HasColumnType("date")
                .HasColumnName("NGAYHETHAN");
            entity.Property(e => e.Nguoilienhe)
                .HasMaxLength(200)
                .HasColumnName("NGUOILIENHE");
            entity.Property(e => e.Sdtlienhe)
                .HasMaxLength(15)
                .IsUnicode(false)
                .HasColumnName("SDTLIENHE");
            entity.Property(e => e.Soluong).HasColumnName("SOLUONG");
            entity.Property(e => e.Thuongluong).HasColumnName("THUONGLUONG");
            entity.Property(e => e.Tieude)
                .HasMaxLength(200)
                .HasColumnName("TIEUDE");
            entity.Property(e => e.Trangthai)
                .HasMaxLength(50)
                .HasColumnName("TRANGTHAI");
            entity.Property(e => e.Yeucaucongviec).HasColumnName("YEUCAUCONGVIEC");
            entity.Property(e => e.Yeucaukynang).HasColumnName("YEUCAUKYNANG");

            entity.HasOne(d => d.IdDiadiemlamviecNavigation).WithMany(p => p.Dangtintuyendungs)
                .HasForeignKey(d => d.IdDiadiemlamviec)
                .HasConstraintName("FK_DANGTINTUYENDUNG_DIADIEMLAMVIEC");

            entity.HasOne(d => d.IdLoaicongviecNavigation).WithMany(p => p.Dangtintuyendungs)
                .HasForeignKey(d => d.IdLoaicongviec)
                .HasConstraintName("FK_DANGTINTUYENDUNG_LOAICONGVIEC");

            entity.HasOne(d => d.IdNhatuyendungNavigation).WithMany(p => p.Dangtintuyendungs)
                .HasForeignKey(d => d.IdNhatuyendung)
                .HasConstraintName("FK_DANGTINTUYENDUNG_NHATUYENDUNG");

            entity.HasMany(d => d.IdNganhs).WithMany(p => p.IdTintuyendungs)
                .UsingEntity<Dictionary<string, object>>(
                    "Nganhtuyendung",
                    r => r.HasOne<Nganh>().WithMany()
                        .HasForeignKey("IdNganh")
                        .OnDelete(DeleteBehavior.ClientSetNull)
                        .HasConstraintName("FK_NGANHTUYENDUNG_NGANH"),
                    l => l.HasOne<Dangtintuyendung>().WithMany()
                        .HasForeignKey("IdTintuyendung")
                        .OnDelete(DeleteBehavior.ClientSetNull)
                        .HasConstraintName("FK_NGANHTUYENDUNG_DANGTINTUYENDUNG"),
                    j =>
                    {
                        j.HasKey("IdTintuyendung", "IdNganh");
                        j.ToTable("NGANHTUYENDUNG");
                        j.IndexerProperty<int>("IdTintuyendung").HasColumnName("ID_TINTUYENDUNG");
                        j.IndexerProperty<int>("IdNganh").HasColumnName("ID_NGANH");
                    });
        });

        modelBuilder.Entity<Diadiemlamviec>(entity =>
        {
            entity.HasKey(e => e.IdDiadiemlamviec);

            entity.ToTable("DIADIEMLAMVIEC");

            entity.Property(e => e.IdDiadiemlamviec).HasColumnName("ID_DIADIEMLAMVIEC");
            entity.Property(e => e.Diachi)
                .HasMaxLength(200)
                .HasColumnName("DIACHI");
            entity.Property(e => e.IdNhatuyendung).HasColumnName("ID_NHATUYENDUNG");
            entity.Property(e => e.IdThanhpho).HasColumnName("ID_THANHPHO");

            entity.HasOne(d => d.IdNhatuyendungNavigation).WithMany(p => p.Diadiemlamviecs)
                .HasForeignKey(d => d.IdNhatuyendung)
                .HasConstraintName("FK_DIADIEMLAMVIEC_NHATUYENDUNG");

            entity.HasOne(d => d.IdThanhphoNavigation).WithMany(p => p.Diadiemlamviecs)
                .HasForeignKey(d => d.IdThanhpho)
                .HasConstraintName("FK_DIADIEMLAMVIEC_THANHPHO");
        });

        modelBuilder.Entity<Kinhnghiemlamviec>(entity =>
        {
            entity.HasKey(e => e.IdKinhnghiemlamviec);

            entity.ToTable("KINHNGHIEMLAMVIEC");

            entity.Property(e => e.IdKinhnghiemlamviec).HasColumnName("ID_KINHNGHIEMLAMVIEC");
            entity.Property(e => e.Chucdanh)
                .HasMaxLength(50)
                .HasColumnName("CHUCDANH");
            entity.Property(e => e.Chucvu)
                .HasMaxLength(50)
                .HasColumnName("CHUCVU");
            entity.Property(e => e.Congviechientai).HasColumnName("CONGVIECHIENTAI");
            entity.Property(e => e.Ghichu)
                .HasColumnType("ntext")
                .HasColumnName("GHICHU");
            entity.Property(e => e.IdUngvien).HasColumnName("ID_UNGVIEN");
            entity.Property(e => e.Tencongty)
                .HasMaxLength(200)
                .HasColumnName("TENCONGTY");
            entity.Property(e => e.Thoigianbatdau)
                .HasColumnType("date")
                .HasColumnName("THOIGIANBATDAU");
            entity.Property(e => e.Thoigianketthuc)
                .HasColumnType("date")
                .HasColumnName("THOIGIANKETTHUC");

            entity.HasOne(d => d.IdUngvienNavigation).WithMany(p => p.Kinhnghiemlamviecs)
                .HasForeignKey(d => d.IdUngvien)
                .HasConstraintName("FK_KINHNGHIEMLAMVIEC_UNGVIEN");
        });

        modelBuilder.Entity<Kynangungvien>(entity =>
        {
            entity.HasKey(e => e.IdKynangcanhan);

            entity.ToTable("KYNANGUNGVIEN");

            entity.Property(e => e.IdKynangcanhan).HasColumnName("ID_KYNANGCANHAN");
            entity.Property(e => e.IdUngvien).HasColumnName("ID_UNGVIEN");
            entity.Property(e => e.Motakynang).HasColumnName("MOTAKYNANG");
            entity.Property(e => e.Tenkynang)
                .HasMaxLength(200)
                .HasColumnName("TENKYNANG");

            entity.HasOne(d => d.IdUngvienNavigation).WithMany(p => p.Kynangungviens)
                .HasForeignKey(d => d.IdUngvien)
                .HasConstraintName("FK_KYNANGUNGVIEN_UNGVIEN");
        });

        modelBuilder.Entity<Loaicongviec>(entity =>
        {
            entity.HasKey(e => e.IdLoaicongviec);

            entity.ToTable("LOAICONGVIEC");

            entity.Property(e => e.IdLoaicongviec).HasColumnName("ID_LOAICONGVIEC");
            entity.Property(e => e.Tenloaicongviec)
                .HasMaxLength(200)
                .HasColumnName("TENLOAICONGVIEC");
        });

        modelBuilder.Entity<Luucongviec>(entity =>
        {
            entity.HasKey(e => new { e.IdUngvien, e.IdTintuyendung }).HasName("PK_LUUCONGVIEC_1");

            entity.ToTable("LUUCONGVIEC");

            entity.Property(e => e.IdUngvien).HasColumnName("ID_UNGVIEN");
            entity.Property(e => e.IdTintuyendung).HasColumnName("ID_TINTUYENDUNG");
            entity.Property(e => e.Ngayluu)
                .HasColumnType("date")
                .HasColumnName("NGAYLUU");

            entity.HasOne(d => d.IdTintuyendungNavigation).WithMany(p => p.Luucongviecs)
                .HasForeignKey(d => d.IdTintuyendung)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_LUUCONGVIEC_DANGTINTUYENDUNG");

            entity.HasOne(d => d.IdUngvienNavigation).WithMany(p => p.Luucongviecs)
                .HasForeignKey(d => d.IdUngvien)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_LUUCONGVIEC_UNGVIEN");
        });

        modelBuilder.Entity<Luuungvien>(entity =>
        {
            entity.HasKey(e => new { e.IdNhatuyendung, e.IdUngvien });

            entity.ToTable("LUUUNGVIEN");

            entity.Property(e => e.IdNhatuyendung).HasColumnName("ID_NHATUYENDUNG");
            entity.Property(e => e.IdUngvien).HasColumnName("ID_UNGVIEN");
            entity.Property(e => e.Ngayluu)
                .HasColumnType("date")
                .HasColumnName("NGAYLUU");

            entity.HasOne(d => d.IdNhatuyendungNavigation).WithMany(p => p.Luuungviens)
                .HasForeignKey(d => d.IdNhatuyendung)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_LUUUNGVIEN_NHATUYENDUNG");

            entity.HasOne(d => d.IdUngvienNavigation).WithMany(p => p.Luuungviens)
                .HasForeignKey(d => d.IdUngvien)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_LUUUNGVIEN_UNGVIEN");
        });

        modelBuilder.Entity<Nganh>(entity =>
        {
            entity.HasKey(e => e.IdNganh);

            entity.ToTable("NGANH");

            entity.Property(e => e.IdNganh).HasColumnName("ID_NGANH");
            entity.Property(e => e.Tennganh)
                .HasMaxLength(200)
                .HasColumnName("TENNGANH");

            entity.HasMany(d => d.IdCongviecmongmuons).WithMany(p => p.IdNganhs)
                .UsingEntity<Dictionary<string, object>>(
                    "Nganhnghemongmuon",
                    r => r.HasOne<Congviecmongmuon>().WithMany()
                        .HasForeignKey("IdCongviecmongmuon")
                        .OnDelete(DeleteBehavior.ClientSetNull)
                        .HasConstraintName("FK_NGANHNGHEMONGMUON_CONGVIECMONGMUON"),
                    l => l.HasOne<Nganh>().WithMany()
                        .HasForeignKey("IdNganh")
                        .OnDelete(DeleteBehavior.ClientSetNull)
                        .HasConstraintName("FK_NGANHNGHEMONGMUON_NGANH"),
                    j =>
                    {
                        j.HasKey("IdNganh", "IdCongviecmongmuon");
                        j.ToTable("NGANHNGHEMONGMUON");
                        j.IndexerProperty<int>("IdNganh").HasColumnName("ID_NGANH");
                        j.IndexerProperty<int>("IdCongviecmongmuon").HasColumnName("ID_CONGVIECMONGMUON");
                    });
        });

        modelBuilder.Entity<Nganhungvien>(entity =>
        {
            entity.HasKey(e => new { e.IdNganh, e.IdUngvien });

            entity.ToTable("NGANHUNGVIEN");

            entity.Property(e => e.IdNganh).HasColumnName("ID_NGANH");
            entity.Property(e => e.IdUngvien).HasColumnName("ID_UNGVIEN");

            entity.HasOne(d => d.IdUngvienNavigation).WithMany(p => p.Nganhungviens)
                .HasForeignKey(d => d.IdUngvien)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_NGANHUNGVIEN_UNGVIEN");
        });

        modelBuilder.Entity<Ngoaingu>(entity =>
        {
            entity.HasKey(e => e.IdNgoaingu);

            entity.ToTable("NGOAINGU");

            entity.Property(e => e.IdNgoaingu).HasColumnName("ID_NGOAINGU");
            entity.Property(e => e.Tenngoaingu)
                .HasMaxLength(50)
                .HasColumnName("TENNGOAINGU");
        });

        modelBuilder.Entity<Ngoainguungvien>(entity =>
        {
            entity.HasKey(e => new { e.IdNgoaingu, e.IdUngvien }).HasName("PK_NGOAINGUUNGVIEN_1");

            entity.ToTable("NGOAINGUUNGVIEN");

            entity.Property(e => e.IdNgoaingu).HasColumnName("ID_NGOAINGU");
            entity.Property(e => e.IdUngvien).HasColumnName("ID_UNGVIEN");
            entity.Property(e => e.Coban).HasColumnName("COBAN");
            entity.Property(e => e.Doc).HasColumnName("DOC");
            entity.Property(e => e.Ghichu)
                .HasColumnType("ntext")
                .HasColumnName("GHICHU");
            entity.Property(e => e.Nghe).HasColumnName("NGHE");
            entity.Property(e => e.Noi).HasColumnName("NOI");
            entity.Property(e => e.Viet).HasColumnName("VIET");

            entity.HasOne(d => d.IdNgoainguNavigation).WithMany(p => p.Ngoainguungviens)
                .HasForeignKey(d => d.IdNgoaingu)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_NGOAINGUUNGVIEN_NGOAINGU");

            entity.HasOne(d => d.IdUngvienNavigation).WithMany(p => p.Ngoainguungviens)
                .HasForeignKey(d => d.IdUngvien)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_NGOAINGUUNGVIEN_UNGVIEN");
        });

        modelBuilder.Entity<Nhatuyendung>(entity =>
        {
            entity.HasKey(e => e.IdNhatuyendung);

            entity.ToTable("NHATUYENDUNG");

            entity.HasIndex(e => e.Email, "UQ__NHATUYEN__161CF7245C40A6BC").IsUnique();

            entity.Property(e => e.IdNhatuyendung).HasColumnName("ID_NHATUYENDUNG");
            entity.Property(e => e.Bannercongty).HasColumnName("BANNERCONGTY");
            entity.Property(e => e.Contentdelete).HasColumnName("CONTENTDELETE");
            entity.Property(e => e.Diachi).HasColumnName("DIACHI");
            entity.Property(e => e.Douutien).HasColumnName("DOUUTIEN");
            entity.Property(e => e.Email)
                .HasMaxLength(50)
                .IsUnicode(false)
                .HasColumnName("EMAIL");
            entity.Property(e => e.Gioithieucongty).HasColumnName("GIOITHIEUCONGTY");
            entity.Property(e => e.Hinhanh).HasColumnName("HINHANH");
            entity.Property(e => e.Isdelete).HasColumnName("ISDELETE");
            entity.Property(e => e.Logo).HasColumnName("LOGO");
            entity.Property(e => e.Luotxem).HasColumnName("LUOTXEM");
            entity.Property(e => e.Matkhau)
                .HasMaxLength(50)
                .IsUnicode(false)
                .HasColumnName("MATKHAU");
            entity.Property(e => e.Nganhnghehoatdong).HasColumnName("NGANHNGHEHOATDONG");
            entity.Property(e => e.Ngaydangky)
                .HasColumnType("date")
                .HasColumnName("NGAYDANGKY");
            entity.Property(e => e.Quymocongty).HasColumnName("QUYMOCONGTY");
            entity.Property(e => e.Sdt)
                .HasMaxLength(15)
                .IsUnicode(false)
                .HasColumnName("SDT");
            entity.Property(e => e.Showlogo).HasColumnName("SHOWLOGO");
            entity.Property(e => e.Sociallogin).HasColumnName("SOCIALLOGIN");
            entity.Property(e => e.Tencongty)
                .HasMaxLength(200)
                .HasColumnName("TENCONGTY");
            entity.Property(e => e.Tenviettat)
                .HasMaxLength(50)
                .IsUnicode(false)
                .HasColumnName("TENVIETTAT");
            entity.Property(e => e.Trangthai).HasColumnName("TRANGTHAI");
            entity.Property(e => e.Videogioithieu).HasColumnName("VIDEOGIOITHIEU");
            entity.Property(e => e.Website).HasColumnName("WEBSITE");

            entity.HasOne(d => d.NganhnghehoatdongNavigation).WithMany(p => p.Nhatuyendungs)
                .HasForeignKey(d => d.Nganhnghehoatdong)
                .HasConstraintName("FK_NHATUYENDUNG_NGANH");
        });

        modelBuilder.Entity<Phucloi>(entity =>
        {
            entity.HasKey(e => e.IdPhucloi);

            entity.ToTable("PHUCLOI");

            entity.Property(e => e.IdPhucloi).HasColumnName("ID_PHUCLOI");
            entity.Property(e => e.Tenphucloi)
                .HasMaxLength(200)
                .HasColumnName("TENPHUCLOI");
        });

        modelBuilder.Entity<Phucloicongviecmongmuon>(entity =>
        {
            entity.HasKey(e => new { e.IdPhucloi, e.IdCongviecmongmuon }).HasName("PK_PHUCLOICONGVIECMONGMUON_1");

            entity.ToTable("PHUCLOICONGVIECMONGMUON");

            entity.Property(e => e.IdPhucloi).HasColumnName("ID_PHUCLOI");
            entity.Property(e => e.IdCongviecmongmuon).HasColumnName("ID_CONGVIECMONGMUON");
            entity.Property(e => e.Phucloimongmuon)
                .HasMaxLength(225)
                .HasColumnName("PHUCLOIMONGMUON");

            entity.HasOne(d => d.IdCongviecmongmuonNavigation).WithMany(p => p.Phucloicongviecmongmuons)
                .HasForeignKey(d => d.IdCongviecmongmuon)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_PHUCLOICONGVIECMONGMUON_CONGVIECMONGMUON1");

            entity.HasOne(d => d.IdPhucloiNavigation).WithMany(p => p.Phucloicongviecmongmuons)
                .HasForeignKey(d => d.IdPhucloi)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_PHUCLOICONGVIECMONGMUON_PHUCLOI");
        });

        modelBuilder.Entity<Phucloituyendung>(entity =>
        {
            entity.HasKey(e => new { e.IdPhucloi, e.IdTintuyendung });

            entity.ToTable("PHUCLOITUYENDUNG");

            entity.Property(e => e.IdPhucloi).HasColumnName("ID_PHUCLOI");
            entity.Property(e => e.IdTintuyendung).HasColumnName("ID_TINTUYENDUNG");
            entity.Property(e => e.Motaphucloi).HasColumnName("MOTAPHUCLOI");

            entity.HasOne(d => d.IdPhucloiNavigation).WithMany(p => p.Phucloituyendungs)
                .HasForeignKey(d => d.IdPhucloi)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_PHUCLOITUYENDUNG_PHUCLOI");

            entity.HasOne(d => d.IdTintuyendungNavigation).WithMany(p => p.Phucloituyendungs)
                .HasForeignKey(d => d.IdTintuyendung)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_PHUCLOITUYENDUNG_DANGTINTUYENDUNG");
        });

        modelBuilder.Entity<Quantrivien>(entity =>
        {
            entity.HasKey(e => e.IdQtv).HasName("PK_NGUOIDUNG");

            entity.ToTable("QUANTRIVIEN");

            entity.Property(e => e.IdQtv).HasColumnName("ID_QTV");
            entity.Property(e => e.Email)
                .HasMaxLength(50)
                .IsUnicode(false)
                .HasColumnName("EMAIL");
            entity.Property(e => e.Matkhau)
                .HasMaxLength(50)
                .IsUnicode(false)
                .HasColumnName("MATKHAU");
            entity.Property(e => e.Sdt)
                .HasMaxLength(15)
                .IsUnicode(false)
                .HasColumnName("SDT");
        });

        modelBuilder.Entity<Thanhpho>(entity =>
        {
            entity.HasKey(e => e.IdThanhpho);

            entity.ToTable("THANHPHO");

            entity.Property(e => e.IdThanhpho).HasColumnName("ID_THANHPHO");
            entity.Property(e => e.Tenthanhpho)
                .HasMaxLength(50)
                .HasColumnName("TENTHANHPHO");
        });

        modelBuilder.Entity<Thanhphoungvien>(entity =>
        {
            entity.HasKey(e => new { e.IdUngvien, e.IdThanhpho });

            entity.ToTable("THANHPHOUNGVIEN");

            entity.Property(e => e.IdUngvien).HasColumnName("ID_UNGVIEN");
            entity.Property(e => e.IdThanhpho).HasColumnName("ID_THANHPHO");

            entity.HasOne(d => d.IdUngvienNavigation).WithMany(p => p.Thanhphoungviens)
                .HasForeignKey(d => d.IdUngvien)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_THANHPHOUNGVIEN_UNGVIEN");
        });

        modelBuilder.Entity<Trinhdo>(entity =>
        {
            entity.HasKey(e => e.IdTrinhdo);

            entity.ToTable("TRINHDO");

            entity.Property(e => e.IdTrinhdo).HasColumnName("ID_TRINHDO");
            entity.Property(e => e.Giatri).HasColumnName("GIATRI");
            entity.Property(e => e.Tentrinhdo)
                .HasMaxLength(200)
                .HasColumnName("TENTRINHDO");

            entity.HasMany(d => d.IdTuyendungs).WithMany(p => p.IdTrinhdos)
                .UsingEntity<Dictionary<string, object>>(
                    "Trinhdotuyendung",
                    r => r.HasOne<Dangtintuyendung>().WithMany()
                        .HasForeignKey("IdTuyendung")
                        .OnDelete(DeleteBehavior.ClientSetNull)
                        .HasConstraintName("FK_TRINHDOTUYENDUNG_DANGTINTUYENDUNG"),
                    l => l.HasOne<Trinhdo>().WithMany()
                        .HasForeignKey("IdTrinhdo")
                        .OnDelete(DeleteBehavior.ClientSetNull)
                        .HasConstraintName("FK_TRINHDOTUYENDUNG_TRINHDO"),
                    j =>
                    {
                        j.HasKey("IdTrinhdo", "IdTuyendung");
                        j.ToTable("TRINHDOTUYENDUNG");
                        j.IndexerProperty<int>("IdTrinhdo").HasColumnName("ID_TRINHDO");
                        j.IndexerProperty<int>("IdTuyendung").HasColumnName("ID_TUYENDUNG");
                    });
        });

        modelBuilder.Entity<Ungtuyen>(entity =>
        {
            entity.HasKey(e => new { e.IdUngvien, e.IdCongviec }).HasName("PK_UNGTUYEN_1");

            entity.ToTable("UNGTUYEN");

            entity.Property(e => e.IdUngvien).HasColumnName("ID_UNGVIEN");
            entity.Property(e => e.IdCongviec).HasColumnName("ID_CONGVIEC");
            entity.Property(e => e.Daxem).HasColumnName("DAXEM");
            entity.Property(e => e.Ngayungtuyen)
                .HasColumnType("date")
                .HasColumnName("NGAYUNGTUYEN");

            entity.HasOne(d => d.IdCongviecNavigation).WithMany(p => p.Ungtuyens)
                .HasForeignKey(d => d.IdCongviec)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_UNGTUYEN_DANGTINTUYENDUNG");

            entity.HasOne(d => d.IdUngvienNavigation).WithMany(p => p.Ungtuyens)
                .HasForeignKey(d => d.IdUngvien)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_UNGTUYEN_UNGVIEN");
        });

        modelBuilder.Entity<Ungvien>(entity =>
        {
            entity.HasKey(e => e.IdUngvien);

            entity.ToTable("UNGVIEN");

            entity.HasIndex(e => e.Email, "UQ__UNGVIEN__161CF724153F43ED").IsUnique();

            entity.HasIndex(e => e.Email, "UQ__UNGVIEN__161CF724469B49EA").IsUnique();

            entity.HasIndex(e => e.Email, "UQ__UNGVIEN__161CF724BA49BA05").IsUnique();

            entity.Property(e => e.IdUngvien).HasColumnName("ID_UNGVIEN");
            entity.Property(e => e.Capbacmongmuon)
                .HasMaxLength(200)
                .HasColumnName("CAPBACMONGMUON");
            entity.Property(e => e.Contentdelete).HasColumnName("CONTENTDELETE");
            entity.Property(e => e.Diachi)
                .HasMaxLength(200)
                .HasColumnName("DIACHI");
            entity.Property(e => e.Docthan).HasColumnName("DOCTHAN");
            entity.Property(e => e.Email)
                .HasMaxLength(50)
                .IsUnicode(false)
                .HasColumnName("EMAIL");
            entity.Property(e => e.Gioitinh).HasColumnName("GIOITINH");
            entity.Property(e => e.Hinhanh).HasColumnName("HINHANH");
            entity.Property(e => e.Ho)
                .HasMaxLength(50)
                .HasColumnName("HO");
            entity.Property(e => e.IdThanhpho).HasColumnName("ID_THANHPHO");
            entity.Property(e => e.Isdelete).HasColumnName("ISDELETE");
            entity.Property(e => e.Kinhnghiem).HasColumnName("KINHNGHIEM");
            entity.Property(e => e.Luong)
                .HasColumnType("money")
                .HasColumnName("LUONG");
            entity.Property(e => e.Luotxem).HasColumnName("LUOTXEM");
            entity.Property(e => e.Matkhau)
                .HasMaxLength(200)
                .IsUnicode(false)
                .HasColumnName("MATKHAU");
            entity.Property(e => e.Mota).HasColumnName("MOTA");
            entity.Property(e => e.Muctieunghenghiep).HasColumnName("MUCTIEUNGHENGHIEP");
            entity.Property(e => e.Nganhmongmuon).HasColumnName("NGANHMONGMUON");
            entity.Property(e => e.Ngaysinh)
                .HasColumnType("date")
                .HasColumnName("NGAYSINH");
            entity.Property(e => e.Ngaytao)
                .HasColumnType("date")
                .HasColumnName("NGAYTAO");
            entity.Property(e => e.Sdt)
                .HasMaxLength(50)
                .IsUnicode(false)
                .HasColumnName("SDT");
            entity.Property(e => e.Sociallogin).HasColumnName("SOCIALLOGIN");
            entity.Property(e => e.Ten)
                .HasMaxLength(50)
                .HasColumnName("TEN");
            entity.Property(e => e.Thoigianlamviecmongmuon)
                .HasMaxLength(500)
                .HasColumnName("THOIGIANLAMVIECMONGMUON");
            entity.Property(e => e.Trangthai)
                .HasMaxLength(50)
                .HasColumnName("TRANGTHAI");
            entity.Property(e => e.UrlCv).HasColumnName("URL_CV");
            entity.Property(e => e.Vitriungtuyen).HasColumnName("VITRIUNGTUYEN");

            entity.HasOne(d => d.IdThanhphoNavigation).WithMany(p => p.Ungviens)
                .HasForeignKey(d => d.IdThanhpho)
                .HasConstraintName("FK_UNGVIEN_THANHPHO");

            entity.HasMany(d => d.IdLoaicongviecs).WithMany(p => p.IdUngviens)
                .UsingEntity<Dictionary<string, object>>(
                    "Loaicongviecungvien",
                    r => r.HasOne<Loaicongviec>().WithMany()
                        .HasForeignKey("IdLoaicongviec")
                        .OnDelete(DeleteBehavior.ClientSetNull)
                        .HasConstraintName("FK_LOAICONGVIECUNGVIEN_LOAICONGVIEC"),
                    l => l.HasOne<Ungvien>().WithMany()
                        .HasForeignKey("IdUngvien")
                        .OnDelete(DeleteBehavior.ClientSetNull)
                        .HasConstraintName("FK_LOAICONGVIECUNGVIEN_UNGVIEN"),
                    j =>
                    {
                        j.HasKey("IdUngvien", "IdLoaicongviec");
                        j.ToTable("LOAICONGVIECUNGVIEN");
                        j.IndexerProperty<int>("IdUngvien").HasColumnName("ID_UNGVIEN");
                        j.IndexerProperty<int>("IdLoaicongviec").HasColumnName("ID_LOAICONGVIEC");
                    });
        });

        modelBuilder.Entity<VwRand>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("vwRand");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
