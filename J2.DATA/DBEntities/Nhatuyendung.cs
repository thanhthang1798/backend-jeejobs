﻿using System;
using System.Collections.Generic;

namespace J2.DATA.DBEntities;

public partial class Nhatuyendung
{
    public int IdNhatuyendung { get; set; }

    public string? Email { get; set; }

    public string? Matkhau { get; set; }

    public string? Logo { get; set; }

    public bool? Showlogo { get; set; }

    public string? Tencongty { get; set; }

    public string? Tenviettat { get; set; }

    public int? Nganhnghehoatdong { get; set; }

    public int? Quymocongty { get; set; }

    public string? Diachi { get; set; }

    public string? Sdt { get; set; }

    public string? Website { get; set; }

    public string? Bannercongty { get; set; }

    public string? Videogioithieu { get; set; }

    public string? Hinhanh { get; set; }

    public string? Gioithieucongty { get; set; }

    public DateTime? Ngaydangky { get; set; }

    public bool? Isdelete { get; set; }

    public string? Contentdelete { get; set; }

    public string? Trangthai { get; set; }

    public int? Douutien { get; set; }

    public int? Luotxem { get; set; }

    public bool? Sociallogin { get; set; }

    public virtual ICollection<Dangtintuyendung> Dangtintuyendungs { get; set; } = new List<Dangtintuyendung>();

    public virtual ICollection<Diadiemlamviec> Diadiemlamviecs { get; set; } = new List<Diadiemlamviec>();

    public virtual ICollection<Luuungvien> Luuungviens { get; set; } = new List<Luuungvien>();

    public virtual Nganh? NganhnghehoatdongNavigation { get; set; }
}
