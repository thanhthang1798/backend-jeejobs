﻿using System;
using System.Collections.Generic;

namespace J2.DATA.DBEntities;

public partial class Phucloituyendung
{
    public int IdPhucloi { get; set; }

    public int IdTintuyendung { get; set; }

    public string? Motaphucloi { get; set; }

    public virtual Phucloi IdPhucloiNavigation { get; set; } = null!;

    public virtual Dangtintuyendung IdTintuyendungNavigation { get; set; } = null!;
}
