﻿using System;
using System.Collections.Generic;

namespace J2.DATA.DBEntities;

public partial class Ungvien
{
    public int IdUngvien { get; set; }

    public string? Email { get; set; }

    public string? Matkhau { get; set; }

    public string? Hinhanh { get; set; }

    public string? Ho { get; set; }

    public string? Ten { get; set; }

    public string? Diachi { get; set; }

    public int? Gioitinh { get; set; }

    public bool? Docthan { get; set; }

    public DateTime? Ngaysinh { get; set; }

    public string? Sdt { get; set; }

    public DateTime? Ngaytao { get; set; }

    public int? IdThanhpho { get; set; }

    public string? UrlCv { get; set; }

    public decimal? Luong { get; set; }

    public int? Kinhnghiem { get; set; }

    public string? Mota { get; set; }

    public bool? Isdelete { get; set; }

    public string? Contentdelete { get; set; }

    public string? Trangthai { get; set; }

    public int? Luotxem { get; set; }

    public int? Nganhmongmuon { get; set; }

    public string? Capbacmongmuon { get; set; }

    public string? Thoigianlamviecmongmuon { get; set; }

    public string? Muctieunghenghiep { get; set; }

    public string? Vitriungtuyen { get; set; }

    public bool? Sociallogin { get; set; }

    public virtual ICollection<Bangcap> Bangcaps { get; set; } = new List<Bangcap>();

    public virtual ICollection<Congviecmongmuon> Congviecmongmuons { get; set; } = new List<Congviecmongmuon>();

    public virtual Thanhpho? IdThanhphoNavigation { get; set; }

    public virtual ICollection<Kinhnghiemlamviec> Kinhnghiemlamviecs { get; set; } = new List<Kinhnghiemlamviec>();

    public virtual ICollection<Kynangungvien> Kynangungviens { get; set; } = new List<Kynangungvien>();

    public virtual ICollection<Luucongviec> Luucongviecs { get; set; } = new List<Luucongviec>();

    public virtual ICollection<Luuungvien> Luuungviens { get; set; } = new List<Luuungvien>();

    public virtual ICollection<Nganhungvien> Nganhungviens { get; set; } = new List<Nganhungvien>();

    public virtual ICollection<Ngoainguungvien> Ngoainguungviens { get; set; } = new List<Ngoainguungvien>();

    public virtual ICollection<Thanhphoungvien> Thanhphoungviens { get; set; } = new List<Thanhphoungvien>();

    public virtual ICollection<Ungtuyen> Ungtuyens { get; set; } = new List<Ungtuyen>();

    public virtual ICollection<Loaicongviec> IdLoaicongviecs { get; set; } = new List<Loaicongviec>();
}
