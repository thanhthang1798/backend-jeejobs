﻿using System;
using System.Collections.Generic;

namespace J2.DATA.DBEntities;

public partial class Quantrivien
{
    public int IdQtv { get; set; }

    public string? Matkhau { get; set; }

    public string? Email { get; set; }

    public string? Sdt { get; set; }
}
