﻿using System;
using System.Collections.Generic;

namespace J2.DATA.DBEntities;

public partial class Loaicongviec
{
    public int IdLoaicongviec { get; set; }

    public string? Tenloaicongviec { get; set; }

    public virtual ICollection<Congviecmongmuon> Congviecmongmuons { get; set; } = new List<Congviecmongmuon>();

    public virtual ICollection<Dangtintuyendung> Dangtintuyendungs { get; set; } = new List<Dangtintuyendung>();

    public virtual ICollection<Ungvien> IdUngviens { get; set; } = new List<Ungvien>();
}
