﻿using System;
using System.Collections.Generic;

namespace J2.DATA.DBEntities;

public partial class Trinhdo
{
    public int IdTrinhdo { get; set; }

    public string? Tentrinhdo { get; set; }

    public int? Giatri { get; set; }

    public virtual ICollection<Bangcap> Bangcaps { get; set; } = new List<Bangcap>();

    public virtual ICollection<Dangtintuyendung> IdTuyendungs { get; set; } = new List<Dangtintuyendung>();
}
