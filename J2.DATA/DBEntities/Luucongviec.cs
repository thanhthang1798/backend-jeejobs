﻿using System;
using System.Collections.Generic;

namespace J2.DATA.DBEntities;

public partial class Luucongviec
{
    public int IdUngvien { get; set; }

    public int IdTintuyendung { get; set; }

    public DateTime? Ngayluu { get; set; }

    public virtual Dangtintuyendung IdTintuyendungNavigation { get; set; } = null!;

    public virtual Ungvien IdUngvienNavigation { get; set; } = null!;
}
