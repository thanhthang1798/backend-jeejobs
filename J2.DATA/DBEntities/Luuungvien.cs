﻿using System;
using System.Collections.Generic;

namespace J2.DATA.DBEntities;

public partial class Luuungvien
{
    public int IdNhatuyendung { get; set; }

    public int IdUngvien { get; set; }

    public DateTime? Ngayluu { get; set; }

    public virtual Nhatuyendung IdNhatuyendungNavigation { get; set; } = null!;

    public virtual Ungvien IdUngvienNavigation { get; set; } = null!;
}
