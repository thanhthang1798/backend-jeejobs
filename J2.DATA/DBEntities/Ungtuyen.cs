﻿using System;
using System.Collections.Generic;

namespace J2.DATA.DBEntities;

public partial class Ungtuyen
{
    public int IdUngvien { get; set; }

    public int IdCongviec { get; set; }

    public DateTime? Ngayungtuyen { get; set; }

    public bool? Daxem { get; set; }

    public virtual Dangtintuyendung IdCongviecNavigation { get; set; } = null!;

    public virtual Ungvien IdUngvienNavigation { get; set; } = null!;
}
