﻿using System;
using System.Collections.Generic;

namespace J2.DATA.DBEntities;

public partial class Phucloi
{
    public int IdPhucloi { get; set; }

    public string? Tenphucloi { get; set; }

    public virtual ICollection<Phucloicongviecmongmuon> Phucloicongviecmongmuons { get; set; } = new List<Phucloicongviecmongmuon>();

    public virtual ICollection<Phucloituyendung> Phucloituyendungs { get; set; } = new List<Phucloituyendung>();
}
