﻿using System;
using System.Collections.Generic;

namespace J2.DATA.DBEntities;

public partial class Nganhungvien
{
    public int IdNganh { get; set; }

    public int IdUngvien { get; set; }

    public virtual Ungvien IdUngvienNavigation { get; set; } = null!;
}
