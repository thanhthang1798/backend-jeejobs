﻿using System;
using System.Collections.Generic;

namespace J2.DATA.DBEntities;

public partial class Thanhphoungvien
{
    public int IdUngvien { get; set; }

    public int IdThanhpho { get; set; }

    public virtual Ungvien IdUngvienNavigation { get; set; } = null!;
}
