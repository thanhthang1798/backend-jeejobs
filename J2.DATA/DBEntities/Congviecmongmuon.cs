﻿using System;
using System.Collections.Generic;

namespace J2.DATA.DBEntities;

public partial class Congviecmongmuon
{
    public int IdCongviecmongmuon { get; set; }

    public int? IdUngvien { get; set; }

    public string? Capbac { get; set; }

    /// <summary>
    /// Nơi làm việc khác ngoài những nơi đã chọn
    /// </summary>
    public bool? Noikhac { get; set; }

    public int? Thongbao { get; set; }

    public int? IdThanhpho { get; set; }

    public int? IdLoaicongviec { get; set; }

    public int? IdNganh { get; set; }

    public decimal? Luong { get; set; }

    public bool? Thuongluong { get; set; }

    public DateTime? Ngaytao { get; set; }

    public string? Tenvieclam { get; set; }

    public virtual Loaicongviec? IdLoaicongviecNavigation { get; set; }

    public virtual Ungvien? IdUngvienNavigation { get; set; }

    public virtual ICollection<Phucloicongviecmongmuon> Phucloicongviecmongmuons { get; set; } = new List<Phucloicongviecmongmuon>();

    public virtual ICollection<Nganh> IdNganhs { get; set; } = new List<Nganh>();

    public virtual ICollection<Thanhpho> IdThanhphos { get; set; } = new List<Thanhpho>();
}
