﻿using System;
using System.Collections.Generic;

namespace J2.DATA.DBEntities;

public partial class Nganh
{
    public int IdNganh { get; set; }

    public string? Tennganh { get; set; }

    public virtual ICollection<Nhatuyendung> Nhatuyendungs { get; set; } = new List<Nhatuyendung>();

    public virtual ICollection<Congviecmongmuon> IdCongviecmongmuons { get; set; } = new List<Congviecmongmuon>();

    public virtual ICollection<Dangtintuyendung> IdTintuyendungs { get; set; } = new List<Dangtintuyendung>();
}
