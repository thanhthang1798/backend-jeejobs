﻿using System;
using System.Collections.Generic;

namespace J2.DATA.DBEntities;

public partial class Phucloicongviecmongmuon
{
    public int IdPhucloi { get; set; }

    public int IdCongviecmongmuon { get; set; }

    public string? Phucloimongmuon { get; set; }

    public virtual Congviecmongmuon IdCongviecmongmuonNavigation { get; set; } = null!;

    public virtual Phucloi IdPhucloiNavigation { get; set; } = null!;
}
