﻿using System;
using System.Collections.Generic;

namespace J2.DATA.DBEntities;

public partial class Ngoainguungvien
{
    public int IdNgoaingu { get; set; }

    public int IdUngvien { get; set; }

    public bool? Nghe { get; set; }

    public bool? Noi { get; set; }

    public bool? Doc { get; set; }

    public bool? Viet { get; set; }

    public bool? Coban { get; set; }

    public string? Ghichu { get; set; }

    public virtual Ngoaingu IdNgoainguNavigation { get; set; } = null!;

    public virtual Ungvien IdUngvienNavigation { get; set; } = null!;
}
