﻿using System;
using System.Collections.Generic;

namespace J2.DATA.DBEntities;

public partial class Ngoaingu
{
    public int IdNgoaingu { get; set; }

    public string? Tenngoaingu { get; set; }

    public virtual ICollection<Ngoainguungvien> Ngoainguungviens { get; set; } = new List<Ngoainguungvien>();
}
