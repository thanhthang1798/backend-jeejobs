﻿using System;
using System.Collections.Generic;

namespace J2.DATA.DBEntities;

public partial class Thanhpho
{
    public int IdThanhpho { get; set; }

    public string? Tenthanhpho { get; set; }

    public virtual ICollection<Diadiemlamviec> Diadiemlamviecs { get; set; } = new List<Diadiemlamviec>();

    public virtual ICollection<Ungvien> Ungviens { get; set; } = new List<Ungvien>();

    public virtual ICollection<Congviecmongmuon> IdCongviecmongmuons { get; set; } = new List<Congviecmongmuon>();
}
