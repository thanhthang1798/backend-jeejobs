﻿using J2.DATA.DBEntities;
using J2.DATA.IRepositories;
using J2.DATA.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J2.DATA.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DBEntities.DBEntities _context;
        public UnitOfWork(DBEntities.DBEntities context) {
            _context = context;
            CitiesRepositories = new CitiesRepositories(_context);
        }
        public ICitiesRepositories CitiesRepositories { get; private set; }

        public void Dispose()
        {
            _context.Dispose();
        }

        public int Save()
        {
            return _context.SaveChanges();
        }
    }
}
