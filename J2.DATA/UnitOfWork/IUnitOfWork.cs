﻿using J2.DATA.IRepositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J2.DATA.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {
        ICitiesRepositories CitiesRepositories { get; }

        int Save();
    }
}
